export default () => `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <style>
      
    #chart {
      margin: auto;
    }
      
    </style>

  
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"></script>

    
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    

    
  </head>

  <body>
     <div id="chart"></div>

    <script>

        const vm = window || document

        document.addEventListener('message', function(data) {
          const dataChart = JSON.parse(data.data)
          var options = {
            series: [dataChart.percent],
            chart: {
              height: dataChart.size,
              width: dataChart.size,
              type: 'radialBar',
              background:'transparent'
            },
            plotOptions: {
              radialBar: {
                hollow: {
                  size: '60%',
                },
                dataLabels: {
                  name: {
                     show: false
                  },
                  value: {
                    show: true
                  }
                }
              },
              track: {
                show: true,
                startAngle: undefined,
                endAngle: undefined,
                background: '#f2f2f2',
                strokeWidth: 30,
                opacity: 1,
                margin: 5, 
                dropShadow: {
                  enabled: false,
                  top: 0,
                  left: 0,
                  blur: 3,
                  opacity: 0.5
                }
              },
            },
            labels: [''],
          };
  
          var chart = new ApexCharts(document.querySelector("#chart"), options);
          chart.render();
        });
      
      
    </script>

    
  </body>
</html>
`;
