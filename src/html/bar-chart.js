export default () => `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <style>
      
    #chart {
      max-width: 650px;
      margin: 35px auto;
    }
      
    </style>

  
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/eligrey-classlist-js-polyfill@1.2.20171210/classList.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/findindex_polyfill_mdn"></script>

    
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    

    
  </head>

  <body>
     <div id="chart"></div>

    <script>

        const vm = window || document

        document.addEventListener('message', function(data) {
          const dataChart = JSON.parse(data.data)
          var options = {
            series: [{
            data: dataChart.chartData
            }],
              chart: {
              type: 'bar',
              toolbar: {
                show: false
              },
              height: 240
            },
            plotOptions: {
              bar: {
                horizontal: false,
                dataLabels: {
                  position: 'top', // top, center, bottom
                },
              }
            },
            dataLabels: {
              enabled: true
            },
            xaxis: {
              categories: dataChart.chartLabel
            }
          };
          var chart = new ApexCharts(document.querySelector("#chart"), options);
          chart.render();
        });
      
      
    </script>

    
  </body>
</html>
`;
