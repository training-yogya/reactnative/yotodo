import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Home from './../../screens/Replenish/screens/Home';
import PickingListReplenish from './../../screens/Replenish/screens/PickingList';
import ArticleCategory from '../../screens/Article/category.screen';
import Article from '../../screens/Article';
import ProductIdentity from './../../screens/Replenish/screens/ProductIdentity';
import TodaySales from './../../screens/Replenish/screens/TodaySales';

import {MyTransition} from './../../utils/navigation';

const ReplenishStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
import BarcodeScan from '../../screens/BarcodeScan';
const ReplenishStackScreen = ({navigation}) => {
  return (
    <ReplenishStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <ReplenishStack.Screen
        name={screenNames.Replenish.Home}
        component={Home}
      />
      <ReplenishStack.Screen
        name={screenNames.BarcodeScan.Scan}
        component={BarcodeScan}
      />
      <ReplenishStack.Screen
        name={screenNames.Article.Article}
        component={Article}
      />
      <ReplenishStack.Screen
        name={screenNames.Article.Category}
        component={ArticleCategory}
      />
      <ReplenishStack.Screen
        name={screenNames.Replenish.TodaySales}
        component={TodaySales}
      />
      <ReplenishStack.Screen
        name={screenNames.Replenish.PickingList}
        component={PickingListReplenish}
      />
      <ReplenishStack.Screen
        name={screenNames.Product.Identity}
        component={ProductIdentity}
      />
    </ReplenishStack.Navigator>
  );
};

export default ReplenishStackScreen;
