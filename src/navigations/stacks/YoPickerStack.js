import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import YoPickerOmsList from '../../screens/YoPicker/screens/OmsList';
import YoPickerOmsListDetail from '../../screens/YoPicker/screens/OmsListDetail';
import ProductIdentity from '../../screens/ProductIdentity';
import BarcodeScan from '../../screens/BarcodeScan';

import {MyTransition} from './../../utils/navigation';

const YoPickerStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
import InputQty from '../../screens/YoPicker/screens/InputQty';

const YoPickerStackScreen = () => {
  return (
    <YoPickerStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <YoPickerStack.Screen
        name={screenNames.YoPicker.OmsList}
        component={YoPickerOmsList}
      />
      <YoPickerStack.Screen
        name={screenNames.YoPicker.OmsListDetail}
        component={YoPickerOmsListDetail}
      />
      <YoPickerStack.Screen
        name={screenNames.YoPicker.InputQty}
        component={InputQty}
      />
      <YoPickerStack.Screen
        name={screenNames.Product.Identity}
        component={ProductIdentity}
      />
      <YoPickerStack.Screen
        name={screenNames.BarcodeScan.Scan}
        component={BarcodeScan}
      />
    </YoPickerStack.Navigator>
  );
};

export default YoPickerStackScreen;
