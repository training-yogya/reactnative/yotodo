import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Note from './../../screens/Notes';
import NoteDetail from './../../screens/Notes/components/Detail';

import {MyTransition} from './../../utils/navigation';

const NoteStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
const NoteStackScreen = ({navigation}) => {
  return (
    <NoteStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <NoteStack.Screen name={screenNames.Home.Note} component={Note} />
      <NoteStack.Screen name={screenNames.Note.Detail} component={NoteDetail} />
    </NoteStack.Navigator>
  );
};

export default NoteStackScreen;
