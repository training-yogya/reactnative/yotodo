import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from './../../utils/screens';

import SplashScreen from './../../screens/SplashScreen';
import Login from './../../screens/Login';
import ChooseStore from './../../screens/ChooseStore';

import {MyTransition} from './../../utils/navigation';
import NavigationHeader from '../../components/NavigationHeader';

const UnAuthenticated = createStackNavigator();

const UnAuthenticatedScreen = () => {
  return (
    <UnAuthenticated.Navigator
      screenOptions={{
        ...MyTransition,
      }}>
      <UnAuthenticated.Screen
        name={screenNames.SplashScreen}
        component={SplashScreen}
      />
      <UnAuthenticated.Screen name={screenNames.Login} component={Login} />
      <UnAuthenticated.Screen
        options={{
          header: props => <NavigationHeader {...props} />,
        }}
        name={screenNames.ChooseStore}
        component={ChooseStore}
      />
    </UnAuthenticated.Navigator>
  );
};

export default UnAuthenticatedScreen;
