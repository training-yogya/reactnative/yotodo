import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Home from './../../screens/NewExpiredCheck/screens/Home';
import List from './../../screens/NewExpiredCheck/screens/List';
import ProductIdentity from '../../screens/NewExpiredCheck/screens/ProductIdentity'

import {MyTransition} from './../../utils/navigation';

const ExpiredStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
import BarcodeScan from '../../screens/BarcodeScan';
import DeleteExpired from '../../screens/NewExpiredCheck/screens/DeleteScreen';
const ExpiredStackScreen = ({navigation}) => {
  return (
    <ExpiredStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <ExpiredStack.Screen
        name={screenNames.ExpiredCheck.Home}
        component={Home}
      />
      <ExpiredStack.Screen
        name={screenNames.BarcodeScan.Scan}
        component={BarcodeScan}
      />
      <ExpiredStack.Screen
        name={screenNames.ExpiredCheck.List}
        component={List}
      />
      <ExpiredStack.Screen
        name={screenNames.Article.ProductIdentity}
        component={ProductIdentity}
      />
      <ExpiredStack.Screen
        name={screenNames.ExpiredCheck.Delete}
        component={DeleteExpired}
      />
    </ExpiredStack.Navigator>
  );
};

export default ExpiredStackScreen;
