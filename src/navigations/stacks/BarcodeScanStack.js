import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import BarcodeScan from '../../screens/BarcodeScan';
import ProductIdentity from '../../screens/ProductIdentity';

import {MyTransition} from './../../utils/navigation';

const BarcodeScanStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';

const BarcodeScanStackScreen = () => {
  return (
    <BarcodeScanStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <BarcodeScanStack.Screen
        name={screenNames.BarcodeScan.Scan}
        component={BarcodeScan}
      />
      <BarcodeScanStack.Screen
        name={screenNames.Article.ProductIdentity}
        component={ProductIdentity}
      />
    </BarcodeScanStack.Navigator>
  );
};

export default BarcodeScanStackScreen;
