import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../../screens/Home';
import ArticleStack from './ArticleStack';
import BarcodeScanStack from './BarcodeScanStack';
import PlanogramStack from './PlanogramStack';
import ProfileStack from './ProfileStack';
import InsightInfoStack from './InsightInfoStack';
import ExpiredStack from './ExpiredStack';
import WebViewScreen from './../../screens/Webview';
import NoteStackScreen from './NotesStack';
import ChooseStore from './../../screens/ChooseStore';
import {screenNames} from '../../utils/screens';
import {MyTransition} from '../../utils/navigation';
import TutorialStackScreen from './TutorialStack';
import PromoNotifStackScreen from './PromoNotifStack';
import NavigationHeader from '../../components/NavigationHeader';
import YoPickerStackScreen from './YoPickerStack';
import SOPartialStackScreen from './SOPartialStack';
import ReplenishStackScreen from './ReplenishStack';
import HomeBakeryScreen from './HomeBakeryStack';
const Home = createStackNavigator();

const HomeStack = () => {
  return (
    <Home.Navigator
      screenOptions={{
        headerShown: false,
        ...MyTransition,
      }}>
      <Home.Screen name={screenNames.Home.Home} component={HomeScreen} />
      <Home.Screen
        name={screenNames.Article.Directorate}
        component={ArticleStack}
      />
      <Home.Screen
        name={screenNames.HomeBakery.Upload}
        component={HomeBakeryScreen}
      />
      <Home.Screen
        name={screenNames.Planogram.Home}
        component={PlanogramStack}
      />
      <Home.Screen
        name={screenNames.BarcodeScan.Scan}
        component={BarcodeScanStack}
      />
      <Home.Screen
        name={screenNames.Home.InsightInfo}
        component={InsightInfoStack}
      />

      <Home.Screen name={screenNames.Home.Note} component={NoteStackScreen} />

      <Home.Screen
        name={screenNames.Home.ExpiredCheck}
        component={ExpiredStack}
      />
      <Home.Screen
        name={screenNames.Replenish.Home}
        component={ReplenishStackScreen}
      />
      <Home.Screen
        name={screenNames.SOPartial.Home}
        component={SOPartialStackScreen}
      />
      <Home.Screen
        name={screenNames.Home.Tutorial}
        component={TutorialStackScreen}
      />
      <Home.Screen
        name={screenNames.Home.PromoNotif}
        component={PromoNotifStackScreen}
      />

      <Home.Screen
        name={screenNames.YoPicker.OmsList}
        component={YoPickerStackScreen}
      />
      <Home.Screen name={screenNames.WebView} component={WebViewScreen} />
      <Home.Screen
        options={{
          header: props => <NavigationHeader {...props} />,
          ...MyTransition,
          headerShown: true,
        }}
        name={screenNames.ChooseStoreHome}
        component={ChooseStore}
      />
      <Home.Screen name={screenNames.Home.Profile} component={ProfileStack} />
    </Home.Navigator>
  );
};

export default HomeStack;
