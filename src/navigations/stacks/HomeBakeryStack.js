import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Upload from '../../screens/HomeBakery/screens/tenImage';
import View from '../../screens/HomeBakery/screens/gallery';
import Camera from '../../screens/Camera';

import {MyTransition} from './../../utils/navigation';

const HomeBakery = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';

const HomeBakeryScreen = () => {
  return (
    <HomeBakery.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <HomeBakery.Screen
        name={screenNames.HomeBakery.Upload}
        component={Upload}
      />
      <HomeBakery.Screen
        name={screenNames.HomeBakery.Gallery}
        component={View}
      />
      <HomeBakery.Screen
        options={{
          title: 'Camera',
          headerLeft: 'none',
          headerRight: false,
        }}
        name={screenNames.HomeBakery.Camera}
        component={Camera}
      />
    </HomeBakery.Navigator>
  );
};

export default HomeBakeryScreen;
