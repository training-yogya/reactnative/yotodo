import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Planogram from './../../screens/Planogram';
import PlanogramList from './../../screens/PlanogramList';
import PlanogramDetails from './../../screens/PlanogramDetails';
import PlanogramInfo from '../../screens/PlanogramInfo';
import PlanogramRequest from '../../screens/PlanogramRequest';
import PlanogramRequestTracking from '../../screens/PlanogramRequestTracker';
import PlanogramRequestTrackingList from '../../screens/PlanogramRequestTracker/screens/RequestList';
import PlanogramTutorial from '../../screens/PlanogramTutorial';
import PlanogramCannotImplementation from '../../screens/PlanogramCannotImplementation';
import PlanogramImplementationStart from '../../screens/PlanogramImplementation/components/Start.screen';
import PlanogramImplementationOnGoing from '../../screens/PlanogramImplementation/components/OnGoing.screen';
import PlanogramImplementationSummary from '../../screens/PlanogramImplementation/components/Summary.screen';
import Camera from '../../screens/Camera';

import {MyTransition} from './../../utils/navigation';

const PlanogramStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
import {ButtonInfo} from '../../components';
const PlanogramStackScreen = ({navigation}) => {
  const action = () => {
    navigation.navigate(screenNames.Planogram.Info, {
      hideRequest: false,
    });
  };
  return (
    <PlanogramStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
        headerRight: <ButtonInfo action={action} />,
      }}>
      <PlanogramStack.Screen
        name={screenNames.Planogram.Home}
        component={Planogram}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.List}
        component={PlanogramList}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Details}
        component={PlanogramDetails}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Info}
        component={PlanogramInfo}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Tutorial}
        component={PlanogramTutorial}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Request}
        component={PlanogramRequest}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.RequestTrackingList}
        component={PlanogramRequestTrackingList}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.RequestTracking}
        component={PlanogramRequestTracking}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.CannotImplementation}
        component={PlanogramCannotImplementation}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Implementation.Start}
        component={PlanogramImplementationStart}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Implementation.OnGoing}
        component={PlanogramImplementationOnGoing}
      />
      <PlanogramStack.Screen
        name={screenNames.Planogram.Implementation.Summary}
        component={PlanogramImplementationSummary}
      />
      <PlanogramStack.Screen
        options={{
          title: 'Camera',
          headerLeft: 'none',
          headerRight: false,
        }}
        name={screenNames.Planogram.Implementation.Camera}
        component={Camera}
      />
    </PlanogramStack.Navigator>
  );
};

export default PlanogramStackScreen;
