import * as React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';
import PlanogramImplementationStart from '../../screens/PlanogramImplementation/components/Start.screen';
import PlanogramImplementationOnGoing from '../../screens/PlanogramImplementation/components/OnGoing.screen';
import PlanogramImplementationSummary from '../../screens/PlanogramImplementation/components/Summary.screen';
import Camera from '../../screens/Camera';

const PlanogramImplementation = createStackNavigator();

const PlanogramImplementationStack = () => {
  return (
    <PlanogramImplementation.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <PlanogramImplementation.Screen
        name={screenNames.Planogram.Implementation.Start}
        component={PlanogramImplementationStart}
      />
      <PlanogramImplementation.Screen
        name={screenNames.Planogram.Implementation.OnGoing}
        component={PlanogramImplementationOnGoing}
      />
      <PlanogramImplementation.Screen
        name={screenNames.Planogram.Implementation.Summary}
        component={PlanogramImplementationSummary}
      />
      <PlanogramImplementation.Screen
        name={screenNames.Planogram.Implementation.Camera}
        component={Camera}
      />
    </PlanogramImplementation.Navigator>
  );
};

export default PlanogramImplementationStack;
