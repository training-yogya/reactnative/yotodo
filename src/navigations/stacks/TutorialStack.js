import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Tutorial from './../../screens/Tutorial';
import TutorialDetail from './../../screens/Tutorial/screens/Detail';
import TutorialSub from './../../screens/Tutorial/screens/SubMenu';

import {MyTransition} from './../../utils/navigation';

const TutorialStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
const TutorialStackScreen = ({navigation}) => {
  return (
    <TutorialStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <TutorialStack.Screen
        name={screenNames.Home.Tutorial}
        component={Tutorial}
      />
      <TutorialStack.Screen
        name={screenNames.Tutorial.Detail}
        component={TutorialDetail}
      />
      <TutorialStack.Screen
        name={screenNames.Tutorial.SubMenu}
        component={TutorialSub}
      />
    </TutorialStack.Navigator>
  );
};

export default TutorialStackScreen;
