import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Home from './../../screens/SOPartial/screens/Home';
import ProductIdentity from '../../screens/ProductIdentity';
import BarcodeScan from '../../screens/BarcodeScan';

import {MyTransition} from './../../utils/navigation';

const SOPartialStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
import PSOList from '../../screens/SOPartial/screens/PSOList';
import PSOListDetail from '../../screens/SOPartial/screens/PSOListDetail';
const SOPartialStackScreen = ({navigation}) => {
  return (
    <SOPartialStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <SOPartialStack.Screen
        name={screenNames.SOPartial.Home}
        component={Home}
      />
      <SOPartialStack.Screen
        name={screenNames.SOPartial.PSOList}
        component={PSOList}
      />
      <SOPartialStack.Screen
        name={screenNames.SOPartial.PSOListDetail}
        component={PSOListDetail}
      />
      <SOPartialStack.Screen
        name={screenNames.Article.ProductIdentity}
        component={ProductIdentity}
      />
      <SOPartialStack.Screen
        name={screenNames.BarcodeScan.Scan}
        component={BarcodeScan}
      />
    </SOPartialStack.Navigator>
  );
};

export default SOPartialStackScreen;
