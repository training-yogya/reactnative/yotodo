import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import Profile from './../../screens/Profile';
import ChangePassword from '../../screens/Profile/components/ChangePassword';
import UpdateProfile from '../../screens/Profile/components/ChangeProfile/index';
import Activity from '../../screens/Profile/components/Activity';

import {MyTransition} from './../../utils/navigation';

const ProfileStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
const ProfileStackScreen = ({navigation}) => {
  return (
    <ProfileStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <ProfileStack.Screen
        name={screenNames.Home.Profile}
        component={Profile}
      />
      <ProfileStack.Screen
        name={screenNames.Profile.ChangePassword}
        component={ChangePassword}
      />
      <ProfileStack.Screen
        name={screenNames.Profile.Activity}
        component={Activity}
      />
      <ProfileStack.Screen
        name={screenNames.Profile.UpdateProfile}
        component={UpdateProfile}
      />
    </ProfileStack.Navigator>
  );
};

export default ProfileStackScreen;
