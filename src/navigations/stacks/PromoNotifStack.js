import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import PromoNotif from './../../screens/PromoNotif';
import PromoNotifDetail from './../../screens/PromoNotif/screens/Detail';
import PromoNotifReadline from './../../screens/PromoNotif/screens/ReadlineSubmit';
import Camera from '../../screens/Camera';

import {MyTransition} from './../../utils/navigation';

const PromoNotifStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
const PromoNotifStackScreen = ({navigation}) => {
  return (
    <PromoNotifStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <PromoNotifStack.Screen
        name={screenNames.Home.PromoNotif}
        component={PromoNotif}
      />
      <PromoNotifStack.Screen
        name={screenNames.PromoNotif.Detail}
        component={PromoNotifDetail}
      />
      <PromoNotifStack.Screen
        name={screenNames.PromoNotif.Readline}
        component={PromoNotifReadline}
      />
      <PromoNotifStack.Screen
        options={{
          title: 'Camera',
          headerLeft: 'none',
          headerRight: false,
        }}
        name={screenNames.PromoNotif.Camera}
        component={Camera}
      />
    </PromoNotifStack.Navigator>
  );
};

export default PromoNotifStackScreen;
