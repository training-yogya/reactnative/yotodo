import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import YoChatList from './../../screens/Yochat/screens/ChatList';
import YoChatRoom from './../../screens/Yochat/screens/ChatRoom';

import {MyTransition} from './../../utils/navigation';

const YoChatStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
const YoChatStackScreen = ({navigation}) => {
  return (
    <YoChatStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <YoChatStack.Screen
        name={screenNames.YoChat.List}
        component={YoChatList}
      />
      <YoChatStack.Screen
        name={screenNames.YoChat.Room}
        component={YoChatRoom}
      />
    </YoChatStack.Navigator>
  );
};

export default YoChatStackScreen;
