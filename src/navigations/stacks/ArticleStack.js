import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from './../../utils/screens';

import Directorate from '../../screens/Directorate';
import ArticleCategory from '../../screens/Article/category.screen';
import Article from '../../screens/Article';
import ProductIdentity from './../../screens/ProductIdentity';
import ReasonAdjustment from './../../screens/ReasonAdjustment';
import FormOtherReason from './../../screens/ReasonAdjustment/components/FormOthersReason';

import {MyTransition} from './../../utils/navigation';

const ArticleStack = createStackNavigator();

import NavigationHeader from './../../components/NavigationHeader';
import FashionFilter from '../../screens/Directorate/fashionFilter';

const ArticleStackScreen = () => {
  return (
    <ArticleStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <ArticleStack.Screen
        name={screenNames.Article.Directorate}
        component={Directorate}
      />
      <ArticleStack.Screen
        name={screenNames.Article.Category}
        component={ArticleCategory}
      />
      <ArticleStack.Screen
        name={screenNames.Article.Article}
        component={Article}
      />
      <ArticleStack.Screen
        name={screenNames.Article.ProductIdentity}
        component={ProductIdentity}
      />
      <ArticleStack.Screen
        name={screenNames.Article.ReasonAdjustment}
        component={ReasonAdjustment}
      />
      <ArticleStack.Screen
        name={screenNames.Article.FormOtherReason}
        component={FormOtherReason}
      />
      <ArticleStack.Screen
        name={screenNames.Article.FashionFilter}
        component={FashionFilter}
      />
    </ArticleStack.Navigator>
  );
};

export default ArticleStackScreen;
