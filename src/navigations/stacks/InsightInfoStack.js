import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../../utils/screens';

import InsightInfo from './../../screens/InsightInfo';
import InsightDirectorate from './../../screens/InsightInfo/screens/InsightDirectorate';
import InsightDevision from './../../screens/InsightInfo/screens/InsightDivision';
import InsightDetail from './../../screens/InsightInfo/screens/InsightDetail';
import InsightPlanogram from './../../screens/PlanogramInsight';
import {InsightDetailPlanogram} from './../../screens/PlanogramInsight/detail';

import {MyTransition} from './../../utils/navigation';

const InsightInfoStack = createStackNavigator();

import NavigationHeader from '../../components/NavigationHeader';
const InsightInfoStackScreen = ({navigation}) => {
  return (
    <InsightInfoStack.Navigator
      screenOptions={{
        header: props => <NavigationHeader {...props} />,
        ...MyTransition,
      }}>
      <InsightInfoStack.Screen
        name={screenNames.Home.InsightInfo}
        component={InsightInfo}
      />
      <InsightInfoStack.Screen
        name={screenNames.InsightInfo.InsightDirectorate}
        component={InsightDirectorate}
      />
      <InsightInfoStack.Screen
        name={screenNames.InsightInfo.InsightDivision}
        component={InsightDevision}
      />
      <InsightInfoStack.Screen
        name={screenNames.InsightInfo.InsightDetail}
        component={InsightDetail}
      />
      <InsightInfoStack.Screen
        name={screenNames.InsightInfo.InsightPlanogram}
        component={InsightPlanogram}
      />
      <InsightInfoStack.Screen
        name={screenNames.Planogram.InsightDetail}
        component={InsightDetailPlanogram}
      />
    </InsightInfoStack.Navigator>
  );
};

export default InsightInfoStackScreen;
