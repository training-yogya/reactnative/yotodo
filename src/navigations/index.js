import * as React from 'react';
import {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {screenNames} from '../utils/screens';

import UnAuthenticatedStack from './stacks/UnAuthenticatedStack';
import HomeTab from './tab/HomeTab';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {SAVE_TOKEN, SET_LOGIN_DEFAULT} from '../redux/actionTypes';

import {MyTransition} from './../utils/navigation';

import config from '../config';
import InitScreen from '../screens/InitScreen';
import client from '../utils/customAxios';
import {setInitialUnreadRooms} from '../redux/actions/chat-action';
import {SafeAreaView} from 'react-native';
import {useCodePush} from '../components/Providers/CodePushProvider';
import CodePush from 'react-native-code-push';
import {ModalDownloadProgress} from '../components/ModalDownloadProgress';
const Stack = createStackNavigator();

function AppNavigation() {
  const [isReady, setIsReady] = React.useState(false);
  const [isUpToDate, setIsUpToDate] = React.useState(false);
  const [progressLoading, setProgressLoading] = React.useState(0);
  const [loadingMessage, setLoadingMessage] = React.useState(
    'Menyiapkan Aplikasi',
  );
  const [initialState, setInitialState] = React.useState();
  const auth = useSelector(state => state.auth);
  const {progress, status} = useCodePush();
  const dispatch = useDispatch();
  const {
    userLogin: {token, id, firebase},
  } = auth;

  useEffect(() => {
    const bootstrapAsync = async () => {
      let tokenLogin = null;
      try {
        tokenLogin = await AsyncStorage.getItem('token');
      } catch (error) {
        // handle error token null
      }
      dispatch(setInitialUnreadRooms());
      dispatch({
        type: SAVE_TOKEN,
        payload: {data: {token: tokenLogin}},
      });
    };
    bootstrapAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (auth.success && token && firebase?.uid) {
      (async () => {
        const fcmToken = await AsyncStorage.getItem('fcm_token');

        const body = new FormData();

        body.append('user_id', id);
        body.append('firebase_user_id', firebase.uid);
        body.append('token', fcmToken);

        try {
          if (fcmToken === null) {
            throw new Error('token null');
          }
          await client.post('/firebase', body);
        } catch (e) {
          console.info('Pass', e);
        }
      })();
    }
    if (auth.success && token) {
      (async () => {
        dispatch({
          type: SET_LOGIN_DEFAULT,
        });
        await AsyncStorage.setItem('token', token);
      })();
    }
  });

  React.useEffect(() => {
    const restoreState = async () => {
      try {
        const savedStateString = await AsyncStorage.getItem(
          config.persistenceKey,
        );
        const state = JSON.parse(savedStateString);

        setInitialState(state);
        setProgressLoading(prev => prev + 0.15);
      } finally {
        setTimeout(() => {
          setIsReady(true);
        }, 500);
      }
    };

    if (!isReady) {
      restoreState();
    }
  }, [isReady]);

  React.useEffect(() => {
    const codePushCheckUpdate = () => {
      switch (status) {
        case CodePush.SyncStatus.UP_TO_DATE:
          if (loadingMessage !== 'Up to date') {
            setLoadingMessage('Up to date');
            setProgressLoading(prev => prev + 1);
            setTimeout(() => {
              setIsUpToDate(true);
            }, 100);
          }
          break;
        case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
          if (loadingMessage !== 'Memeriksa Update') {
            setLoadingMessage('Memeriksa Update');
            setProgressLoading(prev => prev + 0.15);
          }
          break;
        case CodePush.SyncStatus.UNKNOWN_ERROR:
          if (loadingMessage !== 'UNKNOWN ERROR') {
            setLoadingMessage('UNKNOWN ERROR');
            setProgressLoading(1);
            setIsUpToDate(true);
          }
          break;
        case CodePush.SyncStatus.SYNC_IN_PROGRESS:
          if (loadingMessage !== 'Dalam Progress') {
            setLoadingMessage('Dalam Progress');
            setProgressLoading(prev => prev + 0.15);
          }
          break;
        case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
          if (loadingMessage !== 'Download Dalam Progress') {
            setLoadingMessage('Download Dalam Progress');
            setProgressLoading(prev => prev + 0.15);
          }
          break;
        case CodePush.SyncStatus.INSTALLING_UPDATE:
          if (loadingMessage !== 'Menginstall Update') {
            setLoadingMessage('Menginstall Update');
            setProgressLoading(prev => prev + 0.15);
          }
          break;
        default:
          if (loadingMessage !== 'Loading') {
            setLoadingMessage('Loading');
          }
          break;
      }
    };

    if (!isUpToDate) {
      codePushCheckUpdate();
    }
  }, [isUpToDate, loadingMessage, status]);

  if (!isReady || !isUpToDate) {
    return (
      <>
        <InitScreen loadingText={loadingMessage} progress={progressLoading} />
        <ModalDownloadProgress
          progress={0.7}
          visible={loadingMessage === 'Download Dalam Progress'}
        />
      </>
    );
  }

  return (
    <SafeAreaView style={{height: '100%'}}>
      <NavigationContainer
        initialState={initialState}
        onStateChange={state =>
          AsyncStorage.setItem(config.persistenceKey, JSON.stringify(state))
        }>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            ...MyTransition,
          }}>
          {auth.isHomeReady ? (
            <Stack.Screen name={screenNames.Home.Home} component={HomeTab} />
          ) : (
            <Stack.Screen
              name={screenNames.SplashScreen}
              component={UnAuthenticatedStack}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

export default AppNavigation;
