import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeStack from './../stacks/HomeStack';
import YochatStack from './../stacks/YochatStack';
import {screenNames} from '../../utils/screens';
import BottomNavigation from '../../components/BottomNavigation';

const Tab = createBottomTabNavigator();

const HomeTab = () => {
  return (
    <Tab.Navigator
      lazy={true}
      tabBar={props => <BottomNavigation {...props} />}>
      <Tab.Screen name={screenNames.Home.Home} component={HomeStack} />
      <Tab.Screen name={screenNames.YoChat.List} component={YochatStack} />
    </Tab.Navigator>
  );
};

export default HomeTab;
