import React, {useEffect} from 'react';
import client, {monitore} from './../utils/customAxios';
import AsyncStorage from '@react-native-community/async-storage';
import {useDispatch} from 'react-redux';
import {SET_ALL_DEFAULT} from '../redux/actionTypes';
import {Alert} from 'react-native';

const checkRequests = Wrapped => {
  function CheckRequests(props) {
    const dispatch = useDispatch();
    useEffect(() => {
      client.interceptors.response.use(
        function(response) {
          // Do something with response data
          return response;
        },
        function(error) {
          if (error.response) {
            switch (error.response.status) {
              case 401:
                Alert.alert(
                  'Your login session is expired, please login again',
                  '',
                  [
                    {
                      text: 'OK',
                      onPress: () => {
                        AsyncStorage.removeItem('token');
                        dispatch({
                          type: SET_ALL_DEFAULT,
                          payload: {data: {token: null}},
                        });
                      },
                    },
                  ],
                );
                break;
              default:
                break;
            }
          }
          // Do something with response error
          return Promise.reject(error);
        },
      );
      monitore.interceptors.response.use(
        function(response) {
          // Do something with response data
          return response;
        },
        function(error) {
          if (error.response) {
            switch (error.response.status) {
              case 401:
                AsyncStorage.removeItem('token');
                dispatch({
                  type: SET_ALL_DEFAULT,
                  payload: {data: {token: null}},
                });
                break;
              default:
                break;
            }
          }
          // Do something with response error
          return Promise.reject(error);
        },
      );
    });

    return <Wrapped {...props} />;
  }
  return CheckRequests;
};

export default checkRequests;
