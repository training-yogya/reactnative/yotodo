import firebase from '@react-native-firebase/app';

import firebaseConfigJson from './../../../firebase.config.json';

export const initialize = () => {
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfigJson);
  }
};

export * from './auth';
export * from './chat';
export * from './messaging';
