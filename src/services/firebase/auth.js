import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';

export const user = () => {
  return firebase.auth().currentUser;
};

export const login = async data => {
  try {
    const signIn = await firebase
      .auth()
      .signInWithEmailAndPassword(`${data.email}@yogyagroup.com`, '123456');
    return Promise.resolve(signIn);
  } catch (error) {
    if (error.code === 'auth/user-not-found') {
      const signUp = await firebase
        .auth()
        .createUserWithEmailAndPassword(
          `${data.email}@yogyagroup.com`,
          '123456',
        );
      return Promise.resolve(signUp);
    }

    return Promise.reject(error);
  }
};
