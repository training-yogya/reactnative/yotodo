import firebase from '@react-native-firebase/app';
import '@react-native-firebase/database';

const ref = firebase.database().ref();
const channelRef = ref.child('channel-metadata');
const messageRef = ref.child('channel-messages');

const defaultOptions = {};
defaultOptions.numMaxMessages = defaultOptions.numMaxMessages || 50;

export const getChannelList = cb => {
  channelRef.once('value', snapshot => {
    const channelListObj = snapshot.val();
    const channelList = Object.keys(channelListObj).map(key => ({
      ...channelListObj[key],
    }));
    cb(channelList);
  });
};

export const messageChannelList = (channelId, options = defaultOptions, cb) => {
  channelRef.child(channelId).once('value', () => {
    messageRef
      .child(channelId)
      .limitToLast(options.numMaxMessages)
      .on('child_added', snapshot => {
        cb(onNewMessage(channelId, snapshot));
      });
    messageRef
      .child(channelId)
      .limitToLast(options.numMaxMessages)
      .on('child_removed', snapshot => {
        cb(onRemoveMessage(channelId, snapshot));
      });
  });
};

export const sendMessage = (
  {messageContent, messageType, user},
  channelId,
  cb,
) => {
  const message = {
    userId: user.uid,
    name: user.displayName,
    timestamp: firebase.database.ServerValue.TIMESTAMP,
    message: messageContent,
    type: messageType || 'default',
  };

  if (!user) {
    if (cb) {
      cb(new Error('authrequired'));
    }
    return;
  }

  const newMessageRef = messageRef.child(channelId).push();
  const timestamp = Date.now();
  newMessageRef.setWithPriority(message, timestamp, cb);

  channelRef.child(channelId).update({
    newMessage: true,
  });
};

export const deleteMessage = function(channelId, messageId, cb) {
  messageRef
    .child(channelId)
    .child(messageId)
    .remove(cb);
};

export const createChannel = (channelName, channelType, user, cb) => {
  const newChannelRef = channelRef.push();

  const newChannel = {
    id: newChannelRef.key,
    name: channelName,
    type: channelType,
    createdByUserId: user.uid,
    createdAt: firebase.database.ServerValue.TIMESTAMP,
  };

  if (channelType === 'private') {
    newChannel.authorizedUsers = {};
    newChannel.authorizedUsers[this._userId] = true;
  }
  newChannelRef.set(newChannel, error => {
    if (!error) {
      cb(newChannelRef.key);
    }
  });
};

export const enterChannel = (channelId, options = defaultOptions, cb) => {
  getChannel(channelId, channel => {
    const channelName = channel?.name;
    if (!channelId || !channelName) {
      return;
    }
    onEnterChannel({id: channelId, name: channelName});

    messageChannelList(channelId, options, cb);
  });
};

export const leaveChannel = channelId => {
  channelRef
    .child(channelId)
    .update({
      newMessage: false,
    })
    .finally(() => {
      channelRef.child(channelId).off();
    });
};

const getChannel = (channelId, cb) => {
  channelRef.child(channelId).once('value', snapshot => {
    cb(snapshot.val());
  });
};

const onEnterChannel = channel => {
  channelRef.child(channel.id).update({
    newMessage: false,
  });
  return channel;
};

const onNewMessage = (channelId, snapshot) => {
  const messages = snapshot.val();
  messages.id = snapshot.key;
  return {channelId, messages};
};

const onRemoveMessage = (channelId, snapshot) => {
  const messageId = snapshot.key;
  return {channelId, messageId};
};
