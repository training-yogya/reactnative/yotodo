import firebase from '@react-native-firebase/app';
import '@react-native-firebase/messaging';

export const getDevicesToken = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      const authStatus = await firebase.messaging().requestPermission();
      const enabled =
        authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL;

      if (enabled) {
        console.log('Authorization status:', authStatus);
        const token = await firebase.messaging().getToken();

        return resolve(token);
      }
    } catch (error) {
      return reject(error);
    }
  });
};
