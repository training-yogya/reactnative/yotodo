import {BASE_API_URL, VERSION} from 'react-native-dotenv';

const config = {
  baseUrl: BASE_API_URL,
  version: VERSION,
  baseImage: 'https://gold-api.yogyagroup.com/',
  monitore: 'http://monitore.yogyagroup.com',
  gold: 'https://gold-api.yogyagroup.com',
  persistenceKey: 'NAVIGATION_STATE',
  pusher: {
    app_id: '1010307',
    key: '539d8cc88361b3d874a7',
    secret: 'ac33ebeb1fa2b2eee103',
    cluster: 'ap1',
  },
};

export default config;
