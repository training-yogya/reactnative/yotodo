import React, {useEffect} from 'react';
import {Container} from '../../components';
import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import {View, Text, RefreshControl} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';
import {useDispatch, useSelector} from 'react-redux';
import {getPlanogramRequestDetail} from '../../redux/actions/planogram-action';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const iconList = ['drafts', 'edit', 'person', 'print', 'thumb-up'];

const PlanogramRequestTracker = ({navigation, route}) => {
  const dispatch = useDispatch();
  const planogram = useSelector(state => state.planogram);
  navigation.setOptions({
    title: 'Planogram Request Tracking',
  });

  const getDetail = () => {
    const requestId = route?.params?.requestId;
    dispatch(getPlanogramRequestDetail(requestId));
  };

  const refreshControl = (
    <RefreshControl onRefresh={getDetail} refreshing={planogram.loading} />
  );

  useEffect(() => {
    (() => {
      getDetail();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Container style={globalStyles.container}>
      {planogram.planogramRequestDetail?.length > 0 ? (
        <View style={{flex: 1}}>
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 10,
              justifyContent: 'center',
              alignItems: 'center',
              height: 150,
            }}>
            {planogram.planogramRequestDetail.map((item, i) => {
              const isLast = i + 1 === planogram.planogramRequestDetail.length;
              return (
                <View key={'icon' + i}>
                  <MaterialIcon
                    name={iconList[i]}
                    size={32}
                    color={colors.VerdunGreen}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <MaterialIcon
                      name={'check-circle'}
                      size={30}
                      color={colors.BlazeOrange}
                    />
                    {!isLast && (
                      <View
                        style={{
                          borderWidth: 1,
                          width: 30,
                          height: 1,
                          borderColor: colors.BlazeOrange,
                        }}
                      />
                    )}
                  </View>
                </View>
              );
            })}
          </View>
          <View style={{flex: 1}}>
            <Timeline
              circleSize={20}
              circleColor={colors.OrangPeel}
              lineColor={colors.OrangPeel}
              timeContainerStyle={{minWidth: 52, marginTop: 10}}
              timeStyle={{
                textAlign: 'center',
              }}
              descriptionStyle={{color: 'gray'}}
              options={{
                style: {paddingTop: 5},
                removeClippedSubviews: false,
                refreshControl: refreshControl,
              }}
              data={planogram.planogramRequestDetail}
              showTime={true}
              innerCircle={'dot'}
            />
          </View>
        </View>
      ) : (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>---------- There is nothing to display ----------</Text>
        </View>
      )}
    </Container>
  );
};

export default PlanogramRequestTracker;
