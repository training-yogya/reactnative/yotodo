import * as React from 'react';
import {useState} from 'react';
import {Container, AppButton} from '../../../components';
import {
  FlatList,
  TouchableOpacity,
  Text,
  View,
  BackHandler,
  Alert,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {screenNames} from '../../../utils/screens';
import colors from '../../../utils/colors';
import {debounce} from 'lodash';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {
  getPlanogramRequestList,
  getRequestListSearch,
} from '../../../redux/actions/planogram-action';
import moment from 'moment';

const ChooseRequest = props => {
  const dispatch = useDispatch();
  const {navigation} = props;
  const planogram = useSelector(state => state.planogram);
  const [loading, setLoading] = useState(false);
  const [searchText, setSearchText] = useState(false);
  const [requestSearch, setRequestSearch] = useState(false);

  const handleChooseRequest = item => {
    navigation.navigate(screenNames.Planogram.RequestTracking, {
      requestId: item.id,
      title: 'Detail Request Tracking',
    });
  };

  const handleSearch = React.useCallback(
    debounce(text => {
      dispatch(getRequestListSearch(text)).then(requestList => {
        setRequestSearch(requestList);
        setLoading(false);
      });
    }, 50),
    [],
  );

  const handleChangeSearch = text => {
    setSearchText(text);

    if (!loading) {
      setLoading(true);
    }

    handleSearch(text);
  };

  const keyExtractor = (item, i) => i.toString();
  const renderItem = ({item, index}) => {
    // const active = selectedStore.store_code === item.store_code;
    const isLast = planogram.planogramRequestList.length - 1 === index;
    return (
      <TouchableOpacity
        onPress={() => handleChooseRequest(item)}
        style={[
          {
            padding: 10,
            paddingHorizontal: 20,
            borderBottomWidth: 0.7,
            borderColor: colors.Black + '55',
            flexDirection: 'row',
          },
          isLast && {
            borderBottomWidth: 0,
          },
        ]}>
        <View style={{marginLeft: 10, flex: 1}}>
          <Text
            style={{
              color: colors.Black,
              textAlign: 'left',
              fontSize: 14,
              fontWeight: 'bold',
            }}>
            {item.categories}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{color: colors.Black}}>
              {moment(item.created_at).format('DD MMMM YYYY')}
            </Text>
            {/* <Text
              style={{
                backgroundColor: colors.Mantis,
                borderRadius: 10,
                paddingVertical: 1,
                paddingHorizontal: 10,
                color: colors.White,
                fontSize: 11,
                marginLeft: 10
              }}>
              {item.type}
            </Text> */}
          </View>
        </View>
        <View style={{marginLeft: 10}}>
          <Text style={{fontSize: 11}}>Request ID: {item.id}</Text>
          {/* <Text
            style={{
              backgroundColor: colors.EarlsGreen,
              borderRadius: 10,
              paddingVertical: 1,
              paddingHorizontal: 10,
              color: colors.White,
            }}>
            {item.status_request}
          </Text> */}
        </View>
      </TouchableOpacity>
    );
  };

  React.useEffect(() => {
    (() => {
      dispatch(getPlanogramRequestList());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: 'Request Tracking',
  });
  return (
    <Container>
      <ListHeaderComponent onChangeText={handleChangeSearch} />
      <FlatList
        ListFooterComponent={loading && <ActivityIndicator />}
        data={searchText ? requestSearch : planogram.planogramRequestList}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListEmptyComponent={
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>---------- There is nothing to display ----------</Text>
          </View>
        }
      />
    </Container>
  );
};

const ListHeaderComponent = ({onChangeText}) => {
  return (
    <View style={{paddingHorizontal: 10, paddingVertical: 5}}>
      <TextInput
        onChangeText={onChangeText}
        style={{
          borderRadius: 10,
          borderWidth: 0.7,
          height: 56,
          borderColor: colors.Black + '66',
          paddingHorizontal: 20,
        }}
        placeholder={'Search'}
      />
    </View>
  );
};

export default ChooseRequest;
