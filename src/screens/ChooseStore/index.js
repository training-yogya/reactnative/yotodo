import * as React from 'react';
import {useState} from 'react';
import {Container, AppButton} from '../../components';
import {
  FlatList,
  TouchableOpacity,
  Text,
  View,
  BackHandler,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {screenNames} from '../../utils/screens';
import BottomNavigation from '../../components/BottomNavigation';
import colors from '../../utils/colors';
import {debounce} from 'lodash';

import {
  setHomeReady,
  getStoreByUserId,
  getSearch,
} from './../../redux/actions/auth-action';

const ChooseStore = props => {
  const dispatch = useDispatch();
  const {navigation, route} = props;
  const auth = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);
  const [searchText, setSearchText] = useState(false);
  const [storeSearch, setStoreSearch] = useState(false);
  const [selectedStore, setSelectedStore] = useState({});

  const handleStorePress = storeItem => setSelectedStore(storeItem);
  const handleChooseStore = () => {
    const data = {
      isHomeReady: true,
      initial_store: selectedStore.initial_store,
      store_code: selectedStore.store_code,
    };
    dispatch(setHomeReady(data));

    if (route.name === screenNames.ChooseStoreHome) {
      navigation.goBack();
    }
  };

  const handleSearch = React.useCallback(
    debounce(text => {
      dispatch(getSearch(text)).then(storeList => {
        setStoreSearch(storeList);
        setLoading(false);
      });
    }, 50),
    [],
  );

  const handleChangeSearch = text => {
    setSearchText(text);

    if (!loading) {
      setLoading(true);
    }

    handleSearch(text);
  };

  const keyExtractor = (item, i) => i.toString();
  const renderItem = ({item, index}) => {
    const active = selectedStore.store_code === item.store_code;
    const isLast = auth.storelist.length - 1 === index;
    return (
      <TouchableOpacity
        onPress={() => handleStorePress(item)}
        style={[
          {
            padding: 10,
            paddingHorizontal: 20,
            borderBottomWidth: 0.7,
            borderColor: colors.Black + '55',
            flexDirection: 'row',
          },
          active && {
            backgroundColor: colors.BlazeOrange + '88',
          },
          isLast && {
            borderBottomWidth: 0,
          },
        ]}>
        <View
          style={{
            backgroundColor: colors.Silver,
            width: 50,
            borderRadius: 25,
            height: 50,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
              textAlignVertical: 'center',
              fontSize: 16,
            }}>
            {item.initial_store}
          </Text>
        </View>
        <View style={{marginLeft: 10, justifyContent: 'center'}}>
          <Text style={{color: active ? colors.White : colors.Black}}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (route.name === screenNames.ChooseStore) {
          BackHandler.exitApp();
        }
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  React.useEffect(() => {
    if (!auth.logined) {
      if (route.name === screenNames.ChooseStore) {
        return navigation.navigate(screenNames.Login);
      }
    }
  });

  React.useEffect(() => {
    (() => {
      dispatch(getStoreByUserId());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: 'Choose Store',
    headerLeft: 'none',
    headerRight: false,
  });
  return (
    <Container>
      <ListHeaderComponent onChangeText={handleChangeSearch} />
      <FlatList
        ListFooterComponent={loading && <ActivityIndicator />}
        data={searchText ? storeSearch : auth.storelist}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
      <View style={{alignItems: 'center', paddingVertical: 10}}>
        <AppButton
          disabled={!selectedStore.initial_store}
          onPress={handleChooseStore}
          title={'Choose Store'}
          color={colors.BlazeOrange}
          style={{width: 200}}
        />
      </View>
      {route.name === screenNames.ChooseStore && (
        <View style={{marginBottom: -15}}>
          <BottomNavigation {...props} />
        </View>
      )}
    </Container>
  );
};

const ListHeaderComponent = ({onChangeText}) => {
  return (
    <View style={{paddingHorizontal: 10, paddingVertical: 5}}>
      <TextInput
        onChangeText={onChangeText}
        style={{
          borderRadius: 10,
          borderWidth: 0.7,
          height: 56,
          borderColor: colors.Black + '66',
          paddingHorizontal: 20,
        }}
        placeholder={'Search'}
      />
    </View>
  );
};

export default ChooseStore;
