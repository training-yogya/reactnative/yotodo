import React from 'react';
import {useEffect} from 'react';
import {Container, ListItem} from '../../../components';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text} from 'react-native';
import {PSOArticleListHeader} from '../../../redux/actions/product-action';

const PSOList = ({route, navigation}) => {
  const {title, haveAdjusment} = route?.params;
  navigation.setOptions({
    title,
  });

  const dispatch = useDispatch();
  const product = useSelector(state => state.product);

  const handleOnPress = item => () => {
    navigation.navigate(screenNames.SOPartial.PSOListDetail, {
      title: title + ' - ' + item.type,
      id: item.id,
      status: item.status,
    });
  };

  useEffect(() => {
    getproductAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getproductAsync = () => {
    dispatch(PSOArticleListHeader());
  };

  const keyExtractor = (item, i) => i.toString();

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={handleOnPress(item)}
        style={{
          backgroundColor: colors.White,
          padding: 20,
          borderBottomWidth: 0.6,
          borderBottomColor: colors.Silver,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{textTransform: 'uppercase', fontWeight: '700'}}>
          {item.divdesc} - {item.type}
        </Text>
        <Text
          style={{
            color: item.status === 1 ? colors.SeaGreen : colors.YellowOrange,
            fontWeight: 'bold',
          }}>
          {item.status_name}
        </Text>
      </TouchableOpacity>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getproductAsync} refreshing={product.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <View style={{flex: 1}}>
        <FlatList
          style={styles.padding10}
          data={product?.PSOHeader}
          ListEmptyComponent={<EmptyComponent loading={product.loading} />}
          refreshControl={refreshControl}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
        />
      </View>
    </Container>
  );
};

export default PSOList;

const EmptyComponent = ({loading, name}) => (
  <View
    style={{
      height: 200,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View>
        <Text>List {name} Kosong</Text>
      </View>
    )}
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.White, marginBottom: 0},
  padding10: {
    padding: 10,
  },
});
