import React, {useState} from 'react';
import {useEffect} from 'react';
import {Container, ListItem, AppButton, ModalInfo} from '../../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text, Alert} from 'react-native';
import {
  PSOArticelList,
  sendPSOEDP,
  PSOArticleListHeader,
  deletePSOItemDetail,
} from '../../../redux/actions/product-action';

const PSOList = ({route, navigation}) => {
  const {title, haveAdjusment, id, status} = route?.params;
  navigation.setOptions({
    title,
  });

  const dispatch = useDispatch();
  const product = useSelector(state => state.product);

  const [infoMessage, setInfoMessage] = useState('');
  const [infoTitle, setInfoTitle] = useState('');
  const [infoIcon, setInfoIcon] = useState('');
  const [modalInfoVisible, setModalInfoVisible] = useState(false);

  const handleOnPress = sku => () => {
    navigation.navigate(screenNames.Product.Identity, {
      productId: sku,
      hideReason: !haveAdjusment,
      parentTitle: 'Directorate',
    });
  };

  const sendEDP = () => {
    const data = product.PSOList.map(item => {
      return {
        id: item.id,
      };
    });
    dispatch(sendPSOEDP(data, id)).then(result => {
      // setTimeout(() => {
      setInfoIcon('success');
      setInfoTitle('Success');
      setInfoMessage(result.status.message);
      setModalInfoVisible(true);
      // });
      dispatch(PSOArticleListHeader());

      navigation.navigate(screenNames.SOPartial.PSOList);
    });
  };

  const handleSubmit = () => {
    Alert.alert(
      'Apakah Anda sudah yakin?',
      'Apakah Anda sudah yakin dengan data yang diinput dan akan dikirim ke bagian EDP?',
      [
        {
          text: 'Ya,',
          onPress: sendEDP,
        },
        {
          text: 'Kembali',
          onPress: () => {
            navigation.navigate(screenNames.SOPartial.Home);
          },
        },
      ],
    );
  };

  const deletePSOItem = itemId => {
    dispatch(deletePSOItemDetail(itemId))
      .then(() => {
        getproductAsync();
      })
      .catch(e => {
        Alert.alert('Failed');
      });
  };

  const deleteAction = itemId => {
    Alert.alert('Hapus Item Ini?', '', [
      {
        text: 'Ya,',
        onPress: () => deletePSOItem(itemId),
      },
      {
        text: 'Kembali',
      },
    ]);
  };

  useEffect(() => {
    getproductAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getproductAsync = () => {
    dispatch(PSOArticelList(id));
  };

  const keyExtractor = (item, i) => i.toString();

  const renderItem = ({item}) => {
    return (
      <View>
        <ListItem
          markText={''}
          onPress={handleOnPress(item.sku || item.SKU)}
          header={item.sku || item.SKU}
          title={item.skudesc || item.SKUDESC}
          subTitle={`${(item.divdesc || item.DIVISION) &&
            (item.DIVISION || item.divdesc) + ','} ${item.CATEGORY ||
            item.catdesc} ${item.tag ? item.tag.toUpperCase() : ''}`}
          info={`${item.qty + 'pcs' || item.QTY + 'pcs'}`}
          infoStyle={{fontSize: 16, color: colors.Red, fontWeight: 'bold'}}
          deleteIcon={true}
          deleteAction={() => deleteAction(item.id)}
        />
      </View>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getproductAsync} refreshing={product.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <View style={{flex: 1}}>
        <FlatList
          style={styles.padding10}
          data={product?.PSOList}
          ListEmptyComponent={<EmptyComponent loading={product.loading} />}
          refreshControl={refreshControl}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
        />
      </View>
      {product?.PSOList?.length > 0 && status === 0 && (
        <View style={{height: 76, padding: 20, backgroundColor: colors.White}}>
          <AppButton
            title={'Send to EDP'}
            color={colors.BlazeOrange}
            onPress={handleSubmit}
          />
        </View>
      )}
      <ModalInfo
        visible={modalInfoVisible}
        onDismiss={() => setModalInfoVisible(false)}
        message={infoMessage}
        title={infoTitle}
        icon={infoIcon}
        setVisible={setModalInfoVisible}
      />
    </Container>
  );
};

export default PSOList;

const EmptyComponent = ({loading, name}) => (
  <View
    style={{
      height: 200,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View>
        <Text>List {name} Kosong</Text>
      </View>
    )}
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.Light, marginBottom: 0},
  padding10: {
    padding: 10,
  },
});
