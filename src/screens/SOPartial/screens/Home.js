import * as React from 'react';
import {Container, AppButton, Icon} from '../../../components';
import globalStyles from '../../../styles/global';
import colors from '../../../utils/colors';
import {View, Text, ScrollView} from 'react-native';
import {iconsNames} from '../../../utils/icons';
import {screenNames} from '../../../utils/screens';
import {useDispatch} from 'react-redux';

const SOPartial = props => {
  const {navigation} = props;

  const dispatch = useDispatch();

  navigation.setOptions({
    title: 'Stock Opname',
  });

  const handleNavigateBarcode = () => {
    dispatch({type: 'SET_PSO_METHOD', payload: 'manual'});
    navigation.navigate(screenNames.BarcodeScan.Scan, {
      title: 'New Input by Barcode Scan',
    });
  };

  const handleNavigateListBarcode = () => {
    navigation.navigate(screenNames.SOPartial.PSOList, {
      title: 'PSO Artikel List',
    });
  };

  const handleNavigateListMinusStock = () => {
    dispatch({type: 'SET_PSO_METHOD', payload: 'stock-minus'});
    navigation.navigate(screenNames.Home.Supermarket, {
      screen: screenNames.Article.Article,
      params: {
        title: 'Minus Stock',
        parentTitle: 'supermarket',
        barcode: true,
        screenType: 'PSO',
      },
    });
  };

  const handleNavigateListSKU = () => {
    dispatch({type: 'SET_PSO_METHOD', payload: 'must-so-sku'});
    navigation.navigate(screenNames.Home.Supermarket, {
      screen: screenNames.Article.Article,
      params: {
        title: 'SKU',
        parentTitle: 'supermarket',
        barcode: true,
        screenType: 'PSO',
      },
    });
  };

  const handleNavigateListMustSO = () => {
    dispatch({type: 'SET_PSO_METHOD', payload: 'must-so'});
    navigation.navigate(screenNames.Home.Supermarket, {
      screen: screenNames.Article.Category,
      params: {
        title: 'Must SO',
        parentTitle: 'supermarket',
        barcode: true,
        screenType: 'PSO',
      },
    });
  };

  return (
    <Container style={globalStyles.container}>
      <View>
        {/* <Text></Text> */}
        <Text style={{fontWeight: 'bold', fontSize: 17}}>
          {
            'Perhatian !\nSebelum Anda Masuk ke menu\nPARTIAL LIST Stock Opname,\npastikan kembali bahwa List Product\nsudah didaftarkan melalui\nmenu "NEW INPUT by BARCODE SCAN".'
          }
        </Text>
      </View>
      <ScrollView style={{flex: 1, marginTop: 20}}>
        <AppButton
          onPress={handleNavigateBarcode}
          title={'New Input\nby Barcode Scan'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10}}
          color={colors.BlazeOrange}
        />
        <AppButton
          onPress={handleNavigateListBarcode}
          title={'Partial List\nStock Opname'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10}}
          color={colors.SeaGreen}
        />
        <AppButton
          onPress={handleNavigateListMinusStock}
          title={'Partial List\nStock Opname by Minus Stock'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10}}
          color={colors.YellowOrange}
        />
        <AppButton
          onPress={handleNavigateListMustSO}
          title={'Partial List\nStock Opname by Must SO'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10}}
          color={colors.BlueBell}
        />
        <AppButton
          onPress={handleNavigateListSKU}
          title={'Partial List\nStock Opname by SKU'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10}}
          color={colors.GoldenTainoi}
        />
      </ScrollView>
    </Container>
  );
};

export default SOPartial;
