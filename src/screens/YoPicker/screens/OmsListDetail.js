import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  Modal,
  Alert,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {Container, AppButton} from '../../../components';
import globalStyles from '../../../styles/global';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useDispatch, useSelector} from 'react-redux';
import {
  getOmsListDetail,
  finishPicker,
  getOmsList,
  getSearchDetail,
} from '../../../redux/actions/yo-picker-actions';
import {debounce} from 'lodash';

const OmsList = props => {
  const {navigation, route} = props;

  const dispatch = useDispatch();
  const yoPicker = useSelector(state => state.yoPicker);

  const [modalVisible, setModalVisible] = useState(false);
  const [itemSelected, setItemSelected] = useState({});
  const [searchText, setSearchText] = useState('');
  const [searchDetail, setSearchDetail] = useState([]);
  const [searching, setSearching] = useState(false);

  const getYoPickerOrderDetail = id => {
    id = id || route?.params?.id;
    dispatch(getOmsListDetail(id));
  };

  const handlePickingPress = item => {
    if (item.selisih === 'yes' || item.selisih === '\\"yes\\"') {
      setItemSelected(item);
      setModalVisible(true);
    }
  };

  const handleNavigateProductIdentity = item => {
    navigation.navigate(screenNames.Product.Identity, {
      productId: item.sku,
      title: 'Product Identity',
      hideReason: true,
      parentTitle: 'yopicker',
      item: item,
    });
  };

  const handleFinishYopicker = id => {
    id = id || route?.params?.id;
    dispatch(finishPicker(id))
      .then(() => {
        Alert.alert('Success', 'Berhasil', [
          {
            text: 'OK',
            onPress: () => {
              dispatch(getOmsList('oms'));
              navigation.goBack();
            },
          },
        ]);
      })
      .catch(error => {
        console.log(error);
        Alert.alert('Failed', error.message, [
          {
            text: 'OK',
          },
        ]);
      });
  };

  const handleSubmit = id => {
    // const picking0 = yoPicker.omsListDetail.filter((detail => {

    //   if (detail.quantity_picking === "0") {
    //     return true;
    //   }

    // }))

    Alert.alert(
      'Apakah Anda sudah yakin?',
      'Apakah Anda sudah yakin dengan data yang diinput?',
      [
        {
          text: 'Ya,',
          onPress: () => handleFinishYopicker(id),
        },
        {
          text: 'Kembali',
        },
      ],
    );
  };

  const handleSearch = React.useCallback(
    debounce(text => {
      dispatch(getSearchDetail(text)).then(notesListSearch => {
        setSearchDetail(notesListSearch);
        setSearching(false);
      });
    }, 100),
    [],
  );

  const handleChangeSearch = text => {
    setSearchText(text);

    if (!searching) {
      setSearching(true);
    }

    handleSearch(text);
  };

  const keyExtractor = (item, index) => index.toString();
  const renderItem = ({item, index}) => {
    const isValid =
      item.validate === 'yes' &&
      item.selisih === 'no' &&
      item.quantity === item.quantity_picking;
    const isSelisih = item.selisih === 'yes';

    const picking = isSelisih;
    const valid = isValid;

    const backgroundColor = picking ? '#FFFF00' + 'aa' : colors.White;
    const pickingBg = valid ? '#00ff00aa' : '#ffcc29';

    const division = item.division_description.split('-')[0];

    const footerBg =
      division === 'FOOD'
        ? colors.Red
        : division === 'NONFOOD'
        ? colors.BluePrimary
        : division === 'FRESH'
        ? colors.SeaGreen
        : division === 'GMS'
        ? colors.OrangPeel
        : colors.Black + 'aa';

    const fontColor = picking || valid ? colors.Black : colors.PeachOrange;
    return (
      <>
        <View
          style={{
            backgroundColor: footerBg,
            borderRadius: 5,
            borderBottomEndRadius: 0,
            borderBottomStartRadius: 0,
            padding: 20,
            paddingBottom: 20,
            marginHorizontal: 10,
          }}>
          <View>
            <Text
              style={{fontSize: 16, fontWeight: 'bold', color: colors.White}}>
              {item.article_description}
            </Text>
          </View>
        </View>
        <View
          style={{
            backgroundColor: backgroundColor,
            borderRadius: 5,
            borderTopEndRadius: 0,
            borderTopStartRadius: 0,
            padding: 20,
            paddingBottom: 20,
            marginHorizontal: 10,
            marginBottom: 10,
            borderWidth: 2,
            borderColor: footerBg,
          }}>
          <TouchableOpacity onPress={() => handleNavigateProductIdentity(item)}>
            <View
              style={{
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  paddingRight: 10,
                }}>
                <Text>{item.sku}</Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                  justifyContent: 'space-between',
                }}>
                <Text>{item.division_description}</Text>
                <Text
                  style={{
                    color: fontColor,
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}>
                  Lokasi: {item.lokasi}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            <AppButton
              title={`Order : ${item.quantity}`}
              color={colors.SeaGreen}
            />
            <AppButton
              title={`Stock : ${item.stock_gold}`}
              color={colors.YellowOrange}
            />

            <AppButton
              onPress={() => handlePickingPress(item)}
              title={`Picking : ${item.quantity_picking}`}
              color={pickingBg}
            />
          </View>
        </View>
      </>
    );
  };

  const ModalPicker = () => {
    return (
      <Modal
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
        transparent={true}
        animated={true}
        animationType="slide">
        <TouchableOpacity
          onPress={() => setModalVisible(false)}
          style={{
            backgroundColor: colors.Black + '22',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              height: 300,
              width: '90%',
              backgroundColor: colors.White,
              paddingHorizontal: 10,
              justifyContent: 'center',
            }}>
            <Text style={{textAlign: 'center', fontSize: 18}}>
              {
                'Produk ini mengalami\nSELISIH STOCK\ndan telah direcheck oleh :'
              }
            </Text>
            <View style={{marginTop: 5}}>
              <Text style={{fontWeight: '700', fontSize: 16}}>Nama</Text>
              <TextInput
                style={{
                  borderWidth: 0.7,
                  borderColor: colors.Silver,
                  borderRadius: 10,
                  height: 56,
                }}
                value={itemSelected.staff_name}
                editable={false}
                placeholder="Nama"
              />
            </View>
            <View style={{marginTop: 5}}>
              <Text style={{fontWeight: '700', fontSize: 16}}>ST</Text>
              <TextInput
                style={{
                  borderWidth: 0.7,
                  borderColor: colors.Silver,
                  borderRadius: 10,
                  height: 56,
                }}
                value={itemSelected.st_name}
                editable={false}
                placeholder="ST"
              />
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  };

  const refreshControl = () => {
    return (
      <RefreshControl
        onRefresh={getYoPickerOrderDetail}
        refreshing={yoPicker.loading || searching}
      />
    );
  };

  useEffect(() => {
    (() => {
      getYoPickerOrderDetail();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  navigation.setOptions({
    title: `OMS Order List: ${route?.params?.omsCode}`,
  });

  return (
    <Container style={globalStyles.container}>
      <View
        style={{
          margin: 10,
          flexDirection: 'row',
          borderWidth: 0.5,
          borderRadius: 20,
          paddingHorizontal: 10,
          borderColor: colors.Silver,
          alignItems: 'center',
        }}>
        <MaterialIcon color={colors.SilverChalice} name={'search'} size={25} />
        <View style={{flex: 1, height: 42}}>
          <TextInput
            value={searchText}
            onChangeText={handleChangeSearch}
            style={{paddingHorizontal: 10, 
              height: 56,}}
            placeholder={'Search Title'}
          />
        </View>
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: colors.Silver + '66',
          paddingVertical: 10,
        }}>
        <FlatList
          data={searchText !== '' ? searchDetail : yoPicker.omsListDetail}
          keyExtractor={keyExtractor}
          refreshControl={refreshControl()}
          renderItem={renderItem}
        />
      </View>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <AppButton
          style={{width: 250}}
          onPress={handleSubmit}
          title={'Finish'}
          color={colors.EarlsGreen}
        />
      </View>
      <ModalPicker />
    </Container>
  );
};

export default OmsList;
