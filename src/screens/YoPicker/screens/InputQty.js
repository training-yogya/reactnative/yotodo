/* eslint-disable radix */
import React, {useState, useEffect} from 'react';
import {Container, AppButton} from '../../../components';
import globalStyles from '../../../styles/global';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import colors from '../../../utils/colors';

const InputQty = props => {
  const {navigation, route} = props;

  const addPicking = route.params.handleSubmit;

  const [selisihStock, setSelisihStock] = useState(false);
  const [formValid, setFormValid] = useState(false);

  const [quantity, setQuantity] = useState(() => route.params.quantity);
  const [nama, setNama] = useState('');
  const [st, setSt] = useState('');

  const isEmpty = route.params.empty;
  const handleSubmit = () => {
    addPicking({
      staff_name: nama,
      st_name: st,
      quantity: isEmpty ? 0 : quantity,
      selisih: selisihStock ? 'yes' : 'no',
    });
  };

  useEffect(() => {
    if (nama && st && !formValid) {
      setFormValid(true);
    } else if ((!nama || !st) && formValid) {
      setFormValid(false);
    }

    if (isEmpty && !selisihStock) {
      if (parseInt(route.params.quantity) === 0) {
        setSelisihStock(false);
      } else if (parseInt(route.params.quantity) > 0) {
        setSelisihStock(true);
      }
    }

    if (
      parseInt(quantity) !== parseInt(route.params.quantity) &&
      !selisihStock &&
      !isEmpty
    ) {
      setSelisihStock(true);
    } else if (
      parseInt(quantity) === parseInt(route.params.quantity) &&
      selisihStock &&
      !isEmpty
    ) {
      setSelisihStock(false);
    }
  }, [
    nama,
    st,
    formValid,
    quantity,
    selisihStock,
    isEmpty,
    route.params.quantity,
  ]);

  navigation.setOptions({
    title: isEmpty ? 'Quantity' : 'Input Quantity',
  });
  return (
    <Container style={globalStyles.container}>
      <View style={{flex: 1, marginTop: 10}}>
        <ScrollView>
          <View style={{marginBottom: 10}}>
            <Text style={{fontWeight: '700', fontSize: 16}}>
              Input Qty In Pcs
            </Text>
            <View
              style={{
                borderWidth: 0.7,
                borderColor: colors.Silver,
                borderRadius: 10,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TextInput
                style={{flex: 1,
                  height: 56,}}
                value={isEmpty ? '0' : quantity}
                editable={!isEmpty}
                onChangeText={setQuantity}
                keyboardType={'numeric'}
                placeholder={'QTY'}
              />
              {quantity.length > 0 && !isEmpty && (
                <TouchableOpacity
                  style={{padding: 10}}
                  onPress={() => setQuantity('')}>
                  <Text style={{color: colors.Red}}>Clear</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
          {!isEmpty && (
            <View style={{marginBottom: 20}}>
              <Text style={{fontWeight: '700', fontSize: 16}}>
                Apakah Selisih Stock?
              </Text>
              <TouchableOpacity
                onPress={() => setSelisihStock(!selisihStock)}
                style={{
                  backgroundColor: selisihStock
                    ? colors.BlazeOrange + '55'
                    : colors.Silver + '55',

                  borderRadius: 25 / 2,
                  width: 50,
                }}>
                <View
                  style={{
                    width: 25,
                    height: 25,
                    borderRadius: 25 / 2,
                    marginLeft: selisihStock ? 25 : 0,
                    backgroundColor: selisihStock
                      ? colors.BlazeOrange
                      : colors.Silver,
                  }}
                />
              </TouchableOpacity>
            </View>
          )}
          {(selisihStock || isEmpty) && (
            <View>
              <Text style={{fontSize: 16}}>
                {
                  'Product Ini Mengalami Selisih Stock\ndan Telah di Recheck oleh :'
                }
              </Text>
              <View style={{marginTop: 5}}>
                <Text style={{fontWeight: '700', fontSize: 16}}>Nama</Text>
                <TextInput
                  style={{
                    borderWidth: 0.7,
                    borderColor: colors.Silver,
                    borderRadius: 10,
                    height: 56,
                  }}
                  value={nama}
                  onChangeText={setNama}
                  placeholder="Nama"
                />
              </View>
              <View style={{marginTop: 5}}>
                <Text style={{fontWeight: '700', fontSize: 16}}>ST</Text>
                <TextInput
                  style={{
                    borderWidth: 0.7,
                    borderColor: colors.Silver,
                    borderRadius: 10,
                    height: 56,
                  }}
                  value={st}
                  onChangeText={setSt}
                  placeholder="ST"
                />
              </View>
            </View>
          )}
        </ScrollView>
      </View>

      <View>
        <View>
          <AppButton
            disabled={(selisihStock || isEmpty) && !formValid}
            onPress={handleSubmit}
            title="Submit"
            color={
              !(selisihStock || isEmpty)
                ? colors.BlazeOrange
                : formValid
                ? colors.BlazeOrange
                : colors.Silver
            }
          />
        </View>
      </View>
    </Container>
  );
};

export default InputQty;
