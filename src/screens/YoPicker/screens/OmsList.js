import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import {Container, AppButton} from '../../../components';
import globalStyles from '../../../styles/global';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useDispatch, useSelector} from 'react-redux';
import {getOmsList} from '../../../redux/actions/yo-picker-actions';

const OmsList = props => {
  const {navigation, route} = props;

  const dispatch = useDispatch();
  const yoPicker = useSelector(state => state.yoPicker);

  const [omsCode, setOmsCode] = useState('oms');

  const handleSubmitSearch = () => {
    getYoPickerOrder(omsCode);
  };

  const getYoPickerOrder = search => {
    search = search || omsCode;
    dispatch(getOmsList(search));
  };

  const handleItemPress = item => {
    navigation.navigate(screenNames.YoPicker.OmsListDetail, {
      omsCode: item.oms_order_number,
      id: item.id,
    });
  };

  const keyExtractor = (item, index) => index.toString();
  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => handleItemPress(item)}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: colors.White,
          borderRadius: 10,
          padding: 20,
          marginHorizontal: 10,
          marginBottom: 10,
        }}>
        <View>
          <Text style={{fontWeight: 'bold', marginBottom: 10}}>
            OMS ORDER NO :
          </Text>
          <Text>{item.oms_order_number}</Text>
        </View>

        <View style={{alignItems: 'flex-end'}}>
          <Text style={{fontWeight: 'bold', fontSize: 16, marginBottom: 5}}>
            {item.total_item} Articles
          </Text>
          <Text style={{color: colors.Red}}>
            {moment(item.created_at).format('DD MMM YYYY hh:mm')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const refreshControl = () => {
    return (
      <RefreshControl
        onRefresh={handleSubmitSearch}
        refreshing={yoPicker.loading}
      />
    );
  };

  useEffect(() => {
    (() => {
      getYoPickerOrder();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: route?.params?.title ?? 'YOPICKER',
  });

  return (
    <Container style={globalStyles.container}>
      <View>
        <Text>Ketik Nomor OMS order disini: </Text>
        <View
          style={{
            borderWidth: 1,
            width: '100%',
            marginTop: 10,
            borderRadius: 10,
            borderColor: colors.Silver,
            paddingHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <MaterialIcon name="search" size={32} />
          <TextInput
            value={omsCode}
            onChangeText={setOmsCode}
            style={{flex: 1,
              height: 56,}}
            onSubmitEditing={handleSubmitSearch}
          />
          <TouchableOpacity
            onPress={handleSubmitSearch}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: colors.BlazeOrange}}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: colors.Silver + '66',
          paddingVertical: 10,
        }}>
        <FlatList
          data={yoPicker.omsList}
          refreshControl={refreshControl()}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
          ListEmptyComponent={<Text style={{textAlign: 'center'}}>Kosong</Text>}
        />
      </View>
    </Container>
  );
};

export default OmsList;
