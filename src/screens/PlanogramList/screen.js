import * as React from 'react';
import {useEffect} from 'react';
import {Container, ListSimpleItem} from '../../components';
import {FlatList, StyleSheet} from 'react-native';
import {screenNames} from '../../utils/screens';
import {useSelector} from 'react-redux';

const PlanogramList = ({navigation, route, getMenuCategory}) => {
  const title = route.params.title;
  const categoryList = useSelector(state => state.planogram.categoryList);
  const loading = useSelector(state => state.planogram.loading);
  const handleListOnPress = planogramId => () => {
    navigation.navigate(screenNames.Planogram.Details, {
      title: planogramId,
      planogramId,
    });
  };
  const keyExtractor = (item, i) => i.toString();
  const renderItem = ({item, index}) => (
    <ListSimpleItem
      onPress={handleListOnPress(item.categories)}
      title={item.categories}
    />
  );

  useEffect(() => {
    getMenuCategory(title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title,
  });

  return (
    <Container style={styles.wrapper}>
      <FlatList
        data={categoryList}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </Container>
  );
};

export default PlanogramList;

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 10,
  },
});
