import {connect} from 'react-redux';
import PlanogramList from './screen';
import {
  getPlanogramSubmenu,
  menuCategory,
} from './../../redux/actions/planogram-action';

const mapDispatchToProps = dispatch => ({
  getPlanogramSubmenu: menu => dispatch(getPlanogramSubmenu(menu)),
  getMenuCategory: devision => dispatch(menuCategory(devision)),
});

export default connect(
  null,
  mapDispatchToProps,
)(PlanogramList);
