import * as React from 'react';
import {Container} from '../../components';
import {StyleSheet, Dimensions, ActivityIndicator} from 'react-native';

import Pdf from 'react-native-pdf';
import {useDispatch, useSelector} from 'react-redux';
import {getPlanogramInfoUrl} from '../../redux/actions/planogram-action';
const {width, height} = Dimensions.get('window');

const PlanogramTutorial = ({navigation, route}) => {
  const {title} = route.params;
  const dispatch = useDispatch();
  const planogramInfoUrl = useSelector(state => state.planogram.planogramInfo);

  React.useEffect(() => {
    (() => {
      dispatch(getPlanogramInfoUrl());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title,
  });
  return (
    <Container>
      <Pdf
        activityIndicator={<ActivityIndicator />}
        source={{uri: planogramInfoUrl, cache: true}}
        maxScale={20.0}
        style={styles.pdf}
      />
    </Container>
  );
};

export default PlanogramTutorial;

const styles = StyleSheet.create({
  pdf: {
    flex: 1,
    width: width,
    height: height,
  },
});
