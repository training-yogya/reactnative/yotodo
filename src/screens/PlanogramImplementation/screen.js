import * as React from 'react';
import {Container, AppButton} from '../../components';
import {Text, StyleSheet, View, CameraRoll} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../utils/colors';

const PlanogramImplementation = ({navigation}) => {
  navigation.setOptions({
    title: 'planogram implementation',
  });
  return (
    <Container style={styles.wrapper}>
      <View style={{flex: 1}}>
        <CameraForm
          description={'Take Picture of Current Display BEFORE implementation'}
        />
        <View>
          <AppButton color={colors.OrangPeel} title="Start Submit" />
        </View>
        <Text>Start at: 02:00:10</Text>
      </View>
      <View style={{flex: 1}}>
        <CameraForm
          description={'Take Picture of Current Display AFTER implementation'}
        />
        <View>
          <AppButton color={colors.TradeWind} title="Reset" />
          <AppButton color={colors.OrangPeel} title="Start Submit" />
        </View>
        <Text>Fisish at: 02:00:10</Text>
      </View>
    </Container>
  );
};

const CameraForm = ({description}) => {
  return (
    <View>
      <Text>{description}</Text>
      <TouchableOpacity>
        <View style={{height: 150, justifyContent: 'center', borderWidth: 1}}>
          <MaterialIcon
            style={{alignSelf: 'center'}}
            name="camera-alt"
            size={36}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default PlanogramImplementation;

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 10,
    paddingTop: 5,
  },
});
