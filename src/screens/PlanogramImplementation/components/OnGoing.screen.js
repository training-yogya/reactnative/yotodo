import * as React from 'react';
import {Container, AppButton} from '../../../components';
import {View, Text} from 'react-native';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';

const PlanogramImplementationOnGoing = ({navigation}) => {
  const handleButtonNext = () => {
    navigation.navigate(screenNames.Planogram.Implementation.Summary);
  };
  return (
    <Container>
      <View style={{flex: 1}}>
        <Text>Stopwatch</Text>
      </View>
      <View>
        <AppButton
          onPress={handleButtonNext}
          color={colors.BlazeOrange}
          title="Next"
        />
      </View>
    </Container>
  );
};

export default PlanogramImplementationOnGoing;
