import * as React from 'react';
import {Container, AppButton} from '../../../components';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import globalStyles from '../../../styles/global';
import {useDispatch} from 'react-redux';
import {SET_PLANOGRAM_IMPLEMENTATION_START} from '../../../redux/actionTypes';

const PlanogramImplementationStart = ({navigation}) => {
  const dispatch = useDispatch();
  const saveStartPicture = picture => {
    const payload = {
      image: {
        name: picture.name,
        type: picture.type,
        uri: picture.uri,
        coords: picture.coords,
      },
      time: picture.dateInt,
    };
    dispatch({
      type: SET_PLANOGRAM_IMPLEMENTATION_START,
      payload,
    });
    navigation.push(screenNames.Planogram.Details, {
      implementation: true,
      timeStart: picture.dateInt,
      title: 'Planogram Implementation',
    });
  };
  const handleButtonNext = () => {
    navigation.navigate(screenNames.Planogram.Implementation.Camera, {
      savePicture: saveStartPicture,
    });
  };
  const handleTutorialPress = () => {
    navigation.navigate(screenNames.Planogram.Tutorial, {
      title: 'How to read and implementation',
    });
  };
  navigation.setOptions({
    title: 'planogram implementation',
  });
  return (
    <Container>
      <View style={styles.topContainer}>
        <View style={globalStyles.flex1}>
          <TouchableOpacity
            onPress={handleTutorialPress}
            activeOpacity={0.8}
            style={{
              maxHeight: 300,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: colors.BlueBell,
              borderRadius: 4,
            }}>
            <MaterialIcon name="group" color={colors.White} size={96} />
            <Text
              style={{
                color: colors.White,
                fontSize: 18,
                fontWeight: '600',
                maxWidth: 300,
                textAlign: 'center',
              }}>
              How to read and Planogram Implementation
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{paddingHorizontal: 50, marginTop: 10}}>
          <AppButton
            onPress={handleButtonNext}
            color={colors.BlazeOrange}
            title="Start Implementation"
            style={{
              borderWidth: 0.5,
            }}
          />
        </View>
        <View style={[globalStyles.flex1, styles.intruction]}>
          <Text>Perhatian</Text>
          <Text>
            &bull; Tekan tombol "Start Implementation" untuk melanjutkan proses
            implementasi planogram
          </Text>
          <Text>
            &bull; Setelah menekan tombol, anda akan diarahkan ke kamera
          </Text>
          <Text>&bull; Stopwatch akan berjalan setelah mengambil gambar</Text>
        </View>
      </View>
    </Container>
  );
};

export default PlanogramImplementationStart;

const styles = StyleSheet.create({
  topContainer: {
    flex: 1,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  intruction: {
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
});
