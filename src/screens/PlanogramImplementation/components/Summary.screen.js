import * as React from 'react';
import {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import moment from 'moment';
import {Container, AppButton, ModalInfo} from '../../../components';
import {View, Text, Image} from 'react-native';
import colors from '../../../utils/colors';
import globalStyles from '../../../styles/global';
import {submitPlanogramImplementation} from '../../../redux/actions/planogram-action';

const PlanogramImplementationSummary = ({navigation, route}) => {
  const {title} = route.params;
  const {popToTop} = navigation;

  const dispatch = useDispatch();

  const planogramForm = useSelector(state => state.planogram.form);
  const loading = useSelector(state => state.planogram.loading);
  const success = useSelector(state => state.planogram.success);
  const error = useSelector(state => state.planogram.error);
  const message = useSelector(state => state.planogram.message);

  const [modalVisible, setModalVisible] = useState(false);

  const {start, end, duration} = planogramForm;
  const startAt = moment(start.time).format('hh:mm:ss A');
  const endAt = moment(end.time).format('hh:mm:ss A');
  const durationTime = moment.duration(duration, 'milliseconds');
  const durationText = `${durationTime.hours()}:${durationTime.minutes()}:${durationTime.seconds()}`;

  const handleFinish = () => {
    dispatch(submitPlanogramImplementation());
    setModalVisible(true);
  };

  const ModalNotif = React.useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        return popToTop();
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Berhasil disubmit"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [success, error, popToTop, modalVisible, message]);

  navigation.setOptions({
    title: title || 'Summary',
  });

  return (
    <Container style={[globalStyles.pHorizontal10, {paddingTop: 5}]}>
      <View style={{flex: 1}}>
        <Image
          source={{uri: planogramForm.start.image.uri}}
          style={{width: '100%', height: '80%', borderRadius: 4}}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text>Start At</Text>
          <Text style={{fontSize: 18, fontWeight: 'bold', color: colors.Black}}>
            {startAt}
          </Text>
        </View>
      </View>
      <View style={{flex: 1}}>
        <Image
          source={{uri: planogramForm.end.image.uri}}
          style={{width: '100%', height: '80%', borderRadius: 4}}
        />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text>End At</Text>
          <Text style={{fontSize: 18, fontWeight: 'bold', color: colors.Black}}>
            {endAt}
          </Text>
        </View>
      </View>
      <View style={{height: 60}}>
        <Text>Duration</Text>
        <Text
          style={{
            fontSize: 22,
            fontWeight: 'bold',
            color: colors.Black,
            textAlign: 'right',
          }}>
          {durationText}
        </Text>
      </View>
      <View>
        <AppButton
          loading={loading}
          onPress={handleFinish}
          color={colors.BlazeOrange}
          title="Finish"
        />
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default PlanogramImplementationSummary;
