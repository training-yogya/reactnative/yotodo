import {connect} from 'react-redux';
import PlanogramImpementation from './screen';
import {} from './../../redux/actions/planogram-action';

const mapDispatchToProps = dispatch => ({});

export default connect(
  null,
  mapDispatchToProps,
)(PlanogramImpementation);
