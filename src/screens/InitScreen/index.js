import {connect} from 'react-redux';
import SplashScreen from './screen';
import {changeUserToken} from './../../redux/actions/auth-action';

const mapDispatchToProps = dispatch => ({
  changeUserToken: token => dispatch(changeUserToken(token)),
});

export default connect(
  null,
  mapDispatchToProps,
)(SplashScreen);
