import React from 'react';
import {Text, ActivityIndicator, View} from 'react-native';
import {Icon, Container} from './../../components';
import {iconsNames} from './../../utils/icons';
import globalStyles from './../../styles/global';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Bar as ProgressBar} from 'react-native-progress';
import {Dimensions} from 'react-native';

const SplashScreen = ({navigation, changeUserToken, loadingText, progress}) => {
  return (
    <Container style={globalStyles.centerAround}>
      <TouchableOpacity onPress={() => {}}>
        <Container style={globalStyles.center}>
          <Icon name={iconsNames.logo} />
        </Container>
        <View style={{alignItems: 'center'}}>
          <ActivityIndicator size={40} />
          {loadingText !== undefined && <Text>{loadingText}</Text>}
          {progress !== undefined && (
            <ProgressBar progress={progress} width={200} />
          )}
        </View>
        <Container style={globalStyles.centerEnd}>
          <Text>from</Text>
          <Icon name={iconsNames.yogyabanner} />
        </Container>
      </TouchableOpacity>
    </Container>
  );
};

export default SplashScreen;
