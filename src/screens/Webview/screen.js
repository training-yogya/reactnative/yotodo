import * as React from 'react';
import {WebView} from 'react-native-webview';
import {Container} from '../../components';
import globalStyles from '../../styles/global';
import 'react-native-get-random-values';

const WebViewScreen = ({navigation, route}) => {
  const {uri, title} = route.params;
  navigation.setOptions({
    title,
  });
  return (
    <Container style={globalStyles.pTop5}>
      <WebView source={{uri}} />
    </Container>
  );
};

export default WebViewScreen;
