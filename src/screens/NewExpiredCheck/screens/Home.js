import * as React from 'react';
import {Container, AppButton, Icon} from '../../../components';
import globalStyles from '../../../styles/global';
import colors from '../../../utils/colors';
import {View, Text, ScrollView} from 'react-native';
import {iconsNames} from '../../../utils/icons';
import {screenNames} from '../../../utils/screens';
import {useDispatch} from 'react-redux';

const SOPartial = props => {
  const {navigation} = props;

  const dispatch = useDispatch();

  navigation.setOptions({
    title: 'Expired Registration',
  });

  const handleNavigateBarcode = () => {
    navigation.navigate(screenNames.BarcodeScan.Scan, {
      title: 'Barcode Scan',
      type: 'expiredCheck',
    });
  };

  const handleNavigateTodayList = () => {
    navigation.navigate(screenNames.ExpiredCheck.List, {
      title: 'Today Input List',
    });
  };

  const handleNavigatePreviusList = () => {
    navigation.navigate(screenNames.ExpiredCheck.List, {
      title: 'Previous List',
    });
  };

  const handleNavigateList = () => {
    navigation.navigate(screenNames.ExpiredCheck.List, {
      title: 'Expired List',
    });
  };

  const handleNavigateDeletedList = () => {
    navigation.navigate(screenNames.ExpiredCheck.List, {
      title: 'Deleted List',
    });
  };

  return (
    <Container style={globalStyles.container}>
      <View>
        {/* <Text></Text> */}
        <Text style={{fontWeight: 'bold', fontSize: 17, textAlign: 'center'}}>
          {
            'Perhatian !\nFitur ini hanya untuk mendata produk\nyang memiliki Tanggal Kadaluarsa\n[Expired Date] lebih dari 7 hari.'
          }
        </Text>
      </View>
      <ScrollView style={{flex: 1, marginTop: 20}}>
        <AppButton
          onPress={handleNavigateBarcode}
          title={'New Input Data Expired'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55, borderRadius: 10}}
          color={colors.YellowOrange}
        />
        <AppButton
          onPress={handleNavigateTodayList}
          title={'Today Input List'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55, borderRadius: 10}}
          color={colors.DodgerBlue}
        />
        <AppButton
          onPress={handleNavigatePreviusList}
          title={'Previous List'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55, borderRadius: 10}}
          color={colors.BlueBell}
        />
        <AppButton
          onPress={handleNavigateList}
          title={'Expired List'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55, borderRadius: 10}}
          color={colors.BluePrimary}
        />
        <AppButton
          onPress={handleNavigateDeletedList}
          title={'DELETED List'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55, borderRadius: 10}}
          color={colors.Red}
        />
      </ScrollView>
      <Text>Idea by EDP Regional Bogor</Text>
    </Container>
  );
};

export default SOPartial;
