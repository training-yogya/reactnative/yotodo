import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  Text,
  StyleSheet,
  ActivityIndicator,
  Modal,
  TextInput,
  Button,
  ScrollView,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Container, AppButton, Loading, ModalInfo} from '../../../components';
import ListPill from './../../ProductIdentity/components/ListPill';
import colors from '../../../utils/colors';
import globalStyles from '../../../styles/global';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import ImageViewer from 'react-native-image-zoom-viewer';
import {useSelector, useDispatch} from 'react-redux';
import {
  getProductIdentity,
  addExpired,
  getExpiredList,
} from '../../../redux/actions/product-action';

import moment from 'moment';
import {screenNames} from '../../../utils/screens';
import DateTimePicker from '@react-native-community/datetimepicker';
const {width, height} = Dimensions.get('screen');

const ProductIdentity = props => {
  const {navigation, route} = props;

  const {title, productId, parentTitle, viewOnly} = route.params;

  const cabang = useSelector(state => state.auth.userLogin.initial_store);
  const product = useSelector(state => state.product);
  const yoPicker = useSelector(state => state.yoPicker);

  const [date, setDate] = useState(new Date());

  const [showDate, setShowDate] = useState(false);
  const dispatch = useDispatch();

  const [showDefaultImage, setShowDefaultImage] = useState(true);
  const [isImageError, setIsImageError] = useState(false);
  const [modalPictureVisible, setModalPictureVisible] = useState(false);
  const [modalInfoVisible, setModalInfoVisible] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [expDate, setExpDate] = useState('');
  const [infoMessage, setInfoMessage] = useState('');
  const [infoTitle, setInfoTitle] = useState('');
  const [infoIcon, setInfoIcon] = useState('');
  const [qty, setQty] = useState('');

  const {loading, identity} = product;

  const productImage = showDefaultImage
    ? 'https://cdn-2.tstatic.net/default-2.jpg'
    : isImageError
    ? 'https://cdn-2.tstatic.net/default-2.jpg'
    : identity?.image || 'https://cdn-2.tstatic.net/default-2.jpg';

  useEffect(() => {
    dispatch(getProductIdentity(productId, cabang, parentTitle));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: title || 'Product Identity',
  });

  const handleSibmitExp = () => {
    const data = {
      sku: identity.sku,
      skudesc: identity.skudesc,
      tanggal_expored: moment(expDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      note: identity.note,
      divdesc: identity.division,
      catdesc: identity.category,
      tag: identity.tag,
      qty: qty,
    };
    dispatch(addExpired(data)).then(() => {
      // setTimeout(() => {
      setModalVisible(false);
      // });
      navigation.navigate(screenNames.ExpiredCheck.List, {
        title: 'today input list',
      });
      dispatch(getExpiredList());
    });
  };

  const handleOpenDate = () => {
    setShowDate(true);
  };

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShowDate(false);
    setDate(selectedDate);
    setExpDate(moment(currentDate).format('DD/MM/YYYY'));
  };

  const ImageProduct = React.useMemo(() => {
    return (
      <TouchableOpacity
        onPress={() => setModalPictureVisible(true)}
        style={{
          position: 'relative',
        }}>
        {!loading ? (
          <Image
            source={{
              uri: productImage,
            }}
            onLoadEnd={() => setShowDefaultImage(false)}
            // onLoadStart={() => !showDefaultImage && setShowDefaultImage(true)}
            onError={() => {
              setIsImageError(true);
            }}
            resizeMode="cover"
            style={styles.imageProduct}
          />
        ) : (
          <View style={styles.imageProduct} />
        )}
        {showDefaultImage && (
          <ActivityIndicator
            size={46}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
          />
        )}
      </TouchableOpacity>
    );
  }, [loading, productImage, showDefaultImage]);

  const ModalPicture = () => {
    return (
      <Modal
        onRequestClose={() => setModalPictureVisible(false)}
        visible={modalPictureVisible}
        transparent={true}
        animated={true}
        animationType={'slide'}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.7)',
            position: 'relative',
            height: '100%',
          }}>
          <TouchableOpacity
            onPress={() => setModalPictureVisible(false)}
            style={{
              justifyContent: 'center',
              height: 56,
              marginLeft: 20,
            }}>
            <MaterialIcon
              onPress={() => setModalPictureVisible(false)}
              name="close"
              color={colors.White}
              size={30}
            />
          </TouchableOpacity>
          <ImageViewer imageUrls={[{url: productImage}]} />
        </View>
      </Modal>
    );
  };

  return (
    <Container style={styles.wrapper}>
      {(loading || yoPicker.loading) && <Loading />}
      <>{ImageProduct}</>
      <View style={styles.banner}>
        <View>
          <Text style={styles.productTitle}>{identity?.skudesc || '-'}</Text>
          <View style={styles.bannerFooter}>
            <Text style={[globalStyles.textBold, styles.skuText]}>
              {identity?.sku || '-'}
            </Text>
            <View>
              <Text style={globalStyles.textBold}>{identity.division}</Text>
              <Text style={globalStyles.textBold}>{identity.category}</Text>
            </View>
          </View>
        </View>
      </View>
      <ScrollView>
        <ListPill
          title="Selling Price"
          value={`Rp. ${identity?.selling_price || '-'},-`}
        />
        <ListPill title="Last Stock" value={identity?.last_stock || '-'} />
        <ListPill title="Till Code" value={identity?.till_code || '-'} />
      </ScrollView>

      {!viewOnly && (
        <View style={styles.buttonReasonWrapper}>
          <AppButton
            onPress={() => setModalVisible(true)}
            title={'Expired\nRegistration'}
            titleStyle={{textAlign: 'center'}}
            style={{width: 200}}
            color={colors.BlazeOrange}
          />
        </View>
      )}

      {!viewOnly && !showDate && (
        <Modal
          visible={modalVisible}
          transparent={true}
          animated={true}
          onRequestClose={() => setModalVisible(false)}
          animationType={'slide'}>
          <View
            style={{
              flex: 1,
              backgroundColor: `${colors.Black}aa`,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '90%',
                paddingVertical: 10,
                borderRadius: 10,
                backgroundColor: colors.White,
              }}>
              <View style={{alignItems: 'flex-end', paddingRight: 10}}>
                <MaterialIcon
                  onPress={() => setModalVisible(false)}
                  name={'close'}
                  size={15}
                  color={colors.White}
                  style={{
                    padding: 3,
                    backgroundColor: colors.Red,
                    borderRadius: 20,
                  }}
                />
              </View>
              <View style={{padding: 10}}>
                <Text
                  style={{
                    lineHeight: 50,
                    fontWeight: 'bold',
                    fontSize: 17,
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    marginBottom: 20,
                  }}>
                  Input Closest Expiration Date
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={{
                      backgroundColor: colors.Black + '33',
                      borderRadius: 10,
                      height: 56,
                      paddingHorizontal: 10,
                      width: '60%',
                    }}
                    editable={false}
                    value={expDate}
                    onChangeText={text => setExpDate(text)}
                    placeholder={'Exp Date'}
                  />
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: colors.PeachOrange + '55',
                      borderRadius: 20,
                      marginLeft: 10,
                    }}>
                    <MaterialIcon
                      onPress={handleOpenDate}
                      color={colors.PeachOrange}
                      name="date-range"
                      size={32}
                    />
                  </View>
                </View>
                <View style={{height: 10}} />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={{
                      backgroundColor: colors.Black + '11',
                      borderRadius: 10,
                      paddingHorizontal: 10,
                      height: 56,
                      width: '60%',
                      marginHorizontal: 20,
                      textAlign: 'right',
                    }}
                    autoFocus={true}
                    value={qty}
                    keyboardType={Platform.OS === "ios" ? "default" : "numeric"}
                    onChangeText={text => setQty(text)}
                    placeholder={'QTY'}
                  />
                  <View
                    style={{
                      padding: 5,
                      borderRadius: 20,
                      marginLeft: 10,
                      width: 38,
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  marginTop: 30,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <Button
                  title={'Submit'}
                  size={25}
                  color={colors.YellowOrange}
                  onPress={handleSibmitExp}
                />
                <Button
                  title={'Cancel'}
                  size={25}
                  color={colors.Red}
                  onPress={() => setModalVisible(false)}
                />
              </View>
            </View>
          </View>
        </Modal>
      )}

      <ModalPicture />

      <ModalInfo
        visible={modalInfoVisible}
        onDismiss={() => setModalInfoVisible(false)}
        message={infoMessage}
        title={infoTitle}
        icon={infoIcon}
        setVisible={setModalInfoVisible}
      />
      {showDate && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={'date'}
          is24Hour={true}
          display="default"
          onChange={onChangeDate}
        />
      )}
    </Container>
  );
};

export default ProductIdentity;

const styles = StyleSheet.create({
  imageProduct: {
    alignSelf: 'center',
    height: height / 4,
    maxWidth: width - 10,
    minWidth: width - 30,
    borderRadius: 4,
  },
  wrapper: {
    paddingHorizontal: 10,
    marginTop: 5,
  },
  banner: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  buttonReasonWrapper: {
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  productTitle: {
    fontSize: 18,
    color: colors.VerdunGreen,
  },
  bannerFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  skuText: {
    marginRight: 20,
  },
});
