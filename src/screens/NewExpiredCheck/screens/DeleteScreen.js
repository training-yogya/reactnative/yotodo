import * as React from 'react';
import {useState, useEffect, useMemo} from 'react';
import {View, Text, Picker, StyleSheet, Alert} from 'react-native';
import colors from '../../../utils/colors';
import {Container, AppButton, ModalInfo, InputText} from '../../../components';
import {useSelector} from 'react-redux';
import {screenNames} from '../../../utils/screens';
import client from '../../../utils/customAxios';

const ORANG = [
  {
    id: null,
  },
  {
    id: 1,
    name: 'TONO SUMANTO',
    label: 'ST. 11',
  },
  {
    id: 2,
    name: 'TINI SIMINTI',
    label: 'ST. 11',
  },
];

const DeleteExpired = ({navigation, route}) => {
  const article = useSelector(state => state.article);
  const userLogin = useSelector(state => state.auth.userLogin);
  const {loading, error, success, message} = article;
  const {navigate} = navigation;
  const [name, setName] = useState(null);
  const [st, setST] = useState(null);
  const [modalVisible, setModalVisible] = useState(null);
  const sku = route.params.item.sku;

  const handleSubmit = () => {
    Alert.alert('Lakukan verifikasi?', '', [
      {
        text: 'Ya,',
        onPress: async () => {
          const formData = new FormData();
          formData.append('name', name);
          formData.append('st', st);
          formData.append('sku', sku);
          formData.append('initial_store', userLogin.initial_store);
          formData.append('user_id', userLogin.id);
          await client.post('/delete_item_expired/', formData);
          setModalVisible(true);
        },
      },
      {
        text: 'Kembali',
      },
    ]);
  };

  const ModalNotif = useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        return navigate(screenNames.ExpiredCheck.List, {
          title: 'Deleted List',
        });
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Reason Berhasil di Update"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [modalVisible, navigate, success, error, message]);

  useEffect(() => {
    (() => {})();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: 'Delete Confirmation',
  });

  return (
    <Container style={styles.wrapper}>
      <Text
        style={{
          fontWeight: 'bold',
          textAlign: 'center',
          fontSize: 18,
          marginTop: 20,
        }}>
        {'Produk ini telah ditarik\ndari pajangan\ndan telah di Recheck oleh :'}
      </Text>
      <View style={styles.formReason}>
        <Text>Nama</Text>
        <InputText
          value={name}
          onChangeText={text => setName(text)}
          placeholder="Nama"
        />
        <View style={{marginVertical: 20}} />
        <Text>ST</Text>
        <InputText
          value={st}
          onChangeText={text => setST(text)}
          placeholder="ST"
        />
      </View>
      <View style={styles.buttonSubmit}>
        <AppButton
          loading={loading}
          disabled={!name || !st}
          color={!name || !st ? colors.Silver : colors.BlazeOrange}
          onPress={handleSubmit}
          title="Submit"
        />
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default DeleteExpired;

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 10,
    backgroundColor: colors.White,
  },
  formReason: {
    paddingVertical: 5,
    flex: 1,
  },
  buttonSubmit: {
    paddingVertical: 5,
  },
});
