import * as React from 'react';
import {useEffect} from 'react';
import {Container, InputText, ListItem} from '../../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {
  StyleSheet,
  RefreshControl,
  View,
  Text,
  TextInput,
  Picker,
  Platform,
  ActionSheetIOS,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {getExpiredList} from '../../../redux/actions/product-action';

import moment from 'moment';

const DAYS = [
  {
    id: 1,
    value: 10,
    name: '< 10',
  },
  {
    id: 2,
    value: 20,
    name: '< 20',
  },
  {
    id: 3,
    value: 30,
    name: '< 30',
  },
  {
    id: 4,
    value: 60,
    name: '< 60',
  },
  {
    id: 5,
    value: 90,
    name: '< 90',
  },
  {
    id: 6,
    value: 100,
    name: '> 90',
  },
  {
    id: 7,
    value: 0,
    name: 'All',
  },
];

const MinusStock = ({route, navigation}) => {
  const {title, type} = route.params;
  const product = useSelector(state => state.product);
  const dispatch = useDispatch();

  const handleNavigateBarcode = () => {
    return navigation.navigate(screenNames.Home.BarcodeScan, {
      title: 'Barcode Scan',
    });
  };

  navigation.setOptions({
    title,
    headerRight:
      title?.toLowerCase() === 'today input list' ? (
        <MaterialIcon
          name="add"
          size={36}
          color={colors.White}
          onPress={handleNavigateBarcode}
        />
      ) : null,
  });

  const isDeletedList = title?.toLowerCase() === 'deleted list';

  const handleOnPress = sku => () => {
    navigation.navigate(screenNames.Product.Identity, {
      productId: sku,
      parentTitle: 'Expired Check',
      barcode: true,
      viewOnly: true,
      hideReason: true,
    });
  };

  useEffect(() => {
    (() => {
      dispatch(getExpiredList(title?.toLowerCase(), 10));
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getArticleAsync = () => {};
  const keyExtractor = (item, i) => i.toString();

  const unit = ' pcs';

  const calculateDaysRemaining = date => {
    const target = moment(date);
    const today = moment();

    return target.diff(today, 'days');
  };

  const handleDeleteAction = item => {
    navigation.navigate(screenNames.ExpiredCheck.Delete, {
      item,
    });
  };

  const renderItem = ({item}) => {
    return (
      <View>
        <ListItem
          markText={''}
          onPress={handleOnPress(item.sku)}
          header={item.sku}
          title={item.skudesc}
          subTitle={`${item.divdesc && item.divdesc + ','} ${item.catdesc} ${
            item.tag ? item.tag.toUpperCase() : ''
          }`}
          deleteIcon={!isDeletedList}
          deleteAction={() => handleDeleteAction(item)}
          info={`${item.qty ? item.qty + unit : 0 + unit}`}
          footer={() => (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginVertical: 10,
              }}>
              <Text
                style={{
                  backgroundColor: colors.SeaGreen,
                  color: colors.White,
                  fontSize: 12,
                  lineHeight: 30,
                  borderRadius: 6,
                  fontWeight: 'bold',
                  paddingHorizontal: 10,
                }}>
                EXP : {moment(item.tanggal_expired).format('DD/MM/YYYY')}
              </Text>
              <Text
                style={{
                  backgroundColor: colors.YellowOrange,
                  color: colors.White,
                  fontSize: 12,
                  lineHeight: 30,
                  borderRadius: 6,
                  fontWeight: 'bold',
                  paddingHorizontal: 10,
                }}>
                {isDeletedList
                  ? `Validated by: ${item.name} - ST.${item.st}`
                  : calculateDaysRemaining(item.tanggal_expired) +
                    ' days remaining'}
              </Text>
            </View>
          )}
          infoDetail={`${moment(item.tanggal_expired).format('DD MMMM YYYY')}`}
          infoDetailStyle={{color: colors.Red, fontWeight: 'bold'}}
        />
      </View>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getArticleAsync} refreshing={product.loading} />
  );
  const [days, setDays] = React.useState(10);
  const onPickerChange = value => {
    setDays(value);

    dispatch(getExpiredList(title?.toLowerCase(), value));
  };

  return (
    <Container style={styles.wrapper}>
      <ListHeaderComponent
        isDeletedList={isDeletedList}
        onPickerChange={onPickerChange}
        days={days}
      />
      <FlatList
        style={styles.padding10}
        data={product.productExp}
        ListEmptyComponent={
          <EmptyComponent
            loading={product.loading}
            addNew={handleNavigateBarcode}
          />
        }
        refreshControl={refreshControl}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </Container>
  );
};

export default MinusStock;

const EmptyComponent = ({loading, name, addNew}) => (
  <View
    style={{
      height: 500,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>List {name} Kosong</Text>
      </View>
    )}

    <MaterialIcon
      name={'add-circle'}
      onPress={addNew}
      size={56}
      color={colors.BlazeOrange}
    />
  </View>
);

const ListHeaderComponent = ({onChangeText,isDeletedList, days = 1, onPickerChange}) => {
  const showActionSheet = () => {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: DAYS.map(item => item.name),
        destructiveButtonIndex: 7,
        cancelButtonIndex: 7,
      },
      buttonIndex => {
        console.log(buttonIndex)
        const data = DAYS[buttonIndex]
        onPickerChange(data.value)
      },
    );
  };
  return (
    <View
      style={{
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: colors.White,
        flexDirection: 'row',
      }}>
      <TextInput
        onChangeText={onChangeText}
        style={{
          borderRadius: 10,
          borderWidth: 0.7,
          height: 56,
          borderColor: colors.Black + '66',
          paddingHorizontal: 20,
          flex: 1
        }}
        placeholder={'Search by Brand/Till Code/Barcode'}
      />
      {!isDeletedList && (
        <View style={{marginLeft: 10}}>
          <Text>Days Remaining</Text>
          {Platform.OS === 'android' ? (
            <Picker
              style={{
                width: 100,
                height: 40,
                borderRadius: 10,
                borderWidth: 0.7,
                borderColor: colors.Black + '66',
                paddingHorizontal: 20,
              }}
              itemStyle={{
                borderBottomWidth: 1,
                borderBottomColor: 'black',
                width: 100,
              }}
              selectedValue={days}
              onValueChange={onPickerChange}>
              {DAYS.map((item, i) => (
                <Picker.Item
                  key={i}
                  label={item.name ? item.name : 'Choose Person'}
                  value={item.value}
                />
              ))}
            </Picker>
          ) : (
            <TouchableWithoutFeedback onPress={showActionSheet}>
              <View
                style={{
                  borderRadius: 10,
                  borderWidth: 0.7,
                  height: 40,
                  width: 56,
                  borderColor: colors.Black + '66',
                  paddingHorizontal: 10,
                }}>
                <Text style={{lineHeight: 40}}>{DAYS.find(item => item.value === days)?.name}</Text>
              </View>
            </TouchableWithoutFeedback>
          )}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.Light, marginBottom: 10},
  padding10: {
    padding: 10,
  },
});
