import {connect} from 'react-redux';
import PlanogramRequest from './screen';
import {} from './../../redux/actions/planogram-action';

const mapDispatchToProps = dispatch => ({});

export default connect(
  null,
  mapDispatchToProps,
)(PlanogramRequest);
