import React from 'react';
import {useState, useMemo} from 'react';
import {Container, Card, AppButton, ModalInfo, Loading} from '../../components';
import {Text, Picker, View, StyleSheet, Dimensions} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import {screenNames} from '../../utils/screens';
import {requestPlanogramRequest} from '../../redux/actions/planogram-action';

const PlanogramRequest = ({navigation}) => {
  const navigate = navigation.navigate;
  const dispatch = useDispatch();
  const planogram = useSelector(state => state.planogram);
  const {error, success, loading, message, categoryList} = planogram;
  const [checkedCategories, setCheckedCategories] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [requestID, setRequestID] = useState(null);

  const handleSetPlanogramRequest = category => {
    const checked = [].concat(checkedCategories);
    const index = checked.indexOf(category);

    if (index > -1) {
      checked.splice(index, 1);
    } else {
      checked.push(category);
    }

    setCheckedCategories(checked);
  };

  const handleRequestTracking = () => {
    navigate(screenNames.Planogram.RequestTrackingList);
  };

  const handleSubmit = () => {
    const planogramRequestData = checkedCategories.map(item => ({
      categories: item,
    }));
    dispatch(requestPlanogramRequest(null, planogramRequestData)).then(
      response => {
        // console.log(response);
        setRequestID(response.data.status.message);
        setModalVisible(true);
      },
    );
  };

  const ModalNotif = useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        // setTimeout(() => {
        navigate(screenNames.Planogram.RequestTrackingList);
        setModalVisible(false);
        // }, 129);
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message={`${requestID}`}
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [success, error, navigate, requestID, modalVisible, message]);

  navigation.setOptions({
    title: 'Planogram Request',
  });
  return (
    <Container style={globalStyles.container}>
      {loading && <Loading />}
      <Text style={globalStyles.title}>Choose Category</Text>
      <FlatList
        data={categoryList || []}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <Card
            title={item.categories}
            onPress={() => handleSetPlanogramRequest(item.categories)}
            color={
              checkedCategories.includes(item.categories)
                ? colors.PeachOrange
                : colors.Blue
            }
            style={{margin: 5, minHeight: 80}}
            titleStyle={{fontSize: 12}}
          />
        )}
        numColumns={3}
      />
      <View style={styles.buttonContainer}>
        <View style={styles.space}>
          <AppButton
            style={styles.button}
            color={colors.PeachOrange}
            title="Submit Request"
            onPress={handleSubmit}
          />
        </View>
        <View>
          <AppButton
            onPress={handleRequestTracking}
            style={styles.button}
            color={colors.Blue}
            title="Request Tracking"
            titleStyle={globalStyles.textCenter}
          />
        </View>
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default PlanogramRequest;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  space: {marginEnd: 20},
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
    borderTopWidth: 0.5,
  },
  button: {height: 50, width: width / 2 - 50},
});
