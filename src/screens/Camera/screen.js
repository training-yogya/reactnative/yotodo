import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../utils/colors';
import {TouchableOpacity} from 'react-native-gesture-handler';

import Geolocation from '@react-native-community/geolocation';

class Camera extends PureComponent {
  state = {
    coords: {
      latitude: null,
      longitude: null,
    },
    image: null,
    loading: false,
    hasPreview: false,
  };

  constructor(props) {
    super(props);
    this.savePicture = this.props.route.params.savePicture;
  }

  static navigationOptions = {
    title: 'Camera',
  };

  componentDidMount = () => {
    Geolocation.getCurrentPosition(
      result => {
        this.setState({
          coords: {
            latitude: result.coords.latitude,
            longitude: result.coords.longitude,
          },
        });
      },
      error => {},
    );
  };

  componentWillUnmount = () => {};

  takePicture = () => {
    this.setState(
      {
        loading: true,
      },
      async () => {
        if (this.camera) {
          const options = {quality: 0.2, base64: true};
          const data = await this.camera.takePictureAsync(options);
          this.setState({
            image: data,
            hasPreview: true,
            loading: false,
          });
        }
      },
    );
  };

  removeCurrentPicture = () => {
    this.setState({
      image: null,
      hasPreview: false,
    });
  };

  actionNext = () => {
    const {coords, image} = this.state;
    const uri =
      Platform.OS === 'android' ? image.uri : image.uri.replace('file://', '');
    const imageData = {
      name: 'image' + Date.now() + '.jpg',
      type: 'image/jpg',
      uri,
      coords,
      dateInt: Date.now(),
    };
    this.savePicture(imageData);
  };

  render() {
    const {hasPreview, image} = this.state;
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          captureAudio={false}>
          <View style={styles.imagePreviewWrapper}>
            {this.state.loading ? (
              <ActivityIndicator size={42} />
            ) : (
              hasPreview && (
                <Image
                  resizeMode="contain"
                  source={{uri: image.uri}}
                  style={styles.imagePreview}
                />
              )
            )}
          </View>
          <View
            style={[
              styles.bottomButtonWrapper,
              {marginBottom: 10},
              hasPreview
                ? {backgroundColor: colors.Black}
                : {justifyContent: 'center'},
            ]}>
            <TouchableOpacity onPress={this.removeCurrentPicture}>
              <View
                style={[styles.sideButton, !hasPreview && {display: 'none'}]}>
                <Icon
                  name="close"
                  color={hasPreview ? colors.Red : colors.Transparent}
                  size={28}
                />
                <Text>Delete</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={hasPreview ? this.actionNext : this.takePicture}
              style={[
                styles.middleButton,
                // hasPreview && {backgroundColor: colors.Red},
              ]}>
              {hasPreview ? (
                <Icon name="check" color={colors.EarlsGreen} size={42} />
              ) : (
                <Icon name="camera" color={colors.BlazeOrange} size={42} />
              )}
            </TouchableOpacity>
            <View
              style={[
                styles.sideButton,
                {backgroundColor: colors.Transparent},
                !hasPreview && {display: 'none'},
              ]}
            />
          </View>
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  imagePreviewWrapper: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePreview: {
    backgroundColor: colors.Black,
    width: '100%',
    height: '100%',
  },
  bottomButtonWrapper: {
    width: '100%',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'rgba(0,0,0,0.7)',
    flexDirection: 'row',
  },
  middleButton: {
    backgroundColor: colors.White,
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 60,
    borderRadius: 75 / 2,
  },
  sideButton: {
    flexDirection: 'row',
    backgroundColor: colors.White,
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    width: 80,
    borderRadius: 36 / 2,
  },
});

Camera.navigationOptions = () => ({
  title: 'Camera',
});
export default Camera;
