import {connect} from 'react-redux';

import Camera from './screen';

const mapDispatchToProps = dispatch => ({});

export default connect(
  null,
  mapDispatchToProps,
)(Camera);
