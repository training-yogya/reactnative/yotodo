import * as React from 'react';
import {View, Text, Dimensions} from 'react-native';
import colors from '../../../utils/colors';

const {width} = Dimensions.get('screen');
const ListPill = ({title, value}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
      }}>
      <View
        style={{
          width: width - 205,
          height: 42,
          backgroundColor: colors.PeachOrange,
          borderRadius: 28,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>{title}</Text>
      </View>
      <View
        style={{
          width: 140,
          height: 44,
          backgroundColor: colors.Turmeric,
          borderRadius: 28,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 4,
          marginLeft: -25,
          borderColor: colors.White,
        }}>
        <Text>{value}</Text>
      </View>
    </View>
  );
};

export default ListPill;
