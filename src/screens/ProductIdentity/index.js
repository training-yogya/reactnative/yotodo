import {connect} from 'react-redux';
import ProductIdentity from './screen';
import {getProductIdentity} from './../../redux/actions/product-action';

const mapDispatchToProps = dispatch => ({
  getProductIdentity: (sku, cabang, parentTitle) =>
    dispatch(getProductIdentity(sku, cabang, parentTitle)),
});

export default connect(
  null,
  mapDispatchToProps,
)(ProductIdentity);
