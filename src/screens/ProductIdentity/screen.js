import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  Text,
  StyleSheet,
  ActivityIndicator,
  Modal,
  TextInput,
  Button,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  Container,
  AppButton,
  Loading,
  ModalInfo,
  Barchart,
} from '../../components';
import ListPill from './components/ListPill';
import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import {screenNames} from '../../utils/screens';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import ImageViewer from 'react-native-image-zoom-viewer';
import {useSelector, useDispatch} from 'react-redux';
import moment from 'moment';
import {
  addExpired,
  getExpiredList,
  addSOPartial,
  PSOArticelList,
} from '../../redux/actions/product-action';
import {
  validateProduct,
  inputQty,
  getOmsListDetail,
} from '../../redux/actions/yo-picker-actions';
import DateTimePicker from '@react-native-community/datetimepicker';
const {width, height} = Dimensions.get('screen');

const ProductIdentity = ({navigation, route, getProductIdentity}) => {
  const {
    title,
    productId,
    hideReason,
    parentTitle,
    barcode,
    item,
  } = route.params;

  const cabang = useSelector(state => state.auth.userLogin.initial_store);
  const product = useSelector(state => state.product);
  const yoPicker = useSelector(state => state.yoPicker);

  const dispatch = useDispatch();

  const [showDefaultImage, setShowDefaultImage] = useState(true);
  const [isImageError, setIsImageError] = useState(false);
  const [modalPictureVisible, setModalPictureVisible] = useState(false);
  const [modalInfoVisible, setModalInfoVisible] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [expDate, setExpDate] = useState('');
  const [date, setDate] = useState(new Date());

  const [showDate, setShowDate] = useState(false);
  const [infoMessage, setInfoMessage] = useState('');
  const [infoTitle, setInfoTitle] = useState('');
  const [infoIcon, setInfoIcon] = useState('');
  const [formType, setFormType] = useState('');
  const [matchTillCode, setMacthTillCode] = useState(false);
  const [qty, setQty] = useState('');

  const webView = React.useRef();

  const {loading, identity} = product;

  const chartLabel =
    identity?.chart?.map(a =>
      moment(a.TANGGAL, 'DD/MM/YYYY').format('DD MMM'),
    ) ?? [];
  const chartData =
    identity?.chart?.map(a => (a.QTY_SALES ? a.QTY_SALES : 0)) ?? [];

  const isPickerScan = parentTitle.toLowerCase() === 'yopicker';

  const productImage = showDefaultImage
    ? 'https://cdn-2.tstatic.net/default-2.jpg'
    : isImageError
    ? 'https://cdn-2.tstatic.net/default-2.jpg'
    : identity?.image ||
      item?.image ||
      'https://cdn-2.tstatic.net/default-2.jpg';

  const handleOpenDate = () => {
    setShowDate(true);
  };

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShowDate(false);
    setDate(selectedDate);
    setExpDate(moment(currentDate).format('DD/MM/YYYY'));
  };

  const handleSibmitExp = () => {
    const data = {
      sku: identity.sku,
      skudesc: identity.skudesc,
      tanggal_expored: moment(expDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      note: identity.note,
      divdesc: identity.division,
      catdesc: identity.category,
      tag: identity.tag,
      qty: qty,
    };
    dispatch(addExpired(data)).then(() => {
      // setTimeout(() => {
      setModalVisible(false);
      // });
      navigation.navigate(screenNames.ExpiredCheck.List, {
        screen: screenNames.ExpiredCheck.List,
        params: {title: 'Expired List'},
      });
      dispatch(getExpiredList());
    });
  };

  const handlePartialSOSubmit = () => {
    const data = {
      sku: identity.sku,
      skudesc: identity.skudesc,
      tanggal_expored: moment(expDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      note: identity.note,
      till_code: identity.till_code,
      divdesc: identity.division,
      catdesc: identity.category,
      tag: identity.tag,
      qty: qty,
    };
    dispatch(addSOPartial(data))
      .then(result => {
        // setTimeout(() => {
        setModalVisible(false);
        setInfoIcon('success');
        setInfoTitle('Success');
        setInfoMessage(result.status.message);
        setModalInfoVisible(true);
        // });

        setTimeout(() => {
          navigation.goBack();
        }, 100);
        dispatch(PSOArticelList());
      })
      .catch(error => {
        setModalVisible(false);
        setInfoIcon('failed');
        setInfoTitle('Failed');
        setInfoMessage(error.status.message);
        setModalInfoVisible(true);
        setTimeout(() => {
          navigation.goBack();
        }, 100);
      });
  };

  const handleNavigateBarcode = ({hidebarcode = false} = {}) => {
    setMacthTillCode(false);
    navigation.navigate(screenNames.BarcodeScan.Scan, {
      title: 'YoPicker',
      till_code: item.sku,
      hidebarcode: hidebarcode,
      action: handleValidate,
      method: 'matchTillCode',
    });
  };

  const handleValidate = barcodeCode => {
    dispatch(validateProduct(item, barcodeCode))
      .then(() => {
        setInfoIcon('success');
        setInfoTitle('Success');
        setInfoMessage('Valid');
        setModalInfoVisible(true);
        // dispatch(getOmsListDetail(item.order_id));
        handleNavigateInputQty();
      })
      .catch(() => {
        setInfoIcon('error');
        setInfoTitle('Error');
        setInfoMessage('Not Valid');
        setModalInfoVisible(true);
      });
  };

  const handleSubmitInputQty = ({st_name, staff_name, quantity, selisih}) => {
    navigation.goBack();
    dispatch(
      inputQty({
        order_id: item.order_id,
        sku: item.sku,
        id: item.id,
        st_name,
        staff_name,
        quantity_picking: quantity,
        selisih,
      }),
    )
      .then(() => {
        setInfoIcon('success');
        setInfoTitle('Success');
        setInfoMessage('Success Input Picking');
        setModalInfoVisible(true);
        dispatch(getOmsListDetail(item.order_id));
      })
      .catch(() => {
        setInfoIcon('error');
        setInfoTitle('Error');
        setInfoMessage('Error Input Picking');
        setModalInfoVisible(true);
      })
      .finally(() => {
        navigation.navigate(screenNames.YoPicker.OmsListDetail);
      });
  };

  const handleFinishYopicker = () => {
    return;
  };

  const handleNavigateInputQty = (empty = false) => {
    navigation.navigate(screenNames.YoPicker.InputQty, {
      handleSubmit: handleSubmitInputQty,
      quantity: item.quantity,
      empty: empty,
    });
  };

  const handlePartialSO = () => {
    setFormType('partialSO');
    setModalVisible(true);
  };
  const handleExpiredCheck = () => {
    setFormType('Expired');
    setModalVisible(true);
  };

  useEffect(() => {
    getProductIdentity(productId, cabang, parentTitle);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (webView.current) {
      if (chartData && chartLabel) {
        webView.current.postMessage(JSON.stringify({chartData, chartLabel}));
      }
    }
  }, [chartData, chartLabel]);

  navigation.setOptions({
    title: title || 'Product Identity',
  });

  const ImageProduct = React.useMemo(() => {
    return (
      <TouchableOpacity
        onPress={() => setModalPictureVisible(true)}
        style={{
          position: 'relative',
        }}>
        {!loading ? (
          <Image
            source={{
              uri: productImage,
            }}
            onLoadEnd={() => setShowDefaultImage(false)}
            // onLoadStart={() => !showDefaultImage && setShowDefaultImage(true)}
            onError={() => {
              setIsImageError(true);
            }}
            resizeMode="cover"
            style={styles.imageProduct}
          />
        ) : (
          <View style={styles.imageProduct} />
        )}
        {showDefaultImage && (
          <ActivityIndicator
            size={46}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
          />
        )}
      </TouchableOpacity>
    );
  }, [loading, productImage, showDefaultImage]);

  const ModalPicture = () => {
    return (
      <Modal
        onRequestClose={() => setModalPictureVisible(false)}
        visible={modalPictureVisible}
        transparent={true}
        animated={true}
        animationType={'slide'}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.7)',
            position: 'relative',
            height: '100%',
          }}>
          <TouchableOpacity
            onPress={() => setModalPictureVisible(false)}
            style={{
              justifyContent: 'center',
              height: 56,
              marginLeft: 20,
            }}>
            <MaterialIcon
              onPress={() => setModalPictureVisible(false)}
              name="close"
              color={colors.White}
              size={30}
            />
          </TouchableOpacity>
          <ImageViewer imageUrls={[{url: productImage}]} />
        </View>
      </Modal>
    );
  };

  return (
    <Container style={styles.wrapper}>
      <>{ImageProduct}</>
      <View style={styles.banner}>
        {item !== undefined ? (
          <View>
            <Text style={styles.productTitle}>
              {item?.article_description || '-'}
            </Text>
            <View style={styles.bannerFooter}>
              <Text style={[globalStyles.textBold, styles.skuText]}>
                {item?.sku || '-'}
              </Text>
              <View>
                <Text style={globalStyles.textBold}>
                  {item.division_description}
                </Text>
                <Text style={globalStyles.textBold}>
                  {item.category_description}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <View>
            <Text style={styles.productTitle}>{identity?.skudesc || '-'}</Text>
            <View style={styles.bannerFooter}>
              <Text style={[globalStyles.textBold, styles.skuText]}>
                {identity?.sku || '-'}
              </Text>
              <View>
                <Text style={globalStyles.textBold}>{identity.division}</Text>
                <Text style={globalStyles.textBold}>{identity.category}</Text>
              </View>
            </View>
          </View>
        )}
      </View>
      {item !== undefined ? (
        <ScrollView>
          <ListPill title="Selling Price" value={item?.price} />
          <ListPill title="Last Stock" value={item?.stock_gold} />
          <ListPill title="Till code" value={item?.sku} />
        </ScrollView>
      ) : (
        <ScrollView>
          <ListPill title="Tag" value={identity?.tag} />
          {isPickerScan && (
            <ListPill
              title="Item Location"
              value={`${identity?.productLocation || '-'}`}
            />
          )}
          <ListPill title="Last Stock" value={identity?.last_stock} />
          {!isPickerScan && (
            <ListPill
              title="Avrg. Cost Price"
              value={`Rp. ${identity?.avg_price || '-'},-`}
            />
          )}
          <ListPill
            title="Last Rec. Date"
            value={identity?.last_rec_date || '-'}
          />
          <ListPill
            title="Last Rec. Qty "
            value={identity?.last_rec_qty || '-'}
          />
          <ListPill
            title="Qty on Order"
            value={identity?.qty_in_order || '-'}
          />
          <ListPill
            title="Selling Price"
            value={`Rp. ${identity?.selling_price || '-'},-`}
          />
          <ListPill
            title="Avrg. Sales per Day"
            value={
              identity?.avg_sales_day
                ? `${identity?.avg_sales_day || '-'}`
                : '-'
            }
          />
          <ListPill title="Till Code" value={identity?.till_code || '-'} />
          <ListPill title="SV Code" value={identity?.sv_code || '-'} />
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={[globalStyles.textBold, {color: colors.SeaGreen}]}>
              Last 7 Days Sales Trend in Quantity
            </Text>
            {!loading && (
              <Barchart chartData={chartData} chartLabel={chartLabel} />
            )}
            {/* <View style={{width: '100%', height: 300}}>
              <WebView
                ref={webView}
                originWhitelist={['*']}
                allowFileAccess={true}
                source={{html: radialChartHtml()}}
                style={{height: 300, width: '100%'}}
              />
            </View> */}
          </View>
        </ScrollView>
      )}

      <View style={styles.buttonReasonWrapper}>
        {!hideReason && (
          <AppButton
            onPress={() =>
              navigation.navigate(screenNames.Article.ReasonAdjustment)
            }
            title="Reasons"
            color={colors.BlazeOrange}
          />
        )}
      </View>
      {barcode && !isPickerScan && (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
          <AppButton
            onPress={handlePartialSO}
            title="Partial SO"
            color={colors.BlazeOrange}
          />
          <AppButton
            onPress={handleExpiredCheck}
            title="Expired Registration"
            color={colors.DodgerBlue}
          />
        </View>
      )}
      <Modal
        visible={modalVisible}
        transparent={true}
        animated={true}
        onRequestClose={() => setModalVisible(false)}
        animationType={'slide'}>
        <View
          style={{
            flex: 1,
            backgroundColor: `${colors.Black}aa`,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '90%',
              paddingVertical: 10,
              borderRadius: 10,
              backgroundColor: colors.White,
            }}>
            <View style={{alignItems: 'flex-end', paddingRight: 10}}>
              <MaterialIcon
                onPress={() => setModalVisible(false)}
                name={'close'}
                size={15}
                color={colors.White}
                style={{
                  padding: 3,
                  backgroundColor: colors.Red,
                  borderRadius: 20,
                }}
              />
            </View>
            {formType === 'partialSO' ? (
              <View style={{padding: 10}}>
                <View>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontWeight: 'bold',
                      fontSize: 16,
                    }}>
                    Input Real Stock in PCS
                  </Text>
                </View>
                <View style={{height: 10}} />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={{
                      backgroundColor: colors.Black + '11',
                      borderRadius: 10,
                      paddingHorizontal: 10,
                      width: 90,
                      height: 56,
                      marginHorizontal: 20,
                      textAlign: 'right',
                    }}
                    autoFocus={true}
                    value={qty}
                    keyboardType="numeric"
                    onChangeText={text => setQty(text)}
                    placeholder={'QTY'}
                  />
                </View>
              </View>
            ) : (
              <View style={{padding: 10}}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={{
                      backgroundColor: colors.Black + '33',
                      borderRadius: 10,
                      height: 56,
                      paddingHorizontal: 10,
                      width: '60%',
                    }}
                    editable={false}
                    value={expDate}
                    onChangeText={text => setExpDate(text)}
                    placeholder={'Exp Date'}
                  />
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: colors.PeachOrange + '55',
                      borderRadius: 20,
                      marginLeft: 10,
                    }}>
                    <MaterialIcon
                      onPress={handleOpenDate}
                      color={colors.PeachOrange}
                      name="date-range"
                      size={32}
                    />
                  </View>
                </View>
                <View style={{height: 10}} />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={{
                      backgroundColor: colors.Black + '11',
                      borderRadius: 10,
                      paddingHorizontal: 10,
                      height: 56,
                      width: 90,
                      marginHorizontal: 20,
                      textAlign: 'right',
                    }}
                    autoFocus={true}
                    value={qty}
                    keyboardType="numeric"
                    onChangeText={text => setQty(text)}
                    placeholder={'QTY'}
                  />
                </View>
              </View>
            )}
            <View style={{paddingHorizontal: 10}}>
              <Button
                title={'Submit'}
                size={25}
                color={colors.BlazeOrange}
                onPress={
                  formType === 'partialSO'
                    ? handlePartialSOSubmit
                    : handleSibmitExp
                }
              />
            </View>
          </View>
        </View>
      </Modal>

      {isPickerScan && (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <AppButton
            style={{width: 100}}
            onPress={handleNavigateBarcode}
            title={'Input QTY'}
            color={colors.BlueBell}
          />

          {/* <AppButton
            style={{width: 100}}
            onPress={() => handleNavigateBarcode({hidebarcode: true})}
            title={'Validate PDT'}
            color={colors.RegentStBlue}
          /> */}
          <AppButton
            style={{width: 100}}
            onPress={() => handleNavigateInputQty(true)}
            title={'Empty Stock'}
            color={colors.YellowOrange}
          />
        </View>
      )}

      <ModalPicture />

      <ModalInfo
        visible={modalInfoVisible}
        onDismiss={() => setModalInfoVisible(false)}
        message={infoMessage}
        title={infoTitle}
        icon={infoIcon}
        setVisible={setModalInfoVisible}
      />

      {(loading || yoPicker.loading) && <Loading />}
      {showDate && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={'date'}
          is24Hour={true}
          display="default"
          onChange={onChangeDate}
        />
      )}
    </Container>
  );
};

export default ProductIdentity;

const styles = StyleSheet.create({
  imageProduct: {
    alignSelf: 'center',
    height: height / 4,
    maxWidth: width - 10,
    minWidth: width - 30,
    borderRadius: 4,
  },
  wrapper: {
    paddingHorizontal: 10,
    marginTop: 5,
  },
  banner: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  buttonReasonWrapper: {paddingVertical: 5},
  productTitle: {
    fontSize: 18,
    color: colors.VerdunGreen,
  },
  bannerFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  skuText: {
    marginRight: 20,
  },
});
