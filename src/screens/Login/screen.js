import * as React from 'react';
import {useState, useEffect, useMemo} from 'react';
import {
  View,
  StyleSheet,
  Keyboard,
  Alert,
  Text,
  Dimensions,
  ScrollView,
  PermissionsAndroid,
  Modal,
  TouchableOpacity,
  Platform,
} from 'react-native';

import {
  check,
  PERMISSIONS,
  RESULTS,
  openSettings,
  checkMultiple,
} from 'react-native-permissions';

import ImageViewer from 'react-native-image-zoom-viewer';

import SnackBar from 'react-native-snackbar';
import {useSelector} from 'react-redux';

import {Container, Icon, InputText, AppButton} from '../../components';

import globalStyles from '../../styles/global';
import {iconsNames} from '../../utils/icons';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import config from '../../config';

const {width, height} = Dimensions.get('window');

const images = [
  {
    url: '',
    props: {
      source: require('../../assets/icons/covid_banner.jpeg'),
      // headers: ...
    },
  },
];

const Login = ({navigation, doLogin}) => {
  navigation.setOptions({
    headerShown: false,
  });

  const navigate = navigation.navigate;

  const auth = useSelector(state => state.auth);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showFooter, setShowFooter] = useState(true);
  const [modalVisible, setModalVisible] = useState(true);

  const changeUsername = text => setUsername(text);
  const changePassword = text => setPassword(text);

  const handleButtonLogin = () => {
    doLogin({username, password});
  };

  const handleKeyboadDidShow = () => setShowFooter(false);

  const handleKeyboadDidHide = () => setShowFooter(true);

  const requestPermission = () => {
    if (Platform.OS === 'android') {
      checkMultiple([
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        PERMISSIONS.ANDROID.CAMERA,
        PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
        PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      ]).then(isGranted => {
        const camera = isGranted['android.permission.CAMERA'];
        const location = isGranted['android.permission.ACCESS_FINE_LOCATION'];
        const storage = isGranted['android.permission.WRITE_EXTERNAL_STORAGE'];

        const grantStatus = [camera, location, storage];

        const neverAskAgainCount = grantStatus.filter(
          item => item === 'never_ask_again',
        ).length;

        const cameraStatus = check(PERMISSIONS.ANDROID.CAMERA);
        const locationStatus = check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
        const storageStatus = check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);

        Promise.all([cameraStatus, locationStatus, storageStatus]).then(
          result => {
            if (
              result.includes(
                RESULTS.DENIED || result.includes(RESULTS.BLOCKED),
              )
            ) {
              if (neverAskAgainCount !== 3) {
                requestPermission();
              } else {
                SnackBar.show({
                  text: 'Please grant permission manualy',
                  duration: SnackBar.LENGTH_INDEFINITE,
                  action: {
                    text: 'Open Setting',
                    textColor: colors.BlazeOrange,
                    onPress: () =>
                      openSettings().catch(error => console.log(error)),
                  },
                });
              }
            }
          },
        );
      });
    }
  };

  useEffect(() => {
    requestPermission();
    Keyboard.addListener('keyboardDidShow', handleKeyboadDidShow);
    Keyboard.addListener('keyboardDidHide', handleKeyboadDidHide);

    return () => {
      Keyboard.removeListener('keyboardDidShow', handleKeyboadDidShow);
      Keyboard.removeListener('keyboardDidHide', handleKeyboadDidHide);
    };
  });

  const AlertLogin = useMemo(() => {
    if (auth.error) {
      return Alert.alert('Login Failed', auth.message);
    }

    if (auth.logined) {
      return navigate(screenNames.ChooseStore);
    }

    return <View />;
  }, [auth.error, auth.message, auth.logined, navigate]);

  return (
    <Container style={[styles.wrapper]}>
      <>{AlertLogin}</>
      <ScrollView>
        <View
          style={[
            globalStyles.bgWransparent,
            {height: 60, flexDirection: 'row', justifyContent: 'space-between'},
            {padding: 10},
            {marginBottom: 15},
            {elevation: 1},
          ]}>
          <View style={{flex: 1, alignItems: 'flex-start'}}>
            <Icon name={iconsNames.yogyabanner} />
          </View>
        </View>
        <View style={[globalStyles.container, {marginRight: 15}]}>
          <Text
            style={{
              textAlign: 'right',
              fontSize: 26,
              fontWeight: '700',
              color: colors.Black + '88',
            }}>
            {'Selamat Datang\nKembali'}
          </Text>
        </View>
        <Container style={[globalStyles.center, styles.container]}>
          <View style={styles.formWrapper}>
            <InputText
              value={username}
              onChangeText={changeUsername}
              wrapperStyle={styles.usernameField}
              icon="account"
              iconSize={25}
              placeholder="Please enter username"
            />
            <InputText
              value={password}
              onChangeText={changePassword}
              wrapperStyle={styles.passwordField}
              icon="lock"
              iconSize={25}
              placeholder="Please enter password"
              type="password"
            />
            <AppButton
              title="Login"
              style={styles.buttonLogin}
              loading={auth.loading}
              color={colors.BurningOrange}
              onPress={handleButtonLogin}
            />
          </View>
        </Container>
        {showFooter ? (
          <Container
            style={[globalStyles.centerEnd, globalStyles.bgWransparent]}>
            <View
              style={[
                globalStyles.bgWransparent,
                {overflow: 'visible'},
                {flexDirection: 'column-reverse'},
                {marginTop: 30, position: 'relative'},
              ]}>
              <Icon
                name={iconsNames.yogyaDulu}
                style={{
                  width: width - 10,
                  height: height / 3,
                  resizeMode: 'contain',
                }}
              />
              <Icon
                name={iconsNames.logo}
                style={{position: 'absolute', top: -26, left: 10, zIndex: 4}}
              />
            </View>
            <Text>v {config.version}</Text>
          </Container>
        ) : (
          <View style={{flex: 0.5}} />
        )}
      </ScrollView>
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={() => setModalVisible(false)}>
        <ImageViewer
          renderIndicator={() => <Text />}
          enableSwipeDown={true}
          onSwipeDown={() => setModalVisible(false)}
          renderFooter={({currentIndex}) => {
            return (
              <View
                style={{
                  width: width,
                  marginTop: -30,
                  height: 100,
                  backgroundColor: 'rgba(0,0,0,0.4)',
                }}>
                <TouchableOpacity
                  onPress={() => setModalVisible(false)}
                  style={{
                    height: 70,
                    width: '100%',
                    justifyContent: 'center',
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 24,
                      textAlign: 'right',
                      textAlignVertical: 'center',
                    }}>
                    Close
                  </Text>
                </TouchableOpacity>
              </View>
            );
          }}
          backgroundColor="rgba(0,0,0,0.4)"
          imageUrls={images}
        />
      </Modal>
    </Container>
  );
};
export default Login;

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 10,
    justifyContent: 'flex-start',
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  buttonLogin: {
    marginTop: 0,
    borderRadius: 20,
    height: 40,
  },
  icon: {
    flex: 6,
    justifyContent: 'center',
    paddingTop: 30,
    borderRadius: 20,
  },
  formWrapper: {
    flex: 5,
    marginTop: 30,
    minHeight: 10,
    justifyContent: 'flex-start',
  },
  usernameField: {
    marginBottom: 20,
    borderRadius: 20,
    height: 50,
  },
  passwordField: {
    marginBottom: 20,
    borderRadius: 20,
    height: 50,
  },
});
