import {connect} from 'react-redux';
import {doLogin} from './../../redux/actions/auth-action';

import Login from './screen';

const mapDispatchToProps = dispatch => ({
  doLogin: data => dispatch(doLogin(data)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Login);
