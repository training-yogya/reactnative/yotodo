import {connect} from 'react-redux';
import Planogram from './screen';
import {
  getPlanogramMenu,
  menuDevision,
} from './../../redux/actions/planogram-action';

const mapDispatchToProps = dispatch => ({
  getPlanogramMenu: () => dispatch(getPlanogramMenu()),
  getMenuDevision: directorate => dispatch(menuDevision(directorate)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Planogram);
