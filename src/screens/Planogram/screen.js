import * as React from 'react';
import {useEffect} from 'react';
import {Container, MenuItem, ButtonInfo} from '../../components';
import {FlatList, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {iconFinder} from '../../utils/icons';

const Plagoram = ({getPlanogramMenu, getMenuDevision, navigation, route}) => {
  const {title} = route.params;

  const action = () => {
    navigation.navigate(screenNames.Planogram.Info, {
      hideRequest: true,
    });
  };
  navigation.setOptions({
    title,
    headerRight: <ButtonInfo action={action} />,
  });
  const planogram = useSelector(state => state.planogram);

  useEffect(() => {
    (() => {
      getMenuDevision('supermarket');
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleMenuPress = (menuTitle = '') => () => {
    const titleSubStr = menuTitle.substr(0, 6);
    if (titleSubStr === 'LAYOUT') {
      navigation.navigate(screenNames.Planogram.Details, {
        title: menuTitle,
        hideImplement: true,
      });
    } else {
      navigation.navigate(screenNames.Planogram.List, {
        title: menuTitle,
      });
    }
  };

  const keyExtractor = (item, i) => i.toString();
  const renderItem = ({item}) => {
    const [icon, iconType] = iconFinder(item.division || '');
    return (
      <MenuItem
        title={item.division || item.name}
        onPress={handleMenuPress(item.division || item.name)}
        icon={item.icon || icon}
        iconType={item.type || iconType}
        backgroundColor={'#FFFFFF'}
        iconColor={'#FF9900'}
        dark={true}
      />
    );
  };

  return (
    <Container style={styles.wrapper} backgroundColor={colors.Negroni}>
      <FlatList
        data={planogram.devisionList}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        numColumns={2}
      />
    </Container>
  );
};

export default Plagoram;

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 5,
  },
});
