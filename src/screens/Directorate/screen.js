import * as React from 'react';
import {useEffect} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {Container, Loading, MenuItem} from '../../components';
import {useSelector} from 'react-redux';
import {screenNames} from '../../utils/screens';
import colors from '../../utils/colors';

const filter = [
  {
    code: 'A',
    title: 'LADIES',
  },
  {
    code: 'B',
    title: 'MENS',
  },
  {
    code: 'C',
    title: 'BABY AND KIDS',
  },
  {
    code: 'D',
    title: 'SHOES AND BAGS',
  },
  {
    code: 'E',
    title: 'BEAUTY AND ACCESSORIES',
  },
];

const SuperMarket = ({route, navigation, getArticleMenu, getFashionFilter}) => {
  const {title} = route.params;
  const article = useSelector(state => state.article);
  navigation.setOptions({
    title: title,
  });

  useEffect(() => {
    getArticleMenu(title);
    if (title === 'FASHION') {
      getFashionFilter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleOnPress = params => () => {
    if (params.title.toLowerCase() === 'must so') {
      navigation.navigate(screenNames.Article.Category, params);
    } else if (params.upload) {
      navigation.navigate(screenNames.HomeBakery.Upload, {
        screen: screenNames.HomeBakery.Upload,
        params: {title: params.title, menu: params.menu},
      });
    } else if (title === 'FASHION') {
      navigation.navigate(screenNames.Article.FashionFilter, {
        ...params,
        filterList: article.fashionFilter,
      });
    } else {
      navigation.navigate(screenNames.Article.Article, params);
    }
  };

  if (article.loading) {
    return <Loading />;
  }

  return (
    <Container>
      <FlatList
        style={styles.menuWrapper}
        data={article.items}
        renderItem={({item}) => (
          <MenuItem
            disabled={item.disabled}
            title={item.name}
            icon={item.icon}
            onPress={handleOnPress({
              title: item.name,
              parentTitle: title,
              haveAdjusment: item.have_adjusment,
              listType: item.listType,
              upload: item.upload,
              menu: item.menu,
            })}
            backgroundColor={item.disabled ? colors.Silver : item.color}
            iconType={item.type}
          />
        )}
        keyExtractor={(item, i) => i.toString()}
        numColumns={2}
      />
    </Container>
  );
};

export default SuperMarket;

const styles = StyleSheet.create({
  menuWrapper: {
    marginTop: 10,
  },
});
