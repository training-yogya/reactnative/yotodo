import {connect} from 'react-redux';
import Supermarket from './screen';
import {
  getArticleMenu,
  getFashionFilter,
} from './../../redux/actions/article-action';

const mapDispatchToProps = dispatch => ({
  getArticleMenu: articleType => dispatch(getArticleMenu(articleType)),
  getFashionFilter: () => dispatch(getFashionFilter()),
});

export default connect(
  null,
  mapDispatchToProps,
)(Supermarket);
