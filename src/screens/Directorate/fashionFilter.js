import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {screenNames} from '../../utils/screens';

export default function FashionFilter(props) {
  const {navigation, route} = props;
  navigation.setOptions({
    title: route?.params?.third
      ? route?.params?.third.title
      : route?.params?.second
      ? route?.params?.second?.title
      : route?.params?.title,
  });
  const handleOnPress = item => {
    // if (route?.params?.title === 'Dead Stock' && !route?.params?.second) {
    //   navigation.push(screenNames.Article.FashionFilter, {
    //     ...route?.params,
    //     second: item,
    //     filterList: deadStock,
    //   });
    // } else if (
    //   route?.params?.title === 'Yesterday TOP Sales' &&
    //   !route?.params?.second
    // ) {
    //   navigation.push(screenNames.Article.FashionFilter, {
    //     ...route?.params,
    //     second: item,
    //     filterList: topSales,
    //   });
    // } else if (
    //   route?.params?.title === 'Yesterday TOP Sales' &&
    //   !route?.params?.third
    // ) {
    //   navigation.push(screenNames.Article.FashionFilter, {
    //     ...route?.params,
    //     second: route?.params?.second,
    //     third: item,
    //     filterList: topSales2,
    //   });
    // } else {
    navigation.navigate(screenNames.Article.Article, {
      ...route?.params,
      second: !route?.params?.second ? item : route?.params?.second,
      third: item,
    });
    // }
  };
  return (
    <View>
      <FlatList
        data={route?.params?.filterList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={{
                height: 56,
                borderBottomColor: '#aaa',
                borderBottomWidth: 0.5,
                justifyContent: 'center',
                paddingHorizontal: 10,
              }}
              onPress={() => handleOnPress(item)}>
              <Text style={{fontSize: 16}}>{item.name}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
}
