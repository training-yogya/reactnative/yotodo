import {connect} from 'react-redux';
import Notes from './screen';

const mapDispatchToProps = dispatch => ({});

export default connect(
  null,
  mapDispatchToProps,
)(Notes);
