import * as React from 'react';
import {useEffect, useState} from 'react';
import {Container, Loading} from '../../components';
import globalStyles from '../../styles/global';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  TextInput,
  Modal,
  Alert,
  ActivityIndicator,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../utils/colors';
import {useSelector, useDispatch} from 'react-redux';
import {
  getNoteList,
  createNote,
  deleteNote,
  getSearch,
} from '../../redux/actions/note-action';
import {screenNames} from '../../utils/screens';
import {debounce} from 'lodash';

const Notes = ({navigation, route}) => {
  const dispatch = useDispatch();
  const noteState = useSelector(state => state.note);
  const loading = noteState.getLoading;
  const noteList = noteState.noteList;

  const [searchNotes, setSearchNotes] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [searching, setSearching] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [noteTitle, setNoteTitle] = useState('');

  const {title} = route.params;

  const keyExtractor = (item, i) => i.toString();

  const handleNavigateDetail = item => {
    navigation.navigate(screenNames.Note.Detail, {
      title: item.title || 'Detail Notes',
      noteData: item,
    });
  };

  const handleCreateNotes = () => {
    const dataCreate = {
      title: noteTitle,
      notes: '',
    };
    dispatch(createNote(dataCreate)).then(note_id => {
      dispatch(getNoteList());
      setNoteTitle('');
      setSearchText('');
      setModalVisible(false);
    });
  };

  const handleActionDelete = id => {
    dispatch(deleteNote({id})).then(() => {
      dispatch(getNoteList());
    });
  };

  const handleButtonDelete = item => {
    Alert.alert('Delete Note?', '', [
      {text: 'Delete', onPress: () => handleActionDelete(item.id)},
      {text: 'Cancel'},
    ]);
  };

  const handleSearch = React.useCallback(
    debounce(text => {
      dispatch(getSearch(text)).then(notesListSearch => {
        setSearchNotes(notesListSearch);
        setSearching(false);
      });
    }, 100),
    [],
  );

  const handleChangeSearch = text => {
    setSearchText(text);

    if (!searching) {
      setSearching(true);
    }

    handleSearch(text);
  };

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        disabled={true}
        onPress={() => handleNavigateDetail(item)}
        style={{
          height: 48,
          backgroundColor: `${colors.OrangPeel}77`,
          paddingHorizontal: 10,
          borderRadius: 10,
          marginBottom: 10,
          flexDirection: 'row',
        }}>
        <View>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>
            {item.title || '-'}
          </Text>
          <Text style={{fontSize: 11, color: `${colors.Black}77`}}>
            {item.notes.replace(/\n/gi, ' ').substr(0, 36) + '...'}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            flexDirection: 'row',
            alignItems: 'center',
            flex: 1,
          }}>
          <MaterialIcon
            onPress={() => handleNavigateDetail(item)}
            name="edit"
            size={25}
            color={`${colors.BurningOrange}cc`}
          />
          <View style={{width: 10}} />
          <MaterialIcon
            onPress={() => handleButtonDelete(item)}
            name="delete"
            size={25}
            color={`${colors.Red}aa`}
          />
        </View>
      </TouchableOpacity>
    );
  };

  useEffect(() => {
    (() => {
      dispatch(getNoteList());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title,
    headerRight: <HeaderRight onPress={() => setModalVisible(true)} />,
  });
  return (
    <Container style={globalStyles.container}>
      {loading && <Loading />}
      <View
        style={{
          margin: 10,
          flexDirection: 'row',
          borderWidth: 0.5,
          borderRadius: 20,
          paddingHorizontal: 10,
          borderColor: colors.Silver,
          alignItems: 'center',
        }}>
        <MaterialIcon color={colors.SilverChalice} name={'search'} size={25} />
        <View style={{flex: 1, height: 42}}>
          <TextInput
            value={searchText}
            onChangeText={handleChangeSearch}
            style={{paddingHorizontal: 10, height: 42}}
            placeholder={'Search Title'}
          />
        </View>
      </View>
      <FlatList
        data={searchText.length > 0 ? searchNotes : noteList}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListEmptyComponent={
          <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
            -----Empty-----
          </Text>
        }
        ListHeaderComponent={<View>{searching && <ActivityIndicator />}</View>}
      />
      <Modal
        visible={modalVisible}
        transparent={true}
        animated={true}
        onRequestClose={() => setModalVisible(false)}
        animationType={'slide'}>
        <View
          style={{
            flex: 1,
            backgroundColor: `${colors.Black}aa`,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '90%',
              paddingVertical: 10,
              borderRadius: 10,
              backgroundColor: colors.White,
            }}>
            <View style={{alignItems: 'flex-end', paddingRight: 10}}>
              <MaterialIcon
                onPress={() => setModalVisible(false)}
                name={'close'}
                size={15}
                color={colors.White}
                style={{
                  padding: 3,
                  backgroundColor: colors.Red,
                  borderRadius: 20,
                }}
              />
            </View>
            <View style={{padding: 10}}>
              <TextInput
                style={{
                  backgroundColor: colors.Black + '11',
                  borderRadius: 10,
                  height: 56,
                  paddingHorizontal: 10,
                }}
                autoFocus={true}
                value={noteTitle}
                onChangeText={text => setNoteTitle(text)}
                placeholder={'Note Title'}
              />
            </View>
            <View style={{alignItems: 'center'}}>
              <TouchableOpacity
                style={{
                  width: 200,
                  backgroundColor: colors.DodgerBlue,
                  height: 36,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => handleCreateNotes()}>
                <Text style={{color: colors.White}}>Create Note</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </Container>
  );
};

const HeaderRight = props => {
  return (
    <MaterialIcon
      onPress={props.onPress}
      name={'add'}
      color={colors.White}
      size={32}
    />
  );
};

export default Notes;
