import * as React from 'react';
import {useState} from 'react';
import {Container, Loading, ModalInfo} from '../../../components';
import globalStyles from '../../../styles/global';
import {View, TextInput, Text, TouchableOpacity} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../utils/colors';
import {useSelector, useDispatch} from 'react-redux';
import {updateNote, getNoteList} from '../../../redux/actions/note-action';

const Notes = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {title, noteData} = route.params;
  const goBack = navigation.goBack;

  const noteState = useSelector(state => state.note);
  const loading = noteState.updateLoading;
  const success = noteState.updateSuccess;
  const error = noteState.updateFailed;

  const [note, setNote] = useState(() => noteData.notes);
  const [noteTitle, setNoteTitle] = useState(() => noteData.title);
  const [modalVisible, setModalVisible] = useState(false);

  const buttonSaveTitle = note === noteData.notes ? 'Cancel' : 'Save';

  const onChangeText = text => {
    setNote(text);
  };

  const handleSave = () => {
    if (note !== noteData.notes) {
      dispatch(
        updateNote({
          id: noteData.id,
          notes: note,
          title: noteTitle,
        }),
      );
      setModalVisible(true);
    } else {
      goBack();
    }
  };

  const ModalNotif = React.useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        dispatch(getNoteList());
        return goBack();
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Berhasil disubmit"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={''}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [success, error, dispatch, goBack, modalVisible]);

  navigation.setOptions({
    title: title,
    HeaderLeft: (
      <ButtonSave title={buttonSaveTitle} onPress={() => handleSave()} />
    ),
  });
  return (
    <Container style={globalStyles.container}>
      <View style={{paddingTop: 5, marginBottom: 10}}>
        <TextInput
          style={{backgroundColor: colors.Black + '11', borderRadius: 10, height: 56}}
          value={noteTitle}
          onChangeText={text => setNoteTitle(text)}
        />
      </View>
      <View style={{flex: 1, elevation: 1, paddingHorizontal: 10}}>
        <TextInput
          multiline={true}
          textAlignVertical={'top'}
          value={note}
          onChangeText={text => onChangeText(text)}
          style={{
            height: '100%',
            paddingHorizontal: 5,
            backgroundColor: colors.PeachOrange + 'aa',
            color: '#666',
          }}
        />
      </View>
      <>{ModalNotif}</>
      {loading && <Loading />}
    </Container>
  );
};

const ButtonSave = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: -10,
      }}>
      <MaterialIcon name={'chevron-left'} color={colors.White} size={32} />
      <Text style={{fontWeight: 'bold', color: colors.White}}>
        {props.title}
      </Text>
    </TouchableOpacity>
  );
};

export default Notes;
