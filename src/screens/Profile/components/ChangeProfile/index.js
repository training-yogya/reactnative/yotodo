import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  Container,
  InputText,
  AppButton,
  ModalInfo,
  Loading,
} from '../../../../components';
import {View, StyleSheet} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import globalStyles from '../../../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../../../utils/colors';
import {screenNames} from '../../../../utils/screens';
import {
  getProfileData,
  updateProfileData,
} from '../../../../redux/actions/auth-action';
import {ScrollView} from 'react-native-gesture-handler';

const ChangeProfile = ({route, navigation}) => {
  const loading = useSelector(state => state.auth.updateProfileLoading);
  const error = useSelector(state => state.auth.updateProfileError);
  const success = useSelector(state => state.auth.updateProfileSuccess);
  const message = useSelector(state => state.auth.message);
  const profile = useSelector(state => state.auth.userLogin);

  const dispatch = useDispatch();

  const {navigate, goBack} = navigation;

  const [fullname, setFullname] = useState(() => profile.full_name);
  const [nik, setNik] = useState(() => profile.nik);
  const [phoneNumber, setPhoneNumber] = useState(() => profile.phone_number);
  const [jabatan, setJabatan] = useState(() => profile.jabatan);
  const [telegramID, setTelegramID] = useState(() => profile.telegram_id);

  const [modalVisible, setModalVisible] = useState(false);

  const handleUpdateProfile = () => {
    dispatch(
      updateProfileData({
        full_name: fullname,
        phone_number: phoneNumber,
        nik,
        jabatan,
        telegram_id: telegramID,
      }),
    );
    setModalVisible(true);
  };

  const ModalNotif = React.useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        dispatch(getProfileData());
        return navigate(screenNames.Home.Profile, {});
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Profile diupdate"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [success, error, dispatch, navigate, modalVisible, message]);

  useEffect(() => {
    (() => {
      dispatch(getProfileData());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  navigation.setOptions({
    title: 'Update Profile',
  });
  return (
    <Container>
      {loading && <Loading />}
      <ScrollView>
        <View style={{flex: 1}}>
          <View style={globalStyles.center}>
            <MaterialIcon
              name="person-outline"
              color={colors.BlazeOrange}
              size={100}
            />
          </View>
          <View style={[globalStyles.center, styles.spacing]}>
            <View style={styles.inputWrapper}>
              <InputText
                icon={'account-card-details'}
                value={fullname}
                onChangeText={text => setFullname(text)}
                placeholder="Fullname"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                icon={'card-bulleted-outline'}
                value={nik}
                onChangeText={text => setNik(text)}
                placeholder="NIK"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                icon={'cellphone-basic'}
                onChangeText={text => setPhoneNumber(text)}
                value={phoneNumber}
                placeholder="Phone Number"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                icon={'send'}
                onChangeText={text => setTelegramID(text)}
                value={telegramID}
                placeholder="Telegram ID"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                type={'select'}
                options={[null, 'MANAGER', 'SENIOR/CHIEF', 'STAFF']}
                icon={'account-tie'}
                onChangeText={value => setJabatan(value)}
                iconType={'MaterialComunity'}
                value={jabatan}
                placeholder="Jabatan"
              />
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={[styles.inputWrapper, {paddingHorizontal: 20}]}>
        <AppButton
          onPress={handleUpdateProfile}
          title="Save Change"
          color={colors.BlazeOrange}
        />
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default ChangeProfile;

const styles = StyleSheet.create({
  spacing: {
    marginTop: 30,
  },
  inputWrapper: {
    paddingVertical: 10,
  },
});
