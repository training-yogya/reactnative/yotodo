import * as React from 'react';
import {useEffect, useState, useMemo} from 'react';
import {
  Container,
  InputText,
  ModalInfo,
  Loading,
  AppButton,
} from '../../../../components';
import {View, Text, StyleSheet, TouchableOpacity, Modal} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import globalStyles from '../../../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../../../utils/colors';

import {changePassword} from './../../../../redux/actions/auth-action';
import {screenNames} from '../../../../utils/screens';

const ChangePassword = ({route, navigation}) => {
  const navigate = navigation.navigate;

  const auth = useSelector(state => state.auth);
  const loading = auth.changePasswordLoading;
  const success = auth.changePasswordSuccess;
  const error = auth.changePasswordError;
  const message = auth.message;
  const dispatch = useDispatch();

  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  const onChangeOldPassword = text => setOldPassword(text);
  const onChangeNewPassword = text => setNewPassword(text);
  const onChangeConfirmPassword = text => setConfirmPassword(text);

  const handleButtonSave = () => {
    dispatch(changePassword({oldPassword, newPassword, confirmPassword})).then;
    setModalVisible(true);
  };

  const ModalNotif = useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        return navigate(screenNames.Home.Profile, {});
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Password diupdate"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [modalVisible, message, navigate, success, error]);

  navigation.setOptions({
    title: 'Change Password',
    headerRight: <ButtonSave handleSave={handleButtonSave} />,
  });
  return (
    <Container>
      {loading && <Loading />}
      <View style={globalStyles.center}>
        <MaterialIcon
          name="lock-outline"
          color={colors.BlazeOrange}
          size={100}
        />
      </View>
      <View style={[globalStyles.center, styles.spacing]}>
        <View style={styles.inputWrapper}>
          <InputText
            icon={'lock'}
            value={oldPassword}
            onChangeText={onChangeOldPassword}
            placeholder="Old Password"
            type="password"
          />
        </View>
        <View style={styles.inputWrapper}>
          <InputText
            icon={'lock'}
            value={newPassword}
            onChangeText={onChangeNewPassword}
            placeholder="New Password"
            type="password"
          />
        </View>
        <View style={styles.inputWrapper}>
          <InputText
            icon={'lock'}
            value={confirmPassword}
            onChangeText={onChangeConfirmPassword}
            placeholder="Repeat New Password"
            type="password"
          />
        </View>
      </View>
      <View style={{flex: 1}} />
      <View style={{paddingHorizontal: 20, marginBottom: 10}}>
        <AppButton
          onPress={handleButtonSave}
          title={'Save Change'}
          color={colors.BlazeOrange}
        />
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default ChangePassword;

const ButtonSave = ({handleSave}) => {
  return (
    <TouchableOpacity
      onPress={handleSave}
      underlayColor="rgba(0,0,0,0.4)"
      style={{
        padding: 5,
        borderRadius: 10,
      }}>
      <Text
        style={{
          color: colors.White,
          fontSize: 16,
        }}>
        Save
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  spacing: {
    marginTop: 30,
  },
  inputWrapper: {
    paddingVertical: 10,
  },
});
