import * as React from 'react';
import {View, Text, FlatList} from 'react-native';
import {Container} from '../../../../components';
import colors from '../../../../utils/colors';
import globalStyles from '../../../../styles/global';

import {useSelector} from 'react-redux';

const YourActivity = ({navigation}) => {
  const activity = useSelector(state => state.auth.activity);

  navigation.setOptions({
    title: 'Your Activity',
  });
  return (
    <Container style={globalStyles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: 20,
        }}>
        <View
          style={{
            backgroundColor: colors.Blue,
            padding: 10,
            width: '40%',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: colors.Black,
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 12,
            }}>
            Total Hari Ini : {activity?.count_today}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: colors.BlazeOrange,
            padding: 10,
            width: '40%',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: colors.Black,
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 12,
            }}>
            Total Akumulasi YTD : {activity?.count_this_year}
          </Text>
        </View>
      </View>
      <View style={{flex: 1, backgroundColor: colors.Silver + '53'}}>
        <FlatList
          data={activity?.tabel}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  margin: 5,
                  backgroundColor: colors.White,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10,
                }}>
                <View style={{flex: 1}}>
                  <Text style={{fontWeight: 'bold', fontSize: 16}}>
                    {item.menu}
                  </Text>
                </View>
                <View>
                  <View
                    style={{
                      backgroundColor: colors.Blue,
                      paddingHorizontal: 10,
                      paddingVertical: 4,
                      marginBottom: 4,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: colors.Black,
                        fontWeight: 'bold',
                        fontSize: 12,
                      }}>
                      Hari Ini : {item.count_today}
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: colors.BlazeOrange,
                      paddingHorizontal: 10,
                      paddingVertical: 4,
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: colors.Black,
                        fontWeight: 'bold',
                        fontSize: 12,
                      }}>
                      Akumulasi YTD : {item.count_this_year}
                    </Text>
                  </View>
                </View>
              </View>
            );
          }}
        />
      </View>
    </Container>
  );
};

export default YourActivity;
