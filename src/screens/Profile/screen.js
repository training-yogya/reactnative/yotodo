import * as React from 'react';
import {useEffect, useState} from 'react';
import {Container, InputText} from '../../components';
import {View, Text, StyleSheet, TouchableOpacity, Modal} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import globalStyles from '../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {
  getProfileData,
  getProfileActivity,
} from '../../redux/actions/auth-action';
import {ScrollView} from 'react-native-gesture-handler';

const Profille = ({route, navigation}) => {
  const loading = useSelector(state => state.auth.getProfileLoading);
  const activity = useSelector(state => state.auth.activity);
  const profile = useSelector(state => state.auth.userLogin);

  const dispatch = useDispatch();

  const [buttonDisabled] = useState(false);
  const {title} = route.params;

  const buttonChangePassword = () => {
    navigation.navigate(screenNames.Profile.ChangePassword);
  };

  const buttonYourActivity = () =>
    navigation.navigate(screenNames.Profile.Activity);
  const buttonInsightInfo = () => navigation.push(screenNames.Home.InsightInfo);
  const buttonEditProfile = () =>
    navigation.navigate(screenNames.Profile.UpdateProfile);

  useEffect(() => {
    (() => {
      dispatch(getProfileData());
      dispatch(getProfileActivity());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: title || 'Profile',
  });
  return (
    <Container>
      {/* <Text>{JSON.stringify(activity)}</Text> */}
      <ScrollView>
        <View style={{flex: 1}}>
          <View style={globalStyles.center}>
            <MaterialIcon
              name="person-outline"
              color={colors.BlazeOrange}
              size={100}
            />
          </View>
          <View style={[globalStyles.center, styles.spacing]}>
            <View style={styles.inputWrapper}>
              <InputText
                editAble={false}
                icon={'clipboard-account'}
                value={profile.initial_store}
                placeholder="Initial Store"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                editAble={false}
                icon={'account-card-details'}
                value={profile.full_name}
                placeholder="Fullname"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                editAble={false}
                icon={'card-bulleted-outline'}
                value={profile.nik}
                placeholder="NIK"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                editAble={false}
                icon={'cellphone-basic'}
                value={profile.phone_number}
                placeholder="Phone Number"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                editAble={false}
                icon={'send'}
                value={profile.telegram_id}
                placeholder="Telegram ID"
              />
            </View>
            <View style={styles.inputWrapper}>
              <InputText
                editAble={false}
                icon={'account-tie'}
                value={profile.jabatan}
                placeholder="Jabatan"
              />
            </View>
            <View style={[styles.inputWrapper]}>
              <TouchableOpacity onPress={buttonChangePassword}>
                <Text style={{color: colors.DodgerBlue}}>Change Password</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          paddingVertical: 10,
        }}>
        <TouchableOpacity
          onPress={buttonEditProfile}
          style={{marginHorizontal: 20, alignItems: 'center'}}>
          <MaterialIcon name="edit" size={36} color={colors.BlazeOrange} />
          <Text
            style={{
              fontSize: 12,
              color: colors.BlazeOrange,
            }}>
            Edit Profile
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled={buttonDisabled}
          onPress={buttonYourActivity}
          style={{marginHorizontal: 20, alignItems: 'center'}}>
          <MaterialIcon
            name="av-timer"
            size={36}
            color={buttonDisabled ? colors.Silver : colors.BlazeOrange}
          />
          <Text
            style={{
              fontSize: 12,
              color: buttonDisabled ? colors.Silver : colors.BlazeOrange,
            }}>
            Your Activity
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={buttonInsightInfo}
          style={{marginHorizontal: 20, alignItems: 'center'}}>
          <MaterialIcon name="timeline" size={36} color={colors.BlazeOrange} />
          <Text
            style={{
              fontSize: 12,
              color: colors.BlazeOrange,
            }}>
            Insight
          </Text>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

export default Profille;

const styles = StyleSheet.create({
  spacing: {
    marginTop: 30,
  },
  inputWrapper: {
    paddingVertical: 10,
  },
});
