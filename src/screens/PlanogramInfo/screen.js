import * as React from 'react';
import {Container, Card, AppButton} from '../../components';
import {View} from 'react-native';
import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import {screenNames} from '../../utils/screens';

const PlanogramInfo = ({navigation, route}) => {
  const {hideRequest} = route.params;
  navigation.setOptions({
    headerRight: false,
    title: 'Planogram Info & Request',
  });
  const handleTutorialPress = () => {
    navigation.navigate(screenNames.Planogram.Tutorial, {
      title: 'How to read and implementation',
    });
  };
  const handleRequestPress = () => {
    navigation.navigate(screenNames.Planogram.Request, {
      title: 'Planogram Request',
    });
  };
  const handleRequestTracking = () => {
    navigation.navigate(screenNames.Planogram.RequestTrackingList);
  };
  return (
    <Container style={globalStyles.container}>
      <View style={{flexDirection: 'row', flex: 1}}>
        <Card
          onPress={handleTutorialPress}
          title={'How to read and Planogram Implementation'}
          icon="group"
          color={colors.BlueBell}
        />
        <View style={{width: 10}} />
        {!hideRequest && (
          <Card
            onPress={handleRequestPress}
            title={'Planogram Request'}
            icon="add-shopping-cart"
            color={colors.Boulder}
          />
        )}
      </View>
      {/* {hideRequest && ( */}
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <AppButton
          onPress={handleRequestTracking}
          title={'Planogram Request Tracking'}
          color={colors.BlazeOrange}
        />
      </View>
      {/* )} */}
    </Container>
  );
};

export default PlanogramInfo;
