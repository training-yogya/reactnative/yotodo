import {connect} from 'react-redux';
import {getHomeMenuItem} from './../../redux/actions/menu-action';

import Home from './screen';

const mapDispatchToProps = dispatch => ({
  getHomeMenuItem: () => dispatch(getHomeMenuItem()),
});

export default connect(
  null,
  mapDispatchToProps,
)(Home);
