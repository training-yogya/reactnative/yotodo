import * as React from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Dimensions,
  RefreshControl,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {ButtonCircle} from '../../../components';
const height = Dimensions.get('screen').height;
const Menu = ({items, loading, getMenuAsync}) => {
  const navigation = useNavigation();
  const handleOnPress = (screenName, title) => () => {
    navigation.navigate(screenName, {
      screen: screenName,
      params: {title},
    });
  };
  const renderMenuItem = ({item}) => (
    <View style={{flex: 1}}>
      <ButtonCircle
        onPress={handleOnPress(item.to, item.name)}
        height={100}
        wrapperStyle={{
          justifyContent: 'center',
        }}
        disabled={item.disabled}
        iconType={item.iconType}
        icon={item.icon}
        title={item.name}
        badge={undefined}
        size={50}
      />
    </View>
  );
  const MenuKeyExtractor = item => item.id.toString();
  const refreshControl = (
    <RefreshControl onRefresh={getMenuAsync} refreshing={loading} />
  );
  return (
    <View style={styles.menuWrapper}>
      <FlatList
        data={items}
        horizontal={false}
        numColumns={3}
        onRefresh={false}
        refreshControl={refreshControl}
        renderItem={renderMenuItem}
        keyExtractor={MenuKeyExtractor}
      />
    </View>
  );
};

export default Menu;

const styles = StyleSheet.create({
  menuWrapper: {
    flex: 1,
    marginLeft: 20,
  },
});
