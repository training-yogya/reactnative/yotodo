import * as React from 'react';
import {Image, SafeAreaView, StyleSheet, Dimensions} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';

const ContentForYou = ({items}) => {
  const renderItem = ({item}) => (
    <Image source={{uri: item}} style={styles.contentImage} />
  );
  const keyExtractor = (item, index) => index.toString();
  return (
    <SafeAreaView style={styles.wrapper}>
      <FlatList
        data={items}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        horizontal={true}
      />
    </SafeAreaView>
  );
};

export default ContentForYou;

const {width} = Dimensions.get('window');
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingHorizontal: 10,
    paddingBottom: 5,
  },
  contentImage: {
    width: width - 20,
    marginBottom: 10,
    height: 340,
    borderRadius: 4,
  },
});
