import * as React from 'react';
import {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {View, Text} from 'react-native';
import {Container, Icon} from '../../components';

import Menu from './components/Menu';

import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import {iconsNames} from '../../utils/icons';

const Home = ({navigation, getHomeMenuItem}) => {
  const menu = useSelector(state => state.menu);
  const userLogin = useSelector(state => state.auth.userLogin);
  const loading = menu.loading;
  navigation.setOptions({
    title: 'Home',
    headerShown: false,
  });

  useEffect(() => {
    getHomeMenuItem();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container>
      {/* <Header title="Content For You" backgroundColor={colors.RegentStBlue} />
      <ContentForYou items={CONTENTFORYOU} /> */}
      <View
        style={[
          globalStyles.bgWransparent,
          {height: 60, flexDirection: 'row', justifyContent: 'space-between'},
          {padding: 10},
          {marginBottom: 15},
          {elevation: 1},
        ]}>
        <View style={{flex: 1, alignItems: 'flex-start'}}>
          <Icon name={iconsNames.yogyabanner} />
        </View>
        <View style={{flex: 1}}>
          <Text style={{fontWeight: 'bold', color: colors.VerdunGreen}}>
            Selamat Datang
          </Text>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 14,
              color: colors.Black + 'dd',
            }}>
            {userLogin.full_name}-{userLogin.initial_store}
          </Text>
          <Text />
        </View>
      </View>
      <Menu
        loading={loading}
        getMenuAsync={getHomeMenuItem}
        items={menu.items}
      />
    </Container>
  );
};

export default Home;
