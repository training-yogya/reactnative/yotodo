import React, {PureComponent} from 'react';
import {
  TextInput,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {AppButton, Loading} from '../../components';
class BarcodeScan extends PureComponent {
  state = {
    searchingForBarcode: true,
    tillCode: '',
    loading: false,
    hideBarcode: false,
    isYopicker: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      searchingForBarcode: true,
      tillCode: '',
      loading: false,
      hideBarcode: props.route?.params?.hidebarcode ?? false,
      isYopicker: this.props.route.params.title.toLowerCase() === 'yopicker',
    };
    props.navigation.setOptions({
      title: props.route?.params?.title ?? 'Barcode Scan',
    });
  }

  componentDidMount = () => {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      if (!this.state.searchingForBarcode) {
        this.setState({
          searchingForBarcode: true,
        });
      }
    });
  };

  componentWillUnmount = () => {
    this._unsubscribe();
  };

  handleOnBarcodesDetected = async data => {
    if (Platform.OS === 'android') {
      const {barcodes} = data;
      this.setState({
        searchingForBarcode: false,
      });
      const productId = barcodes[0].data;

      if (barcodes[0].type === 'UNKNOWN_FORMAT') {
        this.setState({
          searchingForBarcode: true,
        });
        return;
      }

      if (this.props.route.params.action) {
        this.props.route.params.action(productId);
      }

      this.props.navigation.navigate(screenNames.Product.Identity, {
        productId,
        title: this.props.route.params.title,
        barcode: true,
        hideReason: true,
        parentTitle: this.props.route.params.title,
      });
    } else if (Platform.OS === 'ios') {
      this.setState({
        searchingForBarcode: false,
      });
      const productId = data.data;

      if (data.type === 'UNKNOWN_FORMAT') {
        this.setState({
          searchingForBarcode: true,
        });
        return;
      }

      if (this.props.route.params.action) {
        this.props.route.params.action(productId);
      }

      this.props.navigation.navigate(screenNames.Product.Identity, {
        productId,
        title: this.props.route.params.title,
        barcode: true,
        hideReason: true,
        parentTitle: this.props.route.params.title,
      });
    }

    // this.handleTillCodeChange(productId);
  };

  handleTillCodeChange = text => {
    this.setState({
      tillCode: text,
    });
  };

  handleSubmitTillCode = async () => {
    if (this.state.tillCode) {
      if (this.props.route.params.action) {
        this.props.route.params.action(this.state.tillCode);
      }
      this.props.navigation.navigate(screenNames.Product.Identity, {
        productId: this.state.tillCode,
        title: this.props.route.params.title,
        hideReason: true,
        barcode: true,
        parentTitle: this.props.route.params.title,
      });
      this.handleTillCodeChange('');
    } else {
      Alert.alert('SKU / Till Code tidak boleh kosong');
    }
  };

  handleNavigateExpired = () => {
    this.props.navigation.navigate(screenNames.Home.ExpiredCheck, {
      screen: screenNames.Home.ExpiredCheck,
      params: {title: 'Expired Check'},
    });
  };

  handleNavigatePickingList = () => {
    this.props.navigation.navigate(screenNames.YoPicker.OmsList, {
      title: 'YoPicker',
    });
  };

  handleNavigatePSOLIST = () => {
    this.props.navigation.navigate(screenNames.SOPartial.PSOList, {
      title: 'PSO Artikel LIST',
    });
  };

  handlePickingReplesnish = () => {
    this.props.navigation.navigate(screenNames.Replenish.PickingList, {
      title: 'Picking List',
    });
  };

  handleNavigateExpiredCheck = () => {
    this.props.navigation.navigate(screenNames.ExpiredCheck.List, {
      screen: screenNames.ExpiredCheck.List,
      params: {title: 'Expired Check'},
    });
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading && <Loading />}
        {!this.state.hideBarcode && (
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            type={RNCamera.Constants.Type.back}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            onGoogleVisionBarcodesDetected={
              Platform.OS === 'android' && this.state.searchingForBarcode
                ? this.handleOnBarcodesDetected
                : null
            }
            onBarCodeRead={
              Platform.OS === 'ios' && this.state.searchingForBarcode
                ? this.handleOnBarcodesDetected
                : null
            }
            captureAudio={false}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontSize: 18, color: colors.White}}>
                Scan Barcode
              </Text>
            </View>
          </RNCamera>
        )}
        {this.state.isYopicker && !this.state.hideBarcode ? (
          <View
            style={{
              flex: 1,
              backgroundColor: colors.White,
              alignItems: 'center',
            }}
          />
        ) : (
          <View
            style={{
              flex: 1,
              backgroundColor: colors.White,
              alignItems: 'center',
            }}>
            {!this.state.hideBarcode && (
              <View style={{marginTop: 20}}>
                <Text>---------------- OR ----------------</Text>
              </View>
            )}
            <View
              style={{
                borderWidth: 1,
                width: '90%',
                marginTop: 20,
                borderRadius: 10,
                borderColor: colors.Silver,
                paddingHorizontal: 20,
                flexDirection: 'row',
              }}>
              <TextInput
                autoFocus={this.state.hideBarcode}
                value={this.state.tillCode}
                onChangeText={this.handleTillCodeChange}
                style={{flex: 1, height: 56}}
                keyboardType={'numeric'}
                onSubmitEditing={this.handleSubmitTillCode}
                placeholder={'Insert SKU/Till Code Manually'}
              />
              <TouchableOpacity
                onPress={this.handleSubmitTillCode}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: colors.BlazeOrange}}>Submit</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 1}} />
            {this.state.isYopicker && (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <AppButton
                  title="Picking List Check"
                  style={{marginRight: 20}}
                  onPress={this.handleNavigatePickingList}
                  color={colors.BlazeOrange}
                />
                <AppButton
                  title="Expired Check"
                  style={{marginLeft: 20}}
                  onPress={this.handleNavigateExpired}
                  color={colors.EarlsGreen}
                />
              </View>
            )}
            {this.props.route.params.title.toLowerCase() ===
              'new input by barcode scan' && (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <AppButton
                  title="View PSO LIST"
                  style={{marginRight: 20}}
                  onPress={this.handleNavigatePSOLIST}
                  color={colors.BlazeOrange}
                />
              </View>
            )}
            {this.props.route.params.type === 'replenish' && (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <AppButton
                  title={'Picking List\nCheck'}
                  style={{paddingHorizontal: 30, marginRight: 20}}
                  titleStyle={{textAlign: 'center'}}
                  onPress={this.handlePickingReplesnish}
                  color={colors.BlazeOrange}
                />
                <AppButton
                  title={'Expired\nCheck'}
                  style={{paddingHorizontal: 30, marginLeft: 20}}
                  titleStyle={{textAlign: 'center'}}
                  onPress={this.handleNavigateExpiredCheck}
                  color={colors.EarlsGreen}
                />
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

BarcodeScan.navigationOptions = () => {
  return {
    title: 'Barcode Scan',
  };
};

export default BarcodeScan;
