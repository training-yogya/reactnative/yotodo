import {connect} from 'react-redux';
import BarcodeScan from './screen';

const mapDispatchToProps = dispatch => ({});
const mapStateToProps = state => ({
  userLogin: state.auth.userLogin,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BarcodeScan);
