import * as React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Container, Loading} from '../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../utils/colors';
import {useDispatch, useSelector} from 'react-redux';
import {getTutorialDirectorate} from '../../redux/actions/tutorial-action';
import {screenNames} from '../../utils/screens';

const Tutorial = props => {
  const dispatch = useDispatch();
  const tutorial = useSelector(state => state.tutorial);
  const {navigation, route} = props;

  const title = route?.params?.title;

  const keyExtractor = (item, index) => index.toString();

  React.useEffect(() => {
    (() => {
      dispatch(getTutorialDirectorate());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItemMenu = ({item, index}) => {
    const colorKey = Object.keys(colors);
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate(screenNames.Tutorial.SubMenu, {
            title: item.name?.toUpperCase(),
            id: item.directorate || item.id,
          })
        }
        style={{
          backgroundColor: colors[colorKey[index]],
          flex: 1,
          marginHorizontal: 15,
          marginTop: 20,
          height: 60,
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{color: colors.White, fontSize: 16, textAlign: 'center'}}>
          {item.name?.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderItemSubMenu = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: colors.RegentStBlue,
          height: 40,
          marginTop: index === 0 ? 10 : 0,
          marginBottom: 10,
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() =>
          navigation.navigate(screenNames.Tutorial.Detail, {
            title: item.title?.toUpperCase(),
            uri: item.description,
          })
        }>
        <Text style={{color: colors.White, fontSize: 14, textAlign: 'center'}}>
          {item.title?.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  };

  navigation.setOptions({
    title,
  });

  return (
    <Container>
      {tutorial.loading && <Loading />}
      <View>
        <FlatList
          data={tutorial.menu}
          numColumns={2}
          keyExtractor={keyExtractor}
          renderItem={renderItemMenu}
          columnWrapperStyle={{paddingHorizontal: 25}}
        />
      </View>
      <View style={{marginVertical: 10, flex: 1}}>
        <FlatList
          contentContainerStyle={{paddingHorizontal: 30}}
          data={tutorial.global}
          numColumns={1}
          keyExtractor={keyExtractor}
          renderItem={renderItemSubMenu}
        />
      </View>
    </Container>
  );
};

export default Tutorial;
