import * as React from 'react';
import {Container, Loading} from '../../../components';
import {
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Image,
  Text,
  View,
  Modal,
  TouchableOpacity,
} from 'react-native';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import ImageViewer from 'react-native-image-zoom-viewer';

import Pdf from 'react-native-pdf';
import {useSelector} from 'react-redux';
import Video from 'react-native-video';
import colors from '../../../utils/colors';
import config from '../../../config';
const {width, height} = Dimensions.get('window');

const TutorialDetail = ({navigation, route}) => {
  const {title, uri} = route.params;
  const tutorial = useSelector(state => state.tutorial);
  const [modalPictureVisible, setModalPictureVisible] = React.useState(false);

  navigation.setOptions({
    title,
  });

  const format = uri.split('.').pop();

  const onBuffer = e => {
    console.log(e);
  };

  const videoError = e => {
    console.log(e);
  };

  const ModalPicture = () => {
    return (
      <Modal
        onRequestClose={() => setModalPictureVisible(false)}
        visible={modalPictureVisible}
        transparent={true}
        animated={true}
        animationType={'slide'}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.7)',
            position: 'relative',
            height: '100%',
          }}>
          <TouchableOpacity
            onPress={() => setModalPictureVisible(false)}
            style={{
              justifyContent: 'center',
              height: 56,
              marginLeft: 20,
            }}>
            <MaterialIcon
              onPress={() => setModalPictureVisible(false)}
              name="close"
              color={colors.White}
              size={30}
            />
          </TouchableOpacity>
          <ImageViewer
            imageUrls={[{url: uri?.replace('/app/', config.baseImage)}]}
          />
        </View>
      </Modal>
    );
  };
  return (
    <Container>
      {tutorial.loading && <Loading />}
      {format === 'pdf' ? (
        <Pdf
          activityIndicator={<ActivityIndicator />}
          source={{uri: uri?.replace('/app/', config.baseImage), cache: true}}
          maxScale={20.0}
          style={styles.pdf}
        />
      ) : format === 'mp4' ? (
        <Video
          source={{uri: uri?.replace('/app/', config.baseImage)}} // Can be a URL or a local file
          onBuffer={onBuffer} // Callback when remote video is buffering
          onError={videoError}
          onVideoLoad={e => console.log('on video load', e)}
          onVideoLoadStart={e => console.log('on video load start', e)}
          onVideoProgress={e => console.log('video progress', e)}
          controls={true}
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
          }}
        />
      ) : (
        <TouchableOpacity onPress={() => setModalPictureVisible(true)}>
          <Image
            source={{uri: uri?.replace('/app/', config.baseImage)}}
            style={{width, minHeight: 300, resizeMode: 'contain'}}
          />
        </TouchableOpacity>
      )}
      <ModalPicture />
    </Container>
  );
};

export default TutorialDetail;

const styles = StyleSheet.create({
  pdf: {
    flex: 1,
    width: width,
    height: height,
  },
});
