import * as React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {Container, Loading} from '../../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {useDispatch, useSelector} from 'react-redux';
import {screenNames} from '../../../utils/screens';
import {getTutorialSubDirectorate} from '../../../redux/actions/tutorial-action';

const Tutorial = props => {
  const dispatch = useDispatch();
  const tutorial = useSelector(state => state.tutorial);
  const {navigation, route} = props;

  const title = route?.params?.title;
  const id = route?.params?.id;

  const keyExtractor = (item, index) => index.toString();

  React.useEffect(() => {
    (() => {
      dispatch(getTutorialSubDirectorate(id));
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItemSubMenu = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: colors.RegentStBlue,
          height: 40,
          marginTop: index === 0 ? 10 : 0,
          marginBottom: 10,
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() =>
          navigation.navigate(screenNames.Tutorial.Detail, {
            title: item.title.toUpperCase(),
            uri: item.description,
          })
        }>
        <Text style={{color: colors.White, fontSize: 14, textAlign: 'center'}}>
          {item.title.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  };

  navigation.setOptions({
    title,
  });

  return (
    <Container>
      {tutorial.loading && <Loading />}
      <FlatList
        contentContainerStyle={{paddingHorizontal: 30}}
        data={tutorial.submenu}
        numColumns={1}
        keyExtractor={keyExtractor}
        renderItem={renderItemSubMenu}
      />
    </Container>
  );
};

export default Tutorial;
