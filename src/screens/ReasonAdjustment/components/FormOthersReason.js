import * as React from 'react';
import {useState} from 'react';
import {Container, AppButton} from '../../../components';
import {TextInput} from 'react-native-gesture-handler';
import {View, Text, TouchableHighlight} from 'react-native';
import colors from '../../../utils/colors';
import {useDispatch} from 'react-redux';
import {ADD_REASON_ARTICLE} from '../../../redux/actionTypes';

const FormOtherReason = ({route, navigation}) => {
  const textPlaceHolder = route.params.currentValue || 'Reason';
  const setReason = route.params.setReason;

  const dispatch = useDispatch();
  const [reasonText, setReasonText] = useState('');
  const [loading, setLoading] = useState(false);

  const handleChangeText = text => setReasonText(text);

  const handleButtonSave = () => {
    setLoading(true);
    const reason = {
      value: reasonText,
      other: true,
      adjusment: false,
    };
    dispatch({
      type: ADD_REASON_ARTICLE,
      payload: reason,
    });
    setReason(reason);

    setTimeout(() => {
      navigation.goBack();
      setLoading(false);
    }, 250);
  };
  navigation.setOptions({
    title: 'Other Reason',
    headerLeft: 'none',
    headerRight: <ButtonSave handleSave={handleButtonSave} />,
  });
  return (
    <Container
      style={{
        padding: 5,
        paddingBottom: 0,
      }}>
      <View style={{flex: 1}}>
        <View
          style={{
            borderRadius: 4,
            borderWidth: 0.7,
            borderColor: colors.Silver,
          }}>
          <TextInput
            onChangeText={handleChangeText}
            placeholder={textPlaceHolder}
          />
        </View>
      </View>
      <View
        style={{
          paddingBottom: 10,
        }}>
        <AppButton
          onPress={handleButtonSave}
          title="Save"
          color={colors.PeachOrange}
          loading={loading}
        />
      </View>
    </Container>
  );
};

const ButtonSave = ({handleSave}) => {
  return (
    <TouchableHighlight
      onPress={handleSave}
      underlayColor="rgba(0,0,0,0.4)"
      style={{
        padding: 5,
        borderRadius: 10,
      }}>
      <Text
        style={{
          color: colors.White,
        }}>
        Save
      </Text>
    </TouchableHighlight>
  );
};

export default FormOtherReason;
