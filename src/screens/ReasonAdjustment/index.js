import {connect} from 'react-redux';
import ReasonAdjusyment from './screen';
import {
  getReasonArticle,
  submitReason,
} from './../../redux/actions/article-action';

const mapDispatchToProps = dispatch => ({
  getReasonArticle: () => dispatch(getReasonArticle()),
  submitReason: data => dispatch(submitReason(data)),
});

export default connect(
  null,
  mapDispatchToProps,
)(ReasonAdjusyment);
