import * as React from 'react';
import {useState, useEffect, useMemo} from 'react';
import {View, Text, Picker, StyleSheet, Alert, Platform, TouchableOpacity, ActionSheetIOS} from 'react-native';
import colors from '../../utils/colors';
import {Container, AppButton, ModalInfo} from '../../components';
import {useSelector} from 'react-redux';
import {TextInput} from 'react-native-gesture-handler';
import {screenNames} from '../../utils/screens';

const ReasonAdjustment = ({navigation, getReasonArticle, submitReason}) => {
  const article = useSelector(state => state.article);
  const {loading, error, success, message} = article;
  const {navigate} = navigation;
  const [reason, setReason] = useState(null);
  const [keterangan, setKeterangan] = useState('');
  const [modalVisible, setModalVisible] = useState(null);
  const handleLanguageChange = (itemValue, itemIndex) => {
    if (itemValue === 'Others Reasons') {
      setReason(itemValue);
      navigation.navigate(screenNames.Article.FormOtherReason, {
        currentValue: itemValue,
        setReason: setReason,
      });
    } else {
      setReason(itemValue);
    }
  };
  const handleChangeKeterangan = text => setKeterangan(text);
  const handleSubmitReason = () => {
    Alert.alert('Apakah Anda yakin dengan reason ini?', '', [
      {
        text: 'Ya,',
        onPress: async () => {
          submitReason({reasonId: reason, keterangan}).then;
          setModalVisible(true);
        },
      },
      {
        text: 'Kembali',
      },
    ]);
  };

  const ModalNotif = useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        return navigate(screenNames.Article.ProductIdentity, {
          hideReason: false,
        });
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Reason Berhasil di Update"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [modalVisible, navigate, success, error, message]);

  useEffect(() => {
    (() => {
      getReasonArticle();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: 'Article Reason',
  });

  const handleShowActionSheet = () => {
    ActionSheetIOS.showActionSheetWithOptions({
      options: article.reasonItem.map(item => item.name),
    }, buttunIndex => {
      const value = article.reasonItem[buttunIndex];

      setReason(value.id)
    })
  }

  return (
    <Container style={styles.wrapper}>
      <View style={styles.formReason}>
        <Text>Reasons</Text>
        <View
          style={{
            height: Platform.OS === "ios" ? undefined : 50,
            width: '100%',
            backgroundColor: colors.White,
            borderColor: colors.Silver,
            borderWidth: 0.7,
            borderRadius: 4,
          }}>
            {
              Platform.OS === 'android' ? (
                <Picker selectedValue={reason} onValueChange={handleLanguageChange}>
                  {article.reasonItem.map((item, i) => (
                    <Picker.Item
                      key={i}
                      label={item.name || 'Choose Reason'}
                      value={item.id}
                    />
                  ))}
                </Picker>
              ) : (
                <TouchableOpacity onPress={handleShowActionSheet}>
                  <Text style={{lineHeight: 56}}>{article?.reasonItem[reason]?.name || 'Choose Reason'}</Text>
                </TouchableOpacity>
              )
            }
        </View>
        <View style={{height: 10}} />
        <TextInput
          style={{
            borderColor: colors.Silver,
            borderWidth: 0.7,
            borderRadius: 4,
            height: 56
          }}
          onChangeText={handleChangeKeterangan}
          multiline={true}
          numberOfLines={3}
          textAlignVertical="top"
          placeholder="Comment"
        />
      </View>
      <View style={styles.buttonSubmit}>
        <AppButton
          loading={loading}
          disable={!reason}
          color={!reason ? colors.Silver : colors.BlazeOrange}
          onPress={handleSubmitReason}
          title="Submit Reason"
        />
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default ReasonAdjustment;

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 10,
    backgroundColor: colors.White,
  },
  formReason: {
    paddingVertical: 5,
    flex: 1,
  },
  buttonSubmit: {
    paddingVertical: 5,
  },
});
