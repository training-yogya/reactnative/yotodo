import {connect} from 'react-redux';
import MinusMargin from './screen';
import {
  getArticleList,
  getArticleFashion,
} from './../../redux/actions/article-action';

const mapDispatchToProps = dispatch => ({
  getArticleList: (type, id, categoryId) =>
    dispatch(getArticleList(type, id, categoryId)),
  getArticleFashion: (title, code) => dispatch(getArticleFashion(title, code)),
});

export default connect(
  null,
  mapDispatchToProps,
)(MinusMargin);
