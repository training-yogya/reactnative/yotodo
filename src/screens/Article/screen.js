import * as React from 'react';
import {useEffect} from 'react';
import {Container, ListItem, ListItem2, List3} from '../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {useSelector} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text} from 'react-native';

const MinusStock = ({route, navigation, getArticleList, getArticleFashion}) => {
  const {
    title,
    parentTitle,
    haveAdjusment,
    barcode,
    id,
    screenType,
    listType,
    second,
  } = route.params;
  navigation.setOptions({
    title: second ? second.name : title,
  });

  const article = useSelector(state => state.article);
  const userId = useSelector(state => state.auth.userLogin.id);
  const handleOnPress = sku => () => {
    navigation.navigate(screenNames.Product.Identity, {
      productId: sku,
      hideReason: !haveAdjusment,
      parentTitle: 'Directorate',
      barcode,
      screenType,
    });
  };

  useEffect(() => {
    (() => {
      getArticleAsync();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getArticleAsync = () => {
    if (parentTitle === 'FASHION') {
      getArticleFashion(title, second.code);
    } else {
      getArticleList(
        {directorate: parentTitle, artikel: title, screenType},
        userId,
        id,
      );
    }
  };

  const keyExtractor = (item, i) => i.toString();

  const unit =
    title === 'Minus Margin'
      ? '%'
      : title === 'Over Stock' ||
        title === 'Minus Stock' ||
        title === 'Stock Packaging & Bahan Baku' ||
        title === 'Yesterday TOP Sales'
      ? ''
      : 'pcs';

  const renderItem = ({item}) => {
    const isSubmited = article.submitedArtikel.includes(item.sku || item.SKU);
    return (
      <View>
        {listType === 'estimasi' ? (
          <ListItem2
            onPress={handleOnPress(item.sku || item.SKU)}
            header={item.sku || item.SKU}
            title={item.skudesc || item.SKUDESC}
            subTitle={`${(item.divdesc || item.DIVISION) &&
              (item.DIVISION || item.divdesc) + ','} ${item.CATEGORY ||
              item.catdesc} ${item.tag ? item.tag.toUpperCase() : ''}`}
            info="Estimasi Hari ini: 888"
            infoStyle={{
              backgroundColor: 'cyan',
              padding: 2,
              textAlign: 'center',
              fontWeight: 'bold',
              width: 150,
            }}
            infoDetail="Stock Gold : 888"
            infoDetailStyle={{
              backgroundColor: colors.BlazeOrange,
              padding: 2,
              textAlign: 'center',
              fontWeight: 'bold',
              width: 150,
            }}
          />
        ) : listType === 'shrinkage' ? (
          <List3 item={item} handleNavigateProductIdentity={handleOnPress} />
        ) : (
          <ListItem
            mark={isSubmited}
            markText={''}
            onPress={handleOnPress(item.sku || item.SKU)}
            header={item.sku || item.SKU}
            title={item.skudesc || item.SKUDESC}
            subTitle={`${(item.divdesc || item.DIVISION || item.division) &&
              (item.DIVISION || item.divdesc || item.division) +
                ','} ${item.CATEGORY || item.catdesc || item.category} ${
              item.tag ? item.tag.toUpperCase() : ''
            }`}
            info={
              title === 'SKU'
                ? ''
                : `${item.value || item.VALUE || item.value + unit}`
            }
          />
        )}
      </View>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getArticleAsync} refreshing={article.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <FlatList
        style={styles.padding10}
        data={article.articleList}
        ListEmptyComponent={<EmptyComponent loading={article.loading} />}
        refreshControl={refreshControl}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </Container>
  );
};

export default MinusStock;

const EmptyComponent = ({loading, name}) => (
  <View
    style={{
      height: 200,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View>
        <Text>List {name} Kosong</Text>
      </View>
    )}
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.Light, marginBottom: 10},
  padding10: {
    padding: 10,
  },
});
