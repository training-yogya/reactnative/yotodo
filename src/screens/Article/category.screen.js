import React from 'react';
import {useEffect} from 'react';
import {Container} from '../../components';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text} from 'react-native';
import {getMustSOCategories} from '../../redux/actions/product-action';

const ArticleCategory = ({route, navigation}) => {
  const {title, barcode, screenType} = route?.params;
  navigation.setOptions({
    title,
  });

  const dispatch = useDispatch();
  const product = useSelector(state => state.product);

  const handleOnPress = item => () => {
    navigation.navigate(screenNames.Article.Article, {
      title: title + ' - ' + item.category,
      id: `/${item.category_code}`,
      barcode: barcode,
      screenType,
    });
  };

  useEffect(() => {
    getproductAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getproductAsync = () => {
    dispatch(getMustSOCategories());
  };

  const keyExtractor = (item, i) => i.toString();

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={handleOnPress(item)}
        style={{
          backgroundColor: colors.White,
          padding: 20,
          borderBottomWidth: 0.6,
          borderBottomColor: colors.Silver,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{textTransform: 'uppercase', fontWeight: '500'}}>
          {item.category} - {item.division}
        </Text>
      </TouchableOpacity>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getproductAsync} refreshing={product.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <Text
        style={{
          fontWeight: '700',
          fontSize: 16,
          marginLeft: 10,
          marginTop: 10,
        }}>
        Choose Category
      </Text>
      <View style={{flex: 1}}>
        <FlatList
          style={styles.padding10}
          data={product?.psoCategory}
          ListEmptyComponent={<EmptyComponent loading={product.loading} />}
          refreshControl={refreshControl}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
        />
      </View>
    </Container>
  );
};

export default ArticleCategory;

const EmptyComponent = ({loading, name}) => (
  <View
    style={{
      height: 200,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View>
        <Text>List {name} Kosong</Text>
      </View>
    )}
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.White, marginBottom: 0},
  padding10: {
    padding: 10,
  },
});
