import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Modal,
  Dimensions,
} from 'react-native';
import {Container, AppButton, Loading} from '../../../components';
import colors from '../../../utils/colors';
import ImageViewer from 'react-native-image-zoom-viewer';

const height = Dimensions.get('screen').height;
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {ScrollView} from 'react-native-gesture-handler';
import globalStyles from '../../../styles/global';
import {screenNames} from '../../../utils/screens';
import config from '../../../config';
import {useDispatch, useSelector} from 'react-redux';
import {getPromoDetail} from '../../../redux/actions/promo-notif';
const PromoNotifDetail = props => {
  const {navigation, route} = props;
  const {title, detail} = route?.params;

  const dispatch = useDispatch();
  const promo = useSelector(state => state.promoNotif);

  const [modalPictureVisible, setModalPictureVisible] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);
  navigation.setOptions({
    title,
  });

  const isReadline = ['obral gede', 'fashion discount'].includes(
    title.toLowerCase(),
  );
  const ModalPicture = () => {
    return (
      <Modal
        onRequestClose={() => setModalPictureVisible(false)}
        visible={modalPictureVisible}
        transparent={true}
        animated={true}
        animationType={'slide'}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.7)',
            position: 'relative',
            height: '100%',
          }}>
          <TouchableOpacity
            onPress={() => setModalPictureVisible(false)}
            style={{
              justifyContent: 'center',
              height: 56,
              marginLeft: 20,
            }}>
            <MaterialIcon
              onPress={() => setModalPictureVisible(false)}
              name="close"
              color={colors.White}
              size={30}
            />
          </TouchableOpacity>
          <ImageViewer
            imageUrls={[
              {
                url: promo.listDetail?.[activeIndex]?.image_promo?.replace(
                  '/app/',
                  config.baseImage,
                ),
              },
            ]}
          />
        </View>
      </Modal>
    );
  };

  useEffect(() => {
    dispatch(getPromoDetail(detail.id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const slideLeft = () => {
    const promoListDetail = promo.listDetail;
    const length = promoListDetail.length;

    if (activeIndex > 0) {
      setActiveIndex(activeIndex - 1);
    } else if (activeIndex === 0) {
      setActiveIndex(length - 1);
    }
  };

  const slideRight = () => {
    const promoListDetail = promo.listDetail;
    const length = promoListDetail.length;

    if (activeIndex < length - 1) {
      setActiveIndex(activeIndex + 1);
    } else if (activeIndex === length - 1) {
      setActiveIndex(0);
    }
  };

  return (
    <Container>
      {promo.loading && <Loading />}
      <View style={{flex: 1}}>
        <TouchableOpacity onPress={() => setModalPictureVisible(true)}>
          <Image
            source={{
              uri: promo.listDetail?.[activeIndex]?.image_promo?.replace(
                '/app/',
                config.baseImage,
              ),
            }}
            style={{
              width: '100%',
              height: height / 1.7,
              resizeMode: 'center',
            }}
          />
        </TouchableOpacity>
        <View
          style={{
            borderColor: colors.Silver,
            borderWidth: 0.5,
            flex: 1,
            marginTop: 10,
            marginHorizontal: 5,
          }}>
          <Text style={{fontSize: 10}}>Term and Condition Details</Text>
          <ScrollView>
            <Text>{promo.listDetail?.[activeIndex]?.term_promo}</Text>
          </ScrollView>
        </View>
        {isReadline && (
          <View style={[globalStyles.center, {padding: 5}]}>
            <AppButton
              onPress={() => {
                navigation.navigate(screenNames.PromoNotif.Readline, {
                  parentTitle: title,
                  detail,
                });
              }}
              title={'Submit Readiness'}
              color={colors.BurningOrange}
            />
          </View>
        )}
      </View>
      <ModalPicture />
      <MaterialIcon
        name="chevron-left"
        size={62}
        onPress={slideLeft}
        style={{position: 'absolute', top: 200, left: 0}}
      />
      <MaterialIcon
        name="chevron-right"
        size={62}
        onPress={slideRight}
        style={{position: 'absolute', top: 200, right: 0}}
      />
    </Container>
  );
};

export default PromoNotifDetail;
