import React, {useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Text,
  RefreshControl,
} from 'react-native';
import {Container, AppButton} from '../../components';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {useDispatch, useSelector} from 'react-redux';
import {getPromo} from '../../redux/actions/promo-notif';
import config from '../../config';

const PromoNotif = props => {
  const {navigation, route} = props;
  const {title, khusus} = route?.params;
  const type = khusus ? 'jateng' : 'non';

  const dispatch = useDispatch();
  const promoNotif = useSelector(state => state.promoNotif);

  const keyExtractor = (item, index) => index.toString();
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          width: '50%',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 10,
        }}
        onPress={() => {
          navigation.navigate(screenNames.PromoNotif.Detail, {
            title: item.name_menu,
            detail: item,
          });
        }}>
        <Image
          source={{
            uri: item?.image_name?.replace('/app/', config.baseImage),
          }}
          style={{
            width: '90%',
            height: 200,
          }}
        />
      </TouchableOpacity>
    );
  };

  const refreshControl = (
    <RefreshControl
      onRefresh={() => dispatch(getPromo(type))}
      refreshing={promoNotif.loading}
    />
  );

  useEffect(() => {
    (() => {
      dispatch(getPromo(type));
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title,
  });
  return (
    <Container>
      <View style={{flex: 1}}>
        <FlatList
          refreshControl={refreshControl}
          data={khusus ? promoNotif.listKhusus : promoNotif.list}
          numColumns={2}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
      {!khusus && (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 10,
          }}>
          <AppButton
            title={'Khusus Jawa Tengah'}
            onPress={() => {
              navigation.push(screenNames.Home.PromoNotif, {
                title: 'Promo Khusus Jawa tengah',
                khusus: true,
              });
            }}
            color={colors.EarlsGreen}
          />
        </View>
      )}
    </Container>
  );
};

export default PromoNotif;
