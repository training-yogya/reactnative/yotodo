import React, {useEffect} from 'react';
import {useState} from 'react';
import {Container, AppButton, ModalInfo} from '../../components';
import {Text, View, Dimensions, TouchableHighlight, Image} from 'react-native';
import globalStyles from '../../styles/global';
import {FlatList} from 'react-native-gesture-handler';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {
  getPlanogramReason,
  submitPlanogramCannotImplementation,
} from '../../redux/actions/planogram-action';

const {width, height} = Dimensions.get('screen');

const PlanogramCannotImplementation = ({navigation}) => {
  const REASONS = useSelector(state => state.planogram.reason);
  const loading = useSelector(state => state.planogram.loading);
  const success = useSelector(state => state.planogram.success);
  const error = useSelector(state => state.planogram.error);
  const message = useSelector(state => state.planogram.message);

  const dispatch = useDispatch();

  const [reasons, setReason] = useState([]);
  const [imageCannotImplementation, setImageCannotImplementation] = useState(
    null,
  );
  const [modalVisible, setModalVisible] = useState(false);
  const openCamera = () => {
    return navigation.navigate(screenNames.Planogram.Implementation.Camera, {
      savePicture,
    });
  };
  const savePicture = picture => {
    setImageCannotImplementation(picture);
    navigation.goBack();
  };
  const handleReasonPress = reason => {
    const reasonList = [].concat(reasons);
    const index = reasonList.indexOf(reason);

    if (index > -1) {
      reasonList.splice(index, 1);
    } else {
      reasonList.push(reason);
    }

    setReason(reasonList);
  };
  const navigate = navigation.navigate;

  const handleSubmit = () => {
    dispatch(
      submitPlanogramCannotImplementation({
        reason_id: reasons,
        image: imageCannotImplementation,
      }),
    );
    setModalVisible(true);
  };

  const ModalNotif = React.useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        return navigate(screenNames.Planogram.RequestTrackingList);
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Berhasil disubmit"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message={message}
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [success, error, navigate, modalVisible, message]);

  useEffect(() => {
    (() => {
      dispatch(getPlanogramReason());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: 'Cannot Implementation',
  });

  return (
    <Container style={globalStyles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
        }}>
        <Text style={globalStyles.title}>Choose Reasons</Text>
        <View style={{height: 50, width: 200}}>
          <AppButton
            loading={loading}
            onPress={handleSubmit}
            title="Submit Reason and Planogram Request"
            titleStyle={{textAlign: 'center'}}
            color={colors.YellowOrange}
          />
        </View>
      </View>
      <View
        style={{
          backgroundColor: colors.Blue,
          paddingTop: 10,
          paddingBottom: 10,
          flex: 3,
        }}>
        <FlatList
          data={REASONS}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableHighlight
              onPress={() => handleReasonPress(item.id)}
              underlayColor={colors.Silver}
              style={[
                {
                  backgroundColor: colors.White,
                  marginHorizontal: 10,
                  marginBottom: 20,
                  minHeight: 60,
                  justifyContent: 'center',
                  paddingHorizontal: 5,
                  borderRadius: 4,
                },
                reasons.includes(item.id) && {
                  backgroundColor: colors.PeachOrange,
                },
              ]}>
              <Text>{item.name}</Text>
            </TouchableHighlight>
          )}
        />
      </View>
      <View
        style={{
          flex: 2,
          paddingVertical: 20,
        }}>
        <TouchableHighlight
          onPress={openCamera}
          underlayColor={colors.Silver}
          style={{
            height: '100%',
            backgroundColor: colors.White,
            borderWidth: 0.5,
            borderColor: colors.SilverChalice,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            overflow: 'hidden',
          }}>
          {imageCannotImplementation ? (
            <Image
              style={{height: '100%', width: '100%'}}
              source={{uri: imageCannotImplementation.uri}}
            />
          ) : (
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <MaterialIcon name="camera-alt" size={48} />
              <Text>Sertai dengan foto</Text>
            </View>
          )}
        </TouchableHighlight>
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default PlanogramCannotImplementation;
