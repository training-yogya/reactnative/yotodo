import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableHighlight,
  RefreshControl,
} from 'react-native';
import {Container} from '../../../components';
import globalStyles from '../../../styles/global';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {firebase} from '../../../services';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {setUnreadRooms} from '../../../redux/actions/chat-action';
import {TextInput} from 'react-native';
import {debounce} from 'lodash';

const CHATLIST = [{name: 'Global Chat'}];

const ChatRoom = props => {
  const {navigation} = props;
  const [loading, setLoading] = useState(false);
  const [channelList, setChannelList] = useState([]);
  const chatListLength = CHATLIST.length;

  const unReadRooms = useSelector(state => state.chat.unReadRooms);
  const diapatch = useDispatch();

  const handleChatClick = async chatItem => {
    const unReadRoomsStorage = await AsyncStorage.getItem('unReadRooms');

    const unReadRoomsArray = JSON.parse(unReadRoomsStorage) || [];

    const index = unReadRoomsArray?.findIndex(item => item === chatItem.name);

    unReadRoomsArray?.splice(index, 1);

    diapatch(setUnreadRooms(unReadRoomsArray));
    await AsyncStorage.setItem('unReadRooms', JSON.stringify(unReadRoomsArray));

    navigation.navigate(screenNames.YoChat.Room, {
      title: chatItem.name,
      chatRoom: chatItem,
      channelId: chatItem.id,
    });
  };

  const keyExtractor = (item, i) => i.toString();
  const renderItem = ({item, index}) => {
    return (
      <TouchableHighlight
        onPress={() => handleChatClick(item)}
        underlayColor={`${colors.Silver}aa`}>
        <View
          style={{
            flexDirection: 'row',
            padding: 10,
            borderBottomWidth: chatListLength === index + 1 ? 0 : 1,
            borderColor: `${colors.Silver}44`,
          }}>
          <MaterialIcon
            name="person"
            color={colors.Silver}
            style={{backgroundColor: `${colors.Silver}22`, borderRadius: 25}}
            size={52}
          />
          <View style={[globalStyles.center, {paddingHorizontal: 10}]}>
            <Text style={{color: `${colors.Black}aa`}}>{item.name}</Text>
          </View>
          <View style={{flex: 1}} />
          {unReadRooms?.includes(item.name) && (
            <View
              style={{
                width: 20,
                height: 20,
                borderRadius: 10,
                backgroundColor: colors.Limeade,
              }}
            />
          )}
        </View>
      </TouchableHighlight>
    );
  };

  const getChatRoomList = async () => {
    setLoading(true);
    firebase.getChannelList(channels => {
      setChannelList(channels);
      setLoading(false);
    });

    const unReadRoomsStorage = await AsyncStorage.getItem('unReadRooms');

    diapatch(setUnreadRooms(JSON.parse(unReadRoomsStorage)));
  };

  useEffect(() => {
    const bootstrapAsync = () => getChatRoomList();

    bootstrapAsync();
  }, []);

  navigation.setOptions({
    title: 'Yo Chat',
  });

  const refreshControl = (
    <RefreshControl onRefresh={getChatRoomList} refreshing={loading} />
  );

  const [searchText, setSearchText] = useState('');

  const [searching, setSearching] = useState(false);

  const [searchingChanels, setSearchChannels] = useState([]);

  const handleChangeSearch = text => {
    setSearchText(text);

    if (!searching) {
      setSearching(true);
    }

    handleSearch(text, channelList);
  };

  const handleSearch = React.useCallback(
    debounce((text, list) => {
      getSearch(text, list).then(channelsListSearch => {
        setSearchChannels(channelsListSearch);
        setSearching(false);
      });
    }, 100),
    [],
  );

  const getSearch = (text, list) => {
    text = text.toLowerCase();

    const newList = list.filter(channel => {
      const name = channel.name.toLowerCase();
      // const notesText = notes.notes.toLowerCase();

      return name.includes(text); /*|| notesText.includes(text);*/
    });

    return Promise.resolve(newList);
  };

  return (
    <Container>
      <View
        style={{
          margin: 10,
          flexDirection: 'row',
          borderWidth: 0.5,
          borderRadius: 20,
          paddingHorizontal: 10,
          borderColor: colors.Silver,
          alignItems: 'center',
        }}>
        <MaterialIcon color={colors.SilverChalice} name={'search'} size={25} />
        <View style={{flex: 1, height: 42}}>
          <TextInput
            value={searchText}
            onChangeText={handleChangeSearch}
            style={{paddingHorizontal: 10, height: 42}}
            placeholder={'Search Title'}
          />
        </View>
      </View>
      <FlatList
        refreshControl={refreshControl}
        data={searchText.length > 0 ? searchingChanels : channelList}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </Container>
  );
};

export default ChatRoom;
