import React from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import {connect} from 'react-redux';

import {Container} from './../../../components';
import globalStyles from './../../../styles/global';

import SlackMessage from './components/Messages';
import {firebase} from '../../../services';
import {sendNotif} from '../../../redux/actions/chat-action';

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
    };

    this.channelId = props.route?.params?.channelId;
    this.title = props.route?.params?.title ?? 'Chat Room';
    props.navigation.setOptions({
      title: this.title,
    });
  }

  componentDidMount() {
    firebase.enterChannel(this.channelId, {numMaxMessages: 100}, result => {
      const newMessages = result.messages.message;
      this.setState(prevState => ({
        messages: GiftedChat.append(prevState.messages, newMessages[0]),
      }));
    });
  }
  componentWillUnmount() {
    firebase.leaveChannel(this.channelId);
  }

  get user() {
    return {
      name: this.props.userLogin.full_name,
      email: this.props.userLogin.username,
      initial_store: this.props.userLogin.initial_store,
      avatar: `https://ui-avatars.com/api/?name=${this.props.userLogin.full_name.replace(
        ' ',
        '+',
      )}`,
      _id: firebase.user().uid,
    };
  }

  renderMessage(props) {
    const {
      currentMessage: {text: currText},
    } = props;

    let messageTextStyle;

    return <SlackMessage {...props} messageTextStyle={messageTextStyle} />;
  }

  sendMessage = messageContent => {
    firebase.sendMessage(
      {
        messageContent,
        messageType: 'default',
        user: this.user,
      },
      this.channelId,
    );
    const bodyNotif = `${messageContent[0].user.name}: ${
      messageContent[0].text
    }`;
    this.props.sendNotif(this.title, bodyNotif);
  };

  render() {
    return (
      <Container style={globalStyles.container}>
        <GiftedChat
          messages={this.state.messages}
          onSend={this.sendMessage}
          user={this.user}
          renderMessage={this.renderMessage}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  userLogin: state.auth.userLogin,
});

const mapDispatchToProps = dispatch => ({
  sendNotif: (title, body) => dispatch(sendNotif(title, body)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);
