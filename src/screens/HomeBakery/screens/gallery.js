import React, {useState, useEffect} from 'react';
import {Container, Loading} from '../../../components';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import colors from '../../../utils/colors';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import globalStyles from '../../../styles/global';
import {useDispatch, useSelector} from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';

import moment from 'moment';
import {getGalleryBakery} from '../../../redux/actions/article-action';

const Readline = ({navigation, route}) => {
  const {title, menu} = route?.params;
  const dispatch = useDispatch();
  const article = useSelector(state => state.article);
  const [filterDate, setFilterDate] = useState(() =>
    moment().format('DD/MM/YYYY'),
  );
  const [date, setDate] = useState(new Date());

  const [showDate, setShowDate] = useState(false);

  const keyExtractor = (item, i) => i.toString();

  navigation.setOptions({
    title: title,
  });

  const handleOpenDate = () => {
    setShowDate(true);
  };

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShowDate(false);
    setDate(selectedDate);
    setFilterDate(moment(currentDate).format('DD/MM/YYYY'));

    getGallery(moment(currentDate).format('DD/MM/YYYY'));
  };

  const getGallery = date => {
    dispatch(getGalleryBakery(date || filterDate, menu));
  };

  useEffect(() => {
    getGallery();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container style={globalStyles.container}>
      <Text>Search Date Gallery</Text>
      <View style={{flexDirection: 'row'}}>
        <TextInput
          style={{flex: 1, height: 56}}
          placeholder={'DD/MM/YYYY'}
          value={filterDate}
          editable={false}
        />
        <MaterialIcon
          onPress={handleOpenDate}
          color={colors.PeachOrange}
          name="date-range"
          size={32}
        />
      </View>

      {article.loading ? (
        <Loading />
      ) : (
        <FlatList
          style={{flex: 1}}
          data={article.gallery}
          keyExtractor={keyExtractor}
          ListEmptyComponent={<Text>NO DATA TO SHOW</Text>}
          renderItem={({item}) => {
            return (
              <TouchableOpacity
                delayPressIn={25}
                style={{
                  width: Dimensions.get('window').width - 20,
                  height: Dimensions.get('window').height / 1.5,
                }}>
                <Image
                  source={{uri: item.image, cache: true}}
                  style={{
                    width: Dimensions.get('window').width - 20,
                    height: Dimensions.get('window').height / 1.5,
                  }}
                />
                <Text
                  style={{
                    color: colors.YellowOrange,
                    position: 'absolute',
                    bottom: 0,
                    right: 0,
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  {item.createdAt}
                </Text>
              </TouchableOpacity>
            );
          }}
        />
      )}
      {showDate && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={'date'}
          is24Hour={true}
          display="default"
          onChange={onChangeDate}
        />
      )}
    </Container>
  );
};

export default Readline;
