import React, {useState, useMemo} from 'react';
import {Container, AppButton, ModalInfo, Loading} from '../../../components';
import {
  View,
  TouchableHighlight,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import globalStyles from '../../../styles/global';
import {useDispatch, useSelector} from 'react-redux';
import {uploadPhotoBakery} from '../../../redux/actions/article-action';

const Readline = ({navigation, route}) => {
  const {menu, title} = route?.params;
  const {navigate} = navigation;
  const dispatch = useDispatch();
  const article = useSelector(state => state.article);
  const loading = article.loading;
  const success = article.success;
  const error = article.error;
  const [images, setImages] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const openCamera = () => {
    return navigation.navigate(screenNames.HomeBakery.Camera, {
      savePicture,
    });
  };
  const savePicture = picture => {
    const imageArray = images.slice();
    imageArray.push(picture);
    setImages(imageArray);
    navigation.goBack();
  };
  const handleDeleteImage = index => {
    const imageArray = images.slice();
    imageArray.splice(index, 1);
    setImages(imageArray);
  };
  const keyExtractor = (item, i) => i.toString();
  const renderItem = ({item, index}) => {
    return (
      <View
        style={{marginRight: 10, overflow: 'visible', position: 'relative'}}>
        <Image
          source={{uri: item.uri}}
          style={{width: 100, height: 100, borderRadius: 10}}
        />
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            backgroundColor: colors.Red,
            height: 30,
            width: 30,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 15,
          }}
          onPress={() => handleDeleteImage(index)}>
          <MaterialIcon color={colors.White} size={26} name="close" />
        </TouchableOpacity>
      </View>
    );
  };

  const ModalNotif = useMemo(() => {
    const handleModalShow = () => {
      if (!error) {
        setImages([]);
        return navigate(screenNames.HomeBakery.Gallery, {
          title: title.replace('Images', 'Gallery'),
          menu,
        });
      }
    };
    if (success) {
      return (
        <ModalInfo
          title="Success"
          message="Submit Image Selesai"
          icon="success"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    } else if (error) {
      return (
        <ModalInfo
          title="Error"
          message="Error Submit Image"
          icon="error"
          visible={modalVisible}
          setVisible={setModalVisible}
          onShow={handleModalShow}
        />
      );
    }
  }, [modalVisible, navigate, success, error, title, menu]);

  const submitReadline = () => {
    setModalVisible(true);
    dispatch(uploadPhotoBakery(images, menu));
  };

  const navigateGallery = () => {
    navigation.navigate(screenNames.HomeBakery.Gallery, {
      title: title.replace('Images', 'Gallery'),
      menu,
    });
  };

  navigation.setOptions({
    title,
  });

  return (
    <Container>
      {loading && <Loading />}
      <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 16}}>
        Do it every day before 10 o'clock
      </Text>
      <View
        style={{
          flex: 2,
          padding: 10,
          paddingVertical: 20,
        }}>
        <TouchableHighlight
          onPress={openCamera}
          underlayColor={colors.Silver}
          style={{
            height: '100%',
            backgroundColor: colors.White,
            borderWidth: 0.5,
            borderColor: colors.SilverChalice,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            overflow: 'hidden',
          }}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <MaterialIcon name="camera-alt" size={48} />
            <Text style={{textAlign: 'center'}}>
              {'Take 10 picture of current Display per\nBrand or Display Area'}
            </Text>
          </View>
        </TouchableHighlight>
      </View>
      <View
        style={{
          height: 'auto',
          borderWidth: 0.7,
          padding: 10,
          overflow: 'visible',
        }}>
        <Text>Preview:</Text>
        <FlatList
          style={{overflow: 'visible'}}
          data={images}
          horizontal={true}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
        <Text style={{color: images.length < 10 ? colors.Red : colors.Blue}}>
          Count: {images.length}
        </Text>
      </View>
      <View style={[globalStyles.center, {padding: 5}]}>
        <AppButton
          onPress={submitReadline}
          title={'Submit'}
          disabled={false}
          // color={colors.BurningOrange}
          color={false ? colors.Silver : colors.BurningOrange}
        />
      </View>
      <View style={[{padding: 5}]}>
        <AppButton
          onPress={navigateGallery}
          title={'Foto Gallery'}
          style={{height: 50}}
          // color={colors.BurningOrange}
          color={colors.SeaGreen}
        />
      </View>
      <>{ModalNotif}</>
    </Container>
  );
};

export default Readline;
