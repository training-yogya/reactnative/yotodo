import * as React from 'react';
import {useEffect} from 'react';
import {Container} from '../../components';
import {View, Text, TouchableOpacity} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import globalStyles from '../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../utils/colors';
import {screenNames} from '../../utils/screens';
import {getInsightHeader} from '../../redux/actions/insight-info-action';

const InsightInfo = ({route, navigation}) => {
  const dispatch = useDispatch();
  const insightData = useSelector(state => state.insightInfo);
  const header = insightData.header;

  const handleOnPress = item => () => {
    if (item.type === 'webview') {
      return navigation.navigate(screenNames.WebView, {
        title: item.name,
        uri: item.link,
      });
    } else if (item.type === 'todo') {
      return navigation.navigate(screenNames.InsightInfo.InsightDirectorate, {
        title: 'Todo Insight',
      });
    } else if (item.type === 'planogram') {
      return navigation.navigate(screenNames.InsightInfo.InsightDivision, {
        title: 'Planogram Insight',
        directorateCode: 'planogram',
      });
    }
  };

  useEffect(() => {
    (() => {
      dispatch(getInsightHeader());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const title = route.params?.title ?? 'insight & info';
  navigation.setOptions({
    title,
  });
  return (
    <Container style={globalStyles.container}>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={handleOnPress({type: 'todo'})}
          style={{
            backgroundColor: 'rgba(255, 104, 3, 0.3)',
            padding: 5,
            flex: 1,
            minHeight: 140,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View
            style={{
              position: 'relative',
              alignItems: 'flex-end',
            }}>
            <View>
              <View style={{height: 90}}>
                <Text style={{fontWeight: 'bold'}}>
                  Summary : {header.summary_yotodo} Reasons
                </Text>
              </View>
            </View>
          </View>
          <View style={{alignItems: 'flex-start'}}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {'Summary of\nall ToDo'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleOnPress({type: 'planogram'})}
          style={{
            backgroundColor: `${'rgba(48, 172, 255, 0.3)'}`,
            padding: 5,
            flex: 1,
            minHeight: 140,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View>
            <MaterialIcon size={80} color={colors.DodgerBlue} name={'cube'} />
          </View>
          <View style={{alignItems: 'flex-end'}}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {'Summary of all Planogram'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={handleOnPress({
            type: 'webview',
            name: 'Info Yogya Group\n@ instagram',
            link: header.instagram,
          })}
          style={{
            backgroundColor: 'rgba(255, 104, 3, 0.3)',
            padding: 5,
            flex: 1,
            minHeight: 140,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View
            style={{
              position: 'relative',
              alignItems: 'flex-end',
            }}>
            <View>
              <MaterialIcon
                size={80}
                color={colors.BlazeOrange}
                name={'instagram'}
              />
            </View>
          </View>
          <View style={{alignItems: 'flex-start'}}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {'Info Yogya Group\n@ instagram'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleOnPress({
            type: 'webview',
            name: 'Info Yogya Group\n@ facebook',
            link: header.facebook,
          })}
          style={{
            backgroundColor: `${'rgba(48, 172, 255, 0.3)'}`,
            padding: 5,
            flex: 1,
            minHeight: 140,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View>
            <MaterialIcon
              size={80}
              color={colors.DodgerBlue}
              name={'facebook'}
            />
          </View>
          <View style={{alignItems: 'flex-end'}}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {'Info Yogya Group\n@ facebook'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={handleOnPress({
            type: 'webview',
            name: 'Info Catalog\nPromo Kompetitor',
            link: header.kompetitor,
          })}
          style={{
            backgroundColor: 'rgba(255, 104, 3, 0.3)',
            padding: 5,
            flex: 1,
            minHeight: 140,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View
            style={{
              position: 'relative',
              alignItems: 'flex-end',
            }}>
            <View>
              <MaterialIcon
                size={80}
                color={colors.BlazeOrange}
                name={'cart-outline'}
              />
            </View>
          </View>
          <View style={{alignItems: 'flex-start'}}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {'Info Catalog\nPromo Kompetitor'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleOnPress({
            type: 'webview',
            name: 'Info Prakiraan\ncuaca',
            link: header.weather,
          })}
          style={{
            backgroundColor: `${'rgba(48, 172, 255, 0.3)'}`,
            padding: 5,
            flex: 1,
            minHeight: 140,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View>
            <MaterialIcon
              size={80}
              color={colors.DodgerBlue}
              name={'weather-partlycloudy'}
            />
          </View>
          <View style={{alignItems: 'flex-end'}}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {'Info Prakiraan\ncuaca'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

export default InsightInfo;
