import * as React from 'react';
import {Container, AppButton, Loading} from '../../../components';
import {View, Text, Dimensions, Platform} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import globalStyles from '../../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {BarChart, LineChart} from 'react-native-chart-kit';
import {ScrollView} from 'react-native-gesture-handler';
import {insightDetail} from '../../../redux/actions/insight-info-action';
import {Modal, TouchableOpacity} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

import moment from 'moment';
const width = Dimensions.get('window').width;

const InsightDetail = ({route, navigation}) => {
  const title = route.params?.title ?? 'insight & info';
  const today = route.params?.today ?? 0;
  const ytd = route.params?.ytd ?? 0;
  const dispatch = useDispatch();
  const insightData = useSelector(state => state.insightInfo);
  const loading = insightData.loading;

  const isPlanogram = insightData.directorateName === 'planogram';

  const [modalData, setModalData] = React.useState({
    visible: false,
    title: '',
    description: '',
  });

  const [endDate, setEndDate] = React.useState(new Date());
  const [startDate, setStartDate] = React.useState(
    new Date().setMonth(endDate.getMonth() - 1),
  );
  const [showStartDate, setShowStartDate] = React.useState(false);
  const [showEndDate, setShowEndDate] = React.useState(false);

  const [mode, setMode] = React.useState('date');

  const capitalize = (str, lower = false) =>
    (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, match =>
      match.toUpperCase(),
    );
  const handleOnPress = () => {
    const screenName =
      screenNames.Home[capitalize(insightData.directorateName)];
    return navigation.navigate(screenName, {
      screen: screenName,
      params: {title: capitalize(insightData.directorateName)},
    });
  };

  React.useEffect(() => {
    (() => {
      dispatch(insightDetail(title, startDate, endDate));
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title,
  });

  const onChangeStartDate = (event, selectedDate) => {
    const currentDate = selectedDate || startDate;
    setShowStartDate(Platform.OS === 'ios');
    setStartDate(currentDate);
    dispatch(insightDetail(title, currentDate, endDate));
  };

  const showModeStartDate = currentMode => {
    setShowStartDate(true);
    setMode(currentMode);
  };

  const showDatepickerStartDate = () => {
    showModeStartDate('date');
  };

  const onChangeEndDate = (event, selectedDate) => {
    const currentDate = selectedDate || endDate;
    setShowEndDate(Platform.OS === 'ios');
    setEndDate(currentDate);

    dispatch(insightDetail(title, startDate, currentDate));
  };

  const showModeEndDate = currentMode => {
    setShowEndDate(true);
    setMode(currentMode);
  };

  const showDatepickerEndDate = () => {
    showModeEndDate('date');
  };

  return (
    <Container style={globalStyles.container}>
      {loading && <Loading />}
      <ScrollView>
        {!isPlanogram && (
          <>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={{fontWeight: 'bold', textAlignVertical: 'center'}}>
                Number of PLU per day Trend
              </Text>
              <AppButton
                title={'Article\nList'}
                titleStyle={{textAlign: 'center'}}
                style={{width: 100}}
                onPress={handleOnPress}
                color={colors.BlazeOrange}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                height: 56,
              }}>
              <TouchableOpacity>
                <View>
                  <Text style={{textAlign: 'center'}}>Start Date</Text>
                  <Text
                    style={{fontWeight: 'bold', borderWidth: 1, padding: 5}}
                    onPress={showDatepickerStartDate}>
                    {moment(startDate).format('DD-MM-YYYY')}
                    &nbsp; &#9660;
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View>
                  <Text style={{textAlign: 'center'}}>End Date</Text>
                  <Text
                    style={{fontWeight: 'bold', borderWidth: 1, padding: 5}}
                    onPress={showDatepickerEndDate}>
                    {moment(endDate).format('DD-MM-YYYY')}
                    &nbsp; &#9660;
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </>
        )}
        <View style={{overflow: 'hidden'}}>
          <View
            style={{
              paddingTop: 10,
              position: 'relative',
            }}>
            {insightData?.lineChart?.label?.length > 0 ? (
              <>
                <LineChart
                  bezier
                  onDataPointClick={data => {
                    setModalData({
                      visible: true,
                      title: insightData?.lineChart?.label[data.index],
                      description: data.value,
                    });
                  }}
                  style={{
                    backgroundColor: 'white',
                  }}
                  data={{
                    labels: insightData?.lineChart?.label ?? [0],
                    datasets: [
                      {
                        data: insightData?.lineChart?.value ?? [0],
                      },
                    ],
                  }}
                  width={width * 0.9}
                  height={400}
                  xLabelsOffset={0}
                  yLabelsOffset={5}
                  fromZero={false}
                  withVerticalLabels={false}
                  chartConfig={{
                    backgroundGradientFrom: '#ffffff',
                    backgroundGradientFromOpacity: 1,
                    backgroundGradientTo: '#ffffff',
                    backgroundGradientToOpacity: 1,
                    fillShadowGradientOpacity: 1,
                    color: (opacity = 1, index = 0) => colors.OrangPeel,
                    strokeWidth: 1, // optional, default 3
                    barPercentage: 1,
                    labelColor: () => colors.Black,
                    useShadowColorFromDataset: false,
                    linejoinType: 'square',
                    propsForLabels: {
                      fontWeight: 'bold',
                    },
                  }}
                />
                <Modal
                  visible={modalData.visible}
                  transparent={true}
                  onRequestClose={() => setModalData({visible: false})}>
                  <TouchableOpacity
                    onPress={() => setModalData({visible: false})}
                    style={{
                      backgroundColor: 'rgba(0,0,0,0.3)',
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        backgroundColor: colors.White,
                        width: width * 0.5,
                        padding: 10,
                        borderRadius: 10,
                      }}>
                      <Text style={{fontWeight: 'bold', fontSize: 16}}>
                        {modalData.description} PLU
                      </Text>
                      <Text>
                        {moment(modalData.title).format('DD MMMM YYYY')}
                      </Text>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontWeight: 'bold',
                          color: colors.OrangPeel,
                          marginTop: 10,
                        }}
                        onPress={() => setModalData({visible: false})}>
                        Close
                      </Text>
                    </View>
                  </TouchableOpacity>
                </Modal>
              </>
            ) : (
              <View
                style={{
                  height: 200,
                  justifyContent: 'flex-end',
                  paddingBottom: 70,
                  alignItems: 'center',
                  marginBottom: 30,
                }}>
                <MaterialIcon name="chart-line-variant" size={75} />
                <Text>Tidak ada data untuk ditampilkan</Text>
              </View>
            )}
          </View>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontWeight: 'bold', fontSize: 20, width: 200}}>
                Today
              </Text>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                : {today} Reasons
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontWeight: 'bold', fontSize: 20, width: 200}}>
                YTD
              </Text>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                : {ytd} Reasons
              </Text>
            </View>
          </View>

          <View
            style={{
              marginTop: 20,
              paddingTop: 10,
            }}>
            <BarChart
              showValuesOnTopOfBars={true}
              style={{
                backgroundColor: 'white',
              }}
              fromZero={true}
              data={{
                labels: insightData?.reason?.label?.map(item => item.code) ?? [
                  0,
                ],
                datasets: [
                  {
                    data: insightData?.reason?.value ?? [0],
                  },
                ],
              }}
              width={width}
              height={300}
              yAxisLabel=""
              chartConfig={{
                backgroundGradientFrom: '#ffffff',
                backgroundGradientFromOpacity: 1,
                backgroundGradientTo: '#ffffff',
                backgroundGradientToOpacity: 1,
                fillShadowGradientOpacity: 1,
                color: (opacity = 1, index = 0) => colors.OrangPeel,
                strokeWidth: 6, // optional, default 3
                barPercentage: 1,
                labelColor: () => colors.Black,
                useShadowColorFromDataset: false,
                propsForLabels: {
                  fontWeight: 'bold',
                },
              }}
            />
            <View
              style={{
                marginTop: -20,
                marginBottom: 20,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  backgroundColor: colors.OrangPeel,
                }}
              />
              <Text>Reasons</Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          {insightData.reason?.label?.map((item, index) => (
            <View style={{flexDirection: 'row'}} key={index.toString()}>
              <Text style={{fontWeight: 'normal', fontSize: 16, width: 20}}>
                {item.code}
              </Text>
              <Text style={{fontWeight: 'normal', fontSize: 16}}>
                : {item.description} ({item.value})
              </Text>
            </View>
          ))}
        </View>
      </ScrollView>
      {showStartDate && (
        <DateTimePicker
          testID="dateTimePicker"
          value={startDate}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChangeStartDate}
        />
      )}
      {showEndDate && (
        <DateTimePicker
          testID="dateTimePicker"
          value={endDate}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChangeEndDate}
        />
      )}
    </Container>
  );
};

export default InsightDetail;
