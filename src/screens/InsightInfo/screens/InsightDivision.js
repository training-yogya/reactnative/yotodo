import * as React from 'react';
import {useEffect, useState} from 'react';
import {Container, Loading, AppButton, MenuItem} from '../../../components';
import {View, Text, TouchableOpacity} from 'react-native';
import globalStyles from '../../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {FlatList} from 'react-native-gesture-handler';
import {getArticleMenu} from '../../../redux/actions/insight-info-action';
import {iconFinder} from '../../../utils/icons';

const InsightDivision = ({route, navigation}) => {
  const dispatch = useDispatch();
  const insightData = useSelector(state => state.insightInfo);
  const loading = insightData.loading;
  const menuItems = insightData.menuDivision;

  const directorateName = insightData.directorateName;

  const {title, type, directorateCode} = route.params;
  const handleNavigate = screenTitle => {
    if (directorateName === 'planogram') {
      return navigation.navigate(screenNames.InsightInfo.InsightPlanogram, {
        title: screenTitle.name,
        count: screenTitle.count,
      });
    } else {
      return navigation.navigate(screenNames.InsightInfo.InsightDetail, {
        title: screenTitle.menu_name,
        today: screenTitle.count_today,
        ytd: screenTitle.count_this_year,
      });
    }
  };
  const keyExtractor = (item, index) => index.toString();
  const renderItem = ({item, index}) => {
    if (item.planogram) {
      return (
        <View
          style={{
            justifyContent: 'space-between',
            flex: 1,
            paddingVertical: 10,
            paddingHorizontal: 10,
          }}>
          <AppButton
            style={{height: 50}}
            titleStyle={{
              textAlign: 'center',
              fontSize: 12,
            }}
            onPress={() => {
              navigation.navigate(screenNames.Planogram.Home, {
                screen: screenNames.Planogram.Home,
                params: {title: 'Planogram'},
              });
            }}
            title={'Planogram'}
            color={colors.RegentStBlue}
          />
          <AppButton
            style={{height: 50}}
            titleStyle={{
              textAlign: 'center',
              fontSize: 12,
            }}
            onPress={() => {
              navigation.navigate(screenNames.Planogram.Home, {
                screen: screenNames.Planogram.Tutorial,
                params: {title: 'Planogram Tutorial'},
              });
            }}
            title={'Planogram Tutorial'}
            color={colors.DodgerBlue}
          />
        </View>
      );
    } else {
      const [icon, iconType] = iconFinder(item.name || '');
      return (
        <TouchableOpacity
          onPress={() => handleNavigate(item)}
          style={{
            backgroundColor: `${
              item.planogram
                ? 'transparent'
                : index % 2 === 0
                ? 'rgba(255, 104, 3, 0.3)'
                : 'rgba(48, 172, 255, 0.3)'
            }`,
            padding: 10,
            flex: 1,
            borderRadius: 10,
            marginHorizontal: 10,
            marginVertical: 10,
          }}>
          <View
            style={{
              alignItems:
                directorateName === 'planogram'
                  ? 'center'
                  : index % 2 === 0
                  ? 'flex-end'
                  : 'flex-start',
              position: 'relative',
            }}>
            {item.iconBig !== undefined ? (
              <View />
            ) : directorateName === 'planogram' ? (
              <MenuItem
                icon={icon}
                onPress={() => handleNavigate(item)}
                iconType={iconType}
                iconSize={72}
                height={90}
                backgroundColor={'transparent'}
                iconColor={
                  index % 2 === 0
                    ? 'rgba(255, 104, 3, 0.3)'
                    : 'rgba(48, 172, 255, 0.3)'
                }
                dark={true}
              />
            ) : (
              <View>
                <Text
                  style={{
                    fontWeight: 'bold',
                  }}>
                  Today : {item.count_today}
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                  }}>
                  YTD : {item.count_this_year}
                </Text>
              </View>
            )}
          </View>
          <View
            style={{
              alignItems:
                directorateName === 'planogram'
                  ? 'center'
                  : index % 2 === 0
                  ? 'flex-start'
                  : 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <Text style={{color: 'rgba(0,0,0,0.6)'}}>
              {item.name || item.menu_name}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  };

  useEffect(() => {
    (() => {
      dispatch(getArticleMenu(directorateCode));
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title,
  });
  return (
    <Container style={globalStyles.container}>
      {loading && <Loading />}
      <FlatList
        data={
          directorateName === 'planogram'
            ? menuItems.concat([{planogram: true}])
            : menuItems
        }
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        numColumns={2}
        ListEmptyComponent={
          <Text style={{textAlign: 'center'}}>
            Tidak ada data untuk ditampilkan
          </Text>
        }
      />
    </Container>
  );
};

export default InsightDivision;
