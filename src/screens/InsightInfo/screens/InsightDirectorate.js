import * as React from 'react';
import {useEffect} from 'react';
import {Container, Loading, CardInsight} from '../../../components';
import {View, Picker} from 'react-native';
import globalStyles from '../../../styles/global';
import {useSelector, useDispatch} from 'react-redux';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {getInsightDirectorate} from '../../../redux/actions/insight-info-action';

const InsightDirectorate = ({route, navigation}) => {
  const dispatch = useDispatch();
  const insightData = useSelector(state => state.insightInfo);
  const loading = insightData.loading;
  const todo = insightData.insightTodo;

  const handleOnPress = screenTitle => {
    if (screenTitle.name === 'Compare With\nOther Store') {
      return navigation.push(screenNames.InsightInfo.InsightDirectorate, {
        title: screenTitle.name,
        type: 'compare',
      });
    }

    return navigation.navigate(screenNames.InsightInfo.InsightDivision, {
      title: screenTitle.name,
      type: 'planogram',
      directorateCode: screenTitle.directorateType,
    });
  };

  useEffect(() => {
    dispatch(getInsightDirectorate(route.params?.title));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const title = route.params?.title ?? 'insight & info';
  const isCompare = route.params?.type === 'compare';
  navigation.setOptions({
    title,
  });
  return (
    <Container style={globalStyles.container}>
      {loading && <Loading />}
      {isCompare && (
        <View
          style={{
            borderWidth: 0.5,
            borderColor: colors.BlueBell,
            borderRadius: 20,
            paddingHorizontal: 10,
          }}>
          <Picker>
            <Picker.Item key={1} label={'Choose Store'} value={null} />
          </Picker>
        </View>
      )}
      <View style={{flexDirection: 'row'}}>
        <CardInsight
          name={'Summary of\nSupermarket'}
          handleOnPress={() =>
            handleOnPress({
              name: 'Summary of\nSupermarket',
              directorateType: 'supermarket',
            })
          }
          index={2}
          today={todo.summary_supermarket}
          year={todo.summary_supermarket_year}
          icon={'cart-outline'}
        />
        <CardInsight
          name={'Summary of\nFashion'}
          handleOnPress={() =>
            handleOnPress({
              name: 'Summary of\nFashion',
              directorateType: 'fashion',
            })
          }
          index={1}
          today={todo.summary_fashion}
          year={todo.summary_fashion_year}
          icon={'shopping'}
        />
      </View>
      <View style={{flexDirection: 'row'}}>
        <CardInsight
          name={'Summary of\nBakery'}
          handleOnPress={() =>
            handleOnPress({
              name: 'Summary of\nBakery',
              directorateType: 'home bakery',
            })
          }
          index={2}
          today={todo.summary_homebakery}
          year={todo.summary_homebakery_year}
          icon={'bread-slice-outline'}
        />
        <CardInsight
          name={'Summary of\nFood Station'}
          handleOnPress={() =>
            handleOnPress({
              name: 'Summary of\nFood Station',
              directorateType: 'food station',
            })
          }
          index={1}
          today={todo.summary_foodstation}
          year={todo.summary_foodstation_year}
          icon={'silverware-spoon'}
        />
      </View>
      <View style={{flexDirection: 'row'}}>
        <CardInsight
          name={'Summary of\nYogya Electronic'}
          handleOnPress={() =>
            handleOnPress({
              name: 'Summary of\nYogya Electronic',
              directorateType: 'yo-el',
            })
          }
          index={2}
          today={todo.summary_yoel}
          year={todo.summary_yoel_year}
          icon={'cctv'}
        />
      </View>
    </Container>
  );
};

export default InsightDirectorate;
