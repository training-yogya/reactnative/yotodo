import * as React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import config from '../../config';
import globalStyles from '../../styles/global';
import client from '../../utils/customAxios';

export const InsightDetailPlanogram = ({route, navigation}) => {
  const [data, setData] = React.useState({});

  const getDetail = async () => {
    try {
      const response = await client.get(
        '/planogram/detail/' + route?.params?.id,
      );
      setData(response.data?.data?.[0]);
    } catch (error) {
    }
  };

  React.useEffect(() => {
    getDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: 'Preview',
  });
  return (
    <View style={[globalStyles.container]}>
      <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
        Created by: {data?.users?.[0]?.fullname}
      </Text>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 20,
          }}>
          <Text>Before</Text>
          <Image
            source={{
              uri: data?.image_before?.replace('/app/', config.baseImage),
            }}
            style={{
              width: 300,
              height: 300,
            }}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text>After</Text>
          <Image
            source={{
              uri: data?.image_after?.replace('/app/', config.baseImage),
            }}
            style={{
              width: 300,
              height: 300,
            }}
          />
        </View>
      </ScrollView>
    </View>
  );
};
