import React, {useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {Container, AppButton} from '../../components';
import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import moment from 'moment';
import {useDispatch, useSelector} from 'react-redux';
import {getPlanogramInsight} from '../../redux/actions/planogram-action';
import {screenNames} from '../../utils/screens';

const PlanogramInsight = props => {
  const {navigation, route} = props;
  const dispatch = useDispatch();
  const planogram = useSelector(state => state.planogram);
  const countCategory = planogram.categoryList.length;
  const countApproved = planogram.plogramInsightList.filter(
    a => a.status_sdd === 'Approved',
  ).length;
  const handleNavigatePlanogram = () => {
    navigation.navigate(screenNames.Home.Planogram, {
      screen: screenNames.Planogram.List,
      params: {
        title: route?.params?.title.replace('Planogram ', ''),
      },
    });
  };
  const handleOnpress = item => () => {
    if (item.status_sdd === 'Approved') {
      navigation.navigate(screenNames.Planogram.InsightDetail, {
        title: item.categories,
        id: item.id,
      });
      return;
    }

    if (item.id_lama) {
      return;
    }

    if (item.status_sdd !== 'Rejected') {
      return;
    }

    dispatch({
      type: 'PLANOGRAM_SET_ID_LAMA',
      payload: item.id,
    });
    navigation.navigate(screenNames.Home.Planogram, {
      screen: screenNames.Planogram.Details,
      params: {
        title: item.categories,
      },
    });
  };
  const keyExtractor = (item, index) => index.toString();
  const renderItem = ({item, index}) => {
    const isReject = item.status_sdd === 'Rejected';
    return (
      <TouchableOpacity
        delayPressIn={50}
        onPress={handleOnpress(item)}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: colors.White,
          marginTop: 10,
          marginHorizontal: 10,
          borderRadius: 20,
          padding: 15,
        }}>
        <View style={{flex: 5}}>
          <Text style={{fontWeight: 'bold'}}>{item.categories}</Text>
          <Text style={{color: colors.Persimmon}}>
            {item.implement_status || 'finished'}
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flex: 3}}>
              <Text>{moment(item.created_at).format('DD MMMM YYYY')}</Text>
            </View>
            <View style={{flex: 4}}>
              <Text>{Math.ceil(item.time / 60 / 1000)} Minutes</Text>
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 2,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text style={{color: isReject ? colors.Red : colors.EarlsGreen}}>
            {item.status_sdd}
          </Text>
          <Text
            style={{color: colors.Silver, textAlign: 'center', fontSize: 11}}>
            {item.reason_name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const getPlanogramInsightList = () => {
    const division = route?.params?.title
      .toUpperCase()
      .replace('PLANOGRAM ', '');
    dispatch(getPlanogramInsight(division));
  };

  useEffect(() => {
    (() => {
      getPlanogramInsightList();
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const refreshControl = (
    <RefreshControl
      onRefresh={getPlanogramInsightList}
      refreshing={planogram.loading}
    />
  );

  navigation.setOptions({
    title: route?.params?.title + '\nInsight & Validation',
  });
  return (
    <Container style={globalStyles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingVertical: 10,
        }}>
        <View>
          <Text
            style={{fontSize: 18, fontWeight: 'bold', color: colors.SeaGreen}}>
            Total Approved
          </Text>
          <Text
            style={{
              fontSize: 20,
              color: colors.Persimmon,
            }}>{`${countApproved} Locations`}</Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <AppButton
            onPress={handleNavigatePlanogram}
            title={'Planogram'}
            color={colors.BlazeOrange}
          />
        </View>
      </View>
      <View style={{backgroundColor: `${colors.Silver}55`, flex: 1}}>
        <FlatList
          refreshControl={refreshControl}
          data={planogram.plogramInsightList}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      </View>
    </Container>
  );
};

export default PlanogramInsight;
