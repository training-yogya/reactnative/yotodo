import React, {useState} from 'react';
import {useEffect} from 'react';
import {Container, ListItem, AppButton, ModalInfo} from '../../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text, Alert} from 'react-native';
import {
  deletePickingReplenish,
  getPickingReplenish,
} from '../../../redux/actions/product-action';

const PickingListReplenish = ({route, navigation}) => {
  const {title, haveAdjusment, status} = route?.params ?? {};
  navigation.setOptions({
    title,
  });

  const dispatch = useDispatch();
  const product = useSelector(state => state.product);

  const [infoMessage, setInfoMessage] = useState('');
  const [infoTitle, setInfoTitle] = useState('');
  const [infoIcon, setInfoIcon] = useState('');
  const [modalInfoVisible, setModalInfoVisible] = useState(false);

  const handleOnPress = sku => () => {
    navigation.navigate(screenNames.Product.Identity, {
      productId: sku,
      hideReason: !haveAdjusment,
      parentTitle: 'Directorate',
    });
  };

  useEffect(() => {
    getproductAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getproductAsync = () => {
    dispatch(getPickingReplenish());
  };

  const _onDelete = id => {
    Alert.alert('Confirmation', null, [
      {
        text: 'Cancel',
      },
      {
        text: 'Delete',
        onPress: () => {
          dispatch(deletePickingReplenish(id))
            .then(() => {
              getproductAsync();
            })
            .catch(e => {
              Alert.alert('Failed');
            });
        },
      },
    ]);
  };

  const keyExtractor = (item, i) => i.toString();

  const renderItem = ({item}) => {
    return (
      <View>
        <ListItem
          markText={''}
          onPress={handleOnPress(item.sku || item.SKU)}
          header={item.sku || item.SKU}
          title={item.skudesc || item.SKUDESC}
          deleteIcon={true}
          deleteAction={() => _onDelete(item.id)}
          subTitle={`${(item.divdesc || item.DIVISION) &&
            (item.DIVISION || item.divdesc) + ','} ${item.CATEGORY ||
            item.catdesc} ${item.tag ? item.tag.toUpperCase() : ''}`}
          info={`${item.qty + 'pcs' || item.QTY + 'pcs'}`}
          infoStyle={{fontSize: 16, color: colors.Red, fontWeight: 'bold'}}
        />
      </View>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getproductAsync} refreshing={product.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <View style={{flex: 1}}>
        <FlatList
          style={styles.padding10}
          data={product?.pickingReplenish}
          ListEmptyComponent={<EmptyComponent loading={product.loading} />}
          refreshControl={refreshControl}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
        />
      </View>
      <ModalInfo
        visible={modalInfoVisible}
        onDismiss={() => setModalInfoVisible(false)}
        message={infoMessage}
        title={infoTitle}
        icon={infoIcon}
        setVisible={setModalInfoVisible}
      />
    </Container>
  );
};

export default PickingListReplenish;

const EmptyComponent = ({loading, name}) => (
  <View
    style={{
      height: 200,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View>
        <Text>List {name} Kosong</Text>
      </View>
    )}
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.Light, marginBottom: 0},
  padding10: {
    padding: 10,
  },
});
