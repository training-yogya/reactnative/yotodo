import * as React from 'react';
import {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  Text,
  StyleSheet,
  ActivityIndicator,
  Modal,
  TextInput,
  Button,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Container, AppButton, Loading, ModalInfo} from '../../../components';
import ListPill from './../../ProductIdentity/components/ListPill';
import colors from '../../../utils/colors';
import globalStyles from '../../../styles/global';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import ImageViewer from 'react-native-image-zoom-viewer';
import {useSelector, useDispatch} from 'react-redux';
import {
  getProductIdentity,
  submitPickingReplenish,
  getPickingReplenish,
} from '../../../redux/actions/product-action';
const {width, height} = Dimensions.get('screen');

const ProductIdentity = props => {
  const {navigation, route} = props;

  const {title, productId, parentTitle} = route.params;

  const cabang = useSelector(state => state.auth.userLogin.initial_store);
  const product = useSelector(state => state.product);
  const yoPicker = useSelector(state => state.yoPicker);

  const dispatch = useDispatch();

  const [showDefaultImage, setShowDefaultImage] = useState(true);
  const [isImageError, setIsImageError] = useState(false);
  const [modalPictureVisible, setModalPictureVisible] = useState(false);
  const [modalInfoVisible, setModalInfoVisible] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [infoMessage, setInfoMessage] = useState('');
  const [infoTitle, setInfoTitle] = useState('');
  const [infoIcon, setInfoIcon] = useState('');
  const [qty, setQty] = useState('');

  const {loading, identity} = product;

  const productImage = showDefaultImage
    ? 'https://cdn-2.tstatic.net/default-2.jpg'
    : isImageError
    ? 'https://cdn-2.tstatic.net/default-2.jpg'
    : identity?.image || 'https://cdn-2.tstatic.net/default-2.jpg';

  useEffect(() => {
    dispatch(getProductIdentity(productId, cabang, parentTitle));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  navigation.setOptions({
    title: title || 'Product Identity',
  });

  const handleSubmitQty = () => {
    const data = {
      sku: identity.sku,
      skudesc: identity.skudesc,
      note: identity.note,
      divdesc: identity.division,
      catdesc: identity.category,
      till_code: identity.till_code,
      tag: identity.tag,
      qty: qty,
    };
    dispatch(submitPickingReplenish(data))
      .then(result => {
        // setTimeout(() => {
        setModalVisible(false);
        setInfoIcon('success');
        setInfoTitle('Success');
        setInfoMessage(result.status.message);
        setModalInfoVisible(true);
        // });

        setTimeout(() => {
          navigation.goBack();
        }, 100);
        dispatch(getPickingReplenish());
      })
      .catch(error => {
        setModalVisible(false);
        setInfoIcon('failed');
        setInfoTitle('Failed');
        setInfoMessage(error.status.message);
        setModalInfoVisible(true);
      });
  };

  const ImageProduct = React.useMemo(() => {
    return (
      <TouchableOpacity
        onPress={() => setModalPictureVisible(true)}
        style={{
          position: 'relative',
        }}>
        {!loading ? (
          <Image
            source={{
              uri: productImage,
            }}
            onLoadEnd={() => setShowDefaultImage(false)}
            // onLoadStart={() => !showDefaultImage && setShowDefaultImage(true)}
            onError={() => {
              setIsImageError(true);
            }}
            resizeMode="cover"
            style={styles.imageProduct}
          />
        ) : (
          <View style={styles.imageProduct} />
        )}
        {showDefaultImage && (
          <ActivityIndicator
            size={46}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
          />
        )}
      </TouchableOpacity>
    );
  }, [loading, productImage, showDefaultImage]);

  const ModalPicture = () => {
    return (
      <Modal
        onRequestClose={() => setModalPictureVisible(false)}
        visible={modalPictureVisible}
        transparent={true}
        animated={true}
        animationType={'slide'}>
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,0.7)',
            position: 'relative',
            height: '100%',
          }}>
          <TouchableOpacity
            onPress={() => setModalPictureVisible(false)}
            style={{
              justifyContent: 'center',
              height: 56,
              marginLeft: 20,
            }}>
            <MaterialIcon
              onPress={() => setModalPictureVisible(false)}
              name="close"
              color={colors.White}
              size={30}
            />
          </TouchableOpacity>
          <ImageViewer imageUrls={[{url: productImage}]} />
        </View>
      </Modal>
    );
  };

  return (
    <Container style={styles.wrapper}>
      {(loading || yoPicker.loading) && <Loading />}
      <>{ImageProduct}</>
      <View style={styles.banner}>
        <View>
          <Text style={styles.productTitle}>{identity?.skudesc || '-'}</Text>
          <View style={styles.bannerFooter}>
            <Text style={[globalStyles.textBold, styles.skuText]}>
              {identity?.sku || '-'}
            </Text>
            <View>
              <Text style={globalStyles.textBold}>{identity.division}</Text>
              <Text style={globalStyles.textBold}>{identity.category}</Text>
            </View>
          </View>
        </View>
      </View>
      <ScrollView>
        <ListPill
          title="Selling Price"
          value={`Rp. ${identity?.selling_price || '-'},-`}
        />
        <ListPill title="Last Stock" value={identity?.last_stock || '-'} />
        <ListPill title="Till Code" value={identity?.till_code || '-'} />
      </ScrollView>

      <View style={styles.buttonReasonWrapper}>
        <AppButton
          onPress={() => setModalVisible(true)}
          title="Input Qty"
          style={{width: 200}}
          color={colors.BlazeOrange}
        />
      </View>
      <Modal
        visible={modalVisible}
        transparent={true}
        animated={true}
        onRequestClose={() => setModalVisible(false)}
        animationType={'slide'}>
        <View
          style={{
            flex: 1,
            backgroundColor: `${colors.Black}aa`,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '90%',
              paddingVertical: 10,
              borderRadius: 10,
              backgroundColor: colors.White,
            }}>
            <View style={{alignItems: 'flex-end', paddingRight: 10}}>
              <MaterialIcon
                onPress={() => setModalVisible(false)}
                name={'close'}
                size={15}
                color={colors.White}
                style={{
                  padding: 3,
                  backgroundColor: colors.Red,
                  borderRadius: 20,
                }}
              />
            </View>
            <View style={{padding: 10}}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: colors.SeaGreen,
                  marginBottom: 20,
                }}>
                {'Saran Quantity Picking\nberdasarkan data TPLinux = - pcs'}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TextInput
                  style={{
                    backgroundColor: colors.Black + '11',
                    borderRadius: 10,
                    paddingHorizontal: 10,
                    width: 90,
                    marginHorizontal: 20,
                    textAlign: 'right',
                  }}
                  autoFocus={true}
                  value={qty}
                  keyboardType="numeric"
                  onChangeText={text => setQty(text)}
                  placeholder={'QTY'}
                />
              </View>
            </View>
            <View style={{paddingHorizontal: 10}}>
              <Button
                title={'Submit'}
                size={25}
                color={colors.BlazeOrange}
                onPress={handleSubmitQty}
              />
            </View>
          </View>
        </View>
      </Modal>

      <ModalPicture />

      <ModalInfo
        visible={modalInfoVisible}
        onDismiss={() => setModalInfoVisible(false)}
        message={infoMessage}
        title={infoTitle}
        icon={infoIcon}
        setVisible={setModalInfoVisible}
      />
    </Container>
  );
};

export default ProductIdentity;

const styles = StyleSheet.create({
  imageProduct: {
    alignSelf: 'center',
    height: height / 4,
    maxWidth: width - 10,
    minWidth: width - 30,
    borderRadius: 4,
  },
  wrapper: {
    paddingHorizontal: 10,
    marginTop: 5,
  },
  banner: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  buttonReasonWrapper: {
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  productTitle: {
    fontSize: 18,
    color: colors.VerdunGreen,
  },
  bannerFooter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  skuText: {
    marginRight: 20,
  },
});
