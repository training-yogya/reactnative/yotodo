import * as React from 'react';
import {useEffect} from 'react';
import {Container, ListItem} from '../../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text} from 'react-native';
import {getTodaySales} from '../../../redux/actions/product-action';

import moment from 'moment';

const MinusStock = ({route, navigation}) => {
  const {title} = route.params;
  const product = useSelector(state => state.product);
  const dispatch = useDispatch();

  const handleNavigateBarcode = () => {
    return navigation.navigate(screenNames.Home.BarcodeScan, {
      screen: screenNames.Home.BarcodeScan,
      params: {title: 'Barcode Scan'},
    });
  };

  navigation.setOptions({
    title,
  });

  const handleOnPress = sku => () => {
    navigation.navigate(screenNames.Product.Identity, {
      productId: sku,
      barcode: true,
      hideReason: true,
    });
  };

  useEffect(() => {
    (() => {
      dispatch(getTodaySales());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getArticleAsync = () => dispatch(getTodaySales());
  const keyExtractor = (item, i) => i.toString();

  const unit = ' pcs';

  const renderItem = ({item}) => {
    return (
      <View>
        <ListItem
          markText={''}
          onPress={handleOnPress(item.sku)}
          header={item.sku}
          title={item.skudesc}
          subTitle={`${item.divdesc && item.divdesc + ','} ${item.catdesc} ${
            item.tag ? item.tag.toUpperCase() : ''
          }`}
          info={`${item.qty ? item.qty + unit : 0 + unit}`}
          infoDetail={`${moment(item.tanggal_expired).format('DD MMMM YYYY')}`}
          infoDetailStyle={{color: colors.Black, fontWeight: 'bold'}}
        />
      </View>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getArticleAsync} refreshing={product.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <FlatList
        style={styles.padding10}
        data={product.todaySales}
        ListEmptyComponent={
          <EmptyComponent
            loading={product.loading}
            addNew={handleNavigateBarcode}
          />
        }
        refreshControl={refreshControl}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </Container>
  );
};

export default MinusStock;

const EmptyComponent = ({loading, name, addNew}) => (
  <View
    style={{
      height: 500,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>List {name} Kosong</Text>
      </View>
    )}
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.Light, marginBottom: 10},
  padding10: {
    padding: 10,
  },
});
