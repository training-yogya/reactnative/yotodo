import * as React from 'react';
import {Container, AppButton, Icon} from '../../../components';
import globalStyles from '../../../styles/global';
import colors from '../../../utils/colors';
import {View, Text, ScrollView} from 'react-native';
import {iconsNames} from '../../../utils/icons';
import {screenNames} from '../../../utils/screens';
import {useDispatch} from 'react-redux';

const SOPartial = props => {
  const {navigation} = props;

  const dispatch = useDispatch();

  navigation.setOptions({
    title: 'Replenish',
  });

  const handleNavigateBarcode = () => {
    dispatch({type: 'SET_REPLENISH_METHOD', payload: 'product-identity'});
    navigation.navigate(screenNames.BarcodeScan.Scan, {
      title: 'Barcode Scan',
      type: 'replenish',
    });
  };

  const handleNavigatePickingList = () => {
    navigation.navigate(screenNames.Replenish.PickingList, {
      title: 'Picking List',
    });
  };

  const handleNavigateExpiredCheck = () => {
    dispatch({type: 'SET_REPLENISH_METHOD', payload: 'expired'});
    navigation.navigate(screenNames.ExpiredCheck.Home, {
      screen: screenNames.ExpiredCheck.List,
      params: {title: 'Expired Check', type: 'replenish'},
    });
  };

  const handleNavigateTpLinux = () => {
    dispatch({type: 'SET_REPLENISH_METHOD', payload: 'today-sales'});
    navigation.navigate(screenNames.Replenish.TodaySales, {
      title: 'TPLinux Sales Today by\nRED, BLUE, BLU1, YEL1, YELN',
      type: 'replenish',
    });
  };

  const handleNavigateListMustSO = () => {
    dispatch({type: 'SET_PSO_METHOD', payload: 'oos'});
    navigation.navigate(screenNames.Article.Article, {
      title: 'OUT OF STOCK',
      parentTitle: 'supermarket',
      barcode: true,
      screenType: 'Replenish',
    });
  };

  return (
    <Container style={globalStyles.container}>
      <View>
        {/* <Text></Text> */}
        <Text style={{fontWeight: '600', fontSize: 17}}>
          {
            'Perhatian !\nSebelum Anda Masuk ke menu Picking List,\npastikan anda telah melakukan "picking"\ndi menu Barcode Scan.'
          }
        </Text>
      </View>
      <ScrollView style={{flex: 1, marginTop: 20}}>
        <AppButton
          onPress={handleNavigateBarcode}
          title={'Barcode Scan'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55}}
          color={colors.YellowOrange}
        />
        <AppButton
          onPress={handleNavigatePickingList}
          title={'Picking List'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55}}
          color={colors.DodgerBlue}
        />
        <AppButton
          onPress={handleNavigateExpiredCheck}
          title={'Expired Check'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55}}
          color={colors.BlueBell}
        />
        <AppButton
          onPress={handleNavigateTpLinux}
          title={
            'TPLinux Today Sales by\nRED, BLUE, BLU1, YELN, & YEL1 Articles'
          }
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55}}
          color={colors.BlazeOrange}
        />
        <AppButton
          onPress={handleNavigateListMustSO}
          title={'OUT OF STOCK\nRED, BLUE, BLU1, YELN, & YEL1 Articles'}
          titleStyle={{textAlign: 'center', fontWeight: 'bold'}}
          style={{marginBottom: 20, padding: 10, height: 55}}
          color={colors.SeaGreen}
        />
      </ScrollView>
    </Container>
  );
};

export default SOPartial;
