import * as React from 'react';
import {useEffect} from 'react';
import {Container, ListItem} from '../../../components';
import {FlatList} from 'react-native-gesture-handler';
import colors from '../../../utils/colors';
import {screenNames} from '../../../utils/screens';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, RefreshControl, View, Text} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {getExpiredList} from '../../../redux/actions/product-action';

import moment from 'moment';

const MinusStock = ({route, navigation}) => {
  const {title, type} = route.params;
  const product = useSelector(state => state.product);
  const dispatch = useDispatch();

  const handleNavigateBarcode = () => {
    return navigation.navigate(screenNames.Home.BarcodeScan, {
      screen: screenNames.Home.BarcodeScan,
      params: {title: 'Barcode Scan'},
    });
  };

  navigation.setOptions({
    title,
    headerRight: (
      <MaterialIcon
        name="add"
        size={36}
        color={colors.White}
        onPress={handleNavigateBarcode}
      />
    ),
  });

  const handleOnPress = sku => () => {
    if (type === 'replenish') {
      navigation.navigate(screenNames.Replenish.Home, {
        screen: screenNames.Product.Identity,
        params: {
          productId: sku,
          parentTitle: 'Expired Check',
          barcode: true,
          hideReason: true,
        },
      });
    } else {
      navigation.navigate(screenNames.Product.Identity, {
        productId: sku,
        parentTitle: 'Expired Check',
        barcode: true,
        hideReason: true,
      });
    }
  };

  useEffect(() => {
    (() => {
      dispatch(getExpiredList());
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getArticleAsync = () => {};
  const keyExtractor = (item, i) => i.toString();

  const unit = ' pcs';

  const renderItem = ({item}) => {
    return (
      <View>
        <ListItem
          markText={''}
          onPress={handleOnPress(item.sku)}
          header={item.sku}
          title={item.skudesc}
          subTitle={`${item.divdesc && item.divdesc + ','} ${item.catdesc} ${
            item.tag ? item.tag.toUpperCase() : ''
          }`}
          info={`${item.qty ? item.qty + unit : 0 + unit}`}
          infoDetail={`${moment(item.tanggal_expired).format('DD MMMM YYYY')}`}
          infoDetailStyle={{color: colors.Red, fontWeight: 'bold'}}
        />
      </View>
    );
  };

  const refreshControl = (
    <RefreshControl onRefresh={getArticleAsync} refreshing={product.loading} />
  );

  return (
    <Container style={styles.wrapper}>
      <FlatList
        style={styles.padding10}
        data={product.productExp}
        ListEmptyComponent={
          <EmptyComponent
            loading={product.loading}
            addNew={handleNavigateBarcode}
          />
        }
        refreshControl={refreshControl}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    </Container>
  );
};

export default MinusStock;

const EmptyComponent = ({loading, name, addNew}) => (
  <View
    style={{
      height: 500,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    {loading ? (
      <View />
    ) : (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>List {name} Kosong</Text>
      </View>
    )}

    <MaterialIcon
      name={'add-circle'}
      onPress={addNew}
      size={56}
      color={colors.BlazeOrange}
    />
  </View>
);

const styles = StyleSheet.create({
  wrapper: {backgroundColor: colors.Light, marginBottom: 10},
  padding10: {
    padding: 10,
  },
});
