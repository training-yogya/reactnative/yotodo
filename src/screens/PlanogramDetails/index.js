import {connect} from 'react-redux';
import PlanogramDetails from './screen';
import {getPlanogramData} from './../../redux/actions/planogram-action';

const mapDispatchToProps = dispatch => ({
  getPlanogramData: category => dispatch(getPlanogramData(category)),
});

export default connect(
  null,
  mapDispatchToProps,
)(PlanogramDetails);
