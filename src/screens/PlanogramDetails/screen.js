import * as React from 'react';
import {useEffect, useState, useRef} from 'react';
import {Container, AppButton, Loading} from '../../components';
import {
  StyleSheet,
  Dimensions,
  View,
  BackHandler,
  Text,
  ActivityIndicator,
} from 'react-native';

import {Stopwatch} from 'react-native-stopwatch-timer';

import Pdf from 'react-native-pdf';
import colors from '../../utils/colors';
import globalStyles from '../../styles/global';
import {screenNames} from '../../utils/screens';
import {useFocusEffect} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {SET_PLANOGRAM_IMPLEMENTATION_END} from '../../redux/actionTypes';

import Icon from 'react-native-vector-icons/AntDesign';

const options = {
  container: {
    flex: 1,
  },
  text: {
    fontSize: 30,
    color: colors.Black,
  },
};
const {width, height} = Dimensions.get('window');

const PlanogramDetails = ({navigation, route, getPlanogramData}) => {
  const planogramList = useSelector(state => state.planogram.planogramList);
  const planogramLoading = useSelector(state => state.planogram.loading);
  const dispatch = useDispatch();
  const {implementation, title, timeStart, hideImplement} = route.params;
  const [startTime, setStartTime] = useState(0);
  const [showStopWatch, setShowStopWatch] = useState(false);
  const [stopwatchStart, setStopwatchStart] = useState(false);

  const stopWatchRef = useRef();
  const implementationStart =
    useSelector(state => state.planogram.form.start.time) || timeStart;
  const saveStartPicture = picture => {
    stopWatchRef.current.stop();
    const payload = {
      image: {
        name: picture.name,
        type: picture.type,
        uri: picture.uri,
        coords: picture.coords,
      },
      time: picture.dateInt,
      duration: stopWatchRef.current.state.elapsed,
    };

    dispatch({
      type: SET_PLANOGRAM_IMPLEMENTATION_END,
      payload,
    });

    navigation.push(screenNames.Planogram.Implementation.Summary, {
      implementation: true,
      title: 'Planogram Summary',
    });
  };
  const handleButtonSubmitImplementation = () => {
    navigation.push(screenNames.Planogram.Implementation.Camera, {
      savePicture: saveStartPicture,
    });
  };
  const handleButtonImplementation = () => {
    navigation.navigate(screenNames.Planogram.Implementation.Start, {
      planogramId: 'planogram Id',
    });
  };
  const handleButtonCannotImplementation = () => {
    navigation.navigate(screenNames.Planogram.CannotImplementation, {
      planogramId: 'planogram Id',
    });
  };

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (implementation) {
          return true;
        } else {
          return false;
        }
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [implementation]),
  );

  navigation.setOptions({
    title,
    headerLeft: implementation && 'none',
    headerRight: implementation && null,
  });

  useEffect(() => {
    if (implementation) {
      setStartTime(Date.now() - implementationStart);
      setShowStopWatch(true);
      setStopwatchStart(true);
    } else {
      getPlanogramData(title);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [activePdf, setActifPdf] = useState(0);

  const handlePrevPdf = () => {
    if (activePdf + 1 !== 1) {
      setActifPdf(activePdf - 1);
    }
  };

  const handleNextPdf = () => {
    if (activePdf + 1 < planogramList.length) {
      setActifPdf(activePdf + 1);
    }
  };

  return (
    <Container>
      {planogramLoading && <Loading />}
      <View style={styles.buttonPdfWrapper}>
        <Icon
          onPress={handlePrevPdf}
          name="caretleft"
          style={[styles.buttonPdf, activePdf === 0 && styles.disabled]}
        />
        <View>
          <Text>
            {activePdf + 1} of{' '}
            {Array.isArray(planogramList) ? planogramList?.length ?? 0 : 1} pdf
          </Text>
        </View>
        <Icon
          onPress={handleNextPdf}
          name="caretright"
          style={[
            styles.buttonPdf,
            activePdf + 1 ===
              (Array.isArray(planogramList) ? planogramList?.length ?? 0 : 1) &&
              styles.disabled,
          ]}
        />
      </View>

      {Array.isArray(planogramList) ? (
        planogramList[activePdf]?.url ? (
          <Pdf
            activityIndicator={<ActivityIndicator />}
            source={{
              uri: planogramList[activePdf].url,
              cache: true,
            }}
            maxScale={20.0}
            style={styles.pdf}
          />
        ) : (
          <View style={[styles.pdf, globalStyles.center]}>
            <Text>No Data Available</Text>
          </View>
        )
      ) : (
        <View style={[styles.pdf, globalStyles.center]}>
          <Text>No Data Available</Text>
        </View>
      )}
      {implementation ? (
        <View style={styles.buttonContainer}>
          {showStopWatch ? (
            <Stopwatch
              ref={stopWatchRef}
              startTime={startTime}
              options={options}
              start={stopwatchStart}
            />
          ) : (
            <View style={globalStyles.flex1} />
          )}
          <AppButton
            title={'Submit Implementation'}
            onPress={handleButtonSubmitImplementation}
            color={colors.BlazeOrange}
          />
        </View>
      ) : (
        !hideImplement && (
          <View style={styles.buttonContainer}>
            <View style={styles.space}>
              <AppButton
                style={styles.button}
                color={colors.SeaGreen}
                title="Implementation"
                onPress={handleButtonImplementation}
              />
            </View>
            <View>
              <AppButton
                style={styles.button}
                color={colors.YellowOrange}
                title="Cannot be Implementation"
                titleStyle={globalStyles.textCenter}
                onPress={handleButtonCannotImplementation}
              />
            </View>
          </View>
        )
      )}
    </Container>
  );
};

export default PlanogramDetails;

const styles = StyleSheet.create({
  pdf: {
    flex: 1,
    width: width,
    height: height,
  },
  space: {marginEnd: 20},
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  button: {height: 50, width: width / 2 - 50},
  buttonPdfWrapper: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    padding: 5,
  },
  buttonPdf: {
    backgroundColor: colors.BlazeOrange,
    textAlign: 'center',
    textAlignVertical: 'center',
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
  },
  disabled: {
    backgroundColor: colors.Silver,
  },
});
