import React, {useEffect} from 'react';
import {Text, ActivityIndicator} from 'react-native';
import {Icon, Container} from './../../components';
import {screenNames} from './../../utils/screens';
import {iconsNames} from './../../utils/icons';
import globalStyles from './../../styles/global';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {CommonActions} from '@react-navigation/native';

const SplashScreen = ({navigation, changeUserToken}) => {
  navigation.setOptions({
    headerShown: false,
  });

  useEffect(() => {
    const initApp = async () => {
      setTimeout(() => {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: screenNames.Login,
              },
            ],
          }),
        );
      }, 2000);
    };

    initApp();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container style={globalStyles.centerAround}>
      <TouchableOpacity onPress={() => {}}>
        <Container style={globalStyles.center}>
          <Icon name={iconsNames.logo} />
        </Container>
        <ActivityIndicator size={40} />
        <Container style={globalStyles.centerEnd}>
          <Text>from</Text>
          <Icon name={iconsNames.yogyabanner} />
        </Container>
      </TouchableOpacity>
    </Container>
  );
};

export default SplashScreen;
