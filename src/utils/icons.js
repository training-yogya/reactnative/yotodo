export const iconsNames = {
  logo: 'logo',
  yogyabanner: 'yogya-group',
  yogyaDulu: 'yogya-dulu',
  yogyaDuluRounded: 'yogya-dulu-rounded',
  store: 'store',
  covid: 'covid-banner',
};

export const iconFinder = find => {
  find = find.toLowerCase().replace('planogram ', '');
  switch (find) {
    case 'food':
      return ['food', 'MaterialCommunityIcons'];
    case 'fresh':
      return ['carrot', 'MaterialCommunityIcons'];
    case 'non food':
      return ['food-off', 'MaterialCommunityIcons'];
    case 'gms':
      return ['store', 'MaterialCommunityIcons'];
    case 'stationery':
      return ['paperclip', 'MaterialCommunityIcons'];
    case 'yogya electronic':
      return ['television', 'MaterialCommunityIcons'];
    case 'layout fashion':
      return ['tshirt', 'FontAwesome5'];
    case 'layout food station':
      return ['food-fork-drink', 'MaterialCommunityIcons'];
    case 'layout yoel':
      return ['map', 'Foundation'];
    case 'layout gudang supermarket':
      return ['forklift', 'MaterialCommunityIcons'];
    case 'layout gudang fashion':
      return ['warehouse', 'FontAwesome5'];
    case 'layout supermarket':
      return ['map-o', 'FontAwesome'];
    default:
      return ['layers', 'Feather'];
  }
};
