export default {
  NeonCarrot: '#FF9933',
  Limeade: '#689A03',
  Persimmon: '#FF6247',
  Downy: '#66CCCC',
  BlazeOrange: '#FF6803',
  RegentStBlue: '#A0C0DF',
  Black: '#030303',
  White: '#FFFFFF',
  Boulder: '#757575',
  Silver: '#CCCCCC',
  BurningOrange: '#FF6835',
  BlueBell: '#9999CC',
  SinBad: '#99CCCC',
  MidGray: '#696A6D',
  VividTangerine: '#FF8787',
  Cornflower: '#86C3E1',
  Mantis: '#85C368',
  GoldenTainoi: '#FFCD68',
  Logan: '#A4A4C3',
  Emerald: '#68CD9A',
  SilverChalice: '#A6A6A6',
  EarlsGreen: '#CCCC33',
  DodgerBlue: '#30ACFF',
  Salmon: '#FF8D68',
  Light: '#EEEEEE',
  PeachOrange: '#FFCA94',
  Turmeric: '#D2D24B',
  VerdunGreen: '#336600',
  Negroni: '#FFE3C7',
  SeaGreen: '#359A68',
  YellowOrange: '#FFA245',
  TradeWind: '#62B0B0',
  OrangPeel: '#FF9900',
  Red: '#FF0000',
  Blue: '#9FC2CC',
  Transparent: 'transparent',
  BluePrimary: '#1179da',
};
