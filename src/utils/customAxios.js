import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
import config from './../config';
import {store} from './../redux/store';
import {NetworkInfo} from 'react-native-network-info';
import Geolocation from '@react-native-community/geolocation';

const getPosition = () => {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition(
      result => {
        return resolve(result.coords);
      },
      error => resolve(error),
    );
  });
};

const client = axios.create({
  baseURL: config.baseUrl,
  headers: {
    origin: 'yogyagroup.com',
    source: 'sails-api',
    'content-type': 'multipart/form-data',
  },
});

const monitore = axios.create({
  baseURL: config.monitore,
  headers: {
    ip: '11',
    origin: 'yogyagroup.com',
    source: 'sails-api',
    'content-type': 'multipart/form-data',
  },
});

const gold = axios.create({
  baseURL: config.gold,
  headers: {
    ip: '11',
    origin: 'yogyagroup.com',
    source: 'sails-api',
    'content-type': 'multipart/form-data',
  },
});

client.interceptors.request.use(
  async response => {
    const originalConfig = response;
    const userLogin = store.getState().auth.userLogin;
    try {
      const userToken = await AsyncStorage.getItem('token');
      const ipAddress = await NetworkInfo.getIPAddress();
      const coords = await getPosition();
      if (!coords.latitude) {
        Snackbar.show({text: coords.message});
      }
      originalConfig.headers.latitude = coords.latitude;
      originalConfig.headers.longitude = coords.longitude;
      originalConfig.headers.Authorization = `Bearer ${userToken}`;
      originalConfig.headers.user_id = userLogin.id;
      originalConfig.headers.version = config.version;
      originalConfig.headers.ip = ipAddress;
      return originalConfig;
    } catch (error) {
      return Promise.reject(error);
    }
  },
  error => {
    return Promise.reject(error);
  },
);

monitore.interceptors.request.use(
  async response => {
    const originalConfig = response;
    try {
      const userToken = await AsyncStorage.getItem('token');
      const user = store.getState().auth.userLogin;
      originalConfig.params = {
        branch: user.initial_store.toLowerCase(),
      };
      originalConfig.headers.Authorization = `Bearer ${userToken}`;
      return originalConfig;
    } catch (error) {
      return Promise.reject(error);
    }
  },
  error => {
    return Promise.reject(error);
  },
);

client.interceptors.response.use(
  response => Promise.resolve(response),
  async error => Promise.reject(error),
);

export {monitore, gold};
export default client;
