const url = {
  login: '/user/auth/login',
  requestToken: '/token/request',
  changePassoword: id => '/user/password/' + id,
  profile: id => '/user/' + id,
  menu: id => '/menu/' + id,
  product: {
    identity: '/item',
  },
  articleReason: '/reason',
  supermarket: {
    minusmargin: initial_store => '/api/v1/trans/minusmargin/' + initial_store,
    growthminus: initial_store => '/api/v1/trans/growthminus/' + initial_store,
    minusstock: initial_store => '/api/v1/trans/minusstock/' + initial_store,
    overstock: initial_store => '/api/v1/trans/overstock/' + initial_store,
    outofstock: initial_store => '/api/v1/trans/oos/' + initial_store,
    deadstock: initial_store => '/api/v1/trans/deadstock/' + initial_store,
    newproduct: initial_store => '/api/v1/trans/newproduct/' + initial_store,
    mustso: initial_store => '/api/v1/trans/mustso/' + initial_store,
    goodintransit: '/trans/goodintransit',
    readystock: '/trans/readystock/',
    sku: initial_store => '/sopartial/must_so_sku/' + initial_store,
    fivedaysbeforeoos: initial_store =>
      '/api/v1/trans/beforeoos/' + initial_store,
  },
  supermarketMonitore: {
    minusmargin: 'minusmargin.php',
    fivedaysbeforeoos: 'dailyoosbefore.php',
    outofstock: 'dailyoosbefore.php',
    minusstock: 'minusstock.php',
  },
  fashion: {
    minusmargin: initial_store =>
      '/api/v1/trans/minusmargin_fsh/' + initial_store,
    growthminus: '/fashion/growthminus',
    minusstock: initial_store =>
      '/api/v1/trans/minusstock_fsh/' + initial_store,
    goodintransit: '/trans/goodintransit_fsh',
    overstock: '/fashion/skp',
    outofstock: '/fashion/oos',
    deadstock: '/fashion/deadstock',
    fivedaysbeforeoos: '/fashion/beforeoos',
  },
  homebakery: {
    minusmargin: initial_store =>
      '/api/v1/trans/minusmargin_bakery/' + initial_store,
    growthminus: '/homebakery/growthminus',
    minusstock: initial_store =>
      '/api/v1/trans/minusstock_bakery/' + initial_store,
    overstock: '/homebakery/skp',
    outofstock: '/homebakery/oos',
    deadstock: '/homebakery/deadstock',
    estimasiforproduction: initial_store => initial_store,
    stockpackaging: initial_store =>
      '/api/v1/trans/stock_packaging/' + initial_store,
    fivedaysbeforeoos: '/homebakery/beforeoos',
    shrinkagehomebakery: initial_store =>
      '/api/v1/trans/shrinkage_bakery/' + initial_store,
    yesterdaytopsales: storeCode => '/api/v1/topsales_bakery/' + storeCode,
  },
  foodstation: {
    minusmargin: '/foodstation/minusmargin',
    growthminus: '/foodstation/growthminus',
    minusstock: '/foodstation/minusstock',
    overstock: '/foodstation/skp',
    outofstock: '/foodstation/oos',
    deadstock: '/foodstation/deadstock',
    fivedaysbeforeoos: '/foodstation/beforeoos',
  },
  yoel: {
    minusmargin: initial_store =>
      '/api/v1/trans/minusmargin_yoel/' + initial_store,
    growthminus: '/yoel/growthminus',
    minusstock: initial_store =>
      '/api/v1/trans/minusstock_yoel/' + initial_store,
    overstock: initial_store => '/api/v1/trans/overstock_yoel/' + initial_store,
    outofstock: initial_store => '/api/v1/trans/oos_yoel/' + initial_store,
    deadstock: initial_store => '/api/v1/trans/deadstock_yoel/' + initial_store,
    updateagingproduct: initial_store =>
      '/api/v1/trans/aging_yoel/' + initial_store,
    topsalesarticle: initial_store =>
      '/api/v1/trans/topsales_yoel/' + initial_store,
    fivedaysbeforeoos: '/yoel/beforeoos',
  },
};

export default url;
// lommea;
