import {createLogger} from 'redux-logger';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
// import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';

const appMiddlewares = [];

// const reactNavigation = createReactNavigationReduxMiddleware(
//   state => state.nav,
//   'root',
// );
appMiddlewares.push(thunk);
appMiddlewares.push(promise);

if (__DEV__) {
  appMiddlewares.push(createLogger());
}

// appMiddlewares.push(reactNavigation);

export default appMiddlewares;
