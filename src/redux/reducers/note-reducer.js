import {
  REQUEST_GET_NOTE_PENDING,
  REQUEST_GET_NOTE_SUCCESS,
  REQUEST_GET_NOTE_FAILED,
  REQUEST_POST_NOTE_PENDING,
  REQUEST_POST_NOTE_SUCCESS,
  REQUEST_POST_NOTE_FAILED,
  REQUEST_GET_NOTE_LIST_PENDING,
  REQUEST_GET_NOTE_LIST_SUCCESS,
  REQUEST_GET_NOTE_LIST_FAILED,
  REQUEST_UPDATE_NOTE_PENDING,
  REQUEST_UPDATE_NOTE_SUCCESS,
  REQUEST_UPDATE_NOTE_FAILED,
  REQUEST_DELETE_NOTE_PENDING,
  REQUEST_DELETE_NOTE_SUCCESS,
  REQUEST_DELETE_NOTE_FAILED,
  REQUEST_CREATE_NOTE_PENDING,
  REQUEST_CREATE_NOTE_SUCCESS,
  REQUEST_CREATE_NOTE_FAILED,
} from './../actionTypes';

const initalState = {
  note: '',
  noteList: [],
  getLoading: false,
  getSuccess: false,
  getFailed: false,
  postLoading: false,
  postSuccess: false,
  postFailed: false,

  createLoading: false,
  createSuccess: false,
  createFailed: false,

  updateLoading: false,
  updateSuccess: false,
  updateFailed: false,

  deleteLoading: false,
  deleteSuccess: false,
  deleteFailed: false,
};

const noteReducer = (state = initalState, action) => {
  const {payload, type} = action;
  switch (type) {
    case REQUEST_GET_NOTE_PENDING:
      return {
        ...state,
        getLoading: true,
        getSuccess: false,
        getFailed: false,
      };
    case REQUEST_GET_NOTE_SUCCESS:
      return {
        ...state,
        getLoading: false,
        getSuccess: true,
        note: payload?.notes,
        getFailed: false,
      };
    case REQUEST_GET_NOTE_FAILED:
      return {
        ...state,
        getLoading: false,
        getSuccess: false,
        getFailed: true,
      };
    case REQUEST_POST_NOTE_PENDING:
      return {
        ...state,
        postLoading: false,
        postSuccess: false,
        postFailed: false,
      };
    case REQUEST_POST_NOTE_SUCCESS:
      return {
        ...state,
        postLoading: false,
        postSuccess: false,
        postFailed: false,
        note: payload,
      };
    case REQUEST_POST_NOTE_FAILED:
      return {
        ...state,
        postLoading: false,
        postSuccess: false,
        postFailed: false,
      };

    case REQUEST_GET_NOTE_LIST_PENDING:
      return {
        ...state,
        getLoading: true,
        getSuccess: false,
        getFailed: false,
      };
    case REQUEST_GET_NOTE_LIST_SUCCESS:
      return {
        ...state,
        getLoading: false,
        getSuccess: true,
        getFailed: false,
        noteList: payload,
      };
    case REQUEST_GET_NOTE_LIST_FAILED:
      return {
        ...state,
        getLoading: false,
        getSuccess: false,
        getFailed: true,
      };

    case REQUEST_CREATE_NOTE_PENDING:
      return {
        ...state,
        createLoading: true,
        createSuccess: false,
        createFailed: false,
      };
    case REQUEST_CREATE_NOTE_SUCCESS:
      return {
        ...state,
        createLoading: false,
        createSuccess: true,
        createFailed: false,
        note_id: payload.status.note_id,
      };
    case REQUEST_CREATE_NOTE_FAILED:
      return {
        ...state,
        createLoading: false,
        createSuccess: false,
        createFailed: true,
      };

    case REQUEST_UPDATE_NOTE_PENDING:
      return {
        ...state,
        updateLoading: true,
        updateSuccess: false,
        updateFailed: false,
      };
    case REQUEST_UPDATE_NOTE_SUCCESS:
      return {
        ...state,
        updateLoading: false,
        updateSuccess: true,
        updateFailed: false,
        note: payload,
      };
    case REQUEST_UPDATE_NOTE_FAILED:
      return {
        ...state,
        updateLoading: false,
        updateSuccess: false,
        updateFailed: true,
      };

    case REQUEST_DELETE_NOTE_PENDING:
      return {
        ...state,
        deleteLoading: true,
        deleteSuccess: false,
        deleteFailed: false,
      };
    case REQUEST_DELETE_NOTE_SUCCESS:
      return {
        ...state,
        deleteLoading: false,
        deleteSuccess: true,
        deleteFailed: false,
      };
    case REQUEST_DELETE_NOTE_FAILED:
      return {
        ...state,
        deleteLoading: false,
        deleteSuccess: false,
        deleteFailed: true,
      };
    default:
      return state;
  }
};

export default noteReducer;
