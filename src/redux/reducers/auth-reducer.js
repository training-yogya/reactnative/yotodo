import {
  REQUEST_LOGIN_PENDING,
  REQUEST_LOGIN_SUCCESS,
  REQUEST_LOGIN_FAILED,
  SET_LOGIN_DEFAULT,
  SAVE_TOKEN,
  REQUEST_CHANGE_PASSWORD_PENDING,
  REQUEST_CHANGE_PASSWORD_FAILED,
  REQUEST_CHANGE_PASSWORD_SUCCESS,
  REQUEST_PROFILE_PENDING,
  REQUEST_PROFILE_SUCCESS,
  REQUEST_PROFILE_UPDATE_PENDING,
  REQUEST_PROFILE_UPDATE_SUCCESS,
  REQUEST_PROFILE_UPDATE_FAILED,
  SET_ALL_DEFAULT,
} from './../actionTypes';
const initialState = {
  loading: false,
  success: false,
  error: false,
  storeloading: false,
  storesuccess: false,
  storeerror: false,
  isHomeReady: false,
  logined: false,
  changePasswordLoading: false,
  changePasswordSuccess: false,
  changePasswordError: false,
  getProfileLoading: false,
  updateProfileLoading: false,
  updateProfileSuccess: false,
  updateProfileError: false,
  activity: [],
  errors: [],
  storelist: [],
  message: '',
  prevScreen: null,
  userLogin: {
    id: null,
    username: '',
    full_name: '',
    role: null,
    is_active: null,
    device_token: '',
    store_init: '',
    token: '',
    initial_store: '',
    store_code: '',
    telegram_id: '',
    firebase: {},
  },
};

const authReducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case REQUEST_LOGIN_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_LOGIN_SUCCESS:
      const userLogin = payload.data;
      return {
        ...state,
        userLogin,
      };
    case SAVE_TOKEN:
      const token = payload.data.token;
      return {
        ...state,
        userLogin: {
          ...state.userLogin,
          token,
        },
        success: !!token,
        logined: !!token,
        error: false,
        loading: false,
      };
    case REQUEST_LOGIN_FAILED:
      const data = payload.status;
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        message: data ? data.message : '',
      };
    case SET_LOGIN_DEFAULT:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errors: [],
        message: '',
      };
    case 'SET_HOME_READY':
      return {
        ...state,
        isHomeReady: payload.isHomeReady,
        userLogin: {
          ...state.userLogin,
          initial_store: payload.initial_store,
          store_code: payload.store_code,
        },
      };
    case 'GET_STORE_BY_USER_PENDING':
      return {
        ...state,
        storeloading: true,
        storesuccess: false,
        storeerror: false,
      };
    case 'GET_STORE_BY_USER_SUCCESS':
      return {
        ...state,
        storeloading: false,
        storesuccess: true,
        storeerror: false,
        storelist: payload,
      };
    case 'GET_STORE_BY_USER_FAILED':
      return {
        ...state,
        storeloading: false,
        storesuccess: false,
        storeerror: true,
      };
    case SET_ALL_DEFAULT:
      const returnPayload = payload ? {...payload} : {};
      return {
        ...state,
        ...initialState,
        ...returnPayload,
      };
    case REQUEST_CHANGE_PASSWORD_PENDING:
      return {
        ...state,
        changePasswordLoading: true,
        changePasswordSuccess: false,
        changePasswordError: false,
      };
    case REQUEST_CHANGE_PASSWORD_FAILED:
      const message =
        payload.message === 'password tidak cocok'
          ? 'password tidak cocok'
          : 'update password gagal';
      return {
        ...state,
        changePasswordLoading: false,
        changePasswordSuccess: false,
        changePasswordError: true,
        message,
      };
    case REQUEST_CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        changePasswordLoading: false,
        changePasswordSuccess: true,
        changePasswordError: false,
      };
    case REQUEST_PROFILE_PENDING:
      return {
        ...state,
        getProfileLoading: true,
      };
    case REQUEST_PROFILE_SUCCESS:
      return {
        ...state,
        getProfileLoading: false,
        userLogin: {
          ...state.userLogin,
          id: payload.id,
          full_name: payload.full_name,
          email: payload.email,
          jabatan: payload.jabatan,
          nik: payload.nik,
          telegram_id: payload.telegram_id,
          phone_number: payload.phone_number,
        },
      };
    case REQUEST_PROFILE_UPDATE_PENDING:
      return {
        ...state,
        updateProfileLoading: true,
        updateProfileSuccess: false,
        updateProfileError: false,
      };
    case REQUEST_PROFILE_UPDATE_SUCCESS:
      return {
        ...state,
        updateProfileLoading: false,
        updateProfileSuccess: true,
        updateProfileError: false,
      };
    case REQUEST_PROFILE_UPDATE_FAILED:
      return {
        ...state,
        updateProfileLoading: false,
        updateProfileSuccess: false,
        updateProfileError: true,
      };
    case 'REQUEST_PROFILE_ACTIVITY_PENDING':
      return {
        ...state,
      };
    case 'REQUEST_PROFILE_ACTIVITY_SUCCESS':
      const count_today = payload?.tabel?.reduce(
        (a, b) => a + parseInt(b.count_today || 0),
        0,
      );
      const count_this_year = payload?.tabel?.reduce(
        (a, b) => a + parseInt(b.count_this_year || 0),
        0,
      );
      return {
        ...state,
        activity: {...payload, count_today, count_this_year},
      };
    case 'REQUEST_PROFILE_ACTIVITY_FAILED':
      return {
        ...state,
      };
    case 'SAVE_USER_FIREBASE':
      return {
        ...state,
        userLogin: {
          ...state.userLogin,
          firebase: payload,
        },
      };
    default:
      return state;
  }
};

export default authReducer;
