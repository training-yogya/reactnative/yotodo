import {
  GET_ARTICLE_MENU_SUCCESS,
  ADD_REASON_ARTICLE,
  REQUEST_ARTICLE_LIST_PENDING,
  REQUEST_ARTICLE_LIST_SUCCESS,
  REQUEST_ARTICLE_LIST_FAILED,
  SET_SUBMENU,
  REQUEST_ARTICLE_REASON_SUCCESS,
  REQUEST_SUBMIT_REASON_PENDING,
  REQUEST_SUBMIT_REASON_SUCCESS,
  REQUEST_SUBMIT_REASON_FAILED,
  REQUEST_FASHION_FILTER_PENDING,
  REQUEST_FASHION_FILTER_SUCCESS,
  REQUEST_FASHION_FILTER_FAILED,
} from './../actionTypes';
import config from '../../config';

const initialState = {
  loading: false,
  success: false,
  error: false,
  galleryCreatedAt: '',
  items: [],
  submenu: '',
  directorate: '',
  reasonItem: [],
  articleList: [],
  submitedArtikel: [],
  message: '',
  gallery: [],
};

const submenu = {
  minusmargin: 'minusmargin',
  growthminus: 'growthminus',
  minusstock: 'minusstock',
  overstock: 'skp',
  outofstock: 'oos',
  goodintransit: 'goodintransit',
  deadstock: 'deadstock',
  fivedaysbeforeoos: 'beforeoos',
  readystock: 'readystock',
};

const articleReducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case GET_ARTICLE_MENU_SUCCESS:
      return {
        ...state,
        items: [...payload],
      };
    case REQUEST_ARTICLE_REASON_SUCCESS:
      return {
        ...state,
        reasonItem: [{id: null, name: 'Choose Reason'}, ...payload],
      };
    case ADD_REASON_ARTICLE:
      state.reasonItem.push(payload);
      return {
        ...state,
      };
    case REQUEST_ARTICLE_LIST_PENDING:
    case REQUEST_SUBMIT_REASON_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_ARTICLE_LIST_SUCCESS:
      const data = payload.data;
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        articleList: data,
      };
    case 'REQUEST_ARTICLE_LIST_SUCCESS_2':
      payload.pop();
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        articleList: payload,
      };
    case 'REQUEST_SUBMITED_ARTIKEL':
      return {
        ...state,
        submitedArtikel: payload.map(item => item.sku),
      };
    case 'ADD_SUBMITED_ARTIKEL':
      const submitedArtikel = state.submitedArtikel;
      submitedArtikel.push(payload);
      return {
        ...state,
        submitedArtikel,
      };
    case REQUEST_SUBMIT_REASON_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
      };
    case REQUEST_ARTICLE_LIST_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        articleList: [],
      };
    case REQUEST_SUBMIT_REASON_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        message: payload?.response?.data?.status?.message ?? '',
      };
    case SET_SUBMENU:
      return {
        ...state,
        submenu: submenu[payload],
      };
    case 'SET_ARTIKEL_DIRECTORATE':
      return {
        ...state,
        directorate: payload,
      };
    case 'POST_BAKERY_IMAGE_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'POST_BAKERY_IMAGE_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
      };
    case 'POST_BAKERY_IMAGE_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'GET_BAKERY_IMAGE_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'GET_BAKERY_IMAGE_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        gallery: payload,
        error: false,
      };
    case 'GET_BAKERY_IMAGE_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'SET_GALLERY_CREATED_AT':
      return {
        ...state,
        galleryCreatedAt: payload,
      };
    case REQUEST_FASHION_FILTER_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_FASHION_FILTER_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        fashionFilter: payload,
        error: false,
      };
    case REQUEST_FASHION_FILTER_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    default:
      return state;
  }
};

export default articleReducer;
