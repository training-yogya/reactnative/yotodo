import {
  REQUEST_INSIGHT_HEADER_PENDING,
  REQUEST_INSIGHT_HEADER_SUCCESS,
  REQUEST_INSIGHT_HEADER_FAILED,
  REQUEST_INSIGHT_DIRECTORATE_PENDING,
  REQUEST_INSIGHT_DIRECTORATE_SUCCESS,
  REQUEST_INSIGHT_DIRECTORATE_FAILED,
  REQUEST_INSIGHT_DIVISION_PENDING,
  REQUEST_INSIGHT_DIVISION_SUCCESS,
  REQUEST_INSIGHT_DIVISION_FAILED,
  REQUEST_INSIGHT_DETAIL_PENDING,
  REQUEST_INSIGHT_DETAIL_SUCCESS,
  REQUEST_INSIGHT_DETAIL_FAILED,
} from './../actionTypes';

const menu = [
  {
    id: 1,
    type: 'todo',
    name: 'Summary Of\nAll Todo',
    count: 0,
    color: 'rgba(255, 104, 3, ',
  },
  {
    id: 2,
    type: 'planogram',
    icon: 'cube',
    name: 'Summary Of\nAll Planogram',
    color: 'rgba(48, 172, 255, ',
  },
  {
    id: 3,
    type: 'webview',
    name: 'Info Yogya Group\n@ instagram',
    icon: 'instagram',
    link: 'https://www.instagram.com/info_yogyagroup/',
  },
  {
    id: 4,
    type: 'webview',
    name: 'Info Yogya Group\n@ facebook',
    icon: 'facebook',
    link: 'https://m.facebook.com/BelanjaHematYaYogya/',
  },
  {
    id: 5,
    type: 'webview',
    name: 'Info Catalog\nPromo Kompetitor',
    icon: 'cart-outline',
    link: 'https://katalogpromosi.com/category/pasar-swalayan/',
  },
  {
    id: 6,
    type: 'webview',
    name: 'Info Prakiraan\nCuaca',
    icon: 'weather-partlycloudy',
    link: 'https://www.accuweather.com/',
  },
];

const initialState = {
  loading: false,
  success: false,
  error: false,
  menu: [],
  directorate: [],
  menuDivision: [],
  directorateName: '',
  time: 0,
  reason: {label: [], value: []},
  lineChart: {label: [], value: []},
  devision: '',
  insightTodo: {},
  header: {
    summary_yotodo: 0,
    summary_planogram: 0,
    instagram: '',
    facebook: '',
    kompetitor: '',
    weather: '',
  },
};

const insightInfoReducer = (state = initialState, action) => {
  const {payload, type} = action;

  switch (type) {
    case REQUEST_INSIGHT_HEADER_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_INSIGHT_HEADER_SUCCESS:
      const arrayHeaderKey = Object.keys(payload);
      arrayHeaderKey.shift();
      const menuList = menu.map((menuItem, i) => {
        return {
          ...menuItem,
          link: payload[arrayHeaderKey[i]],
          count: payload[arrayHeaderKey[i]],
        };
      });
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        header: payload,
        menu: menuList,
      };
    case REQUEST_INSIGHT_HEADER_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case REQUEST_INSIGHT_DIRECTORATE_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_INSIGHT_DIRECTORATE_SUCCESS:
      return {
        ...state,
        loading: false,
        success: false,
        directorate: payload,
        error: false,
      };
    case REQUEST_INSIGHT_DIRECTORATE_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
      };
    case REQUEST_INSIGHT_DIVISION_PENDING:
      return {
        ...state,
        loading: true,
        error: false,
        success: false,
      };
    case REQUEST_INSIGHT_DIVISION_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        success: false,
        menuDivision: payload,
      };
    case REQUEST_INSIGHT_DIVISION_FAILED:
      return {
        ...state,
        loading: false,
        error: false,
        success: false,
      };
    case REQUEST_INSIGHT_DETAIL_PENDING:
      return {
        ...state,
        loading: true,
        error: false,
        success: false,
      };
    case REQUEST_INSIGHT_DETAIL_SUCCESS:
      const label = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
      ];

      const reasonLabel = payload.map((item, i) => ({
        code: label[i],
        description: item.name,
        value: item.count,
      }));

      const reasonValue = payload.map(item => parseInt(item.count));

      return {
        ...state,
        loading: false,
        error: false,
        reason: {
          label: reasonLabel,
          value: reasonValue,
        },
        lineChart: {
          label: action.linechart.map(item => item.date),
          value: action.linechart.map(item => parseInt(item.count)),
        },
        success: true,
      };
    case REQUEST_INSIGHT_DETAIL_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
      };
    case 'INSIGHT_SAVE_DIRECTORATE':
      return {
        ...state,
        directorateName: payload,
      };
    case 'INSIGHT TODO':
      return {
        ...state,
        loading: false,
        error: false,
        success: false,
        insightTodo: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default insightInfoReducer;
