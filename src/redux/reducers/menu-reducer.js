import {GET_MENU_SUCCESS} from './../actionTypes';

const initialState = {
  loading: false,
  success: false,
  error: false,
  items: [],
};

const menuReducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case 'GET_MENU_LOADING':
      return {
        ...state,
        loading: true,
      };
    case GET_MENU_SUCCESS:
      return {
        ...state,
        loading: false,
        items: [...payload],
      };
    case 'GET_MENU_ERROR':
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default menuReducer;
