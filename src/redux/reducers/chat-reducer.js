const initalState = {
  unReadRooms: [],
};

export const chatReducer = (state = initalState, action) => {
  switch (action.type) {
    case 'SET_UNREAD_ROOMS':
      return {
        ...state,
        unReadRooms: action.payload,
      };
    default:
      return state;
  }
};
