const initialState = {
  loading: false,
  success: false,
  error: false,
  omsList: [],
  omsListDetail: [],
};

const yoPicker = (state = initialState, action) => {
  const {payload, type} = action;
  switch (type) {
    case 'REQUEST_OMS_LIST_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'REQUEST_OMS_LIST_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        omsList: payload,
      };
    case 'REQUEST_OMS_LIST_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        omsList: [],
      };
    case 'REQUEST_OMS_LIST_DETAIL_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
        omsListDetail: [],
      };
    case 'REQUEST_OMS_LIST_DETAIL_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        omsListDetail: payload,
      };
    case 'REQUEST_OMS_LIST_DETAIL_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        omsListDetail: [],
      };
    case 'SET_QTY_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'SET_QTY_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
      };
    case 'SET_QTY_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'VALIDATE_PRODUCT_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'VALIDATE_PRODUCT_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
      };
    case 'VALIDATE_PRODUCT_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'FINISH_PICKER_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'FINISH_PICKER_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
      };
    case 'FINISH_PICKER_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    default:
      return state;
  }
};

export default yoPicker;
