import {
  REQUEST_TUTORIAL_DIRECTORATE_PENDING,
  REQUEST_TUTORIAL_DIRECTORATE_SUCCESS,
  REQUEST_TUTORIAL_DIRECTORATE_FAILED,
  REQUEST_TUTORIAL_SUBDIRECTORATE_PENDING,
  REQUEST_TUTORIAL_SUBDIRECTORATE_SUCCESS,
  REQUEST_TUTORIAL_SUBDIRECTORATE_FAILED,
} from './../actionTypes';

const initialState = {
  loading: false,
  success: false,
  error: false,
  menu: [],
  global: [],
  submenu: [],
};

const tutorialReducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case REQUEST_TUTORIAL_DIRECTORATE_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_TUTORIAL_DIRECTORATE_SUCCESS:
      return {
        ...state,
        loading: false,
        menu: payload.menu,
        global: payload.title,
        success: true,
        error: false,
      };
    case REQUEST_TUTORIAL_DIRECTORATE_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case REQUEST_TUTORIAL_SUBDIRECTORATE_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_TUTORIAL_SUBDIRECTORATE_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        submenu: payload.title,
        error: false,
      };
    case REQUEST_TUTORIAL_SUBDIRECTORATE_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    default:
      return state;
  }
};

export default tutorialReducer;
