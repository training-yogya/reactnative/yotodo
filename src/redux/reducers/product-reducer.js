import {
  REQUEST_PRODUCT_IDENTITY_PENDING,
  REQUEST_PRODUCT_IDENTITY_SUCCESS,
  REQUEST_PRODUCT_IDENTITY_FAILED,
} from './../actionTypes';

const initialState = {
  loading: true,
  success: false,
  error: false,
  productExp: [],
  PSOList: [],
  soType: 'scan-barcode',
  replenishType: 'product-identity',
  todaySales: [],
  PSOHeader: [],
  psoCategory: [],
  pickingReplenish: [],
  identity: {
    till_code: null,
    sku: null,
    skudesc: null,
    initial_store: null,
    last_stock: null,
    avg_price: null,
    last_rec_date: null,
    last_rec_qty: null,
    qty_in_order: null,
    avg_sales_day: null,
    last_selling_date: null,
    selling_price: null,
    promo_price: null,
    sv_code: null,
    tag: null,
    image: null,
    created_at: null,
    updated_at: null,
  },
};

const productReducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case REQUEST_PRODUCT_IDENTITY_PENDING:
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case REQUEST_PRODUCT_IDENTITY_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        identity: payload,
      };
    case REQUEST_PRODUCT_IDENTITY_FAILED:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        identity: initialState.identity,
      };
    case 'REQUEST_GET_EXPIRED_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'REQUEST_GET_EXPIRED_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        productExp: payload,
      };
    case 'REQUEST_GET_EXPIRED_FAILED':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'REQUEST_GET_PSO_LIST_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'REQUEST_GET_PSO_LIST_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        PSOList: payload,
        error: false,
      };
    case 'REQUEST_GET_PSO_LIST_FAILED':
      return {
        ...state,
        loading: false,
        success: false,
        PSOList: [],
        error: true,
      };
    case 'REQUEST_GET_PSO_LIST_HEADER_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'SET_PSO_METHOD':
      return {
        ...state,
        soType: payload,
      };
    case 'SET_REPLENISH_METHOD':
      return {
        ...state,
        replenishType: payload,
      };
    case 'REQUEST_GET_PSO_LIST_HEADER_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        PSOHeader: payload,
      };
    case 'REQUEST_GET_PSO_LIST_HEADER_FAILED':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'REQUEST_GET_PSO_CAT_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'REQUEST_GET_PSO_CAT_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        psoCategory: payload,
      };
    case 'REQUEST_GET_TODAY_SALES_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'REQUEST_GET_TODAY_SALES_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        todaySales: payload,
      };
    case 'REQUEST_GET_TODAY_SALES_FAILED':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'REQUEST_GET_PSO_CAT_FAILED':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    case 'REQUEST_GET_PICKING_REPLENISH_PENDING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'REQUEST_GET_PICKING_REPLENISH_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        pickingReplenish: payload,
      };
    case 'REQUEST_GET_PICKING_REPLENISH_FAILED':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    default:
      return state;
  }
};

export default productReducer;
