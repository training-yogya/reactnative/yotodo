import {
  GET_PLANOGRAM_MENU_SUCCESS,
  GET_PLANOGRAM_SUBMENU_SUCCESS,
  SET_PLANOGRAM_IMPLEMENTATION_START,
  SET_PLANOGRAM_IMPLEMENTATION_END,
  REQUEST_PLANOGRAM_MENU_DIRECTORATE_PENDING,
  REQUEST_PLANOGRAM_MENU_DIRECTORATE_SUCCESS,
  REQUEST_PLANOGRAM_MENU_DIRECTORATE_FAILED,
  REQUEST_PLANOGRAM_INFO_SUCCESS,
  REQUEST_PLANOGRAM_MENU_DIVISION_PENDING,
  REQUEST_PLANOGRAM_MENU_DIVISION_SUCCESS,
  REQUEST_PLANOGRAM_MENU_DIVISION_FAILED,
  REQUEST_PLANOGRAM_MENU_CATEGORY_PENDING,
  REQUEST_PLANOGRAM_MENU_CATEGORY_SUCCESS,
  REQUEST_PLANOGRAM_MENU_CATEGORY_FAILED,
  REQUEST_PLANOGRAM_PENDING,
  REQUEST_PLANOGRAM_SUCCESS,
  REQUEST_PLANOGRAM_FAILED,
  REQUEST_PLANOGRAM_REQUEST_PENDING,
  REQUEST_PLANOGRAM_REQUEST_SUCCESS,
  REQUEST_PLANOGRAM_REQUEST_FAILED,
  REQUEST_PLANOGRAM_REQUEST_LIST_PENDING,
  REQUEST_PLANOGRAM_REQUEST_LIST_SUCCESS,
  REQUEST_PLANOGRAM_REQUEST_LIST_FAILED,
  REQUEST_PLANOGRAM_REQUEST_DETAIL_PENDING,
  REQUEST_PLANOGRAM_REQUEST_DETAIL_SUCCESS,
  REQUEST_PLANOGRAM_REQUEST_DETAIL_FAILED,
} from './../actionTypes';
import config from '../../config';

const initialState = {
  loading: false,
  success: false,
  error: false,
  menuItem: [2],
  submenuItem: [],
  directorate: 'supermarket',
  devision: '',
  category: '',
  directorateList: [],
  devisionList: [1],
  categoryList: [],
  planogramInfo: '',
  planogramList: [],
  planogramRequestList: [],
  planogramRequestDetail: [],
  plogramInsightList: [],
  message: '',
  id_lama: null,
  reason: [],
  form: {
    id: null,
    start: {
      image: {
        name: '',
        type: '',
        uri: '',
        coords: '',
      },
      time: null,
    },
    end: {
      image: {
        name: '',
        type: '',
        uri: '',
        coords: '',
      },
      time: null,
    },
    duration: null,
  },
};

const planogramReducer = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case GET_PLANOGRAM_MENU_SUCCESS:
      return {
        ...state,
        menuItem: payload,
      };
    case GET_PLANOGRAM_SUBMENU_SUCCESS:
      return {
        ...state,
        submenuItem: payload,
      };
    case SET_PLANOGRAM_IMPLEMENTATION_START:
      return {
        ...state,
        form: {
          ...state.form,
          start: payload,
        },
      };
    case SET_PLANOGRAM_IMPLEMENTATION_END:
      return {
        ...state,
        form: {
          ...state.form,
          end: {
            image: payload.image,
            time: payload.time,
          },
          duration: payload.duration,
        },
      };
    case REQUEST_PLANOGRAM_MENU_DIVISION_PENDING:
      return {
        ...state,
        loading: true,
      };
    case REQUEST_PLANOGRAM_MENU_DIVISION_SUCCESS:
      return {
        ...state,
        loading: false,
        directorate: payload.data[0].directorates.toLowerCase(),
        devisionList: payload.data,
      };
    case REQUEST_PLANOGRAM_MENU_DIVISION_FAILED:
      return {
        ...state,
        loading: false,
      };
    case REQUEST_PLANOGRAM_MENU_CATEGORY_PENDING:
      return {
        ...state,
        loading: true,
      };
    case REQUEST_PLANOGRAM_MENU_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        devision: payload.data[0].division.toLowerCase(),
        categoryList: payload.data,
      };
    case REQUEST_PLANOGRAM_MENU_CATEGORY_FAILED:
      return {
        ...state,
        loading: false,
        categoryList: [],
      };
    case REQUEST_PLANOGRAM_INFO_SUCCESS:
      return {
        ...state,
        planogramInfo: payload,
      };
    case REQUEST_PLANOGRAM_PENDING:
      return {
        ...state,
        loading: true,
      };
    case REQUEST_PLANOGRAM_SUCCESS:
      return {
        ...state,
        loading: false,
        category: payload.category,
        planogramList: Array.isArray(payload.data)
          ? payload.data.map(item => {
              return {
                ...item,
                url: item.url.replace(
                  'http://monitore.yogya.com',
                  config.monitore,
                ),
              };
            })
          : [],
      };
    case REQUEST_PLANOGRAM_FAILED:
      return {
        ...state,
        loading: false,
        planogramList: [],
      };

    case REQUEST_PLANOGRAM_REQUEST_LIST_PENDING:
      return {
        ...state,
        success: false,
        loading: true,
        error: false,
      };
    case REQUEST_PLANOGRAM_REQUEST_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        planogramRequestList: payload,
      };
    case REQUEST_PLANOGRAM_REQUEST_LIST_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
        planogramRequestList: [],
      };
    case REQUEST_PLANOGRAM_REQUEST_DETAIL_PENDING:
      return {
        ...state,
        success: false,
        loading: true,
        error: false,
      };
    case REQUEST_PLANOGRAM_REQUEST_DETAIL_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        planogramRequestDetail: payload,
      };
    case REQUEST_PLANOGRAM_REQUEST_DETAIL_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
        planogramRequestDetail: [],
      };

    case REQUEST_PLANOGRAM_REQUEST_PENDING:
      return {
        ...state,
        success: false,
        loading: true,
        error: false,
      };
    case REQUEST_PLANOGRAM_REQUEST_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        planogramList: payload,
      };
    case REQUEST_PLANOGRAM_REQUEST_FAILED:
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
        planogramList: [],
      };
    case 'PLANOGRAM_REASON_SUCCESS':
      return {
        ...state,
        reason: payload,
      };
    case 'PLANOGRAM_SET_DIVISION': {
      return {
        ...state,
        devision: payload,
      };
    }
    case 'REQUEST_PLANOGRAM_GLOBAL_PENDING':
      return {
        ...state,
        success: false,
        loading: true,
        error: false,
      };
    case 'REQUEST_PLANOGRAM_GLOBAL_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        id_lama: null,
      };
    case 'REQUEST_PLANOGRAM_GLOBAL_FAILED':
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
        id_lama: null,
        message: payload?.response?.data?.status?.message ?? '',
      };
    case 'REQUEST_PLANOGRAM_INSIGHT_PENDING':
      return {
        ...state,
        loading: false,
        error: false,
        success: false,
      };
    case 'REQUEST_PLANOGRAM_INSIGHT_SUCCESS':
      return {
        ...state,
        loading: false,
        plogramInsightList: payload,
        error: false,
        success: false,
      };
    case 'persist/REHYDRATE':
      const planogramData = payload?.planogram ?? {};
      return {
        ...state,
        ...planogramData,
      };
    case 'REQUEST_PLANOGRAM_INSIGHT_FAILED':
      return {
        ...state,
        loading: false,
        error: false,
        success: false,
      };
    case 'PLANOGRAM_SET_ID_LAMA':
      return {
        ...state,
        id_lama: payload,
      };
    default:
      return {...state};
  }
};

export default planogramReducer;
