const initialState = {
  loading: false,
  success: false,
  failed: false,
  postLoading: false,
  postSuccess: false,
  postFailed: false,
  list: [],
  listKhusus: [],
  listDetail: [{}],
};

const promoNotif = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case 'GET_PROMO_LOADING':
      return {
        ...state,
        loading: true,
        success: false,
        failed: false,
      };
    case 'GET_PROMO_SUCCESS':
      const list =
        payload.type === 'Jawa Tengah'
          ? {
              listKhusus: payload.list,
            }
          : {
              list: payload.list,
            };

      return {
        ...state,
        loading: false,
        success: true,
        failed: false,
        ...list,
      };
    case 'GET_PROMO_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        failed: true,
      };
    case 'POST_READLINE_PENDING':
      return {
        ...state,
        postLoading: true,
        postSuccess: false,
        postFailed: false,
      };
    case 'POST_READLINE_SUCCESS':
      return {
        ...state,
        postLoading: false,
        postSuccess: true,
        postFailed: false,
      };
    case 'POST_READLINE_ERROR':
      return {
        ...state,
        postLoading: false,
        postSuccess: false,
        postFailed: true,
      };
    case 'GET_PROMO_DETAIL_LOADING':
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
      };
    case 'GET_PROMO_DETAIL_SUCCESS':
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        listDetail: payload.list,
      };
    case 'GET_PROMO_DETAIL_ERROR':
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
      };
    default:
      return state;
  }
};

export default promoNotif;
