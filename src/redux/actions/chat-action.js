import AsyncStorage from '@react-native-community/async-storage';
import client from './../../utils/customAxios';

import {GET_ROOM_LIST_PENDING, GET_ROOM_LIST_SUCCESS} from './../actionTypes';

export const getRooms = () => {
  return async dispatch => {
    try {
      dispatch({
        type: GET_ROOM_LIST_PENDING,
      });
      const roomList = await client.get('/room/');

      dispatch({
        type: GET_ROOM_LIST_SUCCESS,
        payload: roomList.data.data,
      });
    } catch (error) {
      dispatch({
        type: GET_ROOM_LIST_SUCCESS,
        payload: error,
      });
    }
  };
};

export const setInitialUnreadRooms = () => {
  return async dispatch => {
    const unread = await AsyncStorage.getItem('unReadRooms');

    dispatch({
      type: 'SET_UNREAD_ROOMS',
      payload: JSON.parse(unread),
    });
  };
};

export const setUnreadRooms = unreadroom => {
  return dispatch => {
    dispatch({
      type: 'SET_UNREAD_ROOMS',
      payload: unreadroom,
    });
  };
};

export const sendNotif = (title, body) => {
  return async dispatch => {
    try {
      const formData = new FormData();
      formData.append('room', title);
      formData.append('message', body);
      dispatch({
        type: 'SEND_NOTIF_PENDING',
      });
      const response = client.post('/fcm/send', formData);

      dispatch({
        type: 'SEND_NOTIF_SUCCEED',
        response,
      });
    } catch (error) {
      dispatch({
        type: 'SEND_NOTIF_FAILED',
        error,
      });
    }
  };
};
