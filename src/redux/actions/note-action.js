import client from './../../utils/customAxios';
import {store} from './../store';

import {
  REQUEST_GET_NOTE_PENDING,
  REQUEST_GET_NOTE_SUCCESS,
  REQUEST_GET_NOTE_FAILED,
  REQUEST_GET_NOTE_LIST_PENDING,
  REQUEST_GET_NOTE_LIST_SUCCESS,
  REQUEST_GET_NOTE_LIST_FAILED,
  REQUEST_POST_NOTE_PENDING,
  REQUEST_POST_NOTE_SUCCESS,
  REQUEST_POST_NOTE_FAILED,
  REQUEST_UPDATE_NOTE_PENDING,
  REQUEST_UPDATE_NOTE_SUCCESS,
  REQUEST_UPDATE_NOTE_FAILED,
  REQUEST_DELETE_NOTE_PENDING,
  REQUEST_DELETE_NOTE_SUCCESS,
  REQUEST_DELETE_NOTE_FAILED,
  REQUEST_CREATE_NOTE_PENDING,
  REQUEST_CREATE_NOTE_SUCCESS,
  REQUEST_CREATE_NOTE_FAILED,
} from './../actionTypes';

export const getNote = () => {
  return async dispatch => {
    dispatch({
      type: REQUEST_GET_NOTE_PENDING,
    });
    const state = store.getState();

    const userLogin = state.auth.userLogin;

    try {
      const note = await client.get(
        `/notes/${userLogin.id}/${userLogin.initial_store}`,
      );

      dispatch({
        type: REQUEST_GET_NOTE_SUCCESS,
        payload: note.data.data[0],
      });
    } catch (error) {
      dispatch({
        type: REQUEST_GET_NOTE_FAILED,
        payload: error,
      });
    }
  };
};

export const postNote = notes => {
  return async dispatch => {
    dispatch({
      type: REQUEST_POST_NOTE_PENDING,
    });
    const state = store.getState();

    const userLogin = state.auth.userLogin;

    const data = new FormData();

    data.append('notes', notes);
    data.append('initial_store', userLogin.initial_store);
    data.append('user_id', userLogin.id);

    try {
      const note = await client.post('/notes', data);

      dispatch({
        type: REQUEST_POST_NOTE_SUCCESS,
        payload: notes,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_POST_NOTE_FAILED,
        payload: error,
      });
    }
  };
};

export const getNoteList = () => {
  return async dispatch => {
    try {
      dispatch({
        type: REQUEST_GET_NOTE_LIST_PENDING,
      });
      const state = store.getState();

      const userLogin = state.auth.userLogin;
      const noteList = await client.get(
        `/notes/${userLogin.id}/${userLogin.initial_store}`,
      );

      dispatch({
        type: REQUEST_GET_NOTE_LIST_SUCCESS,
        payload: noteList.data.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_GET_NOTE_LIST_FAILED,
        payload: error,
      });
    }
  };
};

export const createNote = data => {
  return async dispatch => {
    try {
      dispatch({
        type: REQUEST_CREATE_NOTE_PENDING,
      });
      const state = store.getState();

      const body = new FormData();

      body.append('notes', data.notes);
      body.append('title', data.title);

      const userLogin = state.auth.userLogin;
      const noteList = await client.post(
        `/notes/${userLogin.id}/${userLogin.initial_store}/`,
        body,
      );

      dispatch({
        type: REQUEST_CREATE_NOTE_SUCCESS,
        payload: noteList.data,
      });

      return Promise.resolve(noteList.data.status.note_id);
    } catch (error) {
      dispatch({
        type: REQUEST_CREATE_NOTE_FAILED,
        payload: error,
      });
    }
  };
};

export const updateNote = data => {
  return async dispatch => {
    try {
      dispatch({
        type: REQUEST_UPDATE_NOTE_PENDING,
      });
      const state = store.getState();
      const userLogin = state.auth.userLogin;

      const body = new FormData();

      body.append('notes', data.notes);
      body.append('title', data.title);
      body.append('user_id', userLogin.id);
      body.append('inital_store', userLogin.initial_store);

      const noteList = await client.post(
        `/notes/${userLogin.id}/${userLogin.initial_store}/${data.id}`,
        body,
      );

      dispatch({
        type: REQUEST_UPDATE_NOTE_SUCCESS,
        payload: data.notes,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_UPDATE_NOTE_FAILED,
        payload: error,
      });
    }
  };
};

export const deleteNote = data => {
  return async dispatch => {
    try {
      dispatch({
        type: REQUEST_DELETE_NOTE_PENDING,
      });
      const state = store.getState();

      const userLogin = state.auth.userLogin;
      const noteList = await client.delete(
        `/notes/${userLogin.id}/${userLogin.initial_store}/${data.id}`,
      );

      dispatch({
        type: REQUEST_DELETE_NOTE_SUCCESS,
        payload: noteList.data.data,
      });
      return Promise.resolve('deleted');
    } catch (error) {
      dispatch({
        type: REQUEST_DELETE_NOTE_FAILED,
        payload: error,
      });
    }
  };
};

export const getSearch = text => {
  return dispatch => {
    const noteList = store.getState().note.noteList;

    text = text.toLowerCase();

    const newList = noteList.filter(notes => {
      const title = notes.title.toLowerCase();
      // const notesText = notes.notes.toLowerCase();

      return title.includes(text); /*|| notesText.includes(text);*/
    });

    return Promise.resolve(newList);
  };
};
