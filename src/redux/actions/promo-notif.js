import client from './../../utils/customAxios';
import {store} from './../store';
import Axios from 'axios';

export const getPromo = type => {
  return async dispatch => {
    dispatch({
      type: 'GET_PROMO_LOADING',
    });

    try {
      const promo =
        type === 'jateng'
          ? await client.get('/promo/Jawa Tengah')
          : await client.get('/promo/Non Jawa Tengah');

      type = type === 'jateng' ? 'Jawa Tengah' : 'non jateng';

      dispatch({
        type: 'GET_PROMO_SUCCESS',
        payload: {list: promo.data.data, type},
      });
    } catch (error) {
      dispatch({
        type: 'GET_PROMO_ERROR',
      });
    }
  };
};

export const getPromoDetail = promoId => {
  return async dispatch => {
    dispatch({
      type: 'GET_PROMO_DETAIL_LOADING',
    });

    try {
      const promo = await client.get(`/promo/detail/${promoId}`);
      dispatch({
        type: 'GET_PROMO_DETAIL_SUCCESS',
        payload: {list: promo.data.data},
      });
    } catch (error) {
      dispatch({
        type: 'GET_PROMO_DETAIL_ERROR',
      });
    }
  };
};

export const postReadline = (data, menu_id) => {
  return async dispatch => {
    dispatch({
      type: 'POST_READLINE_PENDING',
    });

    const userLogin = store.getState().auth.userLogin;

    const imageArray = data.map(image => {
      const imageForm = new FormData();
      imageForm.append('file', image);

      return imageForm;
    });

    const imageRequest = imageArray.map((formDataImage, i) => {
      const requestImage = Axios.post(
        'https://gold-api.yogyagroup.com/file-upload-readline',
        formDataImage,
      );

      return requestImage;
    });

    const imageResponses = await Promise.all(imageRequest);

    const body = new FormData();
    body.append('user_id', userLogin.id);
    body.append('initial_store', userLogin.initial_store);
    body.append('latitude', data[0].coords.latitude);
    body.append('longitude', data[0].coords.longitude);

    imageResponses.forEach((imageResponse, i) => {
      body.append(`foto_${i + 1}`, imageResponse.data.filename);
    });

    try {
      const readlineResponse = await client.post(
        `/promo/readline/${menu_id}`,
        body,
      );
      dispatch({
        type: 'POST_READLINE_SUCCESS',
        payload: readlineResponse,
      });
    } catch (error) {
      dispatch({
        type: 'POST_READLINE_SUCCESS',
        payload: error,
      });
    }
  };
};
