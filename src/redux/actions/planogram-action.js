import {
  GET_PLANOGRAM_MENU_SUCCESS,
  GET_PLANOGRAM_SUBMENU_SUCCESS,
  REQUEST_PLANOGRAM_MENU_DIRECTORATE_PENDING,
  REQUEST_PLANOGRAM_MENU_DIRECTORATE_SUCCESS,
  REQUEST_PLANOGRAM_MENU_DIRECTORATE_FAILED,
  REQUEST_PLANOGRAM_INFO_PENDING,
  REQUEST_PLANOGRAM_INFO_SUCCESS,
  REQUEST_PLANOGRAM_INFO_FAILED,
  REQUEST_PLANOGRAM_MENU_DIVISION_PENDING,
  REQUEST_PLANOGRAM_MENU_DIVISION_SUCCESS,
  REQUEST_PLANOGRAM_MENU_DIVISION_FAILED,
  REQUEST_PLANOGRAM_MENU_CATEGORY_PENDING,
  REQUEST_PLANOGRAM_MENU_CATEGORY_SUCCESS,
  REQUEST_PLANOGRAM_MENU_CATEGORY_FAILED,
  REQUEST_PLANOGRAM_PENDING,
  REQUEST_PLANOGRAM_SUCCESS,
  REQUEST_PLANOGRAM_FAILED,
  REQUEST_PLANOGRAM_REQUEST_PENDING,
  REQUEST_PLANOGRAM_REQUEST_SUCCESS,
  REQUEST_PLANOGRAM_REQUEST_FAILED,
  REQUEST_PLANOGRAM_REQUEST_LIST_PENDING,
  REQUEST_PLANOGRAM_REQUEST_LIST_SUCCESS,
  REQUEST_PLANOGRAM_REQUEST_LIST_FAILED,
  REQUEST_PLANOGRAM_REQUEST_DETAIL_PENDING,
  REQUEST_PLANOGRAM_REQUEST_DETAIL_SUCCESS,
  REQUEST_PLANOGRAM_REQUEST_DETAIL_FAILED,
} from './../actionTypes';

import client from './../../utils/customAxios';

import axios from 'axios';

import {store} from './../store';
import moment from 'moment';
import {Platform} from 'react-native';
import config from '../../config';

export const getPlanogramMenu = () => {
  return async dispatch => {
    try {
      const menu = await import('./../../data/planogram-menu.json');
      dispatch({
        type: GET_PLANOGRAM_MENU_SUCCESS,
        payload: menu.default,
      });
    } catch (error) {}
  };
};

export const getPlanogramSubmenu = menu => {
  return async dispatch => {
    try {
      menu = menu
        .toLowerCase()
        .replace(/ /gi, '')
        .replace('.', '');
      const submenu = await import('./../../data/planogram-submenu.json');
      dispatch({
        type: GET_PLANOGRAM_SUBMENU_SUCCESS,
        payload: submenu.default[menu],
      });
    } catch (error) {}
  };
};

export const menuDirectorate = () => {
  return async dispatch => {
    try {
      const directorate = await client.get('/directorates');
    } catch (error) {}
  };
};

export const menuDevision = directorate => {
  return async dispatch => {
    dispatch({
      type: REQUEST_PLANOGRAM_MENU_DIVISION_PENDING,
    });
    try {
      const parameter = directorate.toLowerCase().replace(/ /gi, '_');
      // const menu = await import('./../../data/planogram-menu.json');
      const devision = await client.get('/division/' + parameter);
      // devision.data.data = devision.data.data.concat(menu.default);
      dispatch({
        type: REQUEST_PLANOGRAM_MENU_DIVISION_SUCCESS,
        payload: devision.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_MENU_DIVISION_FAILED,
        payload: error,
      });
    }
  };
};

export const menuCategory = devision => {
  return async dispatch => {
    const state = store.getState();

    const directorate = state.planogram.directorate || 'supermarket';

    try {
      const parameter = directorate + '/' + devision.toLowerCase();
      dispatch({
        type: REQUEST_PLANOGRAM_MENU_CATEGORY_PENDING,
      });
      const category = await client.get('/categories/' + parameter);

      if (category.data.data.length === 0) {
        throw new Error('error');
      }

      dispatch({
        type: REQUEST_PLANOGRAM_MENU_CATEGORY_SUCCESS,
        payload: category.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_MENU_CATEGORY_FAILED,
        payload: error,
      });
    }
  };
};

export const getPlanogramData = category => {
  return async dispatch => {
    const state = store.getState();

    category = category.replace(/ /gi, '_').toLowerCase();

    const branch = state.auth.userLogin.initial_store;
    try {
      dispatch({
        type: REQUEST_PLANOGRAM_PENDING,
      });
      const planogram = await axios.get(
        `${config.monitore}/wv.php?rn=${category}&branch=${branch}`,
      );
      dispatch({
        type: REQUEST_PLANOGRAM_SUCCESS,
        payload: {data: planogram.data, category},
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_FAILED,
      });
    }
  };
};

export const getPlanogramInfoUrl = () => {
  return async dispatch => {
    const state = store.getState();

    const userLogin = state.auth.userLogin;

    try {
      dispatch({
        type: REQUEST_PLANOGRAM_INFO_PENDING,
      });
      const planogramInfo = await client.get(`/${userLogin.id}/info_planogram`);

      dispatch({
        type: REQUEST_PLANOGRAM_INFO_SUCCESS,
        payload: planogramInfo.data.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_INFO_FAILED,
        payload: error,
      });
    }
  };
};

export const requestPlanogramRequest = (devision, planogramRequest) => {
  return async dispatch => {
    const state = store.getState();
    const body = new FormData();

    const userLogin = state.auth.userLogin;
    const planogram = state.planogram;
    const devisionText = devision || planogram.devision;
    try {
      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_PENDING,
      });
      body.append('user_id', userLogin.id);
      body.append('initial_store', userLogin.initial_store);
      body.append('division', devisionText.toUpperCase());
      body.append('planogram_request', JSON.stringify(planogramRequest));

      const planogramInfo = await client.post('/planogram/request', body);

      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_SUCCESS,
        payload: planogramInfo.data.data,
      });
      return Promise.resolve(planogramInfo);
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_FAILED,
        payload: error,
      });
    }
  };
};

export const getPlanogramRequestList = () => {
  return async dispatch => {
    const state = store.getState();

    const userLogin = state.auth.userLogin;
    try {
      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_LIST_PENDING,
      });

      const planogramRequestList = await client.get(
        `/planogram/request/${userLogin.id}`,
      );

      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_LIST_SUCCESS,
        payload: planogramRequestList.data.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_LIST_FAILED,
        payload: error,
      });
    }
  };
};

export const getRequestListSearch = text => {
  return dispatch => {
    const requestList = store.getState().planogram.planogramRequestList;

    text = text.toLowerCase();

    const newList = requestList.filter(requestItem => {
      const categories = requestItem.categories.toLowerCase();
      const id = parseInt(text, 10);
      const date = moment(requestItem).format('DD MMMM YYYY');
      return (
        categories.includes(text) ||
        date.includes(text) ||
        id === requestItem.id
      );
    });

    return Promise.resolve(newList);
  };
};

export const getPlanogramRequestDetail = id => {
  return async dispatch => {
    try {
      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_DETAIL_PENDING,
      });

      const planogramRequestDetail = await client.get(
        `/planogram/status/${id}`,
      );

      const payload = planogramRequestDetail.data.data.map(item => {
        return {
          time: moment(item.date).format('HH:mm'),
          title: moment(item.date).format('DD MMMM YYYY'),
          description: item.description,
        };
      });

      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_DETAIL_SUCCESS,
        payload: payload,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PLANOGRAM_REQUEST_DETAIL_FAILED,
        payload: error,
      });
    }
  };
};

export const getPlanogramReason = () => {
  return async dispatch => {
    try {
      const reasons = await client.get('/reason/planogram');

      dispatch({
        type: 'PLANOGRAM_REASON_SUCCESS',
        payload: reasons.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'PLANOGRAM_REASON_FAILED',
        payload: error,
      });
    }
  };
};

export const submitPlanogramImplementation = () => {
  return async dispatch => {
    const state = store.getState();
    const profile = state.auth.userLogin;
    const planogram = state.planogram;
    const body = new FormData();

    const dataImageBefore = new FormData();
    const dataImageAfter = new FormData();

    dataImageBefore.append(
      'file',
      {
        uri:
          Platform.OS === 'android'
            ? planogram.form.start.image.uri
            : planogram.form.start.image.uri.replace('file://', ''),
        name: planogram.form.start.image.name,
        type: planogram.form.start.image.type,
      },
      planogram.form.start.image.name,
    );

    dataImageAfter.append(
      'file',
      {
        uri:
          Platform.OS === 'android'
            ? planogram.form.end.image.uri
            : planogram.form.end.image.uri.replace('file://', ''),
        name: planogram.form.end.image.name,
        type: planogram.form.end.image.type,
      },
      planogram.form.end.image.name,
    );

    try {
      dispatch({
        type: 'REQUEST_PLANOGRAM_GLOBAL_PENDING',
      });

      const fotoBeforeRequest = axios.post(
        'https://gold-api.yogyagroup.com/file-upload-plano',
        dataImageBefore,
      );

      const fotoAfterRequest = axios.post(
        'https://gold-api.yogyagroup.com/file-upload-plano',
        dataImageAfter,
      );

      const [foto_before, foto_after] = await Promise.all([
        fotoBeforeRequest,
        fotoAfterRequest,
      ]);

      body.append('user_id', profile.id);
      body.append('initial_store', profile.initial_store);
      body.append('division', planogram.devision.toUpperCase());
      body.append('categories', planogram.category.toUpperCase());

      body.append('foto_before', foto_before.data.filename);
      body.append('foto_after', foto_after.data.filename);

      body.append('time', planogram.form.duration);
      body.append('latitude', planogram.form.start.image.coords.latitude);
      body.append('longitude', planogram.form.start.image.coords.longitude);
      if (planogram.id_lama) {
        body.append('id_lama', planogram.id_lama);
      }

      const implement = await client.post('/planogram/upload_implement', body);

      dispatch({
        type: 'REQUEST_PLANOGRAM_GLOBAL_SUCCESS',
        payload: implement,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_PLANOGRAM_GLOBAL_FAILED',
        payload: error,
      });
    }
  };
};

export const submitPlanogramCannotImplementation = ({
  reason_id,
  image,
} = {}) => {
  return async dispatch => {
    const state = store.getState();
    const profile = state.auth.userLogin;
    const planogram = state.planogram;
    const body = new FormData();
    const dataImage = new FormData();
    try {
      dispatch({
        type: 'REQUEST_PLANOGRAM_GLOBAL_PENDING',
      });

      dataImage.append('file', image, image.name);

      const imageResponse = await axios.post(
        'https://gold-api.yogyagroup.com/file-upload-plano',
        dataImage,
      );

      body.append('user_id', profile.id);
      body.append('initial_store', profile.initial_store);
      body.append('reason', JSON.stringify(reason_id));

      body.append('division', planogram.devision.toUpperCase());
      body.append('categories', planogram.category.toUpperCase());

      body.append('tanggal', moment().format('YYYY-MM-DD'));
      body.append('foto', imageResponse.data.filename);

      body.append('latitude', image.coords.latitude);
      body.append('longitude', image.coords.longitude);

      const implement = await client.post('/planogram/reason_planogram', body);

      dispatch({
        type: 'REQUEST_PLANOGRAM_GLOBAL_SUCCESS',
        payload: implement,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_PLANOGRAM_GLOBAL_FAILED',
        payload: error,
      });
    }
  };
};

export const getPlanogramInsight = division => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_PLANOGRAM_INSIGHT_PENDING',
    });
    const userLogin = store.getState().auth.userLogin;
    const directorate = store.getState().planogram.directorate || 'supermarket';
    try {
      const parameter = directorate + '/' + division.toLowerCase();
      dispatch({
        type: REQUEST_PLANOGRAM_MENU_CATEGORY_PENDING,
      });
      const category = client.get('/categories/' + parameter);

      const planogramInsight = client.get(
        `/planogram/insight/${userLogin.initial_store}/${division}`,
      );

      const result = await Promise.all([category, planogramInsight]);

      dispatch({
        type: REQUEST_PLANOGRAM_MENU_CATEGORY_SUCCESS,
        payload: result[0].data,
      });
      dispatch({
        type: 'REQUEST_PLANOGRAM_INSIGHT_SUCCESS',
        payload: result[1].data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_PLANOGRAM_INSIGHT_FAILED',
        payload: error,
      });
    }
  };
};
