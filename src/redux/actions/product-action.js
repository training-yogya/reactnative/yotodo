import client, {gold} from './../../utils/customAxios';
import apiUrl from './../../utils/apiList';

import {
  REQUEST_PRODUCT_IDENTITY_PENDING,
  REQUEST_PRODUCT_IDENTITY_SUCCESS,
  REQUEST_PRODUCT_IDENTITY_FAILED,
} from './../actionTypes';
import {store} from '../store';

import {log} from './log-action';

export const getProductIdentity = (sku, cabang, parentTitle) => {
  return async dispatch => {
    dispatch({
      type: REQUEST_PRODUCT_IDENTITY_PENDING,
    });
    const userLogin = store.getState().auth.userLogin;
    try {
      const identityrequest = await gold.get(
        apiUrl.product.identity + '/' + cabang + '/' + sku,
      );
      const chartReqeusest = await gold.get(
        `/api/v1/trans/sales/${userLogin.store_code}/${
          identityrequest.data[0].sku
        }`,
      );
      const productLocation = await client.get(
        `/item_location/${cabang}/${identityrequest.data[0].sku}`,
      );
      const chartData = chartReqeusest.data;
      chartData.pop();
      dispatch({
        type: REQUEST_PRODUCT_IDENTITY_SUCCESS,
        payload: {
          ...identityrequest.data[0],
          chart: chartData,
          productLocation: productLocation?.data?.data?.[0]?.lokasi ?? null,
        },
      });

      dispatch(log('product-identity'));
    } catch (error) {
      dispatch({
        type: REQUEST_PRODUCT_IDENTITY_FAILED,
        payload: error,
      });
    }
  };
};

export const getTodaySales = () => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_GET_TODAY_SALES_PENDING',
    });
    const state = store.getState();

    const userLogin = state.auth.userLogin;

    try {
      const replenish = await client.get(
        `/replenishment/${userLogin.initial_store}/sales`,
      );

      dispatch({
        type: 'REQUEST_GET_TODAY_SALES_SUCCESS',
        payload: replenish.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_GET_TODAY_SALES_FAILED',
        payload: error,
      });
    }
  };
};

export const getExpiredList = (type = '', expired_in) => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_GET_EXPIRED_PENDING',
    });
    const state = store.getState();

    const params = {expired_in};

    const expiredType = type.split(' ')[0];
    const userLogin = state.auth.userLogin;
    let url = `/item_expired/${userLogin.initial_store}`;

    switch (expiredType) {
      case 'today':
        url += '/today/list';
        break;
      case 'previous':
        url += '/previous/list';
        break;
      case 'expired':
        url += '';
        break;
      case 'deleted':
        url += '/deleted/list';
        break;
      default:
        break;
    }

    try {
      const expired = await client.get(url, {params});

      dispatch({
        type: 'REQUEST_GET_EXPIRED_SUCCESS',
        payload: expired.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_GET_EXPIRED_FAILED',
        payload: error,
      });
    }
  };
};

export const addExpired = data => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_ADD_EXPIRED_PENDING',
    });
    const state = store.getState();

    const body = new FormData();

    body.append('sku', data.sku);
    body.append('skudesc', data.skudesc);
    body.append('tanggal_expired', data.tanggal_expored);
    body.append('initial_store', state.auth.userLogin.initial_store);
    body.append('note', data.note || '-');
    body.append('created_by', state.auth.userLogin.id);
    body.append('divdesc', data.divdesc);
    body.append('catdesc', data.catdesc);
    body.append('tag', data.tag);
    body.append('qty', data.qty);

    try {
      const expired = await client.post('/item_expired/', body);

      dispatch({
        type: 'REQUEST_ADD_EXPIRED_SUCCESS',
        payload: expired,
      });
      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: 'REQUEST_ADD_EXPIRED_FAILED',
        payload: error,
      });
      return Promise.reject(new Error());
    }
  };
};

export const PSOArticleListHeader = () => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_GET_PSO_LIST_HEADER_PENDING',
    });
    const state = store.getState();

    const userLogin = state.auth.userLogin;

    try {
      const expired = await client.get(`/sopartial/${userLogin.initial_store}`);

      dispatch({
        type: 'REQUEST_GET_PSO_LIST_HEADER_SUCCESS',
        payload: expired.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_GET_PSO_LIST_HEADER_FAILED',
        payload: error,
      });
    }
  };
};

export const PSOArticelList = id => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_GET_PSO_LIST_PENDING',
    });
    const state = store.getState();

    const userLogin = state.auth.userLogin;

    try {
      const expired = await client.get(
        `/sopartial/${userLogin.initial_store}/${id}`,
      );

      dispatch({
        type: 'REQUEST_GET_PSO_LIST_SUCCESS',
        payload: expired.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_GET_PSO_LIST_FAILED',
        payload: error,
      });
    }
  };
};

export const addSOPartial = data => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_ADD_PARTIAL_SO_PENDING',
    });
    const state = store.getState();

    const body = new FormData();

    body.append('sku', data.sku);
    body.append('skudesc', data.skudesc);
    body.append('initial_store', state.auth.userLogin.initial_store);
    body.append('user_id', state.auth.userLogin.id);
    body.append('type', state.product.soType);
    body.append('note', data.note || '-');
    body.append('created_by', state.auth.userLogin.id);
    body.append('divdesc', data.divdesc);
    body.append('catdesc', data.catdesc);
    body.append('tag', data.tag);
    body.append('qty', data.qty);
    body.append('till_code', data.till_code);

    try {
      const check = await client.get(
        `/sopartial/check/${state.auth.userLogin.initial_store}/${data.sku}`,
      );

      if (check.data.status.id === 1) {
        dispatch({
          type: 'REQUEST_ADD_PARTIAL_SO_FAILED',
          payload: {response: check},
        });
        return Promise.reject(check.data);
      }
    } catch (error) {
      dispatch({
        type: 'REQUEST_ADD_PARTIAL_SO_FAILED',
        payload: error,
      });
      return Promise.reject(error?.response?.data ?? {});
    }

    try {
      const expired = await client.post('/sopartial/', body);

      dispatch({
        type: 'REQUEST_ADD_PARTIAL_SO_SUCCESS',
        payload: expired,
      });

      dispatch({
        type: 'ADD_SUBMITED_ARTIKEL',
        payload: data.sku,
      });
      return Promise.resolve(expired.data);
    } catch (error) {
      dispatch({
        type: 'REQUEST_ADD_PARTIAL_SO_FAILED',
        payload: error,
      });
      return Promise.reject(new Error());
    }
  };
};

export const deletePSOItemDetail = psoId => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_DELETE_PSO_LIST_PENDING',
    });

    try {
      const expired = await client.delete(`/sopartial/${psoId}/delete`);

      dispatch({
        type: 'REQUEST_DELETE_PSO_LIST_SUCCESS',
        payload: expired,
      });
      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: 'REQUEST_DELETE_PSO_LIST_FAILED',
        payload: error,
      });
      return Promise.reject(error);
    }
  };
};

export const sendPSOEDP = (data, id) => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_SUBMIT_PARTIAL_SO_PENDING',
    });
    const state = store.getState();

    const body = new FormData();

    body.append('initial_store', state.auth.userLogin.initial_store);
    body.append('user_id', state.auth.userLogin.id);
    body.append('so_partial', JSON.stringify(data));
    body.append('so_header_id', id);
    try {
      const expired = await client.post('/sopartial/finish', body);

      dispatch({
        type: 'REQUEST_SUBMIT_PARTIAL_SO_SUCCESS',
        payload: expired,
      });
      return Promise.resolve(expired.data);
    } catch (error) {
      dispatch({
        type: 'REQUEST_SUBMIT_PARTIAL_SO_FAILED',
        payload: error,
      });
      return Promise.reject(new Error());
    }
  };
};

export const getMustSOCategories = () => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_GET_PSO_CAT_PENDING',
    });
    try {
      const expired = await client.get('/sopartial/mustso/');

      dispatch({
        type: 'REQUEST_GET_PSO_CAT_SUCCESS',
        payload: expired.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_GET_PSO_CAT_FAILED',
        payload: error,
      });
    }
  };
};

export const getPickingReplenish = () => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_GET_PICKING_REPLENISH_PENDING',
    });
    try {
      const state = store.getState();
      const expired = await client.get(
        `/replenishment/${state.auth.userLogin.initial_store}/`,
      );

      dispatch({
        type: 'REQUEST_GET_PICKING_REPLENISH_SUCCESS',
        payload: expired.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_GET_PICKING_REPLENISH_FAILED',
        payload: error,
      });
    }
  };
};

export const submitPickingReplenish = data => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_ADD_REPLENISH_PENDING',
    });
    const state = store.getState();

    const body = new FormData();

    body.append('user_id', state.auth.userLogin.id);
    body.append('initial_store', state.auth.userLogin.initial_store);
    body.append('sku', data.sku);
    body.append('till_code', data.till_code);
    body.append('skudesc', data.skudesc);
    body.append('divdesc', data.divdesc);
    body.append('catdesc', data.catdesc);
    body.append('tag', data.tag);
    body.append('qty', data.qty);
    body.append('note', data.note || '-');
    body.append('type', state.product.replenishType);

    // try {
    //   const check = await client.get(
    //     `/sopartial/check/${state.auth.userLogin.initial_store}/${data.sku}`,
    //   );

    //   if (check.data.status.id === 1) {
    //     dispatch({
    //       type: 'REQUEST_ADD_REPLENISH_FAILED',
    //       payload: {response: check},
    //     });
    //     return Promise.reject(check.data);
    //   }
    // } catch (error) {
    //   dispatch({
    //     type: 'REQUEST_ADD_REPLENISH_FAILED',
    //     payload: error,
    //   });
    //   return Promise.reject(error?.response?.data ?? {});
    // }

    try {
      const expired = await client.post('/replenishment/', body);

      dispatch({
        type: 'REQUEST_ADD_REPLENISH_SUCCESS',
        payload: expired,
      });

      // dispatch({
      //   type: 'ADD_SUBMITED_ARTIKEL',
      //   payload: data.sku,
      // });
      return Promise.resolve(expired.data);
    } catch (error) {
      dispatch({
        type: 'REQUEST_ADD_REPLENISH_FAILED',
        payload: error,
      });
      return Promise.reject(new Error());
    }
  };
};

export const deletePickingReplenish = id => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_DELETE_REPLENISH_LIST_PENDING',
    });

    try {
      const expired = await client.delete(`/replenishment/${id}/delete`);

      dispatch({
        type: 'REQUEST_DELETE_REPLENISH_LIST_SUCCESS',
        payload: expired,
      });
      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: 'REQUEST_DELETE_REPLENISH_LIST_FAILED',
        payload: error,
      });
      return Promise.reject(error);
    }
  };
};
