import client, {gold} from './../../utils/customAxios';
import apiUrl from './../../utils/apiList';

import moment from 'moment';

import {store} from './../store';

import {
  REQUEST_ARTICLE_LIST_PENDING,
  REQUEST_ARTICLE_LIST_SUCCESS,
  REQUEST_ARTICLE_LIST_FAILED,
  REQUEST_ARTICLE_REASON_PENDING,
  REQUEST_ARTICLE_REASON_SUCCESS,
  GET_ARTICLE_MENU_SUCCESS,
  SET_SUBMENU,
  REQUEST_ARTICLE_REASON_FAILED,
  REQUEST_SUBMIT_REASON_PENDING,
  REQUEST_SUBMIT_REASON_SUCCESS,
  REQUEST_SUBMIT_REASON_FAILED,
  REQUEST_FASHION_FILTER_PENDING,
  REQUEST_FASHION_FILTER_SUCCESS,
  REQUEST_FASHION_FILTER_FAILED,
} from './../actionTypes';
import {log} from './log-action';
import Axios from 'axios';
import config from '../../config';

export const getArticleMenu = articleType => {
  return async dispatch => {
    const menu = await import('../../data/menu-article.json');

    const menuPayload = menu.default[articleType.toLowerCase()];
    dispatch({
      type: GET_ARTICLE_MENU_SUCCESS,
      payload: menuPayload,
    });
  };
};

export const getFashionFilter = () => {
  return async dispatch => {
    dispatch({
      type: REQUEST_FASHION_FILTER_PENDING,
    });

    try {
      const response = await client.get('/fashion_categories');
      dispatch({
        type: REQUEST_FASHION_FILTER_SUCCESS,
        payload: response.data.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_FASHION_FILTER_FAILED,
        payload: error,
      });
    }
  };
};

export const getArticleFashion = (title, code) => {
  return async dispatch => {
    dispatch({
      type: REQUEST_ARTICLE_LIST_PENDING,
    });

    title = title
      .replace(/ /gi, '')
      .replace('5', 'five')
      .toLowerCase()
      .replace('&bahanbaku', '')
      .replace('&7daysnaos', '');

    title = title.split('-')[0];

    dispatch({
      type: 'SET_ARTIKEL_DIRECTORATE',
      payload: 'fashion',
    });
    dispatch({
      type: SET_SUBMENU,
      payload: title,
    });

    const state = store.getState();
    const userLogin = state.auth.userLogin;

    try {
      const fakeTitle = [
        'goodintransit',
        'shinkagefashion',
        'deadstock',
      ].includes(title)
        ? 'minusmargin'
        : title;
      const products = await gold.get(
        `/api/v1/trans/${fakeTitle}_fsh/${code}/${userLogin.initial_store}`,
      );

      const articleSubmited =
        title === 'outofstock'
          ? 'oos'
          : title === 'fivedaysbeforeoos'
          ? 'beforeoos'
          : title === 'overstock'
          ? 'skp'
          : title;

      const submitedArtikelUrl = `/reason/fashion-${articleSubmited}/${
        userLogin.initial_store
      }`;
      const submitedArtikel = await client.get(submitedArtikelUrl);
      dispatch({
        type: 'REQUEST_SUBMITED_ARTIKEL',
        payload: submitedArtikel.data.data,
        submitedArtikel,
      });

      dispatch({
        type: 'REQUEST_ARTICLE_LIST_SUCCESS_2',
        payload: products.data,
      });
      dispatch(log(`fashion-${title}`));
    } catch (error) {
      dispatch({
        type: REQUEST_ARTICLE_LIST_FAILED,
        error: error,
        payload: error.response ? error.response.data : error,
      });
    }
  };
};

export const getArticleList = (
  {directorate = 'supermarket', artikel = 'minusmargin', screenType = 'reason'},
  id,
  categoryId = '',
) => {
  return async dispatch => {
    dispatch({
      type: REQUEST_ARTICLE_LIST_PENDING,
    });

    directorate = directorate
      .replace(' ', '')
      .replace('-', '')
      .toLowerCase();

    artikel = artikel
      .replace(/ /gi, '')
      .replace('5', 'five')
      .toLowerCase()
      .replace('&bahanbaku', '')
      .replace('&7daysnaos', '');

    artikel = artikel.split('-')[0];

    dispatch({
      type: 'SET_ARTIKEL_DIRECTORATE',
      payload: directorate,
    });
    dispatch({
      type: SET_SUBMENU,
      payload: artikel,
    });

    const fromGold = [
      'minusmargin',
      'growthminus',
      'minusstock',
      'overstock',
      'outofstock',
      'deadstock',
      'newproduct',
      'fivedaysbeforeoos',
      'mustso',
      'estimasiforproduction',
      'stockpackaging',
      'shrinkagehomebakery',
      'yesterdaytopsales',
      'topsalesarticle',
    ];

    const isFromGold = fromGold.includes(artikel);
    const state = store.getState();
    const userLogin = state.auth.userLogin;

    const params =
      artikel === 'yesterdaytopsales' || artikel === 'topsalesarticle'
        ? userLogin.store_code
        : userLogin.initial_store;
    try {
      const url = isFromGold
        ? apiUrl[directorate][artikel](params) + categoryId
        : apiUrl[directorate][artikel];
      const body = new FormData();
      body.append('user_id', id);
      body.append('initial_store', userLogin.initial_store);

      const isfunction = typeof url === 'function';

      const products = isFromGold
        ? await gold.get(url)
        : isfunction
        ? await client.get(url(userLogin.initial_store))
        : await client.post(url, body);

      const articleSubmited =
        artikel === 'outofstock'
          ? 'oos'
          : artikel === 'fivedaysbeforeoos'
          ? 'beforeoos'
          : artikel === 'overstock'
          ? 'skp'
          : artikel;

      const submitedArtikelUrl =
        screenType === 'reason'
          ? `/reason/${articleSubmited}/${userLogin.initial_store}`
          : `/sopartial/check/${userLogin.initial_store}`;
      const submitedArtikel = await client.get(submitedArtikelUrl);
      dispatch({
        type: 'REQUEST_SUBMITED_ARTIKEL',
        payload: submitedArtikel.data.data,
      });
      if (isFromGold) {
        dispatch({
          type: 'REQUEST_ARTICLE_LIST_SUCCESS_2',
          payload: products.data,
        });
      } else {
        dispatch({
          type: REQUEST_ARTICLE_LIST_SUCCESS,
          payload: products.data,
          response: products,
        });
      }

      if (isFromGold) {
        dispatch(log(`${directorate}-${artikel}`));
      }
    } catch (error) {
      dispatch({
        type: REQUEST_ARTICLE_LIST_FAILED,
        error: error,
        payload: error.response ? error.response.data : error,
      });
    }
  };
};

export const getReasonArticle = () => {
  return async dispatch => {
    dispatch({
      type: REQUEST_ARTICLE_REASON_PENDING,
    });
    const state = store.getState();
    const directorate = state.article.directorate;

    const submenu =
      directorate === 'fashion'
        ? 'fashion-' + state.article.submenu
        : state.article.submenu;
    try {
      const response = await client.get(apiUrl.articleReason + '/' + submenu);
      console.log(response);
      dispatch({
        type: REQUEST_ARTICLE_REASON_SUCCESS,
        payload: response.data.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_ARTICLE_REASON_FAILED,
        payload: error,
      });
    }
  };
};

export const submitReason = ({reasonId, keterangan}) => {
  return async dispatch => {
    dispatch({
      type: REQUEST_SUBMIT_REASON_PENDING,
    });
    const state = store.getState();
    const body = new FormData();

    body.append('user_id', state.auth.userLogin.id);
    body.append('initial_store', state.auth.userLogin.initial_store);
    body.append('reason_id', reasonId);
    body.append('sku', state.product.identity.sku);
    body.append('tanggal', moment().format('YYYY-MM-DD'));
    body.append('keterangan', keterangan);
    body.append('directorates', state.article.directorate.toUpperCase());
    body.append('count_item', state.article.articleList.length);

    try {
      const response = await client.post(apiUrl.articleReason, body);
      dispatch({
        type: REQUEST_SUBMIT_REASON_SUCCESS,
        payload: response.data,
      });
      dispatch({
        type: 'ADD_SUBMITED_ARTIKEL',
        payload: state.product.identity.sku,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_SUBMIT_REASON_FAILED,
        payload: error,
      });
    }
  };
};

export const uploadPhotoBakery = (data, menu, directorate = 'home-bakery') => {
  return async dispatch => {
    dispatch({
      type: 'POST_BAKERY_IMAGE_PENDING',
    });

    const userLogin = store.getState().auth.userLogin;

    try {
      const imageArray = data.map(image => {
        const imageForm = new FormData();
        imageForm.append('file', image);

        return imageForm;
      });

      const uploadUrl =
        menu === 'READYTOSERVICE'
          ? 'https://gold-api.yogyagroup.com/file-upload-readline-yoel'
          : menu === 'SUPERMARKET'
          ? 'https://gold-api.yogyagroup.com/file-upload-fresh'
          : menu === 'DISPLAYPHOTOREPORT'
          ? 'https://gold-api.yogyagroup.com/file-upload-fsh'
          : 'https://gold-api.yogyagroup.com/file-upload-readline-bakery';

      const imageRequest = imageArray.map((formDataImage, i) => {
        const requestImage = Axios.post(uploadUrl, formDataImage);

        return requestImage;
      });

      const imageResponses = await Promise.all(imageRequest);

      const body = new FormData();
      body.append('user_id', userLogin.id);
      body.append('initial_store', userLogin.initial_store);
      body.append('latitude', data[0].coords.latitude);
      body.append('longitude', data[0].coords.longitude);
      body.append('menu', menu);

      imageResponses.forEach((imageResponse, i) => {
        body.append(`foto_${i + 1}`, imageResponse.data.filename);
      });

      const postUrl =
        menu === 'READYTOSERVICE'
          ? '/yoel_image/'
          : menu === 'SUPERMARKET'
          ? '/fresh_image'
          : menu === 'DISPLAYPHOTOREPORT'
          ? '/fashion_image/'
          : '/homebakery_image/';

      const readlineResponse = await client.post(postUrl, body);
      dispatch({
        type: 'POST_BAKERY_IMAGE_SUCCESS',
        payload: readlineResponse,
      });
    } catch (error) {
      dispatch({
        type: 'POST_BAKERY_IMAGE_ERROR',
        payload: error,
      });
    }
  };
};

export const getGalleryBakery = (date, menu) => {
  return async dispatch => {
    dispatch({
      type: 'GET_BAKERY_IMAGE_PENDING',
    });

    const state = store.getState();

    try {
      const body = new FormData();

      body.append('tanggal', moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD'));
      body.append('initial_store', state.auth.userLogin.initial_store);
      body.append('menu', menu);

      const galleryUrl =
        menu === 'READYTOSERVICE'
          ? '/yoel_image'
          : menu === 'SUPERMARKET'
          ? '/fresh_image'
          : menu === 'DISPLAYPHOTOREPORT'
          ? '/fashion_image'
          : '/homebakery_image';

      const gallery = await client.post(galleryUrl + '/list/', body);

      const dataGallery = gallery.data.data.map(item => {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
          .map(number => {
            const foto = item[`foto_${number}`];
            if (foto) {
              return {
                image: foto.replace('/app/', config.baseImage),
                createdAt: moment(item?.created_at).format('HH:mm:ss'),
              };
            }
          })
          .filter(value => value);
      });
      dispatch({
        type: 'GET_BAKERY_IMAGE_SUCCESS',
        payload: dataGallery.flat(),
        gallery,
        response: gallery,
      });
      dispatch({
        type: 'SET_GALLERY_CREATED_AT',
        payload: moment(gallery.data.data[0]?.created_at).format('HH:mm:ss'),
      });
      return Promise.resolve(dataGallery.flat());
    } catch (error) {
      dispatch({
        type: 'GET_BAKERY_IMAGE_ERROR',
        error,
      });
    }
  };
};
