import client from './../../utils/customAxios';
import {store} from './../store';

export const log = menuName => {
  return async dispatch => {
    const userLogin = store.getState().auth.userLogin;
    const body = new FormData();

    body.append('user_id', userLogin.id);
    body.append('menu', menuName);
    try {
      const logData = await client.post('/log', body);
      dispatch({type: 'DO LOG', payload: logData});
    } catch (error) {
      dispatch({type: 'DO LOG ERROR', payload: error});
    }
  };
};
