import {
  REQUEST_INSIGHT_HEADER_PENDING,
  REQUEST_INSIGHT_HEADER_SUCCESS,
  REQUEST_INSIGHT_HEADER_FAILED,
  REQUEST_INSIGHT_DIRECTORATE_PENDING,
  REQUEST_INSIGHT_DIRECTORATE_SUCCESS,
  REQUEST_INSIGHT_DIRECTORATE_FAILED,
  REQUEST_INSIGHT_DIVISION_PENDING,
  REQUEST_INSIGHT_DIVISION_SUCCESS,
  REQUEST_INSIGHT_DIVISION_FAILED,
  REQUEST_INSIGHT_DETAIL_PENDING,
  REQUEST_INSIGHT_DETAIL_SUCCESS,
  REQUEST_INSIGHT_DETAIL_FAILED,
} from './../actionTypes';

import api from './../../utils/customAxios';

import {store} from './../store';

import moment from 'moment';

const directorate = [
  {
    id: 1,
    name: 'Summary of\nSupermarket',
    count: 0,
    icon: 'cart-outline',
    directorateType: 'supermarket',
    color: 'rgba(255, 104, 3, ',
  },
  {
    id: 2,
    name: 'Summary of\nFashion',
    count: 0,
    icon: 'shopping',
    directorateType: 'fashion',
    color: 'rgba(48, 172, 255, ',
  },
  {
    id: 3,
    name: 'Summary of\nHome Bakery',
    count: 0,
    icon: 'bread-slice-outline',
    directorateType: 'home bakery',
    color: 'rgba(255, 104, 3, ',
  },
  {
    id: 4,
    name: 'Summary of\nFood Station',
    count: 0,
    icon: 'silverware-spoon',
    directorateType: 'food station',
    color: 'rgba(48, 172, 255, ',
  },
  {
    id: 5,
    name: 'Summary of\nYogya Electronic',
    count: 0,
    icon: 'cctv',
    directorateType: 'yo-el',
    color: 'rgba(255, 104, 3, ',
  },
];

const planogram = [
  {
    id: 1,
    name: 'Food',
    count: 0,
    icon: '',
    directorateType: 'food',
    color: 'rgba(255, 104, 3, ',
  },
  {
    id: 2,
    name: 'Non Food',
    count: 0,
    icon: '',
    directorateType: 'non_food',
    color: 'rgba(48, 172, 255, ',
  },
  {
    id: 3,
    name: 'Stationery',
    count: 0,
    icon: '',
    directorateType: 'stationery',
    color: 'rgba(255, 104, 3, ',
  },
  {
    id: 4,
    name: 'GMS',
    count: 0,
    icon: '',
    directorateType: 'gms',
    color: 'rgba(48, 172, 255, ',
  },
  {
    id: 5,
    name: 'Fresh',
    count: 0,
    icon: '',
    directorateType: 'fresh',
    color: 'rgba(255, 104, 3, ',
  },
];

export const getInsightHeader = () => {
  return async dispatch => {
    const userLogin = store.getState().auth.userLogin;

    dispatch({
      type: REQUEST_INSIGHT_HEADER_PENDING,
    });

    try {
      const insightHeader = await api.get(
        `/insight/${userLogin.initial_store}`,
      );

      dispatch({
        type: REQUEST_INSIGHT_HEADER_SUCCESS,
        payload: insightHeader.data.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_INSIGHT_HEADER_FAILED,
        payload: error,
      });
    }
  };
};

export const getInsightDirectorate = type => {
  return async dispatch => {
    const userLogin = store.getState().auth.userLogin;
    dispatch({
      type: REQUEST_INSIGHT_DIRECTORATE_PENDING,
    });

    try {
      if (type.toLowerCase === 'planogram insight') {
        const payload = planogram;
        dispatch({
          type: REQUEST_INSIGHT_DIRECTORATE_SUCCESS,
          payload: payload,
        });
      } else {
        const insightDirectorate = await api.get(
          `/insight/${userLogin.initial_store}/yotodo/`,
        );
        const data = insightDirectorate.data.data;
        dispatch({
          type: 'INSIGHT TODO',
          payload: data,
        });
      }
    } catch (error) {
      dispatch({
        type: REQUEST_INSIGHT_DIRECTORATE_FAILED,
        payload: error,
      });
    }
  };
};

export const getArticleMenu = articleType => {
  return async dispatch => {
    dispatch({type: REQUEST_INSIGHT_DIVISION_PENDING});
    const menu = await import('../../data/menu-article.json');
    const menuPayload = menu.default[articleType.toLowerCase()];
    try {
      const userLogin = store.getState().auth.userLogin;
      const menuFormat = menuPayload
        .filter(obj => obj.insight === true)
        .map((obj, index) => {
          const even = index % 2 === 0;
          return {
            ...obj,
            count: 0,
            color: even ? 'rgba(255, 104, 3, ' : 'rgba(48, 172, 255, ',
          };
        });

      if (articleType.toLowerCase() !== 'planogram') {
        const insight = await api.get(
          `/insight/${userLogin.initial_store}/yotodo/${articleType.replace(
            / /gi,
            '',
          )}/`,
        );

        const data = insight.data.data;

        dispatch({
          type: REQUEST_INSIGHT_DIVISION_SUCCESS,
          payload: data,
        });
      } else {
        const insight = await api.get(
          `/insight/${userLogin.initial_store}/planogram/`,
        );

        const data = insight.data.data;

        const keys = Object.keys(data);

        keys.shift();

        dispatch({
          type: REQUEST_INSIGHT_DIVISION_SUCCESS,
          payload: menuFormat.map((item, i) => {
            return Object.assign({}, item, {count: data[keys[i]]});
          }),
        });
      }

      dispatch({
        type: 'INSIGHT_SAVE_DIRECTORATE',
        payload: articleType.toLowerCase().replace(/ /gi, ''),
      });
    } catch (error) {
      dispatch({
        type: REQUEST_INSIGHT_DIVISION_FAILED,
        payload: error,
      });
    }
  };
};

export const insightDetail = (division, startDate, endDate) => {
  return async dispatch => {
    const directorateName = store.getState().insightInfo.directorateName;
    const userLogin = store.getState().auth.userLogin;

    endDate = moment(endDate).format('YYYY-MM-DD');
    startDate = moment(startDate).format('YYYY-MM-DD');

    dispatch({
      type: REQUEST_INSIGHT_DETAIL_PENDING,
    });
    try {
      const params = {
        menu: directorateName.toUpperCase(),
        submenu: division,
        sortByStore: userLogin.initial_store,
        start_date: startDate,
        end_date: endDate,
      };

      const reason = await api.get('/dashboard/barchart', {params});

      const linechart = await api.get('/dashboard/linechart', {params});

      if (reason.data.data) {
        dispatch({
          type: REQUEST_INSIGHT_DETAIL_SUCCESS,
          payload: reason.data.data,
          linechart: linechart.data.data,
          response: reason,
        });
      } else {
        throw new Error();
      }
    } catch (error) {
      dispatch({
        type: REQUEST_INSIGHT_DETAIL_FAILED,
        payload: error,
      });
    }
  };
};
