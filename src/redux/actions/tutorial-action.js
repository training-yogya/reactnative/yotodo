import {
  REQUEST_TUTORIAL_DIRECTORATE_PENDING,
  REQUEST_TUTORIAL_DIRECTORATE_SUCCESS,
  REQUEST_TUTORIAL_DIRECTORATE_FAILED,
  REQUEST_TUTORIAL_SUBDIRECTORATE_PENDING,
  REQUEST_TUTORIAL_SUBDIRECTORATE_SUCCESS,
  REQUEST_TUTORIAL_SUBDIRECTORATE_FAILED,
} from './../actionTypes';

import api from './../../utils/customAxios';

import {store} from './../store';

export const getTutorialDirectorate = () => {
  return async dispatch => {
    try {
      const userLogin = store.getState().auth.userLogin;

      dispatch({type: REQUEST_TUTORIAL_DIRECTORATE_PENDING, payload});
      const response = await api.get(`/tutorial/${userLogin.id}`);

      const payload = response.data.data.menu
        ? response.data.data
        : response.data;

      dispatch({type: REQUEST_TUTORIAL_DIRECTORATE_SUCCESS, payload});
    } catch (error) {
      dispatch({type: REQUEST_TUTORIAL_DIRECTORATE_FAILED, payload: error});
    }
  };
};

export const getTutorialSubDirectorate = directorate => {
  return async dispatch => {
    try {
      const userLogin = store.getState().auth.userLogin;

      dispatch({type: REQUEST_TUTORIAL_SUBDIRECTORATE_PENDING, payload});
      const response = await api.get(
        `/tutorial/${userLogin.id}/${directorate}`,
      );

      const payload = response.data.data.menu
        ? response.data.data
        : response.data;

      dispatch({type: REQUEST_TUTORIAL_SUBDIRECTORATE_SUCCESS, payload});
    } catch (error) {
      dispatch({type: REQUEST_TUTORIAL_SUBDIRECTORATE_FAILED, payload: error});
    }
  };
};
