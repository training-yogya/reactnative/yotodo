import client from './../../utils/customAxios';
import apiUrl from './../../utils/apiList';

import {store} from './../store';

import {GET_MENU_SUCCESS} from './../actionTypes';

export const getHomeMenuItem = () => {
  return dispatch => {
    const userLogin = store.getState().auth.userLogin;
    const menuResponse = client.get(apiUrl.menu(userLogin.id));
    const menuLocal = import('./../../data/menu-home.json');

    dispatch({type: 'GET_MENU_LOADING'});

    Promise.all([menuResponse, menuLocal])
      .then(([menu1, menu2]) => {
        const menu = menu1.data.data.map((item, i) => ({
          id: item.id,
          icon: menu2[i]?.icon,
          iconType: menu2[i]?.iconType,
          name: item.name,
          to: menu2[i]?.to,
          disabled: !item.is_active,
        }));

        dispatch({
          type: GET_MENU_SUCCESS,
          payload: menu,
        });
      })
      .catch(error => {
        dispatch({
          type: 'GET_MENU_ERROR',
          payload: error,
        });
      });
  };
};
