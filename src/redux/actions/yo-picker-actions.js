import client, {gold} from '../../utils/customAxios';
import {store} from './../store';
import Axios from 'axios';

export const getOmsList = search => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_OMS_LIST_PENDING',
    });
    const userLogin = store.getState().auth.userLogin;
    try {
      const omsList = await client.get(
        `/picker/${userLogin.initial_store}/${search}`,
      );
      dispatch({
        type: 'REQUEST_OMS_LIST_SUCCESS',
        payload: omsList.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_OMS_LIST_ERROR',
        error: error,
      });
    }
  };
};

export const getSearchDetail = text => {
  return dispatch => {
    const noteList = store.getState().yoPicker.omsListDetail;

    text = text.toLowerCase();

    const newList = noteList.filter(notes => {
      const title = notes.article_description.toLowerCase();
      const order_id = notes.id.toLowerCase();
      // const notesText = notes.notes.toLowerCase();

      return title.includes(text) || order_id.includes(text);
    });

    return Promise.resolve(newList);
  };
};

export const getOmsListDetail = id => {
  return async dispatch => {
    dispatch({
      type: 'REQUEST_OMS_LIST_DETAIL_PENDING',
    });

    try {
      const omsList = await client.get(`/picker/detail/${id}`);
      dispatch({
        type: 'REQUEST_OMS_LIST_DETAIL_SUCCESS',
        payload: omsList.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_OMS_LIST_DETAIL_ERROR',
        error: error,
      });
    }
  };
};

export const validateProduct = (data, barcode) => {
  return async dispatch => {
    dispatch({
      type: 'VALIDATE_PRODUCT_PENDING',
    });

    const userLogin = store.getState().auth.userLogin;

    const body = new FormData();

    body.append('order_id', data.order_id);
    body.append('sku', data.sku);

    try {
      const productData = await gold.get(
        `/item/${userLogin.initial_store}/${barcode}`,
      );

      const tillCode = productData.data?.[0]?.till_code;

      if (data.sku !== tillCode) {
        throw new Error('error');
      }

      const omsList = await client.post('/picker/validate', body);
      dispatch({
        type: 'VALIDATE_PRODUCT_SUCCESS',
        payload: omsList.data.data,
      });
      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: 'VALIDATE_PRODUCT_ERROR',
        error: error,
      });

      return Promise.reject(new Error('error'));
    }
  };
};

export const inputQty = data => {
  return async dispatch => {
    dispatch({
      type: 'SET_QTY_PENDING',
    });

    const userLogin = store.getState().auth.userLogin;
    const body = new FormData();

    body.append('order_id', data.order_id);
    body.append('sku', data.sku);
    body.append('quantity_picking', data.quantity_picking);
    body.append('staff_name', data.staff_name);
    body.append('st_name', data.st_name);
    body.append('selisih', data.selisih);
    body.append('user_id', userLogin.id);
    body.append('id', data.id);

    try {
      const qty = await client.post('/picker', body);
      dispatch({
        type: 'SET_QTY_SUCCESS',
        payload: qty,
      });
      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: 'SET_QTY_ERROR',
        payload: error,
      });
      return Promise.reject(new Error('error'));
    }
  };
};

export const finishPicker = data => {
  return async dispatch => {
    dispatch({
      type: 'FINISH_PICKER_PENDING',
    });

    const userLogin = store.getState().auth.userLogin;
    const body = new FormData();

    body.append('order_id', data);
    body.append('user_id', userLogin.id);

    try {
      // const omsTokenResponse = await Axios.post(
      //   'https://oms-api.yogyagroup.com/api/v1/token/request',
      //   {
      //     user_id: '1',
      //     password: '0M54P1M031L3B1',
      //     username_jwt: 'sailsOmsApi',
      //     password_jwt: 'M031L30M44P1B4',
      //   },
      // );

      // const omsListDetail = store.getState().yoPicker.omsListDetail;
      // const bodyOms = new FormData();

      // const dataPicking = omsListDetail.map(item => ({
      //   id: item.id,
      //   quantity_picking: item.quantity_picking,
      // }));

      // bodyOms.append('order_id', data);
      // bodyOms.append('data_picking', JSON.stringify(dataPicking));
      // const omsDoPicker = await Axios.post(
      //   'https://oms-api.yogyagroup.com/api/v1/oms/do-picking-jwt',
      //   bodyOms,
      //   {
      //     headers: {
      //       Authorization: `Bearer ${omsTokenResponse.data.data.token}`,
      //     },
      //   },
      // );

      body.append('keterangan', 'finish');
      await client.post('/picker/status', body);
      dispatch({
        type: 'FINISH_PICKER_SUCCESS',
      });
      return Promise.resolve(true);
    } catch (error) {
      console.log(error);
      dispatch({
        type: 'FINISH_PICKER_ERROR',
        payload: error,
      });
      return Promise.reject(error);
    }
  };
};
