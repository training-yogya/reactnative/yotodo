import client from './../../utils/customAxios';
import apiUrl from './../../utils/apiList';

import {store} from './../../redux/store';

import {
  REQUEST_LOGIN_PENDING,
  REQUEST_LOGIN_SUCCESS,
  SAVE_TOKEN,
  REQUEST_LOGIN_FAILED,
  REQUEST_CHANGE_PASSWORD_PENDING,
  REQUEST_CHANGE_PASSWORD_FAILED,
  REQUEST_CHANGE_PASSWORD_SUCCESS,
  REQUEST_PROFILE_PENDING,
  REQUEST_PROFILE_SUCCESS,
  REQUEST_PROFILE_FAILED,
  REQUEST_PROFILE_UPDATE_PENDING,
  REQUEST_PROFILE_UPDATE_SUCCESS,
  REQUEST_PROFILE_UPDATE_FAILED,
} from './../actionTypes';
import {firebase} from '../../services';

export const doLogin = ({username, password}) => {
  return async dispatch => {
    dispatch({
      type: REQUEST_LOGIN_PENDING,
    });
    const requestLoginBody = new FormData();
    requestLoginBody.append('username', username);
    requestLoginBody.append('password', password);

    try {
      const payloadLogin = await client.post(apiUrl.login, requestLoginBody);
      dispatch({
        type: REQUEST_LOGIN_SUCCESS,
        payload: payloadLogin.data,
      });
      const {data} = payloadLogin.data;
      const requestTokenBody = new FormData();
      requestTokenBody.append('username_jwt', 'sails');
      requestTokenBody.append('password_jwt', 'sails123');
      requestTokenBody.append('user_id', data.id);
      requestTokenBody.append('password', password);

      const payloadToken = await client.post(
        apiUrl.requestToken,
        requestTokenBody,
      );
      dispatch({
        type: SAVE_TOKEN,
        payload: payloadToken.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_LOGIN_FAILED,
        payload: error.response ? error.response.data : error,
      });
    }

    try {
      const firebaseUser = await firebase.login({email: username, password});
      dispatch({
        type: 'SAVE_USER_FIREBASE',
        payload: firebaseUser.user._user,
      });
    } catch (error) {
      dispatch({
        type: 'SAVE_USER_FIREBASE_FAILED',
        payload: error,
      });
    }
  };
};

export const changeUserToken = token => {
  return dispatch => {
    dispatch({
      type: SAVE_TOKEN,
      payload: {
        data: {
          token,
        },
      },
    });
  };
};

export const changePassword = ({oldPassword, newPassword, confirmPassword}) => {
  const user = store.getState().auth.userLogin;
  return async dispatch => {
    const body = new FormData();
    try {
      dispatch({
        type: REQUEST_CHANGE_PASSWORD_PENDING,
      });
      if (newPassword !== confirmPassword) {
        throw new Error('password tidak cocok');
      }
      body.append('password_old', oldPassword);
      body.append('password_new', newPassword);

      const response = await client.put(apiUrl.changePassoword(user.id), body);
      dispatch({
        type: REQUEST_CHANGE_PASSWORD_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_CHANGE_PASSWORD_FAILED,
        payload: error,
      });
    }
  };
};

export const getProfileData = () => {
  return async dispatch => {
    const user = store.getState().auth.userLogin;
    try {
      dispatch({
        type: REQUEST_PROFILE_PENDING,
      });

      const response = await client.get(apiUrl.profile(user.id));
      dispatch({
        type: REQUEST_PROFILE_SUCCESS,
        payload: response.data.data[0],
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PROFILE_FAILED,
        payload: error,
      });
    }
  };
};

export const getProfileActivity = () => {
  return async dispatch => {
    const user = store.getState().auth.userLogin;
    try {
      dispatch({
        type: 'REQUEST_PROFILE_ACTIVITY_PENDING',
      });

      const response = await client.get(`${apiUrl.profile(user.id)}/activity`);
      dispatch({
        type: 'REQUEST_PROFILE_ACTIVITY_SUCCESS',
        payload: response.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'REQUEST_PROFILE_ACTIVITY_FAILED',
        payload: error,
      });
    }
  };
};

export const updateProfileData = data => {
  return async dispatch => {
    const user = store.getState().auth.userLogin;
    try {
      dispatch({
        type: REQUEST_PROFILE_UPDATE_PENDING,
      });

      const body = new FormData();

      Object.keys(data).map(item => {
        body.append(item, data[item]);
      });

      const response = await client.put(apiUrl.profile(user.id), body);
      dispatch({
        type: REQUEST_PROFILE_UPDATE_SUCCESS,
        payload: response,
      });
    } catch (error) {
      dispatch({
        type: REQUEST_PROFILE_UPDATE_FAILED,
        payload: error,
      });
    }
  };
};

export const getStoreByUserId = () => {
  return async dispatch => {
    dispatch({
      type: 'GET_STORE_BY_USER_PENDING',
    });

    const state = store.getState();
    const userLogin = state.auth.userLogin;

    try {
      const storeList = await client.get(`/userstore/${userLogin.id}/`);
      dispatch({
        type: 'GET_STORE_BY_USER_SUCCESS',
        payload: storeList.data.data,
      });
    } catch (error) {
      dispatch({
        type: 'GET_STORE_BY_USER_FAILED',
        payload: error,
      });
    }
  };
};

export const getSearch = text => {
  return dispatch => {
    const storeList = store.getState().auth.storelist;

    text = text.toLowerCase();

    const newList = storeList.filter(storeItem => {
      const name = storeItem.name.toLowerCase();
      const intial = storeItem.initial_store.toLowerCase();

      return name.includes(text) || intial.includes(text);
    });

    return Promise.resolve(newList);
  };
};

export const setHomeReady = data => {
  return dispatch => {
    dispatch({
      type: 'SET_HOME_READY',
      payload: data,
    });
  };
};
