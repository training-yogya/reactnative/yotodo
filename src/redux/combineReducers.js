import {combineReducers} from 'redux';
// import {createNavigationReducer} from 'react-navigation-redux-helpers';
// import RootNavigation from './../navigations';
import authReducer from './reducers/auth-reducer';
import menuReducer from './reducers/menu-reducer';
import articleReducer from './reducers/article-reducer';
import planogramReducer from './reducers/planogram-reducer';
import productReducer from './reducers/product-reducer';
import insightInfoReducer from './reducers/insight-info-reducer';
import noteReducer from './reducers/note-reducer';
import tutorialReducer from './reducers/tutorial-reducer';
import promoNotifReducer from './reducers/promo-notif-reducer';
import yoPickerReducer from './reducers/yo-picker-reducer';
import {chatReducer} from './reducers/chat-reducer';

// const navigation = createNavigationReducer(RootNavigation);

const appReducer = {
  // navigation,
  auth: authReducer,
  menu: menuReducer,
  article: articleReducer,
  planogram: planogramReducer,
  product: productReducer,
  insightInfo: insightInfoReducer,
  note: noteReducer,
  tutorial: tutorialReducer,
  promoNotif: promoNotifReducer,
  yoPicker: yoPickerReducer,
  chat: chatReducer,
};

export default combineReducers(appReducer);
