import {applyMiddleware, createStore, compose} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import {createWhitelistFilter} from 'redux-persist-transform-filter';

import AsyncStorage from '@react-native-community/async-storage';

import appReducer from './combineReducers';
import appMiddleware from './middlewares';

const whiteList = [
  createWhitelistFilter('planogram', [
    'form',
    'directorate',
    'devision',
    'category',
  ]),
  createWhitelistFilter('auth', ['userLogin', 'logined', 'isHomeReady']),
  createWhitelistFilter('menu', ['items']),
];

const persistConfig = {
  timeout: 100000,
  key: 'root',
  storage: AsyncStorage,
  transforms: [...whiteList],
};

const persistedReducer = persistReducer(persistConfig, appReducer);

export const store = compose(applyMiddleware(...appMiddleware))(createStore)(
  persistedReducer,
);

export const persistedStore = persistStore(store);
