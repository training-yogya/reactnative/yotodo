import {StyleSheet} from 'react-native';

import Colors from './../utils/colors';

const globalStyles = StyleSheet.create({
  // background
  bgWhite: {
    backgroundColor: Colors.White,
  },
  bgWransparent: {
    backgroundColor: 'transparent',
  },
  // flexbox
  dFlex: {
    display: 'flex',
  },
  flex1: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  // align
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerBeetwen: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  centerAround: {
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  centerEnd: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  // text
  title: {
    fontSize: 16,
  },
  textCenter: {
    textAlign: 'center',
  },
  textWhite: {
    color: Colors.White,
  },
  textBold: {
    fontWeight: 'bold',
  },
  //padding
  pHorizontal10: {
    paddingHorizontal: 10,
  },
  pTop5: {
    paddingTop: 5,
  },
  //classes
  container: {
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  absolutefill: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 9,
    justifyContent: 'center',
  },
});

export default globalStyles;
