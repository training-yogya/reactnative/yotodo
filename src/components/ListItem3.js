import * as React from 'react';

import {TouchableOpacity, View, Text} from 'react-native';
import colors from '../utils/colors';
import AppButton from './AppButton';

const List3 = ({item, handleNavigateProductIdentity}) => {
  return (
    <View
      style={{
        backgroundColor: colors.White,
        borderRadius: 10,
        padding: 20,
        marginHorizontal: 10,
        marginBottom: 10,
      }}>
      <TouchableOpacity onPress={handleNavigateProductIdentity(item.SKU)}>
        <View>
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>{item.SKUDESC}</Text>
        </View>
        <View
          style={{
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'space-between',
              flexDirection: 'row',
              paddingRight: 10,
            }}>
            <Text>{item.SKU}</Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <Text
              style={{
                color: colors.BlazeOrange,
                fontWeight: 'bold',
                fontSize: 15,
              }}>
              Stock: {item.STOCK}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 10,
          justifyContent: 'center',
        }}>
        <AppButton
          titleStyle={{fontWeight: 'bold'}}
          style={{borderRadius: 0, marginRight: 1}}
          title={`KNW ${item.KNOWN} %`}
          color={colors.Silver}
        />
        <AppButton
          titleStyle={{fontWeight: 'bold'}}
          style={{borderRadius: 0, marginRight: 1}}
          title={`UNK : ${item.UNKNOWN} %`}
          color={colors.Silver}
        />
        <AppButton
          titleStyle={{fontWeight: 'bold'}}
          style={{borderRadius: 0}}
          title={`Total : ${parseFloat(item.KNOWN) +
            parseFloat(item.UNKNOWN)} %`}
          color={colors.Silver}
        />
      </View>
    </View>
  );
};

export default List3;
