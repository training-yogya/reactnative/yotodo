import * as React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import colors from './../utils/colors';

const FluidHeader = ({
  title,
  backgroundColor = colors.BlueBell,
  style,
  wrapperStyle,
  titleStyle,
}) => {
  return (
    <View style={[styles.wrapper, wrapperStyle]}>
      <View style={[styles.container, {backgroundColor}, style]}>
        <Text style={[styles.title, titleStyle]}>{title}</Text>
      </View>
    </View>
  );
};

export default FluidHeader;

const styles = StyleSheet.create({
  wrapper: {
    padding: 0,
    margin: 0,
  },
  container: {
    backgroundColor: colors.BlueBell,
    borderRadius: 0,
    height: 56,
  },
  title: {
    textAlign: 'center',
    lineHeight: 60,
    color: colors.White,
    fontWeight: '700',
  },
});
