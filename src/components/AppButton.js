import * as React from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {TouchableHighlight} from 'react-native-gesture-handler';
import colors from '../utils/colors';

const AppButton = ({
  title,
  onPress,
  color,
  style,
  disabled,
  underlayColor,
  loading = false,
  titleStyle,
}) => {
  return (
    <View>
      <TouchableHighlight
        disabled={disabled}
        onPress={onPress}
        underlayColor={underlayColor || colors.BlazeOrange}
        style={[
          styles.appButton,
          color ? {backgroundColor: color} : {},
          style,
        ]}>
        <View>
          {loading ? (
            <ActivityIndicator color={colors.White} size={21} />
          ) : (
            <Text style={[styles.appButtonTitle, titleStyle]}>{title}</Text>
          )}
        </View>
      </TouchableHighlight>
    </View>
  );
};

export default AppButton;

const styles = StyleSheet.create({
  appButton: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
    borderRadius: 4,
  },
  appButtonTitle: {
    color: colors.White,
  },
});
