import * as React from 'react';
import {
  Text,
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../utils/colors';
import globalStyles from '../styles/global';

const MenuItem = ({
  icon,
  title,
  iconType,
  onPress,
  backgroundColor,
  iconColor,
  iconSize = 40,
  height = width / 3 - 10,
  dark = false,
  disabled = false,
}) => {
  return (
    <View style={[styles.menuItemWrapper, {height}]}>
      <TouchableOpacity
        disabled={disabled}
        onPress={onPress}
        style={[styles.menuItem, globalStyles.flex1, {backgroundColor}]}>
        {iconType === 'Feather' ? (
          <Feather name={icon} color={iconColor || colors.White} size={40} />
        ) : iconType === 'MaterialCommunityIcons' ? (
          <MaterialCommunityIcons
            name={icon}
            color={iconColor || colors.White}
            size={iconSize}
          />
        ) : iconType === 'FontAwesome5' ? (
          <FontAwesome5
            name={icon}
            color={iconColor || colors.White}
            size={iconSize}
          />
        ) : (
          <FontAwesome
            name={icon}
            color={iconColor || colors.White}
            size={iconSize}
          />
        )}
        <Text
          style={[
            styles.menuText,
            globalStyles.textCenter,
            globalStyles.textWhite,
            dark && {
              color: colors.Black,
            },
          ]}>
          {title}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default MenuItem;

const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  menuItemWrapper: {
    width: width / 2 - 5,
    height: width / 3 - 10,
    paddingVertical: 5,
    paddingStart: 10,
    paddingEnd: 10,
    marginBottom: 10,
  },
  menuItem: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    position: 'relative',
    borderRadius: 4,
  },
});
