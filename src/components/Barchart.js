import React, {useState, useEffect} from 'react';
import colors from '../utils/colors';
import {Dimensions, View, Text} from 'react-native';
import {BarChart} from 'react-native-chart-kit';

export default ({chartLabel, chartData}) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    const chartValue = chartData.some(value => value[0]);

    if (chartValue) {
      setShow(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (show) {
    return (
      <BarChart
        data={{
          labels: chartLabel,
          datasets: [
            {
              data: chartData,
            },
          ],
          legend: [],
          barColors: [colors.OrangPeel],
        }}
        width={Dimensions.get('window').width - 20} // from react-native
        height={220}
        withInnerLines={false}
        showValuesOnTopOfBars={true}
        showBarTops={true}
        fromZero={true}
        chartConfig={{
          backgroundGradientFrom: '#ffffff',
          backgroundGradientFromOpacity: 1,
          backgroundGradientTo: '#ffffff',
          backgroundGradientToOpacity: 1,
          fillShadowGradientOpacity: 1,
          color: (opacity = 1, index = 0) => colors.OrangPeel,
          strokeWidth: 6, // optional, default 3
          barPercentage: 1,
          labelColor: () => colors.Black,
          useShadowColorFromDataset: false,
          propsForLabels: {
            fontWeight: 'bold',
          },
        }}
        style={{
          borderRadius: 16,
        }}
      />
    );
  } else {
    return (
      <View style={{height: 220, justifyContent: 'flex-end'}}>
        <Text style={{fontWeight: 'bold'}}>
          Tidak Ada Data Untuk Ditampilkan
        </Text>
      </View>
    );
  }
};
