import * as React from 'react';
import {View, StyleSheet, Alert, Text, Dimensions, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import ButtonCircle from './ButtonCircle';
import {useNavigation, CommonActions} from '@react-navigation/native';
import colors from '../utils/colors';
import {useDispatch, useSelector} from 'react-redux';
import {SAVE_TOKEN, SET_ALL_DEFAULT} from '../redux/actionTypes';
import {screenNames} from '../utils/screens';
import globalStyles from '../styles/global';
import {Container, Icon} from './index';
import {iconsNames} from '../utils/icons';
import {TouchableOpacity} from 'react-native-gesture-handler';

const width = Dimensions.get('screen').width;

const BottomNavigation = props => {
  const screen =
    props?.state?.routes[0]?.state?.routes[1]?.params?.screen ??
    props.route?.name;
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const unReadRooms = useSelector(state => state.chat.unReadRooms);
  const handleButtonLogout = () => {
    Alert.alert('Logout?', '', [
      {text: 'Cancel'},
      {
        text: 'Logout',
        onPress: async () => {
          await AsyncStorage.removeItem('token');
          dispatch({
            type: SET_ALL_DEFAULT,
            payload: {data: {token: null}},
          });
        },
      },
    ]);
  };
  const handleButtonHome = () =>
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: screenNames.Home.Home,
          },
        ],
      }),
    );

  const handleButtonChat = () => navigation.navigate(screenNames.YoChat.List);
  const handleButtonProfile = () =>
    navigation.navigate(screenNames.Home.Profile, {
      screen: screenNames.Home.Profile,
      params: {title: 'Profile'},
    });
  const handleButtonStore = () =>
    navigation.navigate(screenNames.ChooseStoreHome);
  return (
    <View style={styles.wrapper}>
      {screen === screenNames.Home.Profile ? (
        <Container style={[globalStyles.centerEnd, globalStyles.bgWransparent]}>
          <Icon name={iconsNames.yogyabanner} />
        </Container>
      ) : screen === screenNames.ChooseStore ? (
        <View style={styles.wrapper}>
          <ButtonCircle
            onPress={handleButtonLogout}
            title="Logout"
            icon="md-log-out"
          />
        </View>
      ) : (
        <View style={styles.wrapper}>
          <ButtonCircle
            width={width / 5}
            disabled={false}
            onPress={handleButtonStore}
            title="Store"
            iconType="MaterialCommunity"
            icon="store"
          />
          <ButtonCircle
            width={width / 5}
            disabled={false}
            badge={unReadRooms?.length}
            onPress={handleButtonChat}
            title="YoChat"
            icon="md-chatbubbles"
          />
          <TouchableOpacity
            onPress={handleButtonHome}
            style={{
              marginTop: Platform.OS === 'android' ? -10 : 0,
              borderWidth: 1,
              borderRadius: 100,
              borderColor: colors.BlazeOrange,
              elevation: 4,
              marginVertical: 10,
            }}>
            <Icon
              name={iconsNames.logo}
              style={{
                width: 60,
                height: 60,
              }}
            />
          </TouchableOpacity>
          <ButtonCircle
            wrapperStyle={{marginTop: 3}}
            width={width / 5}
            onPress={handleButtonProfile}
            title="Profile"
            iconType="FontAwesome"
            icon="user-circle-o"
          />
          <ButtonCircle
            width={width / 5}
            onPress={handleButtonLogout}
            title="Logout"
            icon="md-log-out"
          />
        </View>
      )}
    </View>
  );
};

export default BottomNavigation;

const styles = StyleSheet.create({
  wrapper: {
    height: Platform.OS === 'android' ? 56 : 60,
    width: width,
    backgroundColor: colors.White,
    elevation: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    overflow: 'visible',
  },
  button: {alignItems: 'center'},
});
