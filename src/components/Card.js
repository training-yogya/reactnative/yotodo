import * as React from 'react';
import globalStyles from '../styles/global';
import colors from '../utils/colors';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {View, Text, TouchableOpacity} from 'react-native';

const Card = ({title, style, titleStyle, icon, color, onPress = () => {}}) => {
  return (
    <TouchableOpacity style={globalStyles.flex1} onPress={onPress}>
      <View
        style={[
          {
            maxHeight: 300,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: color,
            borderRadius: 4,
          },
          style,
        ]}>
        {!!icon && <MaterialIcon name={icon} color={colors.White} size={96} />}
        <Text
          style={[
            {
              color: colors.White,
              fontSize: 18,
              fontWeight: '600',
              maxWidth: 300,
              textAlign: 'center',
            },
            titleStyle,
          ]}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default Card;
