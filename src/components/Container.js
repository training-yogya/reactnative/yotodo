import * as React from 'react';
import {View} from 'react-native';

import styles from './../styles/global';
import colors from '../utils/colors';

const Container = ({children, style, backgroundColor = colors.White}) => {
  return (
    <View
      style={[
        styles.bgWhite,
        styles.dFlex,
        styles.flex1,
        {paddingBottom: 15},
        {backgroundColor},
        style,
      ]}
      children={children}
    />
  );
};

export default Container;
