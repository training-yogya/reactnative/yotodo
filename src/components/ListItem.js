import * as React from 'react';

import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from './../utils/colors';
import globalStyles from './../styles/global';

const ListItem = ({
  onPress,
  title,
  header,
  subTitle,
  info,
  mark,
  markText,
  infoDetail,
  infoDetailStyle,
  infoStyle = {},
  deleteIcon = false,
  deleteAction,
  footer: Footer,
}) => (
  <TouchableOpacity onPress={onPress} style={styles.wrapper}>
    <View
      style={{
        flexDirection: 'row',
      }}>
      <View style={styles.main}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.headerText}>{header}</Text>
        </View>
        <Text style={styles.titleText}>{title}</Text>
        <View style={globalStyles.flexRow}>
          <Text>{subTitle}</Text>
        </View>
      </View>
      <View style={[styles.infoText, {justifyContent: 'space-between'}]}>
        <Text style={infoStyle}>{info}</Text>
        {!!infoDetail && <Text style={infoDetailStyle}>{infoDetail}</Text>}
        {mark && (
          <View
            style={{
              padding: 2,
              borderRadius: 10,
              backgroundColor: colors.SeaGreen,
            }}>
            <MaterialIcon name={'check'} color={colors.White} size={16} />
          </View>
        )}
        {deleteIcon && (
          <TouchableOpacity
            onPress={deleteAction}
            style={{
              padding: 5,
              borderRadius: 10,
              backgroundColor: colors.Red,
            }}>
            <MaterialIcon name={'delete'} color={colors.White} size={20} />
          </TouchableOpacity>
        )}
      </View>
    </View>
    {Footer && <Footer />}
  </TouchableOpacity>
);

export default ListItem;

const styles = StyleSheet.create({
  wrapper: {
    marginBottom: 10,
    backgroundColor: colors.White,
    borderRadius: 4,
    padding: 6,
    elevation: 1,
  },
  main: {
    flex: 4,
  },
  headerText: {
    fontSize: 12,
    fontWeight: '200',
  },
  titleText: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingVertical: 5,
  },
  infoText: {
    flex: 2,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
});
