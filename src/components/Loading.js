import * as React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import globalStyles from '../styles/global';

const Loading = ({size = 56}) => (
  <View style={[globalStyles.absolutefill, styles.background]}>
    <ActivityIndicator size={size} />
  </View>
);

export default Loading;

const styles = StyleSheet.create({
  background: {
    zIndex: 10,
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: 999999999999999999999999,
  },
});
