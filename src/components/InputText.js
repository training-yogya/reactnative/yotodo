import * as React from 'react';
import {useState} from 'react';
import {TextInput, StyleSheet, Dimensions, Picker} from 'react-native';
import {View} from 'react-native';

import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../utils/colors';
import {
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native-gesture-handler';

const InputText = ({
  value,
  placeholder = '',
  onChangeText,
  editAble = true,
  // custom
  icon = undefined,
  wrapperStyle,
  inputStyle,
  iconStyle,
  iconSize = 18,
  type = 'text',
  options,
}) => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const togglePassword = () => setSecureTextEntry(!secureTextEntry);

  return (
    <View style={[styles.inputWrapper, wrapperStyle]}>
      {icon !== undefined && (
        <View style={[styles.icon, iconStyle]}>
          <MaterialIcon color={colors.BurningOrange} name={icon} size={iconSize} />
        </View>
      )}
      {type === 'select' ? (
        <Picker
          style={[styles.input, inputStyle]}
          selectedValue={value}
          onValueChange={onChangeText}>
          {options.map((item, i) => (
            <Picker.Item key={i} label={item || 'Choose'} value={item} />
          ))}
        </Picker>
      ) : (
        <TextInput
          editable={editAble}
          value={value}
          autoCapitalize="none"
          onChangeText={onChangeText}
          style={[styles.input, inputStyle]}
          placeholder={placeholder}
          keyboardType={type === 'password' ? 'numeric' : 'default'}
          secureTextEntry={type === 'password' ? secureTextEntry : false}
        />
      )}
      {type === 'password' && (
        <TouchableOpacity
          onPress={togglePassword}
          underlayColor={colors.BurningOrange}
          containerStyle={[styles.icon, iconStyle]}>
          <MaterialIcon
            color={colors.BurningOrange}
            name={secureTextEntry ? 'eye' : 'eye-off'}
            size={iconSize}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default InputText;

const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  inputWrapper: {
    borderWidth: 1,
    borderColor: colors.Silver,
    elevation: 0,
    paddingVertical: 0,
    width: width - 50,
    flexDirection: 'row',
    borderRadius: 4,
  },
  icon: {
    justifyContent: 'center',
    padding: 5,
  },
  input: {
    paddingVertical: 5,
    flex: 1,
    height: 56
  },
});
