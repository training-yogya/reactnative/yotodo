import * as React from 'react';
import {Modal, Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../utils/colors';

const ModalInfo = ({
  visible,
  onDismiss,
  onShow,
  message,
  title,
  icon,
  setVisible,
}) => {
  const onRequestClose = () => {
    setVisible(false);
  };

  const iconName = icon === 'success' ? 'check-circle' : 'warning';
  const iconColor = icon === 'success' ? colors.SeaGreen : colors.Red;

  return (
    <Modal
      animated={true}
      animationType="fade"
      visible={visible}
      onDismiss={onDismiss}
      onShow={onShow}
      onRequestClose={onRequestClose}
      transparent={true}>
      <TouchableOpacity
        onPress={onRequestClose}
        style={{
          backgroundColor: 'rgba(0,0,0,0.3)',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 20,
        }}>
        <View
          style={{
            backgroundColor: colors.White,
            minHeight: 100,
            padding: 10,
            minWidth: 300,
            borderRadius: 4,
            alignItems: 'center',
            elevation: 5,
          }}>
          <Icon name={iconName} color={iconColor} size={72} />
          <Text>{title}</Text>
          <Text>{message}</Text>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

export default ModalInfo;
