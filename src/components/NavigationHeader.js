import * as React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import colors from '../utils/colors';
import {TouchableHighlight} from 'react-native-gesture-handler';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {screenNames} from '../utils/screens';

const NavigationHeader = ({scene, navigation}) => {
  const {options} = scene.descriptor;
  const title =
    options.headerTitle !== undefined
      ? options.headerTitle
      : options.title !== undefined
      ? options.title
      : scene.route.name;
  const HeaderRight = options.headerRight;
  const headerLeft = options.headerLeft;
  const HeaderLeft = options.HeaderLeft;
  return (
    <View style={[styles.wrapper, options.headerStyle]}>
      {headerLeft || HeaderLeft ? (
        HeaderLeft
      ) : headerLeft !== 'none' ? (
        <TouchableHighlight
          style={styles.backButtonWrapper}
          onPress={() => navigation.goBack()}
          underlayColor="rgba(0,0,0,0.2)">
          <MaterialIcon name="chevron-left" size={36} color={colors.White} />
        </TouchableHighlight>
      ) : (
        <View style={styles.backButtonWrapper} />
      )}
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{title.toUpperCase()}</Text>
      </View>
      {HeaderRight !== undefined ? HeaderRight : <TouchableHighlight />}
    </View>
  );
};

export default NavigationHeader;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.YellowOrange,
    padding: 10,
    elevation: 2,
    height: 56,
    flexDirection: 'row',
    alignItems: 'center',
  },
  backButtonWrapper: {
    height: 36,
    width: 36,
    borderRadius: 36 / 2,
    alignItems: 'center',
  },
  titleWrapper: {
    flex: 1,
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: colors.White,
  },
});
