import * as React from 'react';

import {View, Image} from 'react-native';

import logo from '../assets/icons/logo.png';
import yogyaGroup from '../assets/icons/yogya-group.png';
import yogyaDulu from '../assets/icons/yogya-dulu.jpg';
import yogyaDuluRounded from '../assets/icons/yogya-dulu-rounded.jpg';
import covidBanner from '../assets/icons/covid_banner.jpeg';
import store from '../assets/icons/store.png';
import {iconsNames} from '../utils/icons';

const Icon = ({name = undefined, width = 10, height = 10, style}) => {
  return (
    <View>
      {name === iconsNames.logo ? (
        <Image source={logo} width={width} height={height} style={style} />
      ) : name === iconsNames.yogyabanner ? (
        <Image
          source={yogyaGroup}
          width={width}
          height={height}
          style={style}
        />
      ) : name === iconsNames.yogyaDulu ? (
        <Image source={yogyaDulu} width={width} height={height} style={style} />
      ) : name === iconsNames.yogyaDuluRounded ? (
        <Image
          source={yogyaDuluRounded}
          width={width}
          height={height}
          style={style}
        />
      ) : name === iconsNames.store ? (
        <Image source={store} width={width} height={height} style={style} />
      ) : name === iconsNames.covid ? (
        <Image
          source={covidBanner}
          width={width}
          height={height}
          style={style}
        />
      ) : (
        <View />
      )}
    </View>
  );
};

export default Icon;
