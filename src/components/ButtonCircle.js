import * as React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Foundation from 'react-native-vector-icons/Foundation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialComunity from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from './../utils/colors';
import globalStyles from './../styles/global';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ButtonCircle = ({
  icon = 'md-shirt',
  title,
  size,
  badge = null,
  iconType,
  onPress,
  color,
  disabled,
  width = null,
  height = null,
  wrapperStyle = {},
}) => {
  return (
    <View
      style={[
        styles.menuItemWrapper,
        {
          width: width ? width : styles.menuItemWrapper.width,
          height: height ? height : styles.menuItemWrapper.height,
        },
        wrapperStyle,
      ]}>
      <TouchableOpacity
        onPress={onPress}
        disabled={disabled}
        style={[styles.menuItem]}>
        {iconType === 'Foundation' ? (
          <Foundation
            name={icon}
            color={disabled ? colors.SilverChalice : colors.BlazeOrange}
            size={size || 32}
          />
        ) : iconType === 'FontAwesome' ? (
          <FontAwesome
            name={icon}
            color={disabled ? colors.SilverChalice : colors.BlazeOrange}
            size={size || 32}
          />
        ) : iconType === 'MaterialIcon' ? (
          <MaterialIcon
            name={icon}
            color={disabled ? colors.SilverChalice : colors.BlazeOrange}
            size={size || 32}
          />
        ) : iconType === 'MaterialCommunity' ? (
          <MaterialComunity
            name={icon}
            color={disabled ? colors.SilverChalice : colors.BlazeOrange}
            size={size || 32}
          />
        ) : (
          <IonIcon
            name={icon}
            color={disabled ? colors.SilverChalice : colors.BlazeOrange}
            size={size || 32}
          />
        )}
        <Text style={[styles.menuText, globalStyles.textCenter]}>{title}</Text>
        {badge !== null && (
          <Text
            style={[
              styles.menuBadge,
              disabled ? {backgroundColor: colors.Logan} : {},
            ]}>
            {disabled ? 'disabled' : badge}
          </Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default ButtonCircle;

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
  menuItemWrapper: {
    width: width / 4 - 5,
    padding: 5,
    marginBottom: 10,
  },
  menuItem: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  menuText: {
    fontSize: 10,
  },
  menuBadge: {
    position: 'absolute',
    top: 0,
    right: 10,
    paddingHorizontal: 4,
    textAlign: 'center',
    borderRadius: 10,
    height: 20,
    width: 20,
    fontSize: 12,
    lineHeight: 20,
    backgroundColor: colors.Limeade,
    color: colors.White,
  },
});
