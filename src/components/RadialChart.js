import React, {useRef, useEffect} from 'react';
import WebView from 'react-native-webview';

import radialChart from './../html/radial-chart';
import {Dimensions} from 'react-native';

const width = Dimensions.get('screen').width;
const RadialChart = ({size, percent, color}) => {
  const webView = useRef();

  useEffect(() => {
    if (webView.current !== null) {
      webView.current.postMessage(JSON.stringify({color, size, percent}));
    }
  });

  return (
    <WebView
      ref={webView}
      originWhitelist={['*']}
      allowFileAccess={true}
      source={{html: radialChart()}}
      style={{height: size, width: size, backgroundColor: 'transparent'}}
    />
  );
};

export default RadialChart;
