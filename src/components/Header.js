import * as React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import colors from './../utils/colors';

const Header = ({
  title,
  backgroundColor = colors.BlueBell,
  style,
  wrapperStyle,
  titleStyle,
}) => {
  return (
    <View style={[styles.wrapper, wrapperStyle]}>
      <View style={[styles.container, {backgroundColor}, style]}>
        <Text style={[styles.title, titleStyle]}>{title}</Text>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  wrapper: {
    padding: 10,
  },
  container: {
    backgroundColor: colors.BlueBell,
    borderRadius: 4,
    height: 46,
  },
  title: {
    textAlign: 'center',
    lineHeight: 44,
    color: colors.White,
    fontWeight: '700',
  },
});
