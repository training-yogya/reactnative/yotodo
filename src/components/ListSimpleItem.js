import * as React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../utils/colors';

const ListSimpleItem = ({title, onPress}) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.wrapper}>
      <Text style={styles.title}>{title}</Text>
      <MaterialIcon name="chevron-right" size={28} />
    </View>
  </TouchableOpacity>
);

export default ListSimpleItem;

const styles = StyleSheet.create({
  wrapper: {
    padding: 5,
    borderTopWidth: 0.7,
    borderTopColor: colors.Silver,
    height: 56,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
  },
});
