import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function InsightCard({
  name,
  index,
  handleOnPress,
  iconBig,
  icon,
  today,
  year,
}) {
  return (
    <TouchableOpacity
      onPress={handleOnPress}
      style={{
        backgroundColor: `${
          index % 2 === 0 ? 'rgba(255, 104, 3, 0.3)' : 'rgba(48, 172, 255, 0.3)'
        }`,
        padding: 5,
        flex: 1,
        minHeight: 140,
        borderRadius: 10,
        marginHorizontal: 10,
        marginVertical: 10,
      }}>
      <View
        style={{
          alignItems: index % 2 === 0 ? 'flex-end' : 'flex-start',
          position: 'relative',
        }}>
        {iconBig !== undefined ? (
          <MaterialIcon
            name="store"
            size={100}
            color={'rgba(48, 172, 255, 0.3)'}
          />
        ) : (
          <View>
            <View style={{height: 120}}>
              <Text style={{fontWeight: 'bold'}}>
                Today: {today || '0'} reasons
              </Text>
              <Text style={{fontWeight: 'bold'}}>
                YTD: {year || '0'} reasons
              </Text>
            </View>
          </View>
        )}
      </View>
      <View style={{alignItems: index % 2 === 0 ? 'flex-start' : 'flex-end'}}>
        <Text style={{color: 'rgba(0,0,0,0.6)', fontSize: 13}}>{name}</Text>
      </View>
      <View
        style={{
          alignItems: index % 2 === 0 ? 'flex-start' : 'flex-end',
          position: 'absolute',
          bottom: 47,
          paddingLeft: 10,
          marginLeft: index % 2 === 0 ? 0 : 100,
        }}>
        <MaterialIcon
          size={36}
          color={
            index % 2 === 0
              ? 'rgba(255, 104, 3, 0.4)'
              : 'rgba(48, 172, 255, 0.4)'
          }
          name={icon || 'layers'}
        />
      </View>
    </TouchableOpacity>
  );
}
