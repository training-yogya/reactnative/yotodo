import * as React from 'react';
import {View, Modal, Text} from 'react-native';
import {Bar as ProgressCircle} from 'react-native-progress';
import colors from '../utils/colors';

export const ModalDownloadProgress = ({progress, visible}) => {
  return (
    <Modal visible={visible} transparent animationType="fade">
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.4)',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{backgroundColor: colors.White, padding: 25}}>
          <Text>Download in Progress</Text>
          <ProgressCircle progress={progress} width={200} />
        </View>
      </View>
    </Modal>
  );
};
