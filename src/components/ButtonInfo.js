import * as React from 'react';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../utils/colors';

const ButtonInfo = ({action}) => {
  return (
    <MaterialIcon
      onPress={action}
      name="info-outline"
      color={colors.White}
      size={34}
    />
  );
};

export default ButtonInfo;
