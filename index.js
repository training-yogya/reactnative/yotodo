/**
 * @format
 */
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import PushNotification from 'react-native-push-notification';
import AsyncStorage from '@react-native-community/async-storage';
import {store} from './src/redux/store';
import {setUnreadRooms} from './src/redux/actions/chat-action';

// Must be outside of any component LifeCycle (such as `componentDidMount`).
PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function(token) {
    AsyncStorage.setItem('fcm_token', token.token);
  },

  // (required) Called when a remote is received or opened, or local notification is opened
  onNotification: async function(notification) {
    if (!notification.userInteraction) {
      try {
        let unReadRooms = await AsyncStorage.getItem('unReadRooms');

        if (!unReadRooms) {
          unReadRooms = JSON.stringify([]);
        }

        unReadRooms = JSON.parse(unReadRooms);

        if (!unReadRooms?.includes(notification.title)) {
          unReadRooms?.push(notification.title);
        }

        await AsyncStorage.setItem('unReadRooms', JSON.stringify(unReadRooms));

        store.dispatch(setUnreadRooms(unReadRooms || []));
      } catch (error) {
        console.log(error);
      }

      PushNotification.localNotification({
        autoCancel: true,
        title: notification.title,
        message: notification.message,
        visibility: 'public',
        data: {},
      });
    } else {
      PushNotification.removeAllDeliveredNotifications();
    }

    // process the notification

    // (required) Called when a remote is received or opened, or local notification is opened
    // notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function(notification) {
    console.log('ACTION:', notification.action);
    console.log('NOTIFICATION:', notification);

    // process the action
  },

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: function(err) {
    console.error(err.message, err);
  },

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   * - if you are not using remote notification or do not have Firebase installed, use this:
   *     requestPermissions: Platform.OS === 'ios'
   */
  requestPermissions: true,
});

AppRegistry.registerComponent(appName, () => App);
