import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {YellowBox} from 'react-native';
import {store, persistedStore} from './src/redux/store';
import AppNavigation from './src/navigations';
import checkRequests from './src/HOC/CheckRequest';
import InitScreen from './src/screens/InitScreen';
import {firebase} from './src/services';
import CodePushProvider from './src/components/Providers/CodePushProvider';

YellowBox.ignoreWarnings([
  'Non-serializable values were found in the navigation state',
]);

const AppRoot = checkRequests(AppNavigation);

class App extends React.Component {
  constructor(props) {
    super(props);
    firebase.initialize();
  }
  render() {
    return (
      <CodePushProvider>
        <Provider store={store}>
          <PersistGate loading={<InitScreen />} persistor={persistedStore}>
            <AppRoot />
          </PersistGate>
        </Provider>
      </CodePushProvider>
    );
  }
}

export default App;
